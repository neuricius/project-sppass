/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponente;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import static controller.MainCTRL.findIndexForDate;
import static controller.MainCTRL.findIndexForRoom;
import static view.MainView.tableGridGP;

/**
 *
 * @author Nebojsa Matic
 */
public class PoligonZaKrajRez extends Polygon {

    public static Polygon krajRezPoli;

    public PoligonZaKrajRez(String soba, LocalDate kraj, int status) {

        krajRezPoli = new Polygon();
        krajRezPoli.getPoints().addAll(new Double[]{
            0.0, 2.0,
            0.0, 28.0,
            28.0, 2.0});
        GridPane.setMargin(krajRezPoli, new Insets(1, 0, 1, 0));
        GridPane.setRowIndex(krajRezPoli, findIndexForRoom(soba));
        GridPane.setColumnIndex(krajRezPoli, findIndexForDate(kraj));

        //petlja za setfill
        switch (status) {
            case 1:
                krajRezPoli.setFill(Color.BEIGE);
                break;
            case 2:
                krajRezPoli.setFill(Color.LIGHTGREEN);
                break;
            case 3:
                krajRezPoli.setFill(Color.RED);
                break;
            case 4:
                krajRezPoli.setFill(Color.GRAY);
                break;
        }

        tableGridGP.add(krajRezPoli, findIndexForDate(kraj), findIndexForRoom(soba), 1, 1);
    }

}
