/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponente;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import static controller.MainCTRL.findIndexForDate;
import static controller.MainCTRL.findIndexForRoom;
import static view.MainView.tableGridGP;

/**
 *
 * @author Nebojsa Matic
 */
public class LabelaZaRezervaciju extends Label {

    public static Label rezervaLabel;

    public LabelaZaRezervaciju(String ime, String prezime, String soba, LocalDate start, LocalDate kraj, int status) {

        rezervaLabel = new Label();
        rezervaLabel.setTooltip(new Tooltip(ime + " " + prezime));
        rezervaLabel.setText(ime + " " + prezime);
        rezervaLabel.setWrapText(true);
        rezervaLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        GridPane.setMargin(rezervaLabel, new Insets(1, 0, 1, 0));
        GridPane.setRowIndex(rezervaLabel, findIndexForRoom(soba));
        GridPane.setColumnIndex(rezervaLabel, findIndexForDate(start));
        switch (status) {
            case 1:
                rezervaLabel.setBackground(new Background(new BackgroundFill(Color.BEIGE, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                break;
            case 2:
                rezervaLabel.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                break;
            case 3:
                rezervaLabel.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                break;
            case 4:
                rezervaLabel.setBackground(new Background(new BackgroundFill(Color.GRAY, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                break;
        }
        tableGridGP.add(rezervaLabel, findIndexForDate(start) + 1, findIndexForRoom(soba), findIndexForDate(kraj) - findIndexForDate(start) - 1, 1);
//        rezervaLabel.setContextMenu(new RezervacijeContext(rezervaLabel));
    }

}
