/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponente;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import static controller.MainCTRL.findIndexForDate;
import static controller.MainCTRL.findIndexForRoom;
import static view.MainView.tableGridGP;

/**
 *
 * @author Nebojsa Matic
 */
public class PoligonZaPocetakRez extends Polygon {

    public static Polygon pocetakRezPoli;

    public PoligonZaPocetakRez(String soba, LocalDate start, int status) {

        pocetakRezPoli = new Polygon();
        pocetakRezPoli.getPoints().addAll(new Double[]{
            0.0, 28.0,
            30.0, 2.0,
            30.0, 28.0});
        GridPane.setMargin(pocetakRezPoli, new Insets(1, 0, 1, 0));
        GridPane.setRowIndex(pocetakRezPoli, findIndexForRoom(soba));
        GridPane.setColumnIndex(pocetakRezPoli, findIndexForDate(start));

        //petlja za setfill
        switch (status) {
            case 1:
                pocetakRezPoli.setFill(Color.BEIGE);
                break;
            case 2:
                pocetakRezPoli.setFill(Color.LIGHTGREEN);
                break;
            case 3:
                pocetakRezPoli.setFill(Color.RED);
                break;
            case 4:
                pocetakRezPoli.setFill(Color.GRAY);
                break;
        }
        tableGridGP.add(pocetakRezPoli, findIndexForDate(start), findIndexForRoom(soba), 1, 1);
    }

}
