/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testis;

import java.time.format.DateTimeFormatter;
import javafx.stage.Modality;
import model.Rezervacija;
import testis.GostiPoSobamaProzor;
import static testis.GostiPoSobamaProzor.*;
import util.HibernateUtil;
import static util.Konvertori.konvertujDatumZaApp;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import controller.TakeOverCTRL;

/**
 *
 * @author Nebojsa Matic
 */
public class GostiPoSobamaKontroler implements WindowDecorator, TakeOverCTRL {

    private static GostiPoSobamaProzor inspektorKluzo;

    public GostiPoSobamaKontroler(Integer idRez) {
        pokretInspektore(idRez);
    }

    public void pokretInspektore(Integer idRez) {
        inspektorKluzo = new GostiPoSobamaProzor(idRez);
        WindowDecorator.noConfirmationDecorator(inspektorKluzo);
        addCTRL(idRez);
        inspektorKluzo.setScene(scenaZaGostePoSobama);
        inspektorKluzo.initModality(Modality.APPLICATION_MODAL);
        inspektorKluzo.setTitle("Pregled Gostiju");
        inspektorKluzo.show();

        stampajListuGostijuB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(tabelaGostijuPoSobama))).start();
        });
    }

    @Override
    public void addCTRL(Integer idRez) {

        Rezervacija rez = HibernateUtil.vratiRezervacijuPoId(idRez);

        brRezLZOR.setText(rez.getIdRezervacija().toString());

        nazivSmJedLZOR.setText(rez.getIdSmJed().getNaziv());

        datumPocetkaLZOR.setText(konvertujDatumZaApp(rez.getPrijava()).format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        datumKrajaLZOR.setText(konvertujDatumZaApp(rez.getOdjava()).format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        brojOdraslihLZOR.setText(rez.getBrojOdraslih().toString());

        brDeceLZOR.setText(rez.getBrojDece().toString());

    }

    @Override
    public void addCTRL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
