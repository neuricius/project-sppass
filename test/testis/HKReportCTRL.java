/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testis;

import view.ArrivalsPerDateView;
import view.HouseKeepingReportView;
import view.DeparturesOnDayView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import static view.HouseKeepingReportView.hkReportScene;
import static view.DeparturesOnDayView.deleteB;
import static view.DeparturesOnDayView.departuresTable;

/**
 *
 * @author Nebojsa Matic
 */
public class HKReportCTRL {

    private static HouseKeepingReportView domacica;

    public HKReportCTRL() {
        new ArrivalsPerDateView();
        new DeparturesOnDayView();
        launchHKR();
    }

    public void launchHKR() {
        domacica = new HouseKeepingReportView();
        domacica.setScene(hkReportScene);
        domacica.setTitle("Izvestaj za domacinstvo");
        domacica.show();

        deleteB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(departuresTable))).start();
        });

    }

}
