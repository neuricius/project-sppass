/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Klijent;
import model.Rezervacija;
import util.HibernateUtil;

/**
 *
 * @author Nebojsa Matic
 */
public class ReservationOverView extends Stage {

    List<Rezervacija> reservationList;

    public static Button refreshB, addResB, deleteResB;

    BorderPane mainBP;

    public static TableView<Rezervacija> reservationsTable;
    public static TableColumn<Rezervacija, Integer> reservationIDTC;
    public static TableColumn<Rezervacija, Date> arrivalDateTC;

    public static Scene resOverviewScene;

    public ReservationOverView() {

        mainBP = new BorderPane();

        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Osvezi");

        addResB = new Button();
        addResB.setMinSize(130, 25);
        addResB.setMaxSize(130, 25);
        addResB.setText("Dodaj");

        deleteResB = new Button();
        deleteResB.setMinSize(130, 25);
        deleteResB.setMaxSize(130, 25);
        deleteResB.setText("Obrisi");

        HBox buttonHBox = new HBox(refreshB, addResB, deleteResB);
        mainBP.setTop(buttonHBox);

        // CENTAR
        reservationsTable = new TableView<>();
        reservationsTable.setEditable(false);

        reservationList = HibernateUtil.napraviListuRezervacija();
        ObservableList<Rezervacija> data = FXCollections.observableArrayList(reservationList);

        //kolone
        //osn podaci o rez
        //glavna
        TableColumn<Rezervacija, String> resInfo = new TableColumn<>("Rezervacija");

        //redni broj (ID) rezervacije
        reservationIDTC = new TableColumn<>("#");
        reservationIDTC.setStyle("-fx-alignment: CENTER;");
        reservationIDTC.setPrefWidth(25);
        reservationIDTC.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));

        //status rez
        TableColumn<Rezervacija, String> resStatusTC = new TableColumn<>("Status");
        resStatusTC.setStyle("-fx-alignment: CENTER;");
        resStatusTC.setPrefWidth(75);
        resStatusTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdStatRez().getStatusRezervacije()));

        //
        // naziv sobe
        TableColumn<Rezervacija, String> roomNoTC = new TableColumn<>("Sm. Jed");
        roomNoTC.setStyle("-fx-alignment: CENTER;");
        roomNoTC.setPrefWidth(75);
        roomNoTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdSmJed().getNaziv()));

        //
        resInfo.getColumns().addAll(reservationIDTC, resStatusTC, roomNoTC);

        //broj gostiju
        //glavna kol
        TableColumn<Rezervacija, Integer> NoOfGuestsTC = new TableColumn<>("Broj Gostiju");
        NoOfGuestsTC.setPrefWidth(80);

        //subkolone
        TableColumn<Rezervacija, Integer> NoOfAdultsTC = new TableColumn<>("Odr");
        NoOfAdultsTC.setStyle("-fx-alignment: CENTER;");
        NoOfAdultsTC.setPrefWidth(40);
        NoOfAdultsTC.setCellValueFactory(new PropertyValueFactory<>("brojOdraslih"));

        TableColumn<Rezervacija, Integer> NoOfChildrenTC = new TableColumn<>("Deca");
        NoOfChildrenTC.setStyle("-fx-alignment: CENTER;");
        NoOfChildrenTC.setPrefWidth(40);
        NoOfChildrenTC.setCellValueFactory(new PropertyValueFactory<>("brojDece"));

        NoOfGuestsTC.getColumns().addAll(NoOfAdultsTC, NoOfChildrenTC);

        //period boravka
        //glavna kol
        TableColumn<Rezervacija, Date> stayDurationTC = new TableColumn<>("Period Boravka");

        //subkolone
        arrivalDateTC = new TableColumn<>("Dolazak");
        arrivalDateTC.setStyle("-fx-alignment: CENTER;");
        arrivalDateTC.setPrefWidth(80);
        arrivalDateTC.setCellValueFactory(new PropertyValueFactory<>("prijava"));

        TableColumn<Rezervacija, Date> departureDateTC = new TableColumn<>("Odlazak");
        departureDateTC.setStyle("-fx-alignment: CENTER;");
        departureDateTC.setPrefWidth(80);
        departureDateTC.setCellValueFactory(new PropertyValueFactory<>("odjava"));

        stayDurationTC.getColumns().addAll(arrivalDateTC, departureDateTC);

        //klijent
        //glavna
        TableColumn<Rezervacija, Klijent> clientInfoTC = new TableColumn<>("Klijent");
        clientInfoTC.setCellValueFactory(new PropertyValueFactory<>("idKlijent"));

        //subkolone
        TableColumn<Rezervacija, String> clientLastNameTC = new TableColumn<>("Prezime");
        clientLastNameTC.setPrefWidth(80);
        clientLastNameTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getPrezimeKlijenta()));

        TableColumn<Rezervacija, String> clientFirstNameTC = new TableColumn<>("Ime");
        clientFirstNameTC.setPrefWidth(80);
        clientFirstNameTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getImeKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCountryTC = new TableColumn<>("Drzava");
        clientResidenceCountryTC.setMinWidth(100);
        clientResidenceCountryTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getDrzavaKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCityTC = new TableColumn<>("Mesto");
        clientResidenceCityTC.setMinWidth(100);
        clientResidenceCityTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getMestoKlijenta()));

        clientInfoTC.getColumns().addAll(clientLastNameTC, clientFirstNameTC, clientResidenceCountryTC, clientResidenceCityTC);

        reservationsTable.setItems(data);
        reservationsTable.getColumns().addAll(resInfo, NoOfGuestsTC, stayDurationTC, clientInfoTC);

        //kod za izbor po kojoj koloni da se sortira tabela
        mainBP.setCenter(reservationsTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        resOverviewScene = new Scene(mainBP);
    }

}
