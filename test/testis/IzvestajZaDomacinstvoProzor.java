/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package prozori;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import static view.ArrivalsPerDateView.arrivalTable;
import static view.DeparturesOnDayView.departuresTable;

/**
 *
 * @author Nebojsa Matic
 */
public class IzvestajZaDomacinstvoProzor extends Stage {

    VBox panelaZaIzvestajVBox;
    ScrollPane panelaZaIzvestajSP;
    
    Label listaDolazaka, listaOdlazaka;

    public static Scene scenaZaIzvestaj;
    
    public IzvestajZaDomacinstvoProzor() {
        
        listaDolazaka = new Label();
        listaDolazaka.setMinSize(150, 50);
        listaDolazaka.setMaxSize(150, 50);
        listaDolazaka.setText("Dolasci");
        listaDolazaka.setFont(Font.font("System", FontWeight.BOLD, 18));
        listaDolazaka.setAlignment(Pos.CENTER);
        listaDolazaka.setTextAlignment(TextAlignment.RIGHT);
        listaDolazaka.setPadding(new Insets(4));
        
        listaOdlazaka = new Label();
        listaOdlazaka.setMinSize(150, 50);
        listaOdlazaka.setMaxSize(150, 50);
        listaOdlazaka.setText("Odlasci");
        listaOdlazaka.setFont(Font.font("System", FontWeight.BOLD, 18));
        listaOdlazaka.setAlignment(Pos.CENTER);
        listaOdlazaka.setTextAlignment(TextAlignment.RIGHT);
        listaOdlazaka.setPadding(new Insets(4));
    
    panelaZaIzvestajVBox = new VBox(10, listaDolazaka, arrivalTable, listaOdlazaka, departuresTable);
    
    panelaZaIzvestajSP = new ScrollPane(panelaZaIzvestajVBox);
    panelaZaIzvestajSP.setMaxHeight(700);
    
    scenaZaIzvestaj = new Scene(panelaZaIzvestajSP);
    
    
    }
    
    
    
}
