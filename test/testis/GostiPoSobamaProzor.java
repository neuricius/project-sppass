/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testis;

import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;
import model.Gostiposobama;
import model.Rezervacija;
import util.HibernateUtil;

/**
 *
 * @author Nebojsa Matic
 */
public class GostiPoSobamaProzor extends Stage {

    List<Gostiposobama> listaGostijuPoSobama;
    ObservableList<Gostiposobama> data;
    public static TableView<Gostiposobama> tabelaGostijuPoSobama;
    public static TableColumn<Gostiposobama, Integer> idGps, idRezervacije, idGosta;
    public static TableColumn<Gostiposobama, String> imeGosta, prezimeGosta, mestoGosta;
    public static TableColumn<Gostiposobama, Date> datumRodjenjaGosta;

    Label brRezL, nazivSmJedL, datumPocetkaL, datumKrajaL, brojOdraslihL, brDeceL;

    public static Label brRezLZOR, nazivSmJedLZOR, datumPocetkaLZOR,
            datumKrajaLZOR, brojOdraslihLZOR, brDeceLZOR;

    public static Button osveziGostePoSobamaB, dodajGostePoSobamaB, obrisiGostePoSobamaB,
            stampajListuGostijuB;

    BorderPane panelaZaGostePoSobama;

    HBox panelaZaLabele;

    public static Scene scenaZaGostePoSobama;

    public GostiPoSobamaProzor(Integer idRez) {

        
        panelaZaGostePoSobama = new BorderPane();

        //panela za info o rez - top
        //labele
        brRezL = new Label();
        brRezL.setMinHeight(40);
        brRezL.setText("Sifra rez.:");
        brRezL.setTextAlignment(TextAlignment.RIGHT);

        nazivSmJedL = new Label();
        nazivSmJedL.setMinHeight(40);
        nazivSmJedL.setText("Naziv s.j.:");
        nazivSmJedL.setTextAlignment(TextAlignment.RIGHT);

        datumPocetkaL = new Label();
        datumPocetkaL.setMinHeight(40);
        datumPocetkaL.setText("Pocetak:");
        datumPocetkaL.setTextAlignment(TextAlignment.RIGHT);

        datumKrajaL = new Label();
        datumKrajaL.setMinHeight(40);
        datumKrajaL.setText("Kraj:");
        datumKrajaL.setTextAlignment(TextAlignment.RIGHT);

        brojOdraslihL = new Label();
        brojOdraslihL.setMinHeight(40);
        brojOdraslihL.setText("Odrasli:");
        brojOdraslihL.setTextAlignment(TextAlignment.RIGHT);

        brDeceL = new Label();
        brDeceL.setMinHeight(40);
        brDeceL.setText("Deca:");
        brDeceL.setTextAlignment(TextAlignment.RIGHT);

        //vrednosti
        brRezLZOR = new ReadOnlyLabel();
        brRezLZOR.setMinHeight(40);
        brRezLZOR.setTextAlignment(TextAlignment.LEFT);

        nazivSmJedLZOR = new ReadOnlyLabel();
        nazivSmJedLZOR.setMinHeight(40);
        nazivSmJedLZOR.setTextAlignment(TextAlignment.LEFT);

        datumPocetkaLZOR = new ReadOnlyLabel();
        datumPocetkaLZOR.setMinHeight(40);
        datumPocetkaLZOR.setTextAlignment(TextAlignment.LEFT);

        datumKrajaLZOR = new ReadOnlyLabel();
        datumKrajaLZOR.setMinHeight(40);
        datumKrajaLZOR.setTextAlignment(TextAlignment.LEFT);

        brojOdraslihLZOR = new ReadOnlyLabel();
        brojOdraslihLZOR.setMinHeight(40);
        brojOdraslihLZOR.setTextAlignment(TextAlignment.LEFT);

        brDeceLZOR = new ReadOnlyLabel();
        brDeceLZOR.setMinHeight(40);
        brDeceLZOR.setTextAlignment(TextAlignment.LEFT);

        //kontejner
        panelaZaLabele = new HBox(5, brRezL, brRezLZOR, nazivSmJedL, nazivSmJedLZOR,
                datumPocetkaL, datumPocetkaLZOR, datumKrajaL, datumKrajaLZOR,
                brojOdraslihL, brojOdraslihLZOR, brDeceL, brDeceLZOR);

        panelaZaGostePoSobama.setTop(panelaZaLabele);

        //panela za dugmad - bottom
        osveziGostePoSobamaB = new Button();
        osveziGostePoSobamaB.setMinSize(60, 25);
        osveziGostePoSobamaB.setMaxSize(60, 25);
        osveziGostePoSobamaB.setText("Osvezi");

        dodajGostePoSobamaB = new Button();
        dodajGostePoSobamaB.setMinSize(60, 25);
        dodajGostePoSobamaB.setMaxSize(60, 25);
        dodajGostePoSobamaB.setText("Dodaj");

        obrisiGostePoSobamaB = new Button();
        obrisiGostePoSobamaB.setMinSize(60, 25);
        obrisiGostePoSobamaB.setMaxSize(60, 25);
        obrisiGostePoSobamaB.setText("Obrisi");

        stampajListuGostijuB = new Button();
        stampajListuGostijuB.setMinSize(60, 25);
        stampajListuGostijuB.setMaxSize(60, 25);
        stampajListuGostijuB.setText("Stampaj");

        HBox boxZaDugmad = new HBox(osveziGostePoSobamaB, dodajGostePoSobamaB, stampajListuGostijuB);
        panelaZaGostePoSobama.setBottom(boxZaDugmad);

        //CENTAR
        tabelaGostijuPoSobama = new TableView<>();
        tabelaGostijuPoSobama.setEditable(false);

        listaGostijuPoSobama = HibernateUtil.skupiListuGostijuZaRezervaciju(idRez);
        data = FXCollections.observableArrayList(listaGostijuPoSobama);

        idGosta = new TableColumn<>("#");
        idGosta.setStyle("-fx-alignment: CENTER;");
        idGosta.setPrefWidth(25);
        idGosta.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, Integer> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getIdGost()));

        imeGosta = new TableColumn<>("Ime");
        imeGosta.setStyle("-fx-alignment: CENTER;");
        imeGosta.setPrefWidth(100);
        imeGosta.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getImeGosta()));

        prezimeGosta = new TableColumn<>("Prezime");
        prezimeGosta.setStyle("-fx-alignment: CENTER;");
        prezimeGosta.setPrefWidth(100);
        prezimeGosta.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getPrezimeGosta()));

        mestoGosta = new TableColumn<>("Mesto");
        mestoGosta.setStyle("-fx-alignment: CENTER;");
        mestoGosta.setPrefWidth(100);
        mestoGosta.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getMestoGosta()));

        datumRodjenjaGosta = new TableColumn<>("Datum rodjenja");
        datumRodjenjaGosta.setStyle("-fx-alignment: CENTER;");
        datumRodjenjaGosta.setPrefWidth(100);
        datumRodjenjaGosta.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, Date> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getDatumRodjenjaGosta()));

        tabelaGostijuPoSobama.setItems(data);
        tabelaGostijuPoSobama.getColumns().addAll(idGosta, imeGosta, prezimeGosta, mestoGosta, datumRodjenjaGosta);

        panelaZaGostePoSobama.setCenter(tabelaGostijuPoSobama);

        //scena
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        scenaZaGostePoSobama = new Scene(panelaZaGostePoSobama);
    }

}
