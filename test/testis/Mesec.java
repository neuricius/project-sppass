/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "mesec")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mesec.findAll", query = "SELECT m FROM Mesec m")
    , @NamedQuery(name = "Mesec.findByIdMesec", query = "SELECT m FROM Mesec m WHERE m.idMesec = :idMesec")
    , @NamedQuery(name = "Mesec.findByNazivMeseca", query = "SELECT m FROM Mesec m WHERE m.nazivMeseca = :nazivMeseca")})
public class Mesec implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idMesec")
    private Integer idMesec;
    @Basic(optional = false)
    @Column(name = "NazivMeseca")
    private String nazivMeseca;

    public Mesec() {
    }

    public Mesec(Integer idMesec) {
        this.idMesec = idMesec;
    }

    public Mesec(Integer idMesec, String nazivMeseca) {
        this.idMesec = idMesec;
        this.nazivMeseca = nazivMeseca;
    }

    public Integer getIdMesec() {
        return idMesec;
    }

    public void setIdMesec(Integer idMesec) {
        this.idMesec = idMesec;
    }

    public String getNazivMeseca() {
        return nazivMeseca;
    }

    public void setNazivMeseca(String nazivMeseca) {
        this.nazivMeseca = nazivMeseca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMesec != null ? idMesec.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mesec)) {
            return false;
        }
        Mesec other = (Mesec) object;
        if ((this.idMesec == null && other.idMesec != null) || (this.idMesec != null && !this.idMesec.equals(other.idMesec))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Mesec[ idMesec=" + idMesec + " ]";
    }
    
}
