/*
 * API za slanje mailova, zavrsiti u hodu
 */
package util;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import view.AlertView;

/**
 *
 * @author Nebojsa Matic
 */
public class SlanjeMaila {

    private static final String KORISNICKO_IME = "itakademija.grupa2";  // bez "@gmail.com"
    private static final String SIFRA = "grupa2sifra"; 
    private static final String PRIMALAC = "neske.kv@gmail.com";

    public static void posaljiMail(String kompanija, String posiljalac, String naslov, String tekst) {
        String koSalje = KORISNICKO_IME;
        String siFra = SIFRA;
        String komeSalje = PRIMALAC;
        String naslovPoruke = posiljalac + "(" + kompanija + ")" + "Vam je poslao poruku: " + naslov;
        String tekstPoruke = tekst;

        slanjePutemGMaila(koSalje, siFra, komeSalje, naslovPoruke, tekstPoruke);
    }

    private static void slanjePutemGMaila(String from, String pass, String to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress toSend = new InternetAddress(to);
            message.setRecipient(Message.RecipientType.TO, toSend);
            message.setSubject(subject);
            message.setText(body);
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect(host, from, pass);
                transport.sendMessage(message, message.getAllRecipients());
            }
        } catch (AddressException ae) {
            AlertView.show("Greska u adresi", "Greska");
        } catch (MessagingException me) {
            AlertView.show("Greska", "Greska");
        }
    }
}
