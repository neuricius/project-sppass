/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Converter;
import components.ReservationGroup;
import model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import static util.Konvertori.konvertujDatumZaApp;
import static util.Konvertori.konvertujDatumZaBazu;
import static util.PretragaBuilder.PretragaBuilder;
import static view.MainView.systemMsgL;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Nebojsa Matic
 */
@Converter(autoApply = true)
public class HibernateUtil {

    private static Session session;
    public static Listanaloga targetResult;
    public static List<Nalog> otherResultsList, listaSvihNaloga;
    private static List<Rezervacija> reservationsList;
    public static List<Tipgosta> vrsteGostiju;
    public static List<Statusrezervacije> statRez;
    public static List<Popust> listaPopusta;
    public static List<Smjedinica> roomList;
    public static List<LocalDate> gridDateList;

    public static HashMap<Integer, String> naziviSoba, vrstaGosta, vrstaDokumenta,
            statusRezervacije, mapaPopusta, mapaKompanija, mapaObjekata, mapaKorisnika,
            vrsteSoba;

    public static HashMap<Integer, Integer> brLezajeva;

    public static LocalDate dateGridStart;
    public static LocalDate dateGridEnd;

    private static int velicinaNiza;
    private static int otherUserID;

    public static String[] spisakBrSoba, spisakTipSoba;

    /**
     * Kreiranje defaultnog session factorija
     *
     * @return sessionFactory
     */
    public static SessionFactory getSession() {
        Configuration config = new Configuration();
        config.configure("/xml/hibernate.cfg.xml");
        StandardServiceRegistryBuilder srb = new StandardServiceRegistryBuilder();
        srb.applySettings(config.getProperties());
        StandardServiceRegistry service = srb.build();
        SessionFactory sessionFactory = config.buildSessionFactory(service);
        return sessionFactory;
    }

    /**
     * Metod koji preuzima unos korisnickog imena, kreira upit bazi i vraca
     * sifru
     *
     * @param userName
     * @return
     */
    public static String returnPass(String userName) {
        session = getSession().openSession();
        Query passQuery = session.createQuery("SELECT ln FROM Listanaloga ln WHERE ln.idNaloga.korisnickoIme = :x");
        passQuery.setParameter("x", userName);
        List<Listanaloga> resultList = passQuery.list();
        if (resultList.isEmpty()) {
            targetResult = null;
            session.close();
            return null;
        } else {
            targetResult = resultList.get(0);
            otherResultsList = targetResult.getIdNaloga().getKompanija().getNalogList();
            return targetResult.getIdNaloga().getSifra();
        }

    }

    /**
     * Metod koji is liste korisnika formirane tokom inicijalnog logina ispituje
     * vrednosti korisnickog imena i sifre
     *
     * @param userName korisnicko ime preuzeto iz unosa
     * @param passWord sifra preuzeta iz unosa
     * @return vraca da li korisnik sa tim parametrima postoji ili ne
     */
    public static boolean returnOtherUser(String userName, String passWord) {
        boolean userExists = false;
        String otherUserPass = "";
        for (int i = 0; i < otherResultsList.size(); i++) {
            if (otherResultsList.get(i).getKorisnickoIme().equals(userName)) {
                otherUserPass = otherResultsList.get(i).getSifra();
                otherUserID = otherResultsList.get(i).getIdNalog() - 1;
                userExists = true;
                break;
            }
        }
        if (userExists == true) {
            return otherUserPass.equals(passWord);
        }
        return false;
    }

    /**
     * Ispitivanje nivoa pristupa inicijalno logovanog korisnika
     *
     * @return boolean vrednost koja se kasnije koristi za omogucavanje pristupa
     * odredjenim opcijama
     */
    public static boolean userLevelCheck() {

        return targetResult.getTipNaloga().getTipNaloga().equals("korisnik");
    }

    /**
     * Ispitivanje nivoa pristupa korisnika nakon ponovonog logovanja
     *
     * @return boolean vrednost koja se kasnije koristi za omogucavanje pristupa
     * odredjenim opcijama
     */
    public static boolean userLevelCheckOnRelog() {
        String s = otherResultsList.get(otherUserID).getListanalogaList().get(otherUserID).getTipNaloga().getTipNaloga();
        return s.equals("korisnik");
    }

    /**
     * Preuzimanje podataka o profilu inicijalno logovanog korisnika
     *
     * @return Profil korisnika
     */
    public static Profil userProfile() {
        return targetResult.getIdNaloga().getProfil();
    }

    /**
     * Preuzimanje podataka o imenu inicijalno logovanog korisnika
     *
     * @return String koji sadrzi ime korisnika
     */
    public static String userProfileName() {
        String ime = targetResult.getIdNaloga().getProfil().getImeProfila();
        return ime;
    }

    /**
     * Preuzimanje podataka o imenu korisnika nakon relogovanja
     *
     * @return String koji sadrzi ime korisnika
     */
    public static String userProfileNameOnRelog() {

        return otherResultsList.get(otherUserID).getProfil().getImeProfila();
    }

    /**
     * Preuzimanje podataka o imenu kompanije inicijalno logovanog korisnika
     *
     * @return String koji sadrzi ime kompanije
     */
    public static String companyName() {

        return targetResult.getIdNaloga().getKompanija().getNazivKompanije();
    }

    /**
     * Preuzimanje podataka o imenu objekta inicijalno logovanog korisnika
     *
     * @return String koji sadrzi ime objekta
     */
    public static String facilityName() {

        return targetResult.getIdNaloga().getKompanija().getObjektiList().get(0).getNazivObjekta();
    }

    /**
     * Preuzimanje podataka od vrednosti primarnog kljuca kompanije u kojoj radi
     * inicijalno logovani korisnik
     *
     * @return int koji sadrzi vrednost PK kompanije
     */
    public static int companyID() {

        return targetResult.getIdNaloga().getKompanija().getIdKompanije();
    }

    /**
     * Preuzimanje podataka broju soba objekta
     *
     * @return int vrednost broja soba
     */
    public static int numberOfRooms() {
        return targetResult.getIdNaloga().getKompanija().getObjektiList().get(0).getSmjedinicaList().size();
    }

    /**
     * Sortiranje liste soba preuzete prilikom logovanja
     */
    public static void gatherAndSortRoomInfo() {
        roomList = targetResult.getIdNaloga().getKompanija().getObjektiList().get(0).getSmjedinicaList();
        Collections.sort(roomList, (o1, o2) -> o1.getRedniBrojSJ() - o2.getRedniBrojSJ());
    }

    /**
     * Vracanje tipa sobe za unetu vrednost broja sobe
     *
     * @param x prestava int vrednost broja sobe
     * @return String tipa sobe
     */
    public static String roomTypesList(int x) {
        velicinaNiza = numberOfRooms();
        spisakTipSoba = new String[velicinaNiza];
        gatherAndSortRoomInfo();
        for (int i = 0; i < velicinaNiza; i++) {
            spisakTipSoba[i] = roomList.get(i).getVrstaSmJed().getVrstaSmJedinice().substring(0, 12);
        }
        return spisakTipSoba[x];
    }

    /**
     * Vracanje broja sobe za unetu vrednost
     *
     * @param x prestava int unete vrednosti
     * @return String tipa sobe
     */
    public static String spisakBrojevaSoba(int x) {
        velicinaNiza = numberOfRooms();
        spisakBrSoba = new String[velicinaNiza];
        gatherAndSortRoomInfo();
        for (int i = 0; i < velicinaNiza; i++) {
            spisakBrSoba[i] = String.valueOf(roomList.get(i).getRedniBrojSJ());

        }
        return spisakBrSoba[x];
    }

    /**
     * Preuzimanje informacija o trajanju licence i perioda trajanja sezone
     * objekta inicijalno logovanog korisnika. Na osnovu unesenih podataka se
     * kreira lista datuma koja se koristi prilikom kreiranja mape rezervacija
     * glavnog prozora
     *
     * @param selectedDate
     * @param monthSpan
     */
    public static void createDatesForGrid(LocalDate selectedDate, int monthSpan) {

        //kod za proveru licence. ubaciti naknadno
//        int pocetniMesecSezone = targetResult.getIdNaloga().getKompanija().getObjektiList().get(0).getPocetakSezone();
//        int zavrsniMesecSezone = targetResult.getIdNaloga().getKompanija().getObjektiList().get(0).getKrajSezone();
//        int pocetnaGodinaLicence = targetResult.getIdNaloga().getKompanija().getLicencaList().get(0).getPocetakLicence();
//        int zavrsnaGodinaLicence = 1 + targetResult.getIdNaloga().getKompanija().getLicencaList().get(0).getKrajLicence();
//        if (currentYear > zavrsnaGodinaLicence) {
//            AlertView.show("Licenca istekla", "Greska");
//            System.exit(0);
//        }
        dateGridStart = selectedDate.minusMonths(monthSpan / 2);
        dateGridEnd = selectedDate.plusMonths(monthSpan / 2);
        gridDateList = new ArrayList<>();
        LocalDate current = dateGridStart;
        while (current.isBefore(dateGridEnd)) {
            gridDateList.add(current);
            current = current.plusDays(1);
        }
    }

    /**
     * Ispitivanje vrednosti datuma na oredjenoj poziciji u listi
     *
     * @param x predstavlja poziciju u listi koja se ispituje
     * @return vrednost datuma na poziciji
     */
    public static LocalDate returnDate(int x) {
        return gridDateList.get(x);
    }

    /**
     * Isitivanje velicine liste datuma
     *
     * @param someDate
     * @return int vrednost velicine liste
     */
    public static int numberOfDates(LocalDate someDate) {
        createDatesForGrid(someDate, 6);
        int x = gridDateList.size();
        return x;
    }

    /**
     * Preuzimanje spiska rezervacija iz baze zarad iscrtavanja istih na mai
     * rezervacija
     *
     * @param objekat String naziv objekta za koji se preuzimaju rezervacije
     * @param periodStart
     * @param periodEnd
     */
    public static void iscrtajRezervacije(String objekat) {
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r WHERE r.idSmJed.objekat.nazivObjekta = :x "
                + "AND r.prijava <= :y "
                + "AND r.odjava >= :z"
        );
        upit.setParameter("x", objekat);
        upit.setParameter("y", konvertujDatumZaBazu(dateGridEnd));
        upit.setParameter("z", konvertujDatumZaBazu(dateGridStart));
        reservationsList = upit.list();
        if (reservationsList.isEmpty()) {
            systemMsgL.setText("Nema rezervacija za izabrani period");
        } else {
            for (Rezervacija rez : reservationsList) {
                new ReservationGroup(rez);
                systemMsgL.setText("Rezervacije uspesno preuzete iz baze");
            }
        }
        session.getTransaction().commit();
    }

    /**
     * Azuriranje rezervacija iscrtanih na mapi rezervacija.
     *
     * @param objekat
     */
    public static void azurirajRezervacijeUBazi(String objekat) {
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r WHERE r.idSmJed.objekat.nazivObjekta = :x");
        upit.setParameter("x", objekat);
        reservationsList = upit.list();
        if (reservationsList.isEmpty()) {
            systemMsgL.setText("Nema rezervacija za Vas objekat");
        } else {
            for (Rezervacija rez : reservationsList) {
                if (konvertujDatumZaApp(rez.getPrijava()).equals(LocalDate.now())) {
                    rez.setIdStatRez(new Statusrezervacije(3));
                    session.update(rez);
                }
                if (konvertujDatumZaApp(rez.getOdjava()).equals(LocalDate.now())) {
                    rez.setIdStatRez(new Statusrezervacije(4));
                    session.update(rez);
                    systemMsgL.setText("Rezervacije uspesno azurirane u " + LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm a")));
                }
            }
            session.getTransaction().commit();
        }
    }

    /**
     * Preuzimanje rezervacije sa prethodno unesenim primarnim kljucem
     *
     * @param idRez int vrednost PK rezervacije
     * @return zeljena rezervacija
     */
    public static Rezervacija vratiRezervacijuPoId(Integer idRez) {
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r WHERE r.idRezervacija = :x");
        upit.setParameter("x", idRez);
        Rezervacija rez = (Rezervacija) upit.list().get(0);
        return rez;
    }

    /**
     * Ispitivanje vrednosti naziva sobe na zeljenoj poziciji u listi
     *
     * @param x predstavlja zeljenu vrednost u listi
     * @return String vrednost naziva sobe na datoj poziciji
     */
    public static String vratiNazivSobe(int x) {
        gatherAndSortRoomInfo();
        return roomList.get(x).getNaziv();
    }

    public static List<Rezervacija> napraviListuRezervacija() {
        return reservationsList;
    }

    /**
     * Pretrazivanje rezervacija po zadatim parametrima
     *
     * @param objekat String naziv objekta
     * @param brRez int vrednost broja rezervacije
     * @param imeNosioca String vrednost imena nosioca rezervacije
     * @param prezimeNosioca String vrednost prezimena nosioca rezervacije
     * @param nazivSobe String vrednost naziva sobe
     * @param odDatum LocalDate vrednost datuma pocetka rezervacije
     * @param doDatum LocalDate vrednost datuma kraja rezervacije
     * @return Lista rezervacija koja zadovoljava zadate kriterijume ili null
     * vrednost
     */
    public static List<Rezervacija> pretragaRezervacija(String objekat, Integer brRez, String imeNosioca, String prezimeNosioca, String nazivSobe, LocalDate odDatum, LocalDate doDatum) {
        List<Rezervacija> rezultatPretrage;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery(PretragaBuilder(brRez, imeNosioca, prezimeNosioca, nazivSobe, odDatum, doDatum));
        upit.setParameter("a", objekat);
        if (brRez != null) {
            upit.setParameter("b", brRez);
        }
        if (imeNosioca != null) {
            upit.setParameter("c", imeNosioca);
        }
        if (prezimeNosioca != null) {
            upit.setParameter("d", prezimeNosioca);
        }
        if (nazivSobe != null) {
            upit.setParameter("e", nazivSobe);
        }
        if (odDatum != null && doDatum != null && odDatum.isBefore(doDatum)) {
            upit.setParameter("f", konvertujDatumZaBazu(odDatum));
            upit.setParameter("g", konvertujDatumZaBazu(doDatum));
        }
        rezultatPretrage = upit.list();
        session.getTransaction().commit();
        return rezultatPretrage;
    }

    /**
     * Prikupljanje liste cena smestajnih jedinica za zeljeni objekat
     *
     * @param objekat String vrednost naziva objekta
     * @return lista cena smestajnih jedinica za dati objekat
     */
    public static List<Cenasmjed> skupiListuCena(String objekat) {
        List<Cenasmjed> listaCenaSmJed;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT c FROM Cenasmjed c "
                + "WHERE c.idSmJedinice.objekat.nazivObjekta = :x");
        upit.setParameter("x", objekat);
        listaCenaSmJed = upit.list();
        session.getTransaction().commit();
        return listaCenaSmJed;
    }

    /**
     * Prikupljanje liste naplata za zeljeni objekat
     *
     * @param objekat String vrednost naziva objekta
     * @return lista naplata za dati objekat
     */
    public static List<Naplata> skupiListuNaplata(String objekat) {
        List<Naplata> listaNaplata;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT n FROM Naplata n "
                + "WHERE n.idRezervacija.idSmJed.objekat.nazivObjekta = :x");
        upit.setParameter("x", objekat);
        listaNaplata = upit.list();
        session.getTransaction().commit();
        return listaNaplata;
    }

    /**
     * Prikupljanje liste gostiju za zeljenu rezervaciju
     *
     * @param idRez int vrednost primarnog kljuca rezervacije
     * @return Lista gostiju za datu rezervaciju
     */
    public static List<Gostiposobama> skupiListuGostijuZaRezervaciju(Integer idRez) {
        List<Gostiposobama> listaGostiju;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT g FROM Gostiposobama g "
                + "WHERE g.idjRezervacije.idRezervacija = :x");
        upit.setParameter("x", idRez);
        listaGostiju = upit.list();
        session.getTransaction().commit();
        return listaGostiju;
    }

    /**
     * Prikupljanje liste rezervacija koje pocinju na zeljeni dan
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @param datum LocalDate vrednost datuma za koji se vrsi upit
     * @return Lista rezervacija koje zadovoljavaju zeljene kriterijume
     */
    public static List<Rezervacija> dolasciNaDan(String objekat, LocalDate datum) {
        List<Rezervacija> listaDolazaka;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery(
                "SELECT r FROM Rezervacija r "
                + "WHERE r.idSmJed.objekat.nazivObjekta = :x "
                + "AND r.prijava = :y");
        upit.setParameter("x", objekat);
        upit.setParameter("y", konvertujDatumZaBazu(datum));
        listaDolazaka = upit.list();
        session.getTransaction().commit();
        return listaDolazaka;
    }

    /**
     * Prikupljanje liste rezervacija koje se zavrsavaju na zeljeni dan
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @param datum LocalDate vrednost datuma za koji se vrsi upit
     * @return Lista rezervacija koje zadovoljavaju zeljene kriterijume
     */
    public static List<Rezervacija> odlasciNaDan(String objekat, LocalDate datum) {
        List<Rezervacija> listaOdlazaka;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery(
                "SELECT r FROM Rezervacija r "
                + "WHERE r.idSmJed.objekat.nazivObjekta = :x "
                + "AND r.odjava = :y");
        upit.setParameter("x", objekat);
        upit.setParameter("y", konvertujDatumZaBazu(datum));
        listaOdlazaka = upit.list();
        session.getTransaction().commit();
        return listaOdlazaka;
    }

    /**
     * Prikupljanje liste rezervacija koje NE POCINJU NITI SE ZAVRSAVAJU na
     * zeljeni dan
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @param datum LocalDate vrednost datuma za koji se vrsi upit
     * @return Lista rezervacija koje zadovoljavaju zeljene kriterijume
     */
    public static List<Rezervacija> ostaloNaDan(String objekat, LocalDate datum) {
        List<Rezervacija> listaOstalo;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r "
                + "WHERE r.idSmJed.objekat.nazivObjekta = :x "
                + "AND r.prijava != :y "
                + "AND r.odjava != :y "
                + "AND r.idStatRez.statusRezervacije = :z");
        upit.setParameter("x", objekat);
        upit.setParameter("y", konvertujDatumZaBazu(datum));
        upit.setParameter("z", "aktivna");
        listaOstalo = upit.list();
        session.getTransaction().commit();
        return listaOstalo;
    }

    /**
     * Prikupljanje liste aktivnih rezervacija za zeljeni objekat
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @return Lista rezervacija koje zadovoljavaju zeljene kriterijume
     */
    public static List<Rezervacija> listaAktivnihRezervacija(String objekat) {
        List<Rezervacija> aktivneRezervacijeList;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r "
                + "WHERE r.idSmJed.objekat.nazivObjekta = :x "
                + "AND r.idStatRez.statusRezervacije = :z");
        upit.setParameter("x", objekat);
        upit.setParameter("z", "aktivna");
        aktivneRezervacijeList = upit.list();
        session.getTransaction().commit();
        return aktivneRezervacijeList;
    }

    /**
     * Ispitivanje da li ce unosom rezervacije doci do preklapanja
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @param soba String vrednost naziva sobe za koju se vrsi upit
     * @param pocetak LocalDate vrednost datuma za koji se vrsi upit
     * @param kraj LocalDate vrednost datuma za koji se vrsi upit
     * @return boolean pokazatelj da ce unosom rezervacije doci do preklapanja
     */
    public static boolean proveriPreklapanjaRez(String objekat, String soba, LocalDate pocetak, LocalDate kraj) {
        List<Rezervacija> preklapajuceRez;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT r FROM Rezervacija r "
                + "WHERE r.idSmJed.objekat.nazivObjekta = :objekat "
                + "AND r.idSmJed.naziv = :soba "
                + "AND r.odjava > :dolazak "
                + "AND r.prijava < :odlazak ");
        upit.setParameter("objekat", objekat);
        upit.setParameter("soba", soba);
        upit.setParameter("dolazak", konvertujDatumZaBazu(pocetak));
        upit.setParameter("odlazak", konvertujDatumZaBazu(kraj));
        preklapajuceRez = upit.list();
        session.getTransaction().commit();
        return preklapajuceRez.isEmpty();
    }

    /**
     * Vracanje ukupnog broja gostiju u svim aktivnim rezervacijama za zeljeni
     * objekat
     *
     * @param objekat String vrednost naziva objekta za koji se vrsi upit
     * @return int vrednost ukupnog broja gostiju
     */
    public static int vratiTrenutniBrojGostiju(String objekat) {
        int brGostiju;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT g FROM Gostiposobama g "
                + "WHERE g.idjRezervacije.idStatRez.statusRezervacije = :x");
        upit.setParameter("x", "aktivna");
        brGostiju = upit.list().size();
        session.getTransaction().commit();
        return brGostiju;
    }

    /**
     * Prikupljanje svih vrednosti tabele za tip sobe
     *
     * @return Lista vrednosti za tip sobe
     */
    public static List<Vrstasmjed> sakupiTipoveSoba() {
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT v FROM Vrstasmjed v");
        return upit.list();
    }

    /**
     * Mapiranje naziva sobe i njihovih vrednosti primarnih kljuceva
     *
     * @return Mapa sa nazivima soba
     */
    public static Map<Integer, String> mapirajNaziveSoba() {
        gatherAndSortRoomInfo();
        naziviSoba = new HashMap<>();
        roomList.forEach((smjed) -> {
            naziviSoba.put(smjed.getIdSmJedinice(), smjed.getNaziv());
        });
        return naziviSoba;
    }

    /**
     * Vraca int vrednost primarnog kljuca prethodno mapirane sobe za koju se
     * vrsi upit.
     *
     * @param nazivSobe Strng vrednost naziva sobe za koju se vrsi upit
     * @return
     */
    public static Integer vratiIdSobeIzMape(String nazivSobe) {
        Integer idSobe = null;
        for (int key : naziviSoba.keySet()) {
            if (naziviSoba.get(key).equals(nazivSobe)) {
                idSobe = key;
            }
        }
        return idSobe;
    }

    /**
     * Mapiranje prethodno prikupljenih tipova sobammm
     *
     * @return
     */
    public static Map<Integer, String> mapirajTipoveSoba() {
        vrsteSoba = new HashMap<>();
        for (Vrstasmjed vrstesoba : sakupiTipoveSoba()) {
            vrsteSoba.put(vrstesoba.getIdVrstaSmJed(), vrstesoba.getVrstaSmJedinice());
        }
        return vrsteSoba;
    }

    public static Integer vratiVrstuSobeIzMape(String nazivSobe) {
        Integer tipSobe = null;
        for (int key : vrsteSoba.keySet()) {
            if (vrsteSoba.get(key).equals(nazivSobe)) {
                tipSobe = key;
            }
        }
        return tipSobe;
    }

    public static Map<Integer, Integer> mapirajBrojLezajeva() {
        brLezajeva = new HashMap<>();
        gatherAndSortRoomInfo();
        for (Smjedinica smjed : roomList) {
            brLezajeva.put(smjed.getIdSmJedinice(), (smjed.getBrGlavnihLezaja() + smjed.getBrPomocnihLezaja()));
        }

        return brLezajeva;
    }

    public static Integer vratiBrLezajevaZaSobu(Integer idSobe) {
        Integer brLezZaSobu = null;
        for (int value : brLezajeva.values()) {
            if (brLezajeva.get(value).equals(idSobe)) {
                brLezZaSobu = value;
            }
        }
        return brLezZaSobu;
    }

    public static Map<Integer, String> mapirajVrsteGostiju() {
        vrstaGosta = new HashMap<>();
        session = getSession().openSession();
        Query upit = session.createQuery("SELECT t FROM Tipgosta t");
        vrsteGostiju = upit.list();
        for (Tipgosta tip : vrsteGostiju) {
            vrstaGosta.put(tip.getIdtipgosta(), tip.getTipGosta());
        }
//        sesija.getTransaction().commit();
        return vrstaGosta;
    }

    public static Integer vradiIdTipaGosta(String tip) {
        Integer idTipG = null;
        for (int key : vrstaGosta.keySet()) {
            if (vrstaGosta.get(key).equals(tip)) {
                idTipG = key;
            }
        }
        return idTipG;
    }

    public static Map<Integer, String> mapirajVrsteDokumenata() {
        vrstaDokumenta = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT d FROM Dokumenta d");
        List<Dokumenta> listaDokumenata = upit.list();
        for (Dokumenta dok : listaDokumenata) {
            vrstaDokumenta.put(dok.getIdDokumenta(), dok.getVrstaDokumenta());
        }
//        sesija.getTransaction().commit();
        return vrstaDokumenta;
    }

    public static Integer vratiIdDokumentaIzMape(String dokument) {
        Integer idDokumenta = null;
        for (int key : vrstaDokumenta.keySet()) {
            if (vrstaDokumenta.get(key).equals(dokument)) {
                idDokumenta = key;
            }
        }
        return idDokumenta;
    }

    public static Map<Integer, String> mapirajStatuseRezervacija() {
        statusRezervacije = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT s FROM Statusrezervacije s");
        statRez = upit.list();
        for (Statusrezervacije sr : statRez) {
            statusRezervacije.put(sr.getIdStatRez(), sr.getStatusRezervacije());
        }
//        sesija.getTransaction().commit();
        return statusRezervacije;
    }

    public static Integer vratiIdStatusaRez(String status) {
        Integer idStatusa = null;
        for (int key : vrstaDokumenta.keySet()) {
            if (statusRezervacije.get(key).equals(status)) {
                idStatusa = key;
            }
        }
        return idStatusa;
    }

    public static Map<Integer, String> mapirajPopuste() {
        mapaPopusta = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT p FROM Popust p WHERE p.popustAktivan = :popustAktivan");
        upit.setParameter("popustAktivan", (short) 1);
        listaPopusta = upit.list();
        for (Popust pop : listaPopusta) {
            mapaPopusta.put(pop.getIdPopust(), pop.getNazivPopusta());
        }
//        sesija.getTransaction().commit();
        return mapaPopusta;
    }

    public static Integer vratiIdPopustaIzMape(String nazivPopusta) {
        Integer idPopusta = null;
        for (int key : mapaPopusta.keySet()) {
            if (mapaPopusta.get(key).equals(nazivPopusta)) {
                idPopusta = listaPopusta.get(key - 1).getIdPopust();
            }
        }
        return idPopusta;
    }

    public static Double vratiIznosPopustaIzMape(String nazivPopusta) {
        Double iznosPopusta = null;
        for (int key : mapaPopusta.keySet()) {
            if (mapaPopusta.get(key).equals(nazivPopusta)) {
                iznosPopusta = listaPopusta.get(key - 1).getIznosPopusta();
            }
        }
        return iznosPopusta;
    }

    public static List<Nalog> napraviListuNaloga() {
        mapaKorisnika = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT n FROM Nalog n");
        listaSvihNaloga = upit.list();

        return listaSvihNaloga;
    }

    public static Map<Integer, String> mapirajKompanije() {
        List<Kompanija> listaKompanija;
        mapaKompanija = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT k FROM Kompanija k");
        listaKompanija = upit.list();
        for (Kompanija komp : listaKompanija) {
            mapaKompanija.put(komp.getIdKompanije(), komp.getNazivKompanije());
        }
        return mapaKompanija;
    }

    public static Integer vratiIdKompanijeIzMape(String kompanija) {
        Integer idKompa = null;
        for (int key : mapaKompanija.keySet()) {
            if (mapaKompanija.get(key).equals(kompanija)) {
                idKompa = key;
            }
        }
        return idKompa;
    }

    public static Map<Integer, String> mapirajObjekte(Integer idKomp) {
        List<Objekti> listaObjekata;
        mapaObjekata = new HashMap<>();
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT o FROM Objekti o WHERE o.kompanija.idKompanije = :idKomp");
        upit.setParameter("idKomp", idKomp);
        listaObjekata = upit.list();
        for (Objekti obj : listaObjekata) {
            mapaObjekata.put(obj.getIdObjekta(), obj.getNazivObjekta());
        }
        return mapaObjekata;
    }

    public static Integer vratiIdObjektaIzMape(String objekat) {
        Integer idObj = null;
        for (int key : mapaObjekata.keySet()) {
            if (mapaObjekata.get(key).equals(objekat)) {
                idObj = key;
            }
        }
        return idObj;
    }

    public static Double vratiCenuNocenja(String objekat, LocalDate datum, Integer brSobe) {
        Double cenaNocenja;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT c FROM Cenasmjed c "
                + "WHERE c.idSmJedinice.objekat.nazivObjekta = :x "
                + "AND c.idSmJedinice.idSmJedinice = :idsmj "
                + "AND c.pocetakCene <= :dat "
                + "AND c.krajCene >= :dat");
        upit.setParameter("x", objekat);
        upit.setParameter("idsmj", brSobe);
        upit.setParameter("dat", konvertujDatumZaBazu(datum));
        List<Cenasmjed> rezultat = upit.list();
        cenaNocenja = rezultat.get(0).getCena();
        session.getTransaction().commit();
        return cenaNocenja;
    }

    public static Integer vratiIdCeneNocenja(String objekat, Integer brSobe, Double cena) {
        Integer idCene;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT c FROM Cenasmjed c "
                + "WHERE c.idSmJedinice.objekat.nazivObjekta = :x "
                + "AND c.idSmJedinice.idSmJedinice = :idsmj "
                + "AND c.cena = :cena");
        upit.setParameter("x", objekat);
        upit.setParameter("idsmj", brSobe);
        upit.setParameter("cena", cena);
        List<Cenasmjed> rezultat = upit.list();
        idCene = rezultat.get(0).getIdCenaSmJed();
        session.getTransaction().commit();
        return idCene;
    }

    public static Smjedinica vratiInfoOSmJed(Integer idSobe) {
        Smjedinica trazenaSoba;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT s FROM Smjedinica s WHERE s.idSmJedinice = :x");
        upit.setParameter("x", idSobe);
        trazenaSoba = (Smjedinica) upit.uniqueResult();
        session.getTransaction().commit();
        return trazenaSoba;
    }

    public static List<Svojstvasmjed> vratiSvojstvaSmJed(Integer idSmJed) {
        List<Svojstvasmjed> svojstvaSmjed;
        session = getSession().openSession();
        session.beginTransaction();
        Query upit = session.createQuery("SELECT s FROM Svojstvasmjed s WHERE s.idSmJedinice.idSmJedinice = :x");
        upit.setParameter("x", idSmJed);
        svojstvaSmjed = upit.list();
        session.getTransaction().commit();
        return svojstvaSmjed;
    }

    // HIBERNATE INSERT/REMOVE/UPDATE
    public static void ubaciKlijenta(Klijent kor) {

        session = getSession().openSession();
        session.beginTransaction();

        session.save(kor);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void ubaciRezervaciju(Rezervacija rez) {

        session = getSession().openSession();
        session.beginTransaction();

        session.save(rez);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void odjaviRezervaciju(Integer idRez) {

        Query upit = session.createQuery("SELECT r FROM Rezervacija r WHERE r.idRezervacija = :x ");
        upit.setParameter("x", idRez);
        Rezervacija rez = (Rezervacija) upit.uniqueResult();
        rez.setOdjava(konvertujDatumZaBazu(LocalDate.now()));

        session.update(rez);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void ubaciNaplatu(Naplata nap) {

        session = getSession().openSession();
        session.beginTransaction();

        session.saveOrUpdate(nap);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void ubaciGosta(Gost gost) {
        session = getSession().openSession();
        session.beginTransaction();

        session.save(gost);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void ubaciGPS(Gostiposobama gps) {
        session = getSession().openSession();
        session.beginTransaction();

        session.save(gps);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void obrisiRez(Integer brRez) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT r FROM Rezervacija r WHERE r.idRezervacija = :x ");
        upit.setParameter("x", brRez);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }

    public static void obrisiNaplatu(Integer brRez) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT n FROM Naplata n WHERE n.idRezervacija.idRezervacija = :x");
        upit.setParameter("x", brRez);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }

    public static void obrisiGPS(Integer brRez) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT g FROM Gostiposobama g WHERE g.idjRezervacije.idRezervacija = :x");
        upit.setParameter("x", brRez);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }

    public static void obrisiNalog(Integer idNaloga) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT n FROM Nalog n WHERE n.idNalog = :x");
        upit.setParameter("x", idNaloga);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }

    public static void obrisiProfil(Integer idProfila) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT p FROM Profil p WHERE p.idProfila = :x");
        upit.setParameter("x", idProfila);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }

    public static void ubaciNalog(Nalog nalog) {
        session = getSession().openSession();
        session.beginTransaction();

        session.save(nalog);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static void ubaciProfil(Profil profil) {
        session = getSession().openSession();
        session.beginTransaction();

        session.save(profil);

        session.getTransaction().commit();
//        sesija.close();
    }

    public static boolean korisnickoImeNePostoji(String korisnickoIme) {
        List<Nalog> listaNaloga;
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT n FROM Nalog n WHERE n.korisnickoIme = :x");
        upit.setParameter("x", korisnickoIme);

        listaNaloga = upit.list();

        session.getTransaction().commit();
        return listaNaloga.isEmpty();
    }

    public static void izmeniProfil(Profil profil) {

        session = getSession().openSession();
        session.beginTransaction();

        session.saveOrUpdate(profil);

        session.getTransaction().commit();
    }

    public static void izmeniNalog(Nalog nalog) {

        session = getSession().openSession();
        session.beginTransaction();

        session.saveOrUpdate(nalog);

        session.getTransaction().commit();
    }

    public static void dodajIzmeni(Object o) {
        session = getSession().openSession();
        session.beginTransaction();

        session.saveOrUpdate(o);

        session.getTransaction().commit();
    }

    public static void obrisiSmJed(Integer idSobe) {
        session = getSession().openSession();
        session.beginTransaction();

        Query upit = session.createQuery("SELECT s FROM Smjedinica s WHERE s.idSmJedinice = :x");
        upit.setParameter("x", idSobe);

        session.delete(upit.uniqueResult());

        session.getTransaction().commit();
    }
}
