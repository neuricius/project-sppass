/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.LocalDate;
import view.AlertView;

/**
 *
 * @author Grupa2
 */
public class PretragaBuilder {

    Integer brRez;
    String imeNosioca, prezimeNosioca, nazivSobe;
    LocalDate odDatum, doDatum;

//    public static String
    public static String PretragaBuilder(Integer brRez, String imeNosioca, String prezimeNosioca, String nazivSobe, LocalDate odDatum, LocalDate doDatum) {
        String upit = "SELECT r FROM Rezervacija r WHERE r.idSmJed.objekat.nazivObjekta = :a";
        if (brRez != null) {
            upit = upit + " AND r.idRezervacija = :b";
        }
        if (imeNosioca != null) {
            upit = upit + " AND r.idKlijent.imeKlijenta = :c";
        }
        if (prezimeNosioca != null) {
            upit = upit + " AND r.idKlijent.prezimeKlijenta = :d";
        }
        if (nazivSobe != null) {
            upit = upit + " AND r.idSmJed.naziv = :e";
        }
        if (odDatum != null && doDatum != null && odDatum.isBefore(doDatum)) {
            upit = upit + " AND r.prijava BETWEEN :f AND :g";
        } else {
            AlertView.show("Pogresno uneseni datumi", "Greska");
        }
        

        return upit;
    }

}
