/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static controller.MainCTRL.*;
import static util.HibernateUtil.*;

/**
 *
 * @author Nebojsa Matic
 */
public class AutoAzuriranjeBaze {

    static ScheduledExecutorService service;
    static Runnable potrcko;

    public static void pokreniAAB() {
        
        service = Executors.newSingleThreadScheduledExecutor();
        potrcko = new Runnable() {
            @Override
            public void run() {
                autoUpdateTimer().stop();
                //kod za proveru baze
                azurirajRezervacijeUBazi(facilityName());
                updateResScreen();
                autoUpdateTimer().play();
            }
        };

        try {
            service.scheduleWithFixedDelay(potrcko, 30, 30, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
