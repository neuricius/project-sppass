/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 *
 * @author Nebojsa Matic
 */
public class Konvertori {

    public static LocalDate konvertujDatumZaApp(Date datum) {

        return Instant.ofEpochMilli(datum.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date konvertujDatumZaBazu(LocalDate datum) {

        return Date.from(datum.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}
