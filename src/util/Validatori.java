/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.LocalDate;
import java.util.regex.*;

/**
 *
 * @author Nebojsa Matic
 */
public class Validatori {

    public static boolean validacijaEmail(String email) {
        Pattern obrazacEmaila = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher validatorEmaila = obrazacEmaila.matcher(email);

        return validatorEmaila.find();
    }

    public static boolean validacijaDouble(String doubleBroj) {
        Pattern obrazacDoubleBroja = Pattern.compile("\\d+(\\.\\d+)?");
        Matcher validatorDouble = obrazacDoubleBroja.matcher(doubleBroj);

        return validatorDouble.find();
    }

    public static boolean validacijaTelefon(String brTel) {
        String obrazacSaPlusom = "^(([+]|[0]{2})([\\\\d]{1,3})([\\\\s-]{0,1}))?([\\\\d]{9})$";
        Pattern obrazacBrTelefonaSaPlusom = Pattern.compile(obrazacSaPlusom);
        Matcher validatorBrTelefonaSaPlusom = obrazacBrTelefonaSaPlusom.matcher(brTel);

        String obrazacBezPlusa = "\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}";
        Pattern obrazacBrTelefonaBezPlusa = Pattern.compile(obrazacBezPlusa);
        Matcher validatorTelefonaBezPlusa = obrazacBrTelefonaBezPlusa.matcher(brTel);

        return validatorBrTelefonaSaPlusom.find()||validatorTelefonaBezPlusa.find();
    }

}
