/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.PrintResolution;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;


/**
 *
 * @author Nebojsa Matic
 */
public class Stampac {

    /**
     *
     * @param pane
     * @param stage
     */
    public static void snimiScenu(Pane pane, Stage stage) {
        WritableImage writableImage
                = new WritableImage((int) pane.getWidth(), (int) pane.getHeight());
        pane.getScene().snapshot(writableImage);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Snimi sliku");
        fileChooser.setInitialFileName("SPPASS-snimak" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMddyyHHmmss")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            } catch (IOException ex) {
                Logger.getLogger(Stampac.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    // nastaviti odavde
    public static BufferedImage generatePngFromContainer(Node node) {
        SnapshotParameters param = new SnapshotParameters();
        param.setDepthBuffer(true);
        WritableImage snapshot = node.snapshot(param, null);
        BufferedImage tempImg = SwingFXUtils.fromFXImage(snapshot, null);
        BufferedImage img = null;
        byte[] imageInByte;
        try {
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(tempImg, "png", baos);
                baos.flush();
                imageInByte = baos.toByteArray();
            }
            InputStream in = new ByteArrayInputStream(imageInByte);
            img = ImageIO.read(in);
        } catch (IOException e) {
            // TODO Auto-generated catch block

        }
        //the final image sent to the PDJpeg
        return img;
    }

    public static void stampajNesto(Node node) {

        PrinterJob job = PrinterJob.createPrinterJob();

        if (job != null && job.showPrintDialog(node.getScene().getWindow())) {
            Printer printer = job.getPrinter();
            PageLayout pageLayout = printer.createPageLayout(Paper.TABLOID, PageOrientation.LANDSCAPE, Printer.MarginType.HARDWARE_MINIMUM);

//            node.setVisible(false);
//            node.setManaged(false);
            


            double width = node.getScene().getWidth();
            double height = node.getScene().getHeight();

            PrintResolution resolution = job.getJobSettings().getPrintResolution();

            width /= resolution.getFeedResolution();

            height /= resolution.getCrossFeedResolution();

            double scaleX = pageLayout.getPrintableWidth() / width / 600;
            double scaleY = pageLayout.getPrintableHeight() / height / 600;

            Scale scale = new Scale(scaleX, scaleY);

            node.getTransforms().add(scale);

            boolean success = job.printPage(pageLayout, node);
            if (success) {
                job.endJob();
            }
            node.getTransforms().remove(scale);
        }
//            node.setVisible(true);
//            node.setManaged(true);
//            node.setEffect(new GaussianBlur(0.9));


    }
}
