/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import static controller.MainCTRL.*;
import static util.HibernateUtil.*;

/**
 *
 * @author Nebojsa Matic
 */
public class AutoAzuriranjeBaze2 {

    public static Timeline timeline2;

    public static void pokreniAAB() {
        timeline2 = new Timeline(new KeyFrame(Duration.minutes(5), e -> odradiAAB()));
        timeline2.setCycleCount(Animation.INDEFINITE);
        timeline2.play();
    }

    public static void odradiAAB() {
        autoUpdateTimer().stop();
        //kod za proveru baze
        azurirajRezervacijeUBazi(facilityName());
        System.out.println("Pokusaj azuriranja baze u " + LocalDateTime.now());
        updateResScreen();

        autoUpdateTimer().play();
    }
}
