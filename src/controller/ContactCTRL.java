/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import javafx.stage.StageStyle;
import view.ContactView;
import static util.SlanjeMaila.posaljiMail;
import components.WindowDecorator;
import static view.ContactView.companyROL;
import static view.ContactView.senderROL;
import static view.ContactView.msgSubjectTF;
import static view.ContactView.msgTextTF;
import static view.ContactView.sendMsgB;
import static view.ContactView.contactScene;
import static util.HibernateUtil.userProfileName;
import static util.HibernateUtil.companyName;

/**
 *
 * @author Nebojsa Matic
 */
public class ContactCTRL implements WindowDecorator{

    private static ContactView contactWindow;

    public ContactCTRL() {
        launchMailSender();
        sendMsgB.setOnAction(e -> {
        String posiljalac = senderROL.getText();
        String kompanija = companyROL.getText();
        String naslov = msgSubjectTF.getText();
        String tekst = msgTextTF.getText();
        
        posaljiMail(kompanija, posiljalac, naslov, tekst);
        contactWindow.close();
        });

    }

    public void launchMailSender() {
        contactWindow = new ContactView();
        WindowDecorator.confirmationDecorator(contactWindow);
        contactWindow.setTitle("Kontaktiraj autora - SPPASS v0.5 by Neuricius");
        contactWindow.setScene(contactScene);
        contactWindow.initStyle(StageStyle.UTILITY);
        contactWindow.setResizable(false);
        contactWindow.initModality(Modality.APPLICATION_MODAL);
        contactWindow.setResizable(false);
        companyROL.setText(companyName());
        senderROL.setText(userProfileName());
        contactWindow.show();
    }
}
