/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.stage.Modality;
import model.Rezervacija;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import view.OpenReservationView;
import static view.OpenReservationView.*;
import view.ConfirmationDialog;
import static util.AutoAzuriranjeBaze2.odradiAAB;
import static util.HibernateUtil.obrisiRez;
import static util.HibernateUtil.odjaviRezervaciju;
import static util.Konvertori.konvertujDatumZaApp;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.OpenReservationView.reservationScene;

/**
 *
 * @author Nebojsa Matic
 */
public class OpenResCTRL implements WindowDecorator, TakeOverCTRL {

    private static AutoCompletionBinding<String> autoCompletionBinding;
    private static String[] sugestijeRezervacija = {"neo", "daki", "Nebojsa", "Matic", "Dajana", "Scasni"};
    private static Set<String> sugestijeRezervacijaSet = new HashSet<>(Arrays.asList(sugestijeRezervacija));

    private static OpenReservationView resWindow2;

    public OpenResCTRL(Rezervacija rez) throws FileNotFoundException {
        resWindow2 = new OpenReservationView();
        WindowDecorator.noConfirmationDecorator(resWindow2);
        resWindow2.initModality(Modality.WINDOW_MODAL);
        resWindow2.initOwner(returnMain());
        resWindow2.setResizable(false);
        addCTRL(rez);
        resWindow2.setScene(reservationScene);
        resWindow2.setTitle("Rezervacija - " + rez.getIdRezervacija());
        resWindow2.show();
    }

    public static boolean clientIsGuest(Rezervacija rez) {
        return rez.getIdKlijent().getKlijentJeGost() != 0;
    }

    @Override
    public void addCTRL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {

        if (rez.getIdKlijent().getIdKlijent() != null) {
            clientNumberROL.setText(rez.getIdKlijent().getIdKlijent().toString());
        } else {
            clientNumberROL.setText("/");
        }

        clientIsGuest.setSelected(clientIsGuest(rez));

        if (!rez.getIdKlijent().getImeKlijenta().isEmpty()) {
            clientFirstNameROL.setText(rez.getIdKlijent().getImeKlijenta());

        }

        if (!rez.getIdKlijent().getPrezimeKlijenta().isEmpty()) {
            clientLastNameROL.setText(rez.getIdKlijent().getPrezimeKlijenta());

        }

        if (!rez.getIdKlijent().getMestoKlijenta().isEmpty()) {
            clientResidenceCityROL.setText(rez.getIdKlijent().getMestoKlijenta());
        }

        if (!rez.getIdKlijent().getDrzavaKlijenta().isEmpty()) {
            clientResidenceCountryROL.setText(rez.getIdKlijent().getDrzavaKlijenta());

        }

        if (!rez.getIdKlijent().getEmailKlijenta().isEmpty()) {
            clientEmailROL.setText(rez.getIdKlijent().getEmailKlijenta());
        }

        if (rez.getIdKlijent().getSifraDokumentaKlijenta() != null) {
            clientDocumentTypeROL.setText(rez.getIdKlijent().getSifraDokumentaKlijenta().getVrstaDokumenta());
            clientDocumentValueL.setText(rez.getIdKlijent().getBrDokumentaKlijenta());

        }

        if (!rez.getIdSmJed().getNaziv().isEmpty()) {
            clientRoomROL.setText(rez.getIdSmJed().getIdSmJedinice().toString());
            clientRoomROL.setText(rez.getIdSmJed().getNaziv());
        }

        LocalDate pocetniDatum = konvertujDatumZaApp(rez.getPrijava());
        resStartDateROL.setText(pocetniDatum.format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        LocalDate krajnjiDatum = konvertujDatumZaApp(rez.getOdjava());
        resEndDateROL.setText(krajnjiDatum.format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        numberOfAdultsROL.setText(rez.getBrojOdraslih().toString());

        numberOfChildrenROL.setText(rez.getBrojDece().toString());

        //proveriti jel dobro racuna
        String brNocenja = String.valueOf(DAYS.between(pocetniDatum, krajnjiDatum));

        numberOfNightsROL.setText(brNocenja);

        if (!rez.getBeleskeORez().isEmpty()) {
            resNotesROL.setText(rez.getBeleskeORez());

        }

        if (!rez.getIdStatRez().getStatusRezervacije().isEmpty()) {
            reservationStatusROL.setText(rez.getIdStatRez().getStatusRezervacije());
        }

        double cenaNocenja = rez.getNaplataList().get(0).getCenaNocenja().getCena();
        String cenaNocenjaStr = String.valueOf(cenaNocenja);
        pricePerPeriodROL.setText(cenaNocenjaStr);

        double ukCenaNocenja = rez.getNaplataList().get(0).getCenaSmestaja();
        String ukCenaNocenjaStr = String.valueOf(ukCenaNocenja);
        totalPriceROL.setText(ukCenaNocenjaStr);

        double ostaliTroskovi = rez.getNaplataList().get(0).getOstaliTroskovi();
        String ostaliTroskoviStr = String.valueOf(ostaliTroskovi);
        otherExpensesROL.setText(ostaliTroskoviStr);

        double medjuZbir = rez.getNaplataList().get(0).getMedjuZbir();
        String medjuZbirStr = String.valueOf(medjuZbir);
        subTotalROL.setText(medjuZbirStr);

        String nazivPopusta = rez.getNaplataList().get(0).getSifraPopusta().getNazivPopusta();
        discountROL.setText(nazivPopusta);

        double preostaliIznos = rez.getNaplataList().get(0).getPreostaliIznos();
        String preostaliIznosStr = String.valueOf(preostaliIznos);
        totalBalanceROL.setText(preostaliIznosStr);

        // kod za placeno do sad, treba napraviti da se unesena vrednost uvecava
        double placenoZaSad = rez.getNaplataList().get(0).getUplaceno();
        String placenoZaSadStr = String.valueOf(placenoZaSad);
        currentBalanceROL.setText(placenoZaSadStr);

        double preostaliIznosZaNaplatu = rez.getNaplataList().get(0).getPreostaliIznos();
        String preostaliIznosZaNaplatuStr = String.valueOf(preostaliIznosZaNaplatu);
        remainBalanceROL.setText(preostaliIznosZaNaplatuStr);

        //
        //dugmad
        closeViewB.setOnAction((ActionEvent e) -> {
            resWindow2.close();
        });

        printResB.setOnAction((ActionEvent e) -> {
            new Thread(new PrintActionListener(generatePngFromContainer(mainVBox))).start();
        });

        checkOutResB.setDisable(rez.getIdStatRez().getIdStatRez() == 4);
        checkOutResB.setOnAction((ActionEvent e) -> {
            if (konvertujDatumZaApp(rez.getPrijava()).isAfter(LocalDate.now())) {
                e.consume();
                if (ConfirmationDialog.confirmAction("Rezervacija jos nije pocela/nObrisati?", "Odjava rezervacije", "Da", "Ne")) {
                    obrisiRez(rez.getIdRezervacija());
                    odradiAAB();
                    resWindow2.close();
                }
            }
            if (ConfirmationDialog.confirmAction("Da li ste sigurni?", "Odjava rezervacije", "Da", "Ne")) {
                odjaviRezervaciju(rez.getIdRezervacija());
                odradiAAB();
                resWindow2.close();
            } else {
                e.consume();
            }
        });

        gprB.setOnAction((ActionEvent e) -> {
            new GuestsPerRoomSpecificCTRL(rez.getIdRezervacija().intValue());
        });

        //labele za status bar
        entryAuthorNameL.setText(rez.getAutorPoslednjeIzmene().getImeProfila());
        LocalDate datumIzmene = konvertujDatumZaApp(rez.getDatumPoslednjeIzmene());
        entryDateValueL.setText(datumIzmene.toString());
    }

}
