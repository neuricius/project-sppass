/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.LocalDate;
import javafx.stage.Modality;
import view.SearchView;
import static view.SearchView.*;
import util.HibernateUtil;
import static util.HibernateUtil.vratiNazivSobe;
import components.WindowDecorator;
import model.Rezervacija;

/**
 *
 * @author Nebojsa Matic
 */
public final class SearchCTRL implements WindowDecorator, TakeOverCTRL {

    private static SearchView searchWindow;

    public SearchCTRL() {
        searchWindow = new SearchView();
        WindowDecorator.noConfirmationDecorator(searchWindow);
        addCTRL();
        searchWindow.setScene(SearchScene);
        searchWindow.setTitle("Pretraga Rezervacija");
        searchWindow.setResizable(false);
        searchWindow.initModality(Modality.APPLICATION_MODAL);

        //povlacenje spiska soba
        // iskljuceno da bih mogao da pokrecem sam prozor, vratiti posle
        for (int i = 0; i < HibernateUtil.roomList.size(); i++) {
            roomNameCB.getItems().add(vratiNazivSobe(i));
        }
        roomNameCB.setValue(vratiNazivSobe(0));
        searchWindow.show();
    }

    @Override
    public void addCTRL() {
        searchB.setOnAction(e -> {
            Integer brRez;
            String imeNosioca;
            String prezimeNosioca;

            if (resNoTF.getText().isEmpty()) {
                brRez = null;
            } else {
                brRez = Integer.parseInt(resNoTF.getText());
            }

            if (carrierFirstNameTF.getText().isEmpty()) {
                imeNosioca = null;
            } else {
                imeNosioca = carrierFirstNameTF.getText();
            }

            if (carrierLastNameTF.getText().isEmpty()) {
                prezimeNosioca = null;
            } else {
                prezimeNosioca = carrierLastNameTF.getText();
            }

            String nazivSobe = roomNameCB.getValue();
            LocalDate odDatum = periodStartDP.getValue();
            LocalDate doDatum = periodEndDP.getValue();

            new SearchReservationsCTRL(brRez, imeNosioca, prezimeNosioca, nazivSobe, odDatum, doDatum);
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
