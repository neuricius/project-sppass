/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import javafx.stage.Modality;
import view.NewBillView;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.NewBillView.newBillScene;

/**
 *
 * @author Nebojsa Matic
 */
public class NewBillCTRL implements WindowDecorator {

    private static NewBillView billingWindow;

    public NewBillCTRL() throws FileNotFoundException {
        billingWindow = new NewBillView();
        WindowDecorator.noConfirmationDecorator(billingWindow);
        billingWindow.initModality(Modality.WINDOW_MODAL);
        billingWindow.initOwner(returnMain());
        billingWindow.setResizable(false);
//        dodajSve();
        // iniciranje stage, sav kod pisati pre ovoga
        billingWindow.setScene(newBillScene);
        billingWindow.setTitle("Racun br - " + 21345);
        billingWindow.show();
    }

}
