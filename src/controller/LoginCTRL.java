/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import view.AlertView;
import view.ExitConfirmation;
import view.LoginView;
import static view.LoginView.btnCancel;
import static util.HibernateUtil.*;
import static view.LoginView.btnOK;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import util.HibernateUtil;
import static view.MainView.systemMsgL;
import static view.MainView.currentUserNameL;
import static view.MainView.mainPaneBP;
import static view.LoginView.txtUser;
import static view.LoginView.txtPass;
import static view.LoginView.loginScene;
import static util.HibernateUtil.returnPass;
import static util.HibernateUtil.userProfileName;

/**
 *
 * @author Nebojsa Matic
 */
public class LoginCTRL implements WindowDecorator {

    private static LoginView loginWindow1, loginWindow2;

    public LoginCTRL() {
        launchLogin();
    }

    public static void launchLogin() {
        loginWindow1 = new LoginView();
        WindowDecorator.mainDecorator(loginWindow1);
        loginWindow1.setResizable(false);
        addEventHandlers(loginWindow1);
        loginWindow1.setScene(loginScene);
        loginWindow1.setTitle("Prijava na sistem");

        //dodavanje button actiona iz LP
        txtUser.setOnAction(e -> {
            try {
                loginCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        txtPass.setOnAction(e -> {
            try {
                loginCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        btnOK.setOnAction((ActionEvent e) -> {
            try {
                loginCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        btnCancel.setOnAction(e -> ExitConfirmation.programExit());

        //
        loginWindow1.show();
    }

    public static void launchReLogin() {
        loginWindow2 = new LoginView();
        WindowDecorator.noConfirmationDecorator(loginWindow2);
        loginWindow2.setResizable(false);
        addEventHandlers(loginWindow2);
        loginWindow2.initModality(Modality.APPLICATION_MODAL);
        loginWindow2.setScene(loginScene);
        loginWindow2.setTitle("Promena korisnika");
        //dodavanje button actiona iz LP
        txtUser.setOnAction(e -> {
            try {
                relogCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        txtPass.setOnAction(e -> {
            try {
                relogCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        btnOK.setOnAction(e -> {
            try {
                relogCheck();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        btnCancel.setOnAction(e -> loginWindow2.close());

        //
        loginWindow2.show();
    }

    public static void reLaunchLogin() {
        loginWindow1.close();
        launchLogin();
    }

    public static void addEventHandlers(LoginView loginProzor) {
        loginProzor.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if (t.getCode().equals(KeyCode.ENTER)) {
                t.consume();
                try {
                    loginCheck();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(LoginCTRL.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (t.getCode().equals(KeyCode.ESCAPE)) {
                t.consume();
                ExitConfirmation.programExit();
            }
        });
    }

    /**
     *
     * @throws java.io.FileNotFoundException
     */
    public static void loginCheck() throws FileNotFoundException {
        //smorilo me vise logovanje, skloniti na kraju SVEGA :D
        String userName = "neo";
        String userPass = "neo";
//        String userName = LoginView.txtUser.getText();
//        String userPass = LoginView.txtPass.getText();
        String errorMessage = "";
//        if (LoginView.txtUser.getText().length() == 0) {
//            errorMessage = "Morate uneti korisnicko ime.";
//            AlertView.show(errorMessage, "Greska");
//        }
//        if (LoginView.txtPass.getText().length() == 0) {
//            errorMessage = "Morate uneti sifru.";
//            AlertView.show(errorMessage, "Greska");
//        }
        if (errorMessage.length() == 0) {
            // ubaciti test metod
            String returnedPass = returnPass(userName);
            if (returnedPass.equals(userPass)) {
                String message = "Dobrodosli " + userProfileName() + " :)";
                AlertView.show(message, "Uspesna prijava");
                // ubaceno 5.6, dodati za relog

                loginWindow1.close();
                new MainCTRL();
                systemMsgL.setText("Uspesno povezivanje na bazu");
                currentUserNameL.setText(userProfileName());
            } else {
                errorMessage = "Pogresno korisnicko ime ili sifra.";
                AlertView.show(errorMessage, "Greska");
                reLaunchLogin();
            }

        }
    }

    public static void relogCheck() throws FileNotFoundException {
        String userName = LoginView.txtUser.getText();
        String userPass = LoginView.txtPass.getText();
        String errorMessage = "";
        if (LoginView.txtUser.getText().length() == 0) {
            errorMessage = "Morate uneti korisnicko ime.";
            AlertView.show(errorMessage, "Greska");
        }
        if (LoginView.txtPass.getText().length() == 0) {
            errorMessage = "Morate uneti sifru.";
            AlertView.show(errorMessage, "Greska");
        }
        if (errorMessage.length() == 0) {
            try {
                // preraditi odavde

                if (returnOtherUser(userName, userPass)) {
                    String message = "Dobrodosli " + userProfileNameOnRelog() + " :)";
                    AlertView.show(message, "Uspesna prijava");
                    returnMain().adminMenuSettings.setDisable(userLevelCheckOnRelog());
                    returnMain().deleteReservationMI.setDisable(userLevelCheckOnRelog());
                    mainPaneBP.setEffect(null);
                    loginWindow2.close();
//                    new MainCTRL();
                    systemMsgL.setText("Uspesna promena korisnika");
                    currentUserNameL.setText(userProfileNameOnRelog());
                } else {
                    loginWindow2.close();
                    errorMessage = "Nalog ne pripada istoj kompaniji.";
                    AlertView.show(errorMessage, "Greska");
                    launchLogin();
                }
            } catch (NullPointerException e) {
                System.out.println("NPE na pokusaju reloga, jebat mater posto OPET zabada");
            }
        }
    }

}
