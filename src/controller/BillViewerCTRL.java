/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import javafx.stage.Modality;
import model.Rezervacija;
import view.BillOverView;
import static view.BillOverView.*;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.DeparturesOnDayView.departuresTable;
import static view.BillOverView.billOverviewScene;

/**
 *
 * @author Nebojsa Matic
 */
public class BillViewerCTRL implements WindowDecorator, TakeOverCTRL {

    public static BillOverView billWindow;

    public BillViewerCTRL() throws FileNotFoundException {
        billWindow = new BillOverView();
        WindowDecorator.noConfirmationDecorator(billWindow);
        addCTRL();
        billWindow.setScene(billOverviewScene);
        billWindow.initModality(Modality.WINDOW_MODAL);
        billWindow.initOwner(returnMain());
        billWindow.setTitle("Pregled Cena");
        billWindow.show();
    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(billTable))).start();
        });
        addBillB.setDisable(true);
        deleteBillB.setDisable(true);
    }

    @Override
    public void addCTRL(Integer idRez) {
//
    }

    @Override
    public void addCTRL(Rezervacija rez) {
//
    }

}
