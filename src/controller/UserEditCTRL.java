/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import javafx.stage.StageStyle;
import model.*;
import view.MainView;
import view.AlertView;
import view.ConfirmationDialog;
import view.UserEditView;
import static view.UserEditView.*;
import util.HibernateUtil;
import static util.HibernateUtil.izmeniNalog;
import static util.HibernateUtil.izmeniProfil;
import static util.HibernateUtil.korisnickoImeNePostoji;
import static util.HibernateUtil.listaSvihNaloga;
import static util.HibernateUtil.mapaKompanija;
import static util.HibernateUtil.mapaObjekata;
import static util.HibernateUtil.mapirajKompanije;
import static util.HibernateUtil.mapirajObjekte;
import static util.HibernateUtil.napraviListuNaloga;
import static util.HibernateUtil.obrisiNalog;
import static util.HibernateUtil.obrisiProfil;
import static util.Validatori.*;
import components.WindowDecorator;
import static view.UserEditView.userEditorScene;

/**
 *
 * @author Nebojsa Matic
 */
public class UserEditCTRL implements WindowDecorator, TakeOverCTRL {

    private static UserEditView usEditor;

    Nalog userAccount;
    Integer userTempId;
    Profil userProfile;

    public UserEditCTRL() {
        pokreniUredjivanjeKorisnika();
    }

    private void pokreniUredjivanjeKorisnika() {
        usEditor = new UserEditView();
        WindowDecorator.noConfirmationDecorator(usEditor);
        addCTRL();
        usEditor.setScene(userEditorScene);
        usEditor.initStyle(StageStyle.UTILITY);
        usEditor.initModality(Modality.APPLICATION_MODAL);
        usEditor.setResizable(false);
        usEditor.setTitle("Uredjivanje korisnika - SPASS v0.5 by Neuricius");
        usEditor.show();
        System.out.println("Width: " + usEditor.getWidth());
        System.out.println("Height: " + usEditor.getHeight());
    }

    @Override
    public void addCTRL() {

        for (Nalog nalog : napraviListuNaloga()) {
            userIDCB.getItems().add(nalog.getIdNalog());
        }
        userIDCB.setPromptText("Izaberite korisnika");

        mapirajKompanije();
        for (int i = 1; i <= mapaKompanija.size(); i++) {
            userCompanyCB.getItems().add(mapaKompanija.get(i));
        }
        userCompanyCB.setValue(mapaKompanija.get(1));

        mapirajObjekte(HibernateUtil.vratiIdKompanijeIzMape(userCompanyCB.getValue()));
        for (int i = 1; i <= mapaObjekata.size(); i++) {
            userObjectCB.getItems().add(mapaObjekata.get(i));
        }
        userObjectCB.setValue(mapaObjekata.get(1));

        userIDCB.setOnAction(e -> {
            userAccount = listaSvihNaloga.get(userIDCB.getValue() - 1);
            userTempId = userAccount.getIdNalog();
            userProfile = userAccount.getProfil();

            userNameTF.setText(userAccount.getKorisnickoIme());

            userPassTF.setText(userAccount.getSifra());

            userFirstNameTF.setText(userProfile.getImeProfila());

            userLastNameTF.setText(userProfile.getPrezimeProfila());

            userPhoneTF.setText(userProfile.getBrTelefonaProfila());

            userEmailTF.setText(userProfile.getEmail());

        });

        addUserB.setOnMouseClicked(e -> {

            userNameTF.clear();
            userPassTF.clear();
            userFirstNameTF.clear();
            userLastNameTF.clear();
            userPhoneTF.clear();
            userEmailTF.clear();

            userIDCB.setPromptText("Novi korisnik");
            userIDCB.setDisable(true);
            userProfile = new Profil();
            userAccount = new Nalog();

        });

        //dodati insert iznad
        deleteUserB.setOnAction(e -> {
            if (userAccount == null) {
                e.consume();
                usEditor.close();
                AlertView.show("Nema naloga za brisanje", "Greska");
                pokreniUredjivanjeKorisnika();
            }
            usEditor.close();
            if (ConfirmationDialog.confirmAction("Da li ste sigurni", "Brisanje korisnika", "Da", "Ne")) {
                obrisiNalog(userAccount.getIdNalog());
                obrisiProfil(userProfile.getIdProfila());
                MainView.systemMsgL.setText("Nalog uspesno obrisan");
                pokreniUredjivanjeKorisnika();
            }
        });

        saveChangesB.setOnAction(e -> {
            if (userIDCB.isDisabled() && !korisnickoImeNePostoji(userNameTF.getText())) {
                e.consume();
                AlertView.show("Korisnicko ime vec postoji!", "Greska");
                userNameTF.requestFocus();
            }
            if (!validacijaTelefon(userPhoneTF.getText())) {
                e.consume();
                AlertView.show("Nije ispravan format telefona!", "Greska");
                userPhoneTF.requestFocus();
            }
            if (!validacijaEmail(userEmailTF.getText())) {
                e.consume();
                AlertView.show("Nije ispravan format maila!", "Greska");
                userEmailTF.requestFocus();
            }

            userProfile = new Profil();
            userAccount = new Nalog();

            if (!userIDCB.isDisabled()) {
                userProfile.setIdProfila(userIDCB.getValue());
            }
            userProfile.setImeProfila(userFirstNameTF.getText());
            userProfile.setPrezimeProfila(userLastNameTF.getText());
            userProfile.setBrTelefonaProfila(userPhoneTF.getText());
            userProfile.setEmail(userEmailTF.getText());
            userProfile.setKompanija(new Kompanija(HibernateUtil.vratiIdKompanijeIzMape(userCompanyCB.getValue())));

            izmeniProfil(userProfile);

            userAccount.setIdNalog(userTempId);
            userAccount.setKorisnickoIme(userNameTF.getText());
            userAccount.setSifra(userPassTF.getText());
            userAccount.setProfil(new Profil(userProfile.getIdProfila()));
            userAccount.setKompanija(new Kompanija(HibernateUtil.vratiIdKompanijeIzMape(userCompanyCB.getValue())));

            izmeniNalog(userAccount);
            MainView.systemMsgL.setText("Uspesna izmena korisnika");
            usEditor.close();
            AlertView.show("Uspesna izmena korisnika", "Uspeh!");
            pokreniUredjivanjeKorisnika();
        });

        closeViewB.setOnAction(e -> {
            if (ConfirmationDialog.confirmAction("Da li ste sigurni?\nPromene mozda nisu snimljene", "Zatvaranje prozora", "Da", "Ne")) {
                e.consume();
                usEditor.close();
            }
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
