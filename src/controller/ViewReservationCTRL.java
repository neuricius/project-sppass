/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import view.ReservationOverView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import model.Rezervacija;
import static view.ReservationOverView.refreshB;
import static view.ReservationOverView.addResB;
import static view.ReservationOverView.deleteResB;
import static view.ReservationOverView.reservationsTable;
import static view.ReservationOverView.reservationIDTC;
import static view.ReservationOverView.arrivalDateTC;
import static view.ReservationOverView.resOverviewScene;

/**
 *
 * @author Nebojsa Matic
 */
public class ViewReservationCTRL implements WindowDecorator, TakeOverCTRL {

    private static ReservationOverView viewResWindow;

    public ViewReservationCTRL() {
        viewResWindow = new ReservationOverView();
        WindowDecorator.noConfirmationDecorator(viewResWindow);
        addCTRL();
        viewResWindow.setScene(resOverviewScene);
        viewResWindow.initModality(Modality.APPLICATION_MODAL);
        viewResWindow.setTitle("Pregled Rezervacija");
        viewResWindow.show();
    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            reservationsTable.getSortOrder().clear();
            reservationsTable.getSortOrder().add(arrivalDateTC);

        });

        addResB.setOnAction(e -> {
            reservationsTable.getSortOrder().clear();
            reservationsTable.getSortOrder().add(reservationIDTC);

        });

        deleteResB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(reservationsTable))).start();
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
