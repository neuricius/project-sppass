/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Modality;
import model.Rezervacija;
import view.GuestsPerRoomSpecificView;
import static view.GuestsPerRoomSpecificView.*;
import util.HibernateUtil;
import static util.Konvertori.konvertujDatumZaApp;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static view.GuestsPerRoomSpecificView.resNoROL;
import static view.GuestsPerRoomSpecificView.roomNameROL;
import static view.GuestsPerRoomSpecificView.startDateROL;
import static view.GuestsPerRoomSpecificView.endDateROL;
import static view.GuestsPerRoomSpecificView.numberOfAdultsROL;
import static view.GuestsPerRoomSpecificView.numberOfChildrenROL;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestsPerRoomSpecificCTRL implements WindowDecorator, TakeOverCTRL {

    private static GuestsPerRoomSpecificView GPRSpecWindow;

    public GuestsPerRoomSpecificCTRL(Integer idRez) {
        launchGPR(idRez);
    }

    public void launchGPR(Integer idRez) {
        GPRSpecWindow = new GuestsPerRoomSpecificView(idRez);
        WindowDecorator.confirmationDecorator(GPRSpecWindow);
        addCTRL(idRez);
        GPRSpecWindow.setScene(GPRScene);
        GPRSpecWindow.initModality(Modality.APPLICATION_MODAL);
        GPRSpecWindow.setTitle("Pregled Gostiju");
        GPRSpecWindow.setResizable(false);
        GPRSpecWindow.show();

    }

    @Override
    public void addCTRL(Integer idRez) {
        
        Rezervacija rez = HibernateUtil.vratiRezervacijuPoId(idRez);

        resNoROL.setText(idRez.toString());

        roomNameROL.setText(rez.getIdSmJed().getNaziv());

        startDateROL.setText(konvertujDatumZaApp(rez.getPrijava()).format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        endDateROL.setText(konvertujDatumZaApp(rez.getOdjava()).format(DateTimeFormatter.ofPattern("dd-MMM-YY")));

        numberOfAdultsROL.setText(rez.getBrojOdraslih().toString());

        numberOfChildrenROL.setText(rez.getBrojDece().toString());

        printGPRB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(guestsPerRoomView))).start();
        });

        addGPRB.setOnAction(e -> {
            try {
                new GuestInfoOpenCTRL(idRez);
                GPRSpecWindow.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GuestsPerRoomSpecificCTRL.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @Override
    public void addCTRL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
