/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import javafx.stage.Modality;
import model.Rezervacija;
import view.PriceOverView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.PriceOverView.refreshB;
import static view.PriceOverView.addPriceB;
import static view.PriceOverView.printB;
import static view.PriceOverView.priceTable;
import static view.PriceOverView.roomPriceIDTC;
import static view.PriceOverView.priceOverviewScene;

/**
 *
 * @author Nebojsa Matic
 */
public class RoomPriceViewerCTRL implements WindowDecorator, TakeOverCTRL {

    public static PriceOverView priceViewer;

    public RoomPriceViewerCTRL() throws FileNotFoundException {
        priceViewer = new PriceOverView();
        WindowDecorator.noConfirmationDecorator(priceViewer);
        addCTRL();
        priceViewer.setScene(priceOverviewScene);
        priceViewer.initModality(Modality.WINDOW_MODAL);
        priceViewer.initOwner(returnMain());
        priceViewer.setTitle("Pregled Cena");
        priceViewer.show();
    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            priceTable.getSortOrder().clear();
            priceTable.getSortOrder().add(roomPriceIDTC);

        });

        addPriceB.setDisable(true);
        /*.setOnAction(e -> {
            new DodavanjeCeneSJKontroler();
        });*/

        printB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(priceTable))).start();
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
//
    }

    @Override
    public void addCTRL(Rezervacija rez) {
//
    }

}
