/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import javafx.stage.Modality;
import model.Rezervacija;
import view.ArrivalsPerDateView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.ArrivalsPerDateView.refreshB;
import static view.ArrivalsPerDateView.addArrivalB;
import static view.ArrivalsPerDateView.deleteArrivalB;
import static view.ArrivalsPerDateView.arrivalTable;
import static view.ArrivalsPerDateView.arrivalViewScene;

/**
 *
 * @author Nebojsa Matic
 */
public class ArrivalsOnDayCTRL implements WindowDecorator, TakeOverCTRL {

    private static ArrivalsPerDateView arrivalsViewer;

    public ArrivalsOnDayCTRL() throws FileNotFoundException {
        launchArrivalsViewer();
    }

    public void launchArrivalsViewer() throws FileNotFoundException {
        arrivalsViewer = new ArrivalsPerDateView();
        addCTRL();
        WindowDecorator.noConfirmationDecorator(arrivalsViewer);
        arrivalsViewer.setScene(arrivalViewScene);
        arrivalsViewer.setTitle("Dolasci za dan");
        arrivalsViewer.initModality(Modality.WINDOW_MODAL);
        arrivalsViewer.initOwner(returnMain());
        arrivalsViewer.show();

    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(arrivalTable))).start();
        });
        addArrivalB.setDisable(true);
        deleteArrivalB.setDisable(true);
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
