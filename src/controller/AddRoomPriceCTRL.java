/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import javafx.stage.StageStyle;
import view.AddRoomPriceView;
import components.WindowDecorator;
import static controller.RoomPriceViewerCTRL.priceViewer;
import static view.AddRoomPriceView.priceAddScene;

/**
 *
 * @author Nebojsa Matic
 */
public class AddRoomPriceCTRL implements WindowDecorator {

    public static AddRoomPriceView editorial;

    public AddRoomPriceCTRL() {
        editorial = new AddRoomPriceView();
        WindowDecorator.noConfirmationDecorator(editorial);
        editorial.setScene(priceAddScene);
        editorial.initStyle(StageStyle.UTILITY);
        editorial.initModality(Modality.WINDOW_MODAL);
        editorial.initOwner(priceViewer);
        editorial.setResizable(false);
        editorial.setTitle("Uredjivanje cena smestajnih jedinica - SPASS v0.5 by Neuricius");
        editorial.show();
    }

}
