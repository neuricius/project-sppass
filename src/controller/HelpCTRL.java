/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import javafx.stage.StageStyle;
import view.HelpView;
import components.WindowDecorator;
import static view.HelpView.testScene;

/**
 *
 * @author Nebojsa Matic
 */
public class HelpCTRL implements WindowDecorator {

    private static HelpView helpWindow;

    public HelpCTRL() {
        helpWindow = new HelpView();
        WindowDecorator.noConfirmationDecorator(helpWindow);
        helpWindow.setScene(testScene);
        helpWindow.initStyle(StageStyle.UTILITY);
        helpWindow.setResizable(false);
        helpWindow.initModality(Modality.APPLICATION_MODAL);
        helpWindow.setTitle("Pomoc - SPPASS v0.5 by Neuricius");
        helpWindow.show();
    }

}
