/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import model.Dokumenta;
import model.Gost;
import model.Gostiposobama;
import model.Rezervacija;
import model.Tipgosta;
import view.MainView;
import view.GuestInfoView;
import static view.GuestInfoView.*;
import view.AlertView;
import view.ConfirmationDialog;
import util.HibernateUtil;
import static util.HibernateUtil.*;
import util.Validatori;
import components.WindowDecorator;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestInfoOpenCTRL implements WindowDecorator, TakeOverCTRL {

    private static GuestInfoView guestInfoWindow;

    public GuestInfoOpenCTRL(Integer idRez) throws FileNotFoundException {
        guestInfoWindow = new GuestInfoView();
        WindowDecorator.noConfirmationDecorator(guestInfoWindow);
        addCTRL(idRez);
        guestInfoWindow.initModality(Modality.APPLICATION_MODAL);
//        gostivoje.initOwner(nadjiGlavonju());
        guestInfoWindow.setResizable(false);
        guestInfoWindow.setScene(guestInfoScene);
        guestInfoWindow.setTitle("Dodavanje gosta");
        guestInfoWindow.show();
        System.out.println("Width: " + guestInfoWindow.getWidth());
        System.out.println("Height: " + guestInfoWindow.getHeight());
    }

    @Override
    public void addCTRL(Integer idRez) {
        mapirajVrsteGostiju();
        for (int i = 1; i <= HibernateUtil.vrstaGosta.size(); i++) {
            guestTypeCB.getItems().add(vrstaGosta.get(i));
        }
        guestTypeCB.setValue(vrstaGosta.get(0));
        guestTypeCB.setEditable(false);

        mapirajVrsteDokumenata();
        for (int i = 1; i <= HibernateUtil.vrstaDokumenta.size(); i++) {
            docTypeCB.getItems().add(vrstaDokumenta.get(i));
        }
        docTypeCB.setValue(vrstaDokumenta.get(0));

        guestPhoneNoTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    if (!Validatori.validacijaTelefon(guestPhoneNoTF.getText())) {
                        ke.consume();
                        AlertView.show("Nije validan format/ntelefon!", "Greska");
                        guestPhoneNoTF.requestFocus();
                        break;
                    }
                default:
                    break;
            }
        });

        guestEmailTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    if (!Validatori.validacijaEmail(guestEmailTF.getText())) {
                        ke.consume();
                        AlertView.show("Nije validan format/nmaila!", "Greska");
                        guestEmailTF.requestFocus();
                        break;
                    }
                default:
                    break;
            }
        });

        //ubaciti id iz konstruktora
        resNoROL.setText(idRez.toString());

        entryAuthorROL.setText(userProfileName());

        entryDateROL.setText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MMM/yyyy")));

        cancelAddingB.setOnAction(e -> {
            if (ConfirmationDialog.confirmAction("Da li ste sigurni?", "Otkazivanje promena", "Da", "Ne")) {
                guestInfoWindow.close();
            }
        });

        saveGuestB.setOnAction(e -> {
            addGPR(idRez);
            guestInfoWindow.close();
            new GuestsPerRoomSpecificCTRL(idRez);
        });

        nextGuestB.setOnAction(e -> {
            addGPR(idRez);
            docTypeCB.setPromptText("Izaberite dokument");
            guestFirstNameTF.clear();
            guestLastNameTF.clear();
            guestResidenceCityTF.clear();
            guestResidenceCountryTF.clear();
            guestPhoneNoTF.clear();
            guestEmailTF.clear();
            guestDocumentTF.clear();
            guestNotesTF.clear();
            guestFirstNameTF.requestFocus();
        });
    }

    public void addGPR(Integer idRez) {
        Gost noviGost = new Gost();
        noviGost.setTipGosta(new Tipgosta(vradiIdTipaGosta(guestTypeCB.getValue())));
        noviGost.setImeGosta(guestFirstNameTF.getText());
        noviGost.setPrezimeGosta(guestLastNameTF.getText());
        noviGost.setMestoGosta(guestResidenceCityTF.getText());
        noviGost.setDrzavaGosta(guestResidenceCountryTF.getText());
        noviGost.setTelefonGosta(guestPhoneNoTF.getText());
        noviGost.setEmailGosta(guestEmailTF.getText());
        if (docTypeCB.getValue() != null) {
            noviGost.setSifraDokumentaGosta(new Dokumenta(vratiIdDokumentaIzMape(docTypeCB.getValue())));
            noviGost.setBrDokumentaGosta(guestDocumentTF.getText());
        }

        ubaciGosta(noviGost);

        Gostiposobama gps = new Gostiposobama();
        gps.setIdjRezervacije(new Rezervacija(idRez));
        gps.setIdGosta(new Gost(noviGost.getIdGost()));

        ubaciGPS(gps);

        MainView.systemMsgL.setText("Gost uspesno dodat");
    }

    @Override
    public void addCTRL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
