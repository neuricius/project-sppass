/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Polygon;
import javafx.util.Duration;
import javafx.util.StringConverter;
import view.MainView;
import static view.MainView.*;
import view.AlertView;
import view.ExitConfirmation;
import util.AutoAzuriranjeBaze2;
import util.HibernateUtil;
import static util.HibernateUtil.*;
import components.WindowDecorator;
import static controller.MainCTRL.mainWindow;
import static controller.LoginCTRL.launchLogin;
import static view.MainView.mainScene;

/**
 *
 * @author Nebojsa Matic
 */
public class MainCTRL implements WindowDecorator {

    public static MainView mainWindow;
    public static NewReservationCTRL newResWindow;
    private static Node targetNode;
    private static int dateColumnIndex;
    private static final int MOVE_LEFT = 13;
    private static int NodeCoordRow;
    private static int NodeCoordColumn;
    public static Timeline timeline;

    public MainCTRL() throws FileNotFoundException {
        launchMain();
        addButtonCTRL();
        autoUpdateTimer().play();
        
        // nakon promene iscrtavanja grida, ovaj deo NE RADI. Revizija
        SliderStartPosition(sp1, findNodeOnGrid(timeGridGP, findIndexForDate(LocalDate.now().plusDays(MOVE_LEFT)), 0));
        iscrtajRezervacije(facilityName());

        //labele
        numberOfAvailableCountL.setText(String.valueOf(
                HibernateUtil.roomList.size()
                - listaAktivnihRezervacija(facilityName()).size()));
        numberOfGuestsCountL.setText("" + vratiTrenutniBrojGostiju(facilityName()));
        numberOfArrivalsCountL.setText(String.valueOf(dolasciNaDan(facilityName(), LocalDate.now()).size()));
        numberOfDeparturesCountL.setText("" + odlasciNaDan(facilityName(), LocalDate.now()).size());
        numberOfOccupiedCountL.setText("" + listaAktivnihRezervacija(facilityName()).size());

        //
        selectDayDP.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });

        selectDayDP.setOnAction(e -> {
            //vrati na pocetak
            SliderStartPosition(sp1, findNodeOnGrid(timeGridGP, 0, 0));
            SliderStartPosition(sp1, findNodeOnGrid(timeGridGP, findIndexForDate(selectDayDP.getValue().plusDays(MOVE_LEFT)), 0));

        });
        AutoAzuriranjeBaze2.pokreniAAB();

    }

    public static void launchMain() throws FileNotFoundException {
        mainWindow = returnMain();
        WindowDecorator.mainDecorator(mainWindow);
        
        repaintWeekends(gridDateList);
        repaintPresentDay();
        mainWindow.setScene(mainScene);
        mainWindow.setMinWidth(690);
        mainWindow.setMinHeight(530);
        mainWindow.adminMenuSettings.setDisable(userLevelCheck());
        mainWindow.deleteReservationMI.setDisable(userLevelCheck());
        mainWindow.setTitle(companyName() + " - SPPASS 0.5 by Neuricius");
        mainWindow.show();
    }

    public static void addButtonCTRL() {
        //meni
        changeUserMI.setOnAction(e -> {
            LoginCTRL.launchReLogin();
        });
        logoutUserMI.setOnAction(e -> {
            try {
                returnMain().close();
                launchLogin();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        exitProgramMI.setOnAction((ActionEvent t) -> {
            ExitConfirmation.programExit();
        });
        newReservationMI.setOnAction(e -> {
            try {
                new NewReservationCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        showAllReservationsMI.setOnAction(e -> {
            new ViewReservationCTRL();
        });

        updateReservationsMI.setOnAction(e -> {
            azurirajRezervacijeUBazi(facilityName());
            updateResScreen();
        });

        mainWindow.deleteReservationMI.setOnAction(e -> {
            AlertView.show("Ova opcija je u izradi", "Izvinite");
        });

        newBillMI.setOnAction(e -> {
            try {
                new NewBillCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainCTRL.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        billViewerMI.setOnAction(e -> {
            try {
                new BillViewerCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        guestsPerRoomMI.setOnAction(e -> {
            new GuestsPerRoomDefaultCTRL();
        });
        arrivalsPerDayMI.setOnAction(e -> {
            try {
                new ArrivalsOnDayCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainCTRL.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        departuresPerDayMI.setOnAction(e -> {
            new DeparturesPerDayCTRL();
        });
        housekeepingReportMI.setOnAction(e -> {
            new HKReportCTRL();
        });
        editRoomsMI.setOnAction(e -> {
            try {
                new RoomEditorCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainCTRL.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        editUsersMI.setOnAction(e -> {
            new UserEditCTRL();
        });
        editPricingMI.setOnAction(e -> {
            try {
                new RoomPriceViewerCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        helpHelpMI.setOnAction(e -> {
            new HelpCTRL();
        });
        helpAboutMI.setOnAction(e -> {
            new AboutCTRL();
        });
        helpContactMI.setOnAction(e -> {
            new ContactCTRL();
        });

        //main
        mainSearchB.setOnAction(e -> {
            new SearchCTRL();
        });
        mainNewResB.setOnAction((ActionEvent e) -> {
            try {
                new NewReservationCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        mainReportB.setOnAction(e -> {
            new HKReportCTRL();
        });

        //test kod za prefarbavanje poligona
        refreshB.setOnAction(e -> {
            azurirajRezervacijeUBazi(facilityName());
            updateResScreen();
        });
    }

    public static void repaintFieldForDay(GridPane gridx, LocalDate chosenDay, String color) {
        int x = countRows(gridx);
        for (int i = 0; i < x; i++) {
            findNodeOnGrid(gridx, findIndexForDate(chosenDay), i);
            targetNode.setStyle("-fx-background-color: " + color + ";");
        }
    }

    public static int findIndexForDate(LocalDate chosenDay) {
        dateColumnIndex = 0;

        for (int i = 0; i < gridDateList.size(); i++) {
            if (gridDateList.get(i).equals(chosenDay)) {
                dateColumnIndex = gridDateList.indexOf(chosenDay);
                break;
            }
        }
        return dateColumnIndex;
    }

    public static int findIndexForRoom(String room) {
        dateColumnIndex = 0;
        for (int i = 0; i < roomList.size(); i++) {
            if (room.equals(vratiNazivSobe(i))) {
                dateColumnIndex = i;
            }
        }

        return dateColumnIndex;
    }

    public static Node findNodeOnGrid(GridPane gridx, Integer col, Integer row) {
        targetNode = null;
        for (Node node : gridx.getChildren()) {
            if (GridPane.getRowIndex(node).equals(row)
                    && GridPane.getColumnIndex(node).equals(col)) {
                targetNode = node;
                break;
            }

        }
        return targetNode;
    }

    private static void SliderStartPosition(ScrollPane pane, Node node) {
        Bounds viewport = pane.getViewportBounds();
        double contentHeight = pane.getContent().getBoundsInLocal().getHeight();
        double contentWidth = pane.getContent().getBoundsInLocal().getWidth();
        double nodeMinY = node.getBoundsInParent().getMinY();
        double nodeMaxY = node.getBoundsInParent().getMaxY();
        double nodeMinX = node.getBoundsInParent().getMinX();
        double nodeMaxX = node.getBoundsInParent().getMaxX();
        double viewportMinY = (contentHeight - viewport.getHeight()) * pane.getVvalue();
        double viewportMaxY = viewportMinY + viewport.getHeight();
        double viewportMinX = (contentWidth - viewport.getWidth()) * pane.getHvalue();
        double viewportMaxX = viewportMinX + viewport.getWidth();
        if (nodeMinY < viewportMinY) {
            pane.setVvalue(nodeMinY / (contentHeight - viewport.getHeight()));
        } else if (nodeMaxY > viewportMaxY) {
            pane.setVvalue((nodeMaxY - viewport.getHeight()) / (contentHeight - viewport.getHeight()));
        }
        if (nodeMinX < viewportMinX) {
            pane.setHvalue(nodeMinX / (contentWidth - viewport.getWidth()));
        } else if (nodeMaxX > viewportMaxX) {
            pane.setHvalue((nodeMaxX - viewport.getWidth()) / (contentWidth - viewport.getWidth()));
        }

    }

    public static void repaintPresentDay() {
        repaintFieldForDay(tableGridGP, LocalDate.now(), "yellow");
        repaintFieldForDay(timeGridGP, LocalDate.now(), "yellow");
    }

    public static void repaintWeekends(List<LocalDate> list) {
        for (int i = 0; i < retrieveWeekendIndexes(list).size(); i++) {
            repaintFieldForDay(tableGridGP, gridDateList.get(retrieveWeekendIndexes(list).get(i)), "pink");
            repaintFieldForDay(timeGridGP, gridDateList.get(retrieveWeekendIndexes(list).get(i)), "pink");
        }
    }

    private static int countRows(GridPane pane) {
        int numRows = pane.getRowConstraints().size();
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                Integer rowIndex = GridPane.getRowIndex(child);
                if (rowIndex != null) {
                    numRows = Math.max(numRows, rowIndex + 1);
                }
            }
        }
        return numRows;
    }

    private static int countColumns(GridPane pane) {
        int numCols = pane.getColumnConstraints().size();
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                Integer colIndex = GridPane.getColumnIndex(child);
                if (colIndex != null) {
                    numCols = Math.max(numCols, colIndex + 1);
                }
            }
        }
        return numCols;
    }

    private static ArrayList<Integer> retrieveWeekendIndexes(List<LocalDate> list) {
        ArrayList<Integer> indexList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                indexList.add(list.indexOf(list.get(i)));
            }
        }
        return indexList;
    }

    public static void findNodeCoordinates(Node source) throws FileNotFoundException {
        NodeCoordColumn = GridPane.getColumnIndex(source);
        NodeCoordRow = GridPane.getRowIndex(source);

        new NewReservationCTRL(gridDateList.get(NodeCoordColumn), roomList.get(NodeCoordRow).getNaziv());

        System.out.printf("Klik misem na polje [%d, %d]%n", NodeCoordColumn, NodeCoordRow);
        System.out.print("Sm.Jedinica: " + roomList.get(NodeCoordRow).getNaziv());
        System.out.print(" || ");
        System.out.println("Datum: " + gridDateList.get(NodeCoordColumn).toString());
    }

    public static void updateResScreen() {
        for (int i = 0; i < tableGridGP.getChildren().size(); i++) {
            if (tableGridGP.getChildren().get(i).getClass().equals(Polygon.class) || tableGridGP.getChildren().get(i).getClass().equals(Label.class)) {
                tableGridGP.getChildren().remove(i);
                i--;
            }
        }
        iscrtajRezervacije(facilityName());
        systemMsgL.setText("Rezervacije uspesno azurirane u " + LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm a")));
        System.out.println("Pokusaj azuriranja rez. na ekranu u " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("hh:mm a")));
    }

    public static Timeline autoUpdateTimer() {
        timeline = returnTimeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.minutes(1), e -> updateResScreen()));
        timeline.setCycleCount(Animation.INDEFINITE);
        return timeline;
    }

    public static MainView returnMain() throws FileNotFoundException {
        if (mainWindow == null) {
            mainWindow = new MainView(LocalDate.now());
        }
        return mainWindow;
    }
    
    public static MainView recreateMain(LocalDate newDate) throws FileNotFoundException {
        if (mainWindow != null) {
            mainWindow.close();
        }
        return new MainView(newDate);
    }

    //Singlton za timeline
    public static Timeline returnTimeline() {

        if (timeline == null) {
            timeline = new Timeline();
        }
        return timeline;
    }
}
