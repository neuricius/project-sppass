/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.ConfirmationDialog;
import view.MainView;
import view.NewReservationView;
import view.AlertView;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.util.StringConverter;
import components.ReservationGroup;
import model.*;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;
import static view.NewReservationView.*;
import util.HibernateUtil;
import static util.HibernateUtil.*;
import static util.Konvertori.konvertujDatumZaBazu;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import util.Validatori;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;

/**
 *
 * @author Nebojsa Matic
 */
public class NewReservationCTRL implements WindowDecorator, TakeOverCTRL {

    private static AutoCompletionBinding<String> autoCompletionBinding;
    private static String[] suggestionsReservation = {"neo", "daki", "Nebojsa", "Matic", "Dajana", "Scasni"};
    private static Set<String> suggResSet = new HashSet<>(Arrays.asList(suggestionsReservation));

    public static boolean isEverythingFinished = false;

    long numberOfNights;
    Double priceForPeriod, totalCost, otherExpenses, subTotal,
            totalAmount, currentBalance, remainingBalance;

    private static NewReservationView newResWindow;

    public NewReservationCTRL() throws FileNotFoundException {
        newReservation();
    }

    public NewReservationCTRL(LocalDate date, String room) throws FileNotFoundException {
        newReservation();
        reservStartDP.setValue(date);
        roomListCB.setValue(room);
    }

    private void newReservation() throws FileNotFoundException {
        newResWindow = new NewReservationView();
        WindowDecorator.noConfirmationDecorator(newResWindow);
        newResWindow.initModality(Modality.WINDOW_MODAL);
        newResWindow.initOwner(returnMain());
        newResWindow.setResizable(false);
        addCTRL();
        refreshFields();
        newResWindow.setScene(reservationScene);
        newResWindow.setTitle("Nova Rezervacija");
        newResWindow.show();
    }

    private void refreshFields() {
        numberOfNights = reservStartDP.getValue().until(reservEndDP.getValue(), ChronoUnit.DAYS);
        priceForPeriod = HibernateUtil.vratiCenuNocenja(HibernateUtil.facilityName(), reservStartDP.getValue(), vratiIdSobeIzMape(roomListCB.getValue()));
        totalCost = priceForPeriod * numberOfNights;
        otherExpenses = Double.valueOf(otherExpensesTF.getText());
        subTotal = totalCost + otherExpenses;
        totalAmount = subTotal * (1 - vratiIznosPopustaIzMape(discountCB.getValue()));
        currentBalance = Double.valueOf(currentBalanceTF.getText());
        remainingBalance = totalAmount - currentBalance;

        numberOfNightsTF.setText(String.valueOf(numberOfNights));
        pricePerPeriodTF.setText(String.valueOf(priceForPeriod));
        totalPriceTF.setText(String.valueOf(totalCost));
        subTotalTF.setText(String.valueOf(subTotal));
        totalBalanceTF.setText(String.valueOf(totalAmount));
        remainBalanceTF.setText(String.valueOf(remainingBalance));
    }

    private void autoCompletionLearnWord(TextField tf, String newWord) {
        suggResSet.add(newWord);

        // we dispose the old binding and recreate a new binding
        if (autoCompletionBinding != null) {
            autoCompletionBinding.dispose();
        }
        autoCompletionBinding = TextFields.bindAutoCompletion(tf, suggResSet);
    }

    private List<Double> sracunajNocenja(LocalDate start, LocalDate kraj, String nazivSobe) {
        List<Double> listaCenaNocenja = new ArrayList<>();
        Integer idSobe = vratiIdSobeIzMape(nazivSobe);
        LocalDate tempDate = start;
        while (tempDate.isBefore(kraj)) {
            Double temp = HibernateUtil.vratiCenuNocenja(HibernateUtil.facilityName(), tempDate, idSobe);
            listaCenaNocenja.add(temp);
            tempDate.plusDays(1L);
        }
        return listaCenaNocenja;
    }

    @Override
    public void addCTRL() {
        //povezivanje polja sa funkciojom za auto-ucenje
        autoCompletionBinding = TextFields.bindAutoCompletion(clientFirstNameTF, suggResSet);
        clientFirstNameTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    autoCompletionLearnWord(clientFirstNameTF, clientFirstNameTF.getText().trim());
                    break;
                default:
                    break;
            }
        });

        autoCompletionBinding = TextFields.bindAutoCompletion(clientLastNameTF, suggResSet);
        clientLastNameTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    autoCompletionLearnWord(clientLastNameTF, clientLastNameTF.getText().trim());
                    break;
                default:
                    break;
            }
        });

        autoCompletionBinding = TextFields.bindAutoCompletion(clientResidenceCityTF, suggResSet);
        clientResidenceCityTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    autoCompletionLearnWord(clientResidenceCityTF, clientResidenceCityTF.getText().trim());
                    break;
                default:
                    break;
            }
        });

        autoCompletionBinding = TextFields.bindAutoCompletion(clientResidenceCountryTF, suggResSet);
        clientResidenceCountryTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    autoCompletionLearnWord(clientResidenceCountryTF, clientResidenceCountryTF.getText().trim());
                    break;
                default:
                    break;
            }
        });

        autoCompletionBinding = TextFields.bindAutoCompletion(clientEmailTF, suggResSet);
        clientEmailTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    if (!Validatori.validacijaEmail(clientEmailTF.getText())) {
                        ke.consume();
                        AlertView.show("Nije validan format e-maila", "Greska");
                        clientEmailTF.requestFocus();
                        break;
                    } else {
                        autoCompletionLearnWord(clientEmailTF, clientEmailTF.getText().trim());
                        break;
                    }
                default:
                    break;
            }
        });

        //bindovanje datepicekera da uvek pokazuju 7 dana razlike
        reservStartDP.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });

        reservEndDP.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });

        reservStartDP.setOnAction(e -> {
            reservEndDP.setValue(reservStartDP.getValue().plusDays(7));
            refreshFields();
        });

//        clientIsGuest.setSelected(true);
//        clientIsGuest.setDisable(true);
        //blok za dodavanje vrednosti combo-boksevima
        mapirajNaziveSoba();
        for (int i = 1; i <= HibernateUtil.naziviSoba.size(); i++) {
            roomListCB.getItems().add(naziviSoba.get(i));
        }
        roomListCB.setValue(naziviSoba.get(1));

//        clientTypeCB
        mapirajVrsteGostiju();
        for (int i = 1; i <= HibernateUtil.vrstaGosta.size(); i++) {
            clientTypeCB.getItems().add(vrstaGosta.get(i));
        }
        clientTypeCB.setValue(vrstaGosta.get(0));

        mapirajVrsteDokumenata();
        for (int i = 1; i <= HibernateUtil.vrstaDokumenta.size(); i++) {
            documentTypeCB.getItems().add(vrstaDokumenta.get(i));
        }

        mapirajStatuseRezervacija();
        for (int i = 1; i <= HibernateUtil.statusRezervacije.size(); i++) {
            reservationStatusCB.getItems().add(statusRezervacije.get(i));
        }
        reservationStatusCB.setValue(statusRezervacije.get(1));

        mapirajPopuste();
        for (int i = 1; i <= HibernateUtil.mapaPopusta.size(); i++) {
            discountCB.getItems().add(mapaPopusta.get(i));
        }
        discountCB.setValue(mapaPopusta.get(1));

        otherExpensesTF.setText("0");

        currentBalanceTF.setText("0");

        int brojLezajeva = mapirajBrojLezajeva().get(vratiIdSobeIzMape(roomListCB.getValue()));
        for (int i = 0; i <= brojLezajeva; i++) {
            numberOfAdultsCB.getItems().add(i);
            numberOfChildrenCB.getItems().add(i);
        }
        numberOfAdultsCB.setValue(0);
        numberOfChildrenCB.setValue(0);

        roomListCB.setOnAction(e -> {
            numberOfAdultsCB.getItems().clear();
            numberOfChildrenCB.getItems().clear();

            int noviBrojLezajeva = mapirajBrojLezajeva().get(vratiIdSobeIzMape(roomListCB.getValue()));
            for (int i = 0; i <= noviBrojLezajeva; i++) {
                numberOfAdultsCB.getItems().add(i);
                numberOfChildrenCB.getItems().add(i);
            }
            numberOfAdultsCB.setValue(0);
            numberOfChildrenCB.setValue(0);
            refreshFields();
        });

        //ubaciti validatore za pravilan unos broja 
        //
        //polja za cifre
        reservEndDP.setOnAction(e -> {
            refreshFields();
        });

        otherExpensesTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    if (!Validatori.validacijaDouble(otherExpensesTF.getText())) {
                        ke.consume();
                        AlertView.show("Nije validan format", "Greska");
                        otherExpensesTF.requestFocus();
                        break;
                    } else {
                        refreshFields();
                        break;
                    }
                default:
                    break;
            }
        });

        discountCB.setOnAction(e -> {
            refreshFields();
        });

        currentBalanceTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    if (!Validatori.validacijaDouble(currentBalanceTF.getText())) {
                        ke.consume();
                        AlertView.show("Nije validan format", "Greska");
                        currentBalanceTF.requestFocus();
                        break;
                    } else {
                        refreshFields();
                        break;
                    }
                default:
                    break;
            }
        });

        clientDocumentTF.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                default:
                    if (documentTypeCB.getSelectionModel().isEmpty() && !clientDocumentTF.getText().isEmpty()) {
                        ke.consume();
                        AlertView.show("Izaberite vrstu dokumenta", "Greska");
                        clientDocumentTF.requestFocus();
                        break;
                    }
            }
        });

        //labele za status bar
        entryAuthorNameL.setText(HibernateUtil.userProfileName());
        entryDateVaueL.setText(LocalDate.now().toString());

        //
        //dugmad
        saevChangesB.setOnAction((ActionEvent e) -> {
            if ((numberOfAdultsCB.getValue() + numberOfChildrenCB.getValue()) > (mapirajBrojLezajeva().get(vratiIdSobeIzMape(roomListCB.getValue())))) {
                AlertView.show("Nema dovoljno lezajeva", "Greska");
                numberOfAdultsCB.requestFocus();
                e.consume();
            }
            if (reservStartDP.getValue().isBefore(LocalDate.now().minusDays(1L)) || reservEndDP.getValue().isBefore(LocalDate.now().minusDays(1L))) {
                AlertView.show("Pocetak ili kraj rezervacije\nje u proslosti", "Greska");
                reservStartDP.requestFocus();
                e.consume();
            }
            if (!proveriPreklapanjaRez(facilityName(), roomListCB.getValue(), reservStartDP.getValue(), reservEndDP.getValue())) {
                AlertView.show("Preklapanje sa drugom rez!\nProverite datume prijave i odjave.", "Greska");
                reservStartDP.requestFocus();
                e.consume();
            }
            //klijent
            Klijent klijentela = new Klijent();
            klijentela.setImeKlijenta(clientFirstNameTF.getText());
            klijentela.setPrezimeKlijenta(clientLastNameTF.getText());
            klijentela.setMestoKlijenta(clientResidenceCityTF.getText());
            klijentela.setDrzavaKlijenta(clientResidenceCountryTF.getText());
            klijentela.setEmailKlijenta(clientEmailTF.getText());
            if (clientIsGuest.isSelected()) {
                klijentela.setKlijentJeGost((short) 1);
            } else {
                klijentela.setKlijentJeGost((short) 0);
            }
            if (!documentTypeCB.getSelectionModel().isEmpty()) {
                klijentela.setSifraDokumentaKlijenta(new Dokumenta(vratiIdDokumentaIzMape(documentTypeCB.getValue())));
                klijentela.setBrDokumentaKlijenta(clientDocumentTF.getText());
            }

            ubaciKlijenta(klijentela);
            //rez
            Rezervacija rez = new Rezervacija();

            rez.setIdSmJed(new Smjedinica(vratiIdSobeIzMape(roomListCB.getValue())));
            rez.setIdKlijent(new Klijent(klijentela.getIdKlijent()));
            rez.setPrijava(konvertujDatumZaBazu(reservStartDP.getValue()));
            rez.setOdjava(konvertujDatumZaBazu(reservEndDP.getValue()));
            rez.setBrojOdraslih(numberOfAdultsCB.getValue());
            rez.setBrojDece(numberOfChildrenCB.getValue());
            rez.setBeleskeORez(guestNotesTF.getText());
            rez.setAutorPoslednjeIzmene(new Profil(userProfile().getIdProfila()));
            rez.setIdStatRez(new Statusrezervacije(vratiIdStatusaRez(reservationStatusCB.getValue())));
            rez.setDatumPoslednjeIzmene(konvertujDatumZaBazu(LocalDate.now()));

            ubaciRezervaciju(rez);
            //naplata
            Naplata noviRacun = new Naplata();

            noviRacun.setAutorIzmene(new Profil(userProfile().getIdProfila()));
            noviRacun.setDatumIzmene(konvertujDatumZaBazu(LocalDate.now()));
            noviRacun.setIdRezervacija(new Rezervacija(rez.getIdRezervacija()));
            noviRacun.setCenaNocenja(new Cenasmjed(vratiIdCeneNocenja(facilityName(), vratiIdSobeIzMape(roomListCB.getValue()), Double.parseDouble(pricePerPeriodTF.getText()))));
            noviRacun.setCenaSmestaja(Double.parseDouble(totalPriceTF.getText()));
            noviRacun.setOstaliTroskovi(Double.parseDouble(otherExpensesTF.getText()));
            noviRacun.setMedjuZbir(Double.parseDouble(subTotalTF.getText()));
            noviRacun.setSifraPopusta(new Popust(vratiIdPopustaIzMape(discountCB.getValue())));
            noviRacun.setStatusNaplate(new Statusnaplate(1));
            noviRacun.setUplaceno(Double.parseDouble(currentBalanceTF.getText()));
            noviRacun.setPreostaliIznos(Double.parseDouble(remainBalanceTF.getText()));

            ubaciNaplatu(noviRacun);

            //kreirati goste po sobama(ako treba)
            if (clientIsGuest.isSelected()) {
                Gost noviGost = new Gost();
                noviGost.setImeGosta(klijentela.getImeKlijenta());
                noviGost.setPrezimeGosta(klijentela.getPrezimeKlijenta());
                noviGost.setMestoGosta(klijentela.getMestoKlijenta());
                noviGost.setTipGosta(new Tipgosta(vradiIdTipaGosta(clientTypeCB.getValue())));
                noviGost.setDrzavaGosta(klijentela.getDrzavaKlijenta());
                noviGost.setEmailGosta(klijentela.getEmailKlijenta());

                if (!documentTypeCB.getSelectionModel().isEmpty()) {
                    noviGost.setSifraDokumentaGosta(klijentela.getSifraDokumentaKlijenta());
                    noviGost.setBrDokumentaGosta(klijentela.getBrDokumentaKlijenta());
                }

                ubaciGosta(noviGost);

                Gostiposobama gps = new Gostiposobama();
                gps.setIdjRezervacije(new Rezervacija(rez.getIdRezervacija()));
                gps.setIdGosta(new Gost(noviGost.getIdGost()));

                ubaciGPS(gps);
            }

            ReservationGroup grupaZaRezervaciju = new ReservationGroup(vratiRezervacijuPoId(rez.getIdRezervacija()));
            if (isEverythingFinished) {
                newResWindow.close();
                MainView.systemMsgL.setText("Rezervacija uspesno dodata");
            }
        });

        discardChangesB.setOnAction(e -> {
            if (ConfirmationDialog.confirmAction("Promene nece biti sacuvane. Da li ste sigurni", "Otkazivanje promena", "Da", "Ne")) {
                newResWindow.close();
                MainView.systemMsgL.setText("Otkazano dodavanje rezervacije");
            }
        });

        printResB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(reservMainVBox))).start();
            MainView.systemMsgL.setText("Rezervacija poslata na stampu");
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
