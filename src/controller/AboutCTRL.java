/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import javafx.stage.StageStyle;
import view.AboutView;
import static view.AboutView.scenaOProgramu;
import components.WindowDecorator;

/**
 *
 * @author Nebojsa Matic
 */
public class AboutCTRL implements WindowDecorator{

    private static AboutView aboutWindow;

    public AboutCTRL() {
        aboutWindow = new AboutView();
        WindowDecorator.noConfirmationDecorator(aboutWindow);
        aboutWindow.setTitle("O Programu - SPPASS v0.5 by Neuricius");
        aboutWindow.setScene(scenaOProgramu);
        aboutWindow.setResizable(false);
        aboutWindow.initStyle(StageStyle.UTILITY);
        aboutWindow.initModality(Modality.APPLICATION_MODAL);
        aboutWindow.show();

    }

}
