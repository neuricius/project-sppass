/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.LocalDate;
import javafx.stage.Modality;
import view.SearchResView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import model.Rezervacija;
import static view.SearchResView.refreshB;
import static view.SearchResView.addB;
import static view.SearchResView.deleteB;
import static view.SearchResView.reservationTable;
import static view.SearchResView.resIDTC;
import static view.SearchResView.arrivalDateTC;
import static view.SearchResView.searchResScene;

/**
 *
 * @author Nebojsa Matic
 */
public class SearchReservationsCTRL implements WindowDecorator, TakeOverCTRL {

    private static SearchResView reservationSearchWindow;

    public SearchReservationsCTRL(Integer resNo, String clientFirstName, String clientLastName, String roomName, LocalDate startDate, LocalDate endDate) {
        reservationSearchWindow = new SearchResView(resNo, clientFirstName, clientLastName, roomName, startDate, endDate);
        WindowDecorator.noConfirmationDecorator(reservationSearchWindow);
        addCTRL();
        reservationSearchWindow.setScene(searchResScene);
        reservationSearchWindow.setTitle("Pretraga Rezervacija");
        reservationSearchWindow.setResizable(false);
        reservationSearchWindow.initModality(Modality.APPLICATION_MODAL);
        reservationSearchWindow.show();
    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            reservationTable.getSortOrder().clear();
            reservationTable.getSortOrder().add(arrivalDateTC);

        });

        addB.setOnAction(e -> {
            reservationTable.getSortOrder().clear();
            reservationTable.getSortOrder().add(resIDTC);

        });

        deleteB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(reservationTable))).start();
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
