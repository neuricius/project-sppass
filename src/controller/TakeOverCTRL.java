/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Rezervacija;

/**
 *
 * @author Nebojsa Matic
 */
public interface TakeOverCTRL {
    
    void addCTRL();
    
    void addCTRL(Integer idRez);
    
    void addCTRL(Rezervacija rez);
}
