/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import model.Objekti;
import model.Rezervacija;
import model.Smjedinica;
import model.Svojstva;
import model.Svojstvasmjed;
import model.Vrstasmjed;
import view.RoomEditorView;
import static view.RoomEditorView.*;
import view.MainView;
import view.AlertView;
import view.ConfirmationDialog;
import util.HibernateUtil;
import static util.HibernateUtil.*;
import util.Konvertori;
import components.WindowDecorator;
import static controller.MainCTRL.returnMain;
import static view.MainView.roomsNo;

/**
 *
 * @author Nebojsa Matic
 */
public class RoomEditorCTRL implements WindowDecorator, TakeOverCTRL {

    private static RoomEditorView smjeditor;
    public static Smjedinica smjed;

    public RoomEditorCTRL() throws FileNotFoundException {
        pokreniEditorSmJed();
    }

    private void pokreniEditorSmJed() throws FileNotFoundException {
        smjeditor = new RoomEditorView();
        WindowDecorator.confirmationDecorator(smjeditor);
        addCTRL();
        smjeditor.setScene(roomEditScene);
        smjeditor.initStyle(StageStyle.UTILITY);
        smjeditor.initModality(Modality.WINDOW_MODAL);
        smjeditor.initOwner(returnMain());
        smjeditor.setResizable(false);
        smjeditor.setTitle("Uredjivanje smestajnih jedinica - SPASS v0.5 by Neuricius");
        smjeditor.show();

    }

    
    // preraditi da posle edita ne mora relog. relaunxh main ;)
    @Override
    public void addCTRL() {
        entryDateValueL.setText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MMM-uu")));

        mapirajNaziveSoba();
        mapirajTipoveSoba();
        mapirajKompanije();
        mapirajObjekte(vratiIdKompanijeIzMape(HibernateUtil.companyName()));

        for (int i = 1; i <= naziviSoba.size(); i++) {
            roomNumberCB.getItems().add(naziviSoba.get(i));
        }

        for (int i = 1; i <= vrsteSoba.size(); i++) {
            roomTypeCB.getItems().add(vrsteSoba.get(i));
        }

        for (int i = 1; i <= mapaObjekata.size(); i++) {
            roomHHNameCB.getItems().add(mapaObjekata.get(i));
        }

        roomNumberCB.setOnAction(e -> {
            smjed = vratiInfoOSmJed(vratiIdSobeIzMape(roomNumberCB.getValue()));

            roomNameTF.setText(smjed.getNaziv());

            roomHHNameCB.setValue(smjed.getObjekat().getNazivObjekta());

            roomTypeCB.setValue(smjed.getVrstaSmJed().getVrstaSmJedinice());

            floorNumberTF.setText(String.valueOf(smjed.getSprat()));

            bedNumberMainTF.setText(String.valueOf(smjed.getBrGlavnihLezaja()));

            bedNumberAuxTF.setText(String.valueOf(smjed.getBrPomocnihLezaja()));

            numberOfRoomsTF.setText(String.valueOf(smjed.getBrojProstorija()));

            numberOfBedroomsTF.setText(String.valueOf(smjed.getBrojSpSoba()));

            numberOfBathroomsTF.setText(String.valueOf(smjed.getBrKupatila()));

            roomNotesTF.setText(smjed.getNapomene());

            entryDateValueL.setText(Konvertori.konvertujDatumZaApp(smjed.getDatumIzmene()).format(DateTimeFormatter.ofPattern("dd-MMM-uu")));

            for (Svojstvasmjed svojstva : vratiSvojstvaSmJed(vratiIdSobeIzMape(roomNumberCB.getValue()))) {
                switch (svojstva.getSvojstva().getIdSvojstva()) {
                    default:
                        break;
                    case (1):
                        hasWiFi.setSelected(true);
                        break;
                    case (2):
                        hasACU.setSelected(true);
                        break;
                    case (3):
                        hasTV.setSelected(true);
                        break;
                    case (4):
                        hasTerrace.setSelected(true);
                        break;
                }
            }

        });

        discardChangesB.setOnAction(e -> {
            if (ConfirmationDialog.confirmAction("Da li ste sigurni?", "Izlazak", "Da", "Ne")) {
                e.consume();
                smjeditor.close();
            }
        });

        addRoomB.setOnAction(e -> {
            roomNumberCB.setPromptText("Nova");
            roomNumberCB.setDisable(true);
            roomNameTF.clear();
            floorNumberTF.clear();
            bedNumberMainTF.clear();
            bedNumberAuxTF.clear();
            numberOfRoomsTF.clear();
            numberOfBedroomsTF.clear();
            numberOfBathroomsTF.clear();
            roomNotesTF.clear();
            hasWiFi.setSelected(false);
            hasACU.setSelected(false);
            hasTV.setSelected(false);
            hasTerrace.setSelected(false);
            smjed = new Smjedinica();
        });

        deleteRoomB.setOnAction(e -> {
            if (smjed == null) {
                e.consume();
                smjeditor.close();
                AlertView.show("Nema naloga za brisanje", "Greska");
                try {
                    pokreniEditorSmJed();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(RoomEditorCTRL.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (ConfirmationDialog.confirmAction("Da li ste sigurni", "Brisanje korisnika", "Da", "Ne")) {
//kod za brisanje
                smjeditor.close();
                obrisiSmJed(smjed.getIdSmJedinice());
                MainCTRL.mainWindow.close();
                MainCTRL.mainWindow = null;
                AlertView.show("Sm. jedinica uspesno obrisana\nUlogujte se ponovo.", "Uspesno dodavanje");
                new LoginCTRL();
                roomsNo--;
                MainView.systemMsgL.setText("Smestajna jedinica uspesno obrisana");
                mapirajNaziveSoba();
                mapirajTipoveSoba();
                mapirajKompanije();
                mapirajObjekte(vratiIdKompanijeIzMape(HibernateUtil.companyName()));
            }
        });

        saveChangesB.setOnAction(e -> {
            smjed = new Smjedinica();
            if (!roomNumberCB.isDisabled()) {
                smjed.setIdSmJedinice(vratiIdSobeIzMape(roomNumberCB.getValue()));
            }

            smjed.setNaziv(roomNameTF.getText());

            smjed.setObjekat(new Objekti(vratiIdObjektaIzMape(roomHHNameCB.getValue())));

            smjed.setVrstaSmJed(new Vrstasmjed(vratiVrstuSobeIzMape(roomTypeCB.getValue())));

            smjed.setRedniBrojSJ(roomList.size() + 1);

            smjed.setSprat(Integer.parseInt(numberOfBedroomsTF.getText()));

            smjed.setBrGlavnihLezaja(Integer.parseInt(bedNumberMainTF.getText()));

            smjed.setBrPomocnihLezaja(Integer.parseInt(bedNumberMainTF.getText()));

            smjed.setBrojProstorija(Integer.parseInt(numberOfRoomsTF.getText()));

            smjed.setBrojSpSoba(Integer.parseInt(numberOfBedroomsTF.getText()));

            smjed.setBrKupatila(Integer.parseInt(numberOfBathroomsTF.getText()));

            smjed.setNapomene(roomNotesTF.getText());

            smjed.setDatumIzmene(Konvertori.konvertujDatumZaBazu(LocalDate.now()));

            dodajIzmeni(smjed);

            if (hasWiFi.isSelected()) {
                Svojstvasmjed ssj = new Svojstvasmjed();
                ssj.setIdSmJedinice(new Smjedinica(smjed.getIdSmJedinice()));
                ssj.setSvojstva(new Svojstva(1));
                dodajIzmeni(ssj);
            }
            if (hasACU.isSelected()) {
                Svojstvasmjed ssj = new Svojstvasmjed();
                ssj.setIdSmJedinice(new Smjedinica(smjed.getIdSmJedinice()));
                ssj.setSvojstva(new Svojstva(2));
                dodajIzmeni(ssj);
            }
            if (hasTV.isSelected()) {
                Svojstvasmjed ssj = new Svojstvasmjed();
                ssj.setIdSmJedinice(new Smjedinica(smjed.getIdSmJedinice()));
                ssj.setSvojstva(new Svojstva(3));
                dodajIzmeni(ssj);
            }
            if (hasTerrace.isSelected()) {
                Svojstvasmjed ssj = new Svojstvasmjed();
                ssj.setIdSmJedinice(new Smjedinica(smjed.getIdSmJedinice()));
                ssj.setSvojstva(new Svojstva(4));
                dodajIzmeni(ssj);
            }
            MainCTRL.mainWindow.close();
            MainCTRL.mainWindow = null;
            AlertView.show("Sm. jedinica uspesno dodata\nUlogujte se ponovo.", "Uspesno dodavanje");
            new LoginCTRL();
            roomsNo++;
            MainView.systemMsgL.setText("Sm. jedinica uspesno dodata ");
        });
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
