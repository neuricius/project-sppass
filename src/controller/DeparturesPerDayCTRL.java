/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import model.Rezervacija;
import view.DeparturesOnDayView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static view.ArrivalsPerDateView.addArrivalB;
import static view.DeparturesOnDayView.refreshB;
import static view.DeparturesOnDayView.deleteB;
import static view.DeparturesOnDayView.departuresTable;
import static view.DeparturesOnDayView.departuresScene;

/**
 *
 * @author Nebojsa Matic
 */
public class DeparturesPerDayCTRL implements WindowDecorator, TakeOverCTRL {

    private static DeparturesOnDayView departuresViewer;

    public DeparturesPerDayCTRL() {
        departuresViewer = new DeparturesOnDayView();
        WindowDecorator.noConfirmationDecorator(departuresViewer);
        addCTRL();
        departuresViewer.setScene(departuresScene);
        departuresViewer.initModality(Modality.APPLICATION_MODAL);
        departuresViewer.setTitle("Odlasci za dan");
        departuresViewer.show();
    }

    @Override
    public void addCTRL() {
        refreshB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(departuresTable))).start();
        });
        addArrivalB.setDisable(true);
        deleteB.setDisable(true);
    }

    @Override
    public void addCTRL(Integer idRez) {
//
    }

    @Override
    public void addCTRL(Rezervacija rez) {
//
    }

}
