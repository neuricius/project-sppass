/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import view.ArrivalsPerDateView;
import view.GuestsPerRoomDefaultView;
import view.DeparturesOnDayView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static view.GuestsPerRoomDefaultView.printB;
import static view.GuestsPerRoomDefaultView.closeB;
import static view.GuestsPerRoomDefaultView.GPSPaneVBox;
import static view.GuestsPerRoomDefaultView.GPSScene;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestsPerRoomDefaultCTRL implements WindowDecorator{

    private static GuestsPerRoomDefaultView GPRDefWindow;

    public GuestsPerRoomDefaultCTRL() {
        new ArrivalsPerDateView();
        new DeparturesOnDayView();
        launchGPR();
    }

    public void launchGPR() {
        GPRDefWindow = new GuestsPerRoomDefaultView();
        WindowDecorator.noConfirmationDecorator(GPRDefWindow);
        GPRDefWindow.setResizable(false);
        GPRDefWindow.setScene(GPSScene);
        GPRDefWindow.initModality(Modality.APPLICATION_MODAL);
        GPRDefWindow.setTitle("Gosti po sobama");
        GPRDefWindow.show();

        printB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(GPSPaneVBox))).start();
        });

        closeB.setOnAction(e -> {
            GPRDefWindow.close();
        });
    }

}
