/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.stage.Modality;
import model.Rezervacija;
import view.ArrivalsPerDateView;
import view.HouseKeepingReportView;
import view.DeparturesOnDayView;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import components.WindowDecorator;
import static view.HouseKeepingReportView.printB;
import static view.HouseKeepingReportView.closeB;
import static view.HouseKeepingReportView.hkReportMainVBox;
import static view.HouseKeepingReportView.hkReportScene;

/**
 *
 * @author Nebojsa Matic
 */
public class HKReportCTRL implements WindowDecorator, TakeOverCTRL {

    private static HouseKeepingReportView housekeeper;

    public HKReportCTRL() {
        new ArrivalsPerDateView();
        new DeparturesOnDayView();
        launchHKR();
    }

    public void launchHKR() {
        housekeeper = new HouseKeepingReportView();
        WindowDecorator.noConfirmationDecorator(housekeeper);
        addCTRL();
        housekeeper.setScene(hkReportScene);
        housekeeper.initModality(Modality.APPLICATION_MODAL);
        housekeeper.setTitle("Izvestaj za domacinstvo");
        housekeeper.setResizable(false);
        housekeeper.show();

    }

    @Override
    public void addCTRL() {
        printB.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(hkReportMainVBox))).start();
        });

        closeB.setOnAction(e -> {
            housekeeper.close();
        });
        
    }

    @Override
    public void addCTRL(Integer idRez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCTRL(Rezervacija rez) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
