/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "tipgosta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipgosta.findAll", query = "SELECT t FROM Tipgosta t")
    , @NamedQuery(name = "Tipgosta.findByIdtipgosta", query = "SELECT t FROM Tipgosta t WHERE t.idtipgosta = :idtipgosta")
    , @NamedQuery(name = "Tipgosta.findByTipGosta", query = "SELECT t FROM Tipgosta t WHERE t.tipGosta = :tipGosta")})
public class Tipgosta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipgosta")
    private Integer idtipgosta;
    @Basic(optional = false)
    @Column(name = "TipGosta")
    private String tipGosta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipGosta", fetch = FetchType.LAZY)
    private List<Gost> gostList;

    public Tipgosta() {
    }

    public Tipgosta(Integer idtipgosta) {
        this.idtipgosta = idtipgosta;
    }

    public Tipgosta(Integer idtipgosta, String tipGosta) {
        this.idtipgosta = idtipgosta;
        this.tipGosta = tipGosta;
    }

    public Integer getIdtipgosta() {
        return idtipgosta;
    }

    public void setIdtipgosta(Integer idtipgosta) {
        this.idtipgosta = idtipgosta;
    }

    public String getTipGosta() {
        return tipGosta;
    }

    public void setTipGosta(String tipGosta) {
        this.tipGosta = tipGosta;
    }

    @XmlTransient
    public List<Gost> getGostList() {
        return gostList;
    }

    public void setGostList(List<Gost> gostList) {
        this.gostList = gostList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipgosta != null ? idtipgosta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipgosta)) {
            return false;
        }
        Tipgosta other = (Tipgosta) object;
        return (this.idtipgosta != null || other.idtipgosta == null) && (this.idtipgosta == null || this.idtipgosta.equals(other.idtipgosta));
    }

    @Override
    public String toString() {
        return "model.Tipgosta[ idtipgosta=" + idtipgosta + " ]";
    }
    
}
