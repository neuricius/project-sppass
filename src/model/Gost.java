/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "gost")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gost.findAll", query = "SELECT g FROM Gost g")
    , @NamedQuery(name = "Gost.findByIdGost", query = "SELECT g FROM Gost g WHERE g.idGost = :idGost")
    , @NamedQuery(name = "Gost.findByImeGosta", query = "SELECT g FROM Gost g WHERE g.imeGosta = :imeGosta")
    , @NamedQuery(name = "Gost.findByPrezimeGosta", query = "SELECT g FROM Gost g WHERE g.prezimeGosta = :prezimeGosta")
    , @NamedQuery(name = "Gost.findByDatumRodjenjaGosta", query = "SELECT g FROM Gost g WHERE g.datumRodjenjaGosta = :datumRodjenjaGosta")
    , @NamedQuery(name = "Gost.findByMestoGosta", query = "SELECT g FROM Gost g WHERE g.mestoGosta = :mestoGosta")
    , @NamedQuery(name = "Gost.findByDrzavaGosta", query = "SELECT g FROM Gost g WHERE g.drzavaGosta = :drzavaGosta")
    , @NamedQuery(name = "Gost.findByTelefonGosta", query = "SELECT g FROM Gost g WHERE g.telefonGosta = :telefonGosta")
    , @NamedQuery(name = "Gost.findByEmailGosta", query = "SELECT g FROM Gost g WHERE g.emailGosta = :emailGosta")
    , @NamedQuery(name = "Gost.findByBrDokumentaGosta", query = "SELECT g FROM Gost g WHERE g.brDokumentaGosta = :brDokumentaGosta")})
public class Gost implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idGost")
    private Integer idGost;
    @Column(name = "ImeGosta")
    private String imeGosta;
    @Column(name = "PrezimeGosta")
    private String prezimeGosta;
    @Column(name = "DatumRodjenjaGosta")
    @Temporal(TemporalType.DATE)
    private Date datumRodjenjaGosta;
    @Column(name = "MestoGosta")
    private String mestoGosta;
    @Column(name = "DrzavaGosta")
    private String drzavaGosta;
    @Column(name = "TelefonGosta")
    private String telefonGosta;
    @Column(name = "EmailGosta")
    private String emailGosta;
    @Column(name = "brDokumentaGosta")
    private String brDokumentaGosta;
    @JoinColumn(name = "TipGosta", referencedColumnName = "idtipgosta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipgosta tipGosta;
    @JoinColumn(name = "sifraDokumentaGosta", referencedColumnName = "idDokumenta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Dokumenta sifraDokumentaGosta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGosta", fetch = FetchType.LAZY)
    private List<Gostiposobama> gostiposobamaList;

    public Gost() {
    }

    public Gost(Integer idGost) {
        this.idGost = idGost;
    }

    public Integer getIdGost() {
        return idGost;
    }

    public void setIdGost(Integer idGost) {
        this.idGost = idGost;
    }

    public String getImeGosta() {
        return imeGosta;
    }

    public void setImeGosta(String imeGosta) {
        this.imeGosta = imeGosta;
    }

    public String getPrezimeGosta() {
        return prezimeGosta;
    }

    public void setPrezimeGosta(String prezimeGosta) {
        this.prezimeGosta = prezimeGosta;
    }

    public Date getDatumRodjenjaGosta() {
        return datumRodjenjaGosta;
    }

    public void setDatumRodjenjaGosta(Date datumRodjenjaGosta) {
        this.datumRodjenjaGosta = datumRodjenjaGosta;
    }

    public String getMestoGosta() {
        return mestoGosta;
    }

    public void setMestoGosta(String mestoGosta) {
        this.mestoGosta = mestoGosta;
    }

    public String getDrzavaGosta() {
        return drzavaGosta;
    }

    public void setDrzavaGosta(String drzavaGosta) {
        this.drzavaGosta = drzavaGosta;
    }

    public String getTelefonGosta() {
        return telefonGosta;
    }

    public void setTelefonGosta(String telefonGosta) {
        this.telefonGosta = telefonGosta;
    }

    public String getEmailGosta() {
        return emailGosta;
    }

    public void setEmailGosta(String emailGosta) {
        this.emailGosta = emailGosta;
    }

    public String getBrDokumentaGosta() {
        return brDokumentaGosta;
    }

    public void setBrDokumentaGosta(String brDokumentaGosta) {
        this.brDokumentaGosta = brDokumentaGosta;
    }

    public Tipgosta getTipGosta() {
        return tipGosta;
    }

    public void setTipGosta(Tipgosta tipGosta) {
        this.tipGosta = tipGosta;
    }

    public Dokumenta getSifraDokumentaGosta() {
        return sifraDokumentaGosta;
    }

    public void setSifraDokumentaGosta(Dokumenta sifraDokumentaGosta) {
        this.sifraDokumentaGosta = sifraDokumentaGosta;
    }

    @XmlTransient
    public List<Gostiposobama> getGostiposobamaList() {
        return gostiposobamaList;
    }

    public void setGostiposobamaList(List<Gostiposobama> gostiposobamaList) {
        this.gostiposobamaList = gostiposobamaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGost != null ? idGost.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gost)) {
            return false;
        }
        Gost other = (Gost) object;
        return (this.idGost != null || other.idGost == null) && (this.idGost == null || this.idGost.equals(other.idGost));
    }

    @Override
    public String toString() {
        return "model.Gost[ idGost=" + idGost + " ]";
    }
    
}
