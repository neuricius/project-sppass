/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "tipnaloga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipnaloga.findAll", query = "SELECT t FROM Tipnaloga t")
    , @NamedQuery(name = "Tipnaloga.findByIdTipNaloga", query = "SELECT t FROM Tipnaloga t WHERE t.idTipNaloga = :idTipNaloga")
    , @NamedQuery(name = "Tipnaloga.findByTipNaloga", query = "SELECT t FROM Tipnaloga t WHERE t.tipNaloga = :tipNaloga")})
public class Tipnaloga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipNaloga")
    private Integer idTipNaloga;
    @Basic(optional = false)
    @Column(name = "TipNaloga")
    private String tipNaloga;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipNaloga", fetch = FetchType.LAZY)
    private List<Listanaloga> listanalogaList;

    public Tipnaloga() {
    }

    public Tipnaloga(Integer idTipNaloga) {
        this.idTipNaloga = idTipNaloga;
    }

    public Tipnaloga(Integer idTipNaloga, String tipNaloga) {
        this.idTipNaloga = idTipNaloga;
        this.tipNaloga = tipNaloga;
    }

    public Integer getIdTipNaloga() {
        return idTipNaloga;
    }

    public void setIdTipNaloga(Integer idTipNaloga) {
        this.idTipNaloga = idTipNaloga;
    }

    public String getTipNaloga() {
        return tipNaloga;
    }

    public void setTipNaloga(String tipNaloga) {
        this.tipNaloga = tipNaloga;
    }

    @XmlTransient
    public List<Listanaloga> getListanalogaList() {
        return listanalogaList;
    }

    public void setListanalogaList(List<Listanaloga> listanalogaList) {
        this.listanalogaList = listanalogaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipNaloga != null ? idTipNaloga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipnaloga)) {
            return false;
        }
        Tipnaloga other = (Tipnaloga) object;
        return (this.idTipNaloga != null || other.idTipNaloga == null) && (this.idTipNaloga == null || this.idTipNaloga.equals(other.idTipNaloga));
    }

    @Override
    public String toString() {
        return "model.Tipnaloga[ idTipNaloga=" + idTipNaloga + " ]";
    }
    
}
