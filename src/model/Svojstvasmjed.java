/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "svojstvasmjed")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Svojstvasmjed.findAll", query = "SELECT s FROM Svojstvasmjed s")
    , @NamedQuery(name = "Svojstvasmjed.findByIdSvojstvaSmJed", query = "SELECT s FROM Svojstvasmjed s WHERE s.idSvojstvaSmJed = :idSvojstvaSmJed")})
public class Svojstvasmjed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSvojstvaSmJed")
    private Integer idSvojstvaSmJed;
    @JoinColumn(name = "idSmJedinice", referencedColumnName = "idSmJedinice")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Smjedinica idSmJedinice;
    @JoinColumn(name = "Svojstva", referencedColumnName = "idSvojstva")
    @ManyToOne(fetch = FetchType.LAZY)
    private Svojstva svojstva;

    public Svojstvasmjed() {
    }

    public Svojstvasmjed(Integer idSvojstvaSmJed) {
        this.idSvojstvaSmJed = idSvojstvaSmJed;
    }

    public Integer getIdSvojstvaSmJed() {
        return idSvojstvaSmJed;
    }

    public void setIdSvojstvaSmJed(Integer idSvojstvaSmJed) {
        this.idSvojstvaSmJed = idSvojstvaSmJed;
    }

    public Smjedinica getIdSmJedinice() {
        return idSmJedinice;
    }

    public void setIdSmJedinice(Smjedinica idSmJedinice) {
        this.idSmJedinice = idSmJedinice;
    }

    public Svojstva getSvojstva() {
        return svojstva;
    }

    public void setSvojstva(Svojstva svojstva) {
        this.svojstva = svojstva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSvojstvaSmJed != null ? idSvojstvaSmJed.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Svojstvasmjed)) {
            return false;
        }
        Svojstvasmjed other = (Svojstvasmjed) object;
        return (this.idSvojstvaSmJed != null || other.idSvojstvaSmJed == null) && (this.idSvojstvaSmJed == null || this.idSvojstvaSmJed.equals(other.idSvojstvaSmJed));
    }

    @Override
    public String toString() {
        return "model.Svojstvasmjed[ idSvojstvaSmJed=" + idSvojstvaSmJed + " ]";
    }
    
}
