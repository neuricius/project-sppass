/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "gostiposobama")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gostiposobama.findAll", query = "SELECT g FROM Gostiposobama g")
    , @NamedQuery(name = "Gostiposobama.findByIdgps", query = "SELECT g FROM Gostiposobama g WHERE g.idgps = :idgps")})
public class Gostiposobama implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgps")
    private Integer idgps;
    @JoinColumn(name = "idGosta", referencedColumnName = "idGost")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Gost idGosta;
    @JoinColumn(name = "idjRezervacije", referencedColumnName = "idRezervacija")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Rezervacija idjRezervacije;

    public Gostiposobama() {
    }

    public Gostiposobama(Integer idgps) {
        this.idgps = idgps;
    }

    public Integer getIdgps() {
        return idgps;
    }

    public void setIdgps(Integer idgps) {
        this.idgps = idgps;
    }

    public Gost getIdGosta() {
        return idGosta;
    }

    public void setIdGosta(Gost idGosta) {
        this.idGosta = idGosta;
    }

    public Rezervacija getIdjRezervacije() {
        return idjRezervacije;
    }

    public void setIdjRezervacije(Rezervacija idjRezervacije) {
        this.idjRezervacije = idjRezervacije;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgps != null ? idgps.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gostiposobama)) {
            return false;
        }
        Gostiposobama other = (Gostiposobama) object;
        return (this.idgps != null || other.idgps == null) && (this.idgps == null || this.idgps.equals(other.idgps));
    }

    @Override
    public String toString() {
        return "model.Gostiposobama[ idgps=" + idgps + " ]";
    }
    
}
