/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "objekti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objekti.findAll", query = "SELECT o FROM Objekti o")
    , @NamedQuery(name = "Objekti.findByIdObjekta", query = "SELECT o FROM Objekti o WHERE o.idObjekta = :idObjekta")
    , @NamedQuery(name = "Objekti.findByNazivObjekta", query = "SELECT o FROM Objekti o WHERE o.nazivObjekta = :nazivObjekta")
    , @NamedQuery(name = "Objekti.findByPocetakSezone", query = "SELECT o FROM Objekti o WHERE o.pocetakSezone = :pocetakSezone")
    , @NamedQuery(name = "Objekti.findByKrajSezone", query = "SELECT o FROM Objekti o WHERE o.krajSezone = :krajSezone")})
public class Objekti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idObjekta")
    private Integer idObjekta;
    @Column(name = "NazivObjekta")
    private String nazivObjekta;
    @Basic(optional = false)
    @Column(name = "PocetakSezone")
    private int pocetakSezone;
    @Basic(optional = false)
    @Column(name = "KrajSezone")
    private int krajSezone;
    @JoinColumn(name = "Kompanija", referencedColumnName = "idKompanije")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Kompanija kompanija;
    @JoinColumn(name = "OdgLice1", referencedColumnName = "idProfila")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profil odgLice1;
    @JoinColumn(name = "OdgLice2", referencedColumnName = "idProfila")
    @ManyToOne(fetch = FetchType.LAZY)
    private Profil odgLice2;
    @JoinColumn(name = "TipObjekta", referencedColumnName = "idTipObjekta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipobjekta tipObjekta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "objekat", fetch = FetchType.LAZY)
    private List<Smjedinica> smjedinicaList;

    public Objekti() {
    }

    public Objekti(Integer idObjekta) {
        this.idObjekta = idObjekta;
    }

    public Objekti(Integer idObjekta, int pocetakSezone, int krajSezone) {
        this.idObjekta = idObjekta;
        this.pocetakSezone = pocetakSezone;
        this.krajSezone = krajSezone;
    }

    public Integer getIdObjekta() {
        return idObjekta;
    }

    public void setIdObjekta(Integer idObjekta) {
        this.idObjekta = idObjekta;
    }

    public String getNazivObjekta() {
        return nazivObjekta;
    }

    public void setNazivObjekta(String nazivObjekta) {
        this.nazivObjekta = nazivObjekta;
    }

    public int getPocetakSezone() {
        return pocetakSezone;
    }

    public void setPocetakSezone(int pocetakSezone) {
        this.pocetakSezone = pocetakSezone;
    }

    public int getKrajSezone() {
        return krajSezone;
    }

    public void setKrajSezone(int krajSezone) {
        this.krajSezone = krajSezone;
    }

    public Kompanija getKompanija() {
        return kompanija;
    }

    public void setKompanija(Kompanija kompanija) {
        this.kompanija = kompanija;
    }

    public Profil getOdgLice1() {
        return odgLice1;
    }

    public void setOdgLice1(Profil odgLice1) {
        this.odgLice1 = odgLice1;
    }

    public Profil getOdgLice2() {
        return odgLice2;
    }

    public void setOdgLice2(Profil odgLice2) {
        this.odgLice2 = odgLice2;
    }

    public Tipobjekta getTipObjekta() {
        return tipObjekta;
    }

    public void setTipObjekta(Tipobjekta tipObjekta) {
        this.tipObjekta = tipObjekta;
    }

    @XmlTransient
    public List<Smjedinica> getSmjedinicaList() {
        return smjedinicaList;
    }

    public void setSmjedinicaList(List<Smjedinica> smjedinicaList) {
        this.smjedinicaList = smjedinicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObjekta != null ? idObjekta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objekti)) {
            return false;
        }
        Objekti other = (Objekti) object;
        return (this.idObjekta != null || other.idObjekta == null) && (this.idObjekta == null || this.idObjekta.equals(other.idObjekta));
    }

    @Override
    public String toString() {
        return "model.Objekti[ idObjekta=" + idObjekta + " ]";
    }
    
}
