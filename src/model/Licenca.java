/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "licenca")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Licenca.findAll", query = "SELECT l FROM Licenca l")
    , @NamedQuery(name = "Licenca.findByIdLicenca", query = "SELECT l FROM Licenca l WHERE l.idLicenca = :idLicenca")
    , @NamedQuery(name = "Licenca.findByPocetakLicence", query = "SELECT l FROM Licenca l WHERE l.pocetakLicence = :pocetakLicence")
    , @NamedQuery(name = "Licenca.findByKrajLicence", query = "SELECT l FROM Licenca l WHERE l.krajLicence = :krajLicence")})
public class Licenca implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLicenca")
    private Integer idLicenca;
    @Basic(optional = false)
    @Column(name = "pocetakLicence")
    private int pocetakLicence;
    @Basic(optional = false)
    @Column(name = "krajLicence")
    private int krajLicence;
    @JoinColumn(name = "Kompanija", referencedColumnName = "idKompanije")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Kompanija kompanija;

    public Licenca() {
    }

    public Licenca(Integer idLicenca) {
        this.idLicenca = idLicenca;
    }

    public Licenca(Integer idLicenca, int pocetakLicence, int krajLicence) {
        this.idLicenca = idLicenca;
        this.pocetakLicence = pocetakLicence;
        this.krajLicence = krajLicence;
    }

    public Integer getIdLicenca() {
        return idLicenca;
    }

    public void setIdLicenca(Integer idLicenca) {
        this.idLicenca = idLicenca;
    }

    public int getPocetakLicence() {
        return pocetakLicence;
    }

    public void setPocetakLicence(int pocetakLicence) {
        this.pocetakLicence = pocetakLicence;
    }

    public int getKrajLicence() {
        return krajLicence;
    }

    public void setKrajLicence(int krajLicence) {
        this.krajLicence = krajLicence;
    }

    public Kompanija getKompanija() {
        return kompanija;
    }

    public void setKompanija(Kompanija kompanija) {
        this.kompanija = kompanija;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLicenca != null ? idLicenca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licenca)) {
            return false;
        }
        Licenca other = (Licenca) object;
        return (this.idLicenca != null || other.idLicenca == null) && (this.idLicenca == null || this.idLicenca.equals(other.idLicenca));
    }

    @Override
    public String toString() {
        return "model.Licenca[ idLicenca=" + idLicenca + " ]";
    }
    
}
