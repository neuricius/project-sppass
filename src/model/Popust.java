/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "popust")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Popust.findAll", query = "SELECT p FROM Popust p")
    , @NamedQuery(name = "Popust.findByIdPopust", query = "SELECT p FROM Popust p WHERE p.idPopust = :idPopust")
    , @NamedQuery(name = "Popust.findByNazivPopusta", query = "SELECT p FROM Popust p WHERE p.nazivPopusta = :nazivPopusta")
    , @NamedQuery(name = "Popust.findByIznosPopusta", query = "SELECT p FROM Popust p WHERE p.iznosPopusta = :iznosPopusta")
    , @NamedQuery(name = "Popust.findByOpisPopusta", query = "SELECT p FROM Popust p WHERE p.opisPopusta = :opisPopusta")
    , @NamedQuery(name = "Popust.findByPopustAktivan", query = "SELECT p FROM Popust p WHERE p.popustAktivan = :popustAktivan")})
public class Popust implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPopust")
    private Integer idPopust;
    @Basic(optional = false)
    @Column(name = "NazivPopusta")
    private String nazivPopusta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "IznosPopusta")
    private Double iznosPopusta;
    @Column(name = "OpisPopusta")
    private String opisPopusta;
    @Basic(optional = false)
    @Column(name = "PopustAktivan")
    private short popustAktivan;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sifraPopusta", fetch = FetchType.LAZY)
    private List<Naplata> naplataList;

    public Popust() {
    }

    public Popust(Integer idPopust) {
        this.idPopust = idPopust;
    }

    public Popust(Integer idPopust, String nazivPopusta, short popustAktivan) {
        this.idPopust = idPopust;
        this.nazivPopusta = nazivPopusta;
        this.popustAktivan = popustAktivan;
    }

    public Integer getIdPopust() {
        return idPopust;
    }

    public void setIdPopust(Integer idPopust) {
        this.idPopust = idPopust;
    }

    public String getNazivPopusta() {
        return nazivPopusta;
    }

    public void setNazivPopusta(String nazivPopusta) {
        this.nazivPopusta = nazivPopusta;
    }

    public Double getIznosPopusta() {
        return iznosPopusta;
    }

    public void setIznosPopusta(Double iznosPopusta) {
        this.iznosPopusta = iznosPopusta;
    }

    public String getOpisPopusta() {
        return opisPopusta;
    }

    public void setOpisPopusta(String opisPopusta) {
        this.opisPopusta = opisPopusta;
    }

    public short getPopustAktivan() {
        return popustAktivan;
    }

    public void setPopustAktivan(short popustAktivan) {
        this.popustAktivan = popustAktivan;
    }

    @XmlTransient
    public List<Naplata> getNaplataList() {
        return naplataList;
    }

    public void setNaplataList(List<Naplata> naplataList) {
        this.naplataList = naplataList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPopust != null ? idPopust.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Popust)) {
            return false;
        }
        Popust other = (Popust) object;
        return (this.idPopust != null || other.idPopust == null) && (this.idPopust == null || this.idPopust.equals(other.idPopust));
    }

    @Override
    public String toString() {
        return "model.Popust[ idPopust=" + idPopust + " ]";
    }
    
}
