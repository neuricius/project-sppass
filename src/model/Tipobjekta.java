/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "tipobjekta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipobjekta.findAll", query = "SELECT t FROM Tipobjekta t")
    , @NamedQuery(name = "Tipobjekta.findByIdTipObjekta", query = "SELECT t FROM Tipobjekta t WHERE t.idTipObjekta = :idTipObjekta")
    , @NamedQuery(name = "Tipobjekta.findByTipObjekta", query = "SELECT t FROM Tipobjekta t WHERE t.tipObjekta = :tipObjekta")})
public class Tipobjekta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipObjekta")
    private Integer idTipObjekta;
    @Basic(optional = false)
    @Column(name = "TipObjekta")
    private String tipObjekta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipObjekta", fetch = FetchType.LAZY)
    private List<Objekti> objektiList;

    public Tipobjekta() {
    }

    public Tipobjekta(Integer idTipObjekta) {
        this.idTipObjekta = idTipObjekta;
    }

    public Tipobjekta(Integer idTipObjekta, String tipObjekta) {
        this.idTipObjekta = idTipObjekta;
        this.tipObjekta = tipObjekta;
    }

    public Integer getIdTipObjekta() {
        return idTipObjekta;
    }

    public void setIdTipObjekta(Integer idTipObjekta) {
        this.idTipObjekta = idTipObjekta;
    }

    public String getTipObjekta() {
        return tipObjekta;
    }

    public void setTipObjekta(String tipObjekta) {
        this.tipObjekta = tipObjekta;
    }

    @XmlTransient
    public List<Objekti> getObjektiList() {
        return objektiList;
    }

    public void setObjektiList(List<Objekti> objektiList) {
        this.objektiList = objektiList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipObjekta != null ? idTipObjekta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipobjekta)) {
            return false;
        }
        Tipobjekta other = (Tipobjekta) object;
        return (this.idTipObjekta != null || other.idTipObjekta == null) && (this.idTipObjekta == null || this.idTipObjekta.equals(other.idTipObjekta));
    }

    @Override
    public String toString() {
        return "model.Tipobjekta[ idTipObjekta=" + idTipObjekta + " ]";
    }
    
}
