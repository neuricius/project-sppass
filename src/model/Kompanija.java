/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "kompanija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kompanija.findAll", query = "SELECT k FROM Kompanija k")
    , @NamedQuery(name = "Kompanija.findByIdKompanije", query = "SELECT k FROM Kompanija k WHERE k.idKompanije = :idKompanije")
    , @NamedQuery(name = "Kompanija.findByNazivKompanije", query = "SELECT k FROM Kompanija k WHERE k.nazivKompanije = :nazivKompanije")
    , @NamedQuery(name = "Kompanija.findByAdresaKompanije1", query = "SELECT k FROM Kompanija k WHERE k.adresaKompanije1 = :adresaKompanije1")
    , @NamedQuery(name = "Kompanija.findByAdresaKompanije2", query = "SELECT k FROM Kompanija k WHERE k.adresaKompanije2 = :adresaKompanije2")
    , @NamedQuery(name = "Kompanija.findByGrad", query = "SELECT k FROM Kompanija k WHERE k.grad = :grad")
    , @NamedQuery(name = "Kompanija.findByDrzava", query = "SELECT k FROM Kompanija k WHERE k.drzava = :drzava")
    , @NamedQuery(name = "Kompanija.findByPostanskiBroj", query = "SELECT k FROM Kompanija k WHERE k.postanskiBroj = :postanskiBroj")
    , @NamedQuery(name = "Kompanija.findByTelefon", query = "SELECT k FROM Kompanija k WHERE k.telefon = :telefon")
    , @NamedQuery(name = "Kompanija.findByEmailKomp", query = "SELECT k FROM Kompanija k WHERE k.emailKomp = :emailKomp")})
public class Kompanija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idKompanije")
    private Integer idKompanije;
    @Basic(optional = false)
    @Column(name = "NazivKompanije")
    private String nazivKompanije;
    @Basic(optional = false)
    @Column(name = "AdresaKompanije1")
    private String adresaKompanije1;
    @Basic(optional = false)
    @Column(name = "AdresaKompanije2")
    private String adresaKompanije2;
    @Basic(optional = false)
    @Column(name = "Grad")
    private String grad;
    @Basic(optional = false)
    @Column(name = "Drzava")
    private String drzava;
    @Basic(optional = false)
    @Column(name = "PostanskiBroj")
    private String postanskiBroj;
    @Basic(optional = false)
    @Column(name = "Telefon")
    private String telefon;
    @Basic(optional = false)
    @Column(name = "EmailKomp")
    private String emailKomp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kompanija", fetch = FetchType.LAZY)
    private List<Objekti> objektiList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kompanija", fetch = FetchType.LAZY)
    private List<Profil> profilList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kompanija", fetch = FetchType.LAZY)
    private List<Nalog> nalogList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kompanija", fetch = FetchType.LAZY)
    private List<Licenca> licencaList;

    public Kompanija() {
    }

    public Kompanija(Integer idKompanije) {
        this.idKompanije = idKompanije;
    }

    public Kompanija(Integer idKompanije, String nazivKompanije, String adresaKompanije1, String adresaKompanije2, String grad, String drzava, String postanskiBroj, String telefon, String emailKomp) {
        this.idKompanije = idKompanije;
        this.nazivKompanije = nazivKompanije;
        this.adresaKompanije1 = adresaKompanije1;
        this.adresaKompanije2 = adresaKompanije2;
        this.grad = grad;
        this.drzava = drzava;
        this.postanskiBroj = postanskiBroj;
        this.telefon = telefon;
        this.emailKomp = emailKomp;
    }

    public Integer getIdKompanije() {
        return idKompanije;
    }

    public void setIdKompanije(Integer idKompanije) {
        this.idKompanije = idKompanije;
    }

    public String getNazivKompanije() {
        return nazivKompanije;
    }

    public void setNazivKompanije(String nazivKompanije) {
        this.nazivKompanije = nazivKompanije;
    }

    public String getAdresaKompanije1() {
        return adresaKompanije1;
    }

    public void setAdresaKompanije1(String adresaKompanije1) {
        this.adresaKompanije1 = adresaKompanije1;
    }

    public String getAdresaKompanije2() {
        return adresaKompanije2;
    }

    public void setAdresaKompanije2(String adresaKompanije2) {
        this.adresaKompanije2 = adresaKompanije2;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public String getPostanskiBroj() {
        return postanskiBroj;
    }

    public void setPostanskiBroj(String postanskiBroj) {
        this.postanskiBroj = postanskiBroj;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmailKomp() {
        return emailKomp;
    }

    public void setEmailKomp(String emailKomp) {
        this.emailKomp = emailKomp;
    }

    @XmlTransient
    public List<Objekti> getObjektiList() {
        return objektiList;
    }

    public void setObjektiList(List<Objekti> objektiList) {
        this.objektiList = objektiList;
    }

    @XmlTransient
    public List<Profil> getProfilList() {
        return profilList;
    }

    public void setProfilList(List<Profil> profilList) {
        this.profilList = profilList;
    }

    @XmlTransient
    public List<Nalog> getNalogList() {
        return nalogList;
    }

    public void setNalogList(List<Nalog> nalogList) {
        this.nalogList = nalogList;
    }

    @XmlTransient
    public List<Licenca> getLicencaList() {
        return licencaList;
    }

    public void setLicencaList(List<Licenca> licencaList) {
        this.licencaList = licencaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKompanije != null ? idKompanije.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kompanija)) {
            return false;
        }
        Kompanija other = (Kompanija) object;
        return (this.idKompanije != null || other.idKompanije == null) && (this.idKompanije == null || this.idKompanije.equals(other.idKompanije));
    }

    @Override
    public String toString() {
        return "model.Kompanija[ idKompanije=" + idKompanije + " ]";
    }
    
}
