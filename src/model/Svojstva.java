/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "svojstva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Svojstva.findAll", query = "SELECT s FROM Svojstva s")
    , @NamedQuery(name = "Svojstva.findByIdSvojstva", query = "SELECT s FROM Svojstva s WHERE s.idSvojstva = :idSvojstva")
    , @NamedQuery(name = "Svojstva.findBySvojstvo", query = "SELECT s FROM Svojstva s WHERE s.svojstvo = :svojstvo")})
public class Svojstva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSvojstva")
    private Integer idSvojstva;
    @Column(name = "Svojstvo")
    private String svojstvo;
    @OneToMany(mappedBy = "svojstva", fetch = FetchType.LAZY)
    private List<Svojstvasmjed> svojstvasmjedList;

    public Svojstva() {
    }

    public Svojstva(Integer idSvojstva) {
        this.idSvojstva = idSvojstva;
    }

    public Integer getIdSvojstva() {
        return idSvojstva;
    }

    public void setIdSvojstva(Integer idSvojstva) {
        this.idSvojstva = idSvojstva;
    }

    public String getSvojstvo() {
        return svojstvo;
    }

    public void setSvojstvo(String svojstvo) {
        this.svojstvo = svojstvo;
    }

    @XmlTransient
    public List<Svojstvasmjed> getSvojstvasmjedList() {
        return svojstvasmjedList;
    }

    public void setSvojstvasmjedList(List<Svojstvasmjed> svojstvasmjedList) {
        this.svojstvasmjedList = svojstvasmjedList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSvojstva != null ? idSvojstva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Svojstva)) {
            return false;
        }
        Svojstva other = (Svojstva) object;
        return (this.idSvojstva != null || other.idSvojstva == null) && (this.idSvojstva == null || this.idSvojstva.equals(other.idSvojstva));
    }

    @Override
    public String toString() {
        return "model.Svojstva[ idSvojstva=" + idSvojstva + " ]";
    }
    
}
