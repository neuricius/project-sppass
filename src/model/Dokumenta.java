/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "dokumenta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dokumenta.findAll", query = "SELECT d FROM Dokumenta d")
    , @NamedQuery(name = "Dokumenta.findByIdDokumenta", query = "SELECT d FROM Dokumenta d WHERE d.idDokumenta = :idDokumenta")
    , @NamedQuery(name = "Dokumenta.findByVrstaDokumenta", query = "SELECT d FROM Dokumenta d WHERE d.vrstaDokumenta = :vrstaDokumenta")})
public class Dokumenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDokumenta")
    private Integer idDokumenta;
    @Basic(optional = false)
    @Column(name = "vrstaDokumenta")
    private String vrstaDokumenta;
    @OneToMany(mappedBy = "sifraDokumentaKlijenta", fetch = FetchType.LAZY)
    private List<Klijent> klijentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sifraDokumentaGosta", fetch = FetchType.LAZY)
    private List<Gost> gostList;

    public Dokumenta() {
    }

    public Dokumenta(Integer idDokumenta) {
        this.idDokumenta = idDokumenta;
    }

    public Dokumenta(Integer idDokumenta, String vrstaDokumenta) {
        this.idDokumenta = idDokumenta;
        this.vrstaDokumenta = vrstaDokumenta;
    }

    public Integer getIdDokumenta() {
        return idDokumenta;
    }

    public void setIdDokumenta(Integer idDokumenta) {
        this.idDokumenta = idDokumenta;
    }

    public String getVrstaDokumenta() {
        return vrstaDokumenta;
    }

    public void setVrstaDokumenta(String vrstaDokumenta) {
        this.vrstaDokumenta = vrstaDokumenta;
    }

    @XmlTransient
    public List<Klijent> getKlijentList() {
        return klijentList;
    }

    public void setKlijentList(List<Klijent> klijentList) {
        this.klijentList = klijentList;
    }

    @XmlTransient
    public List<Gost> getGostList() {
        return gostList;
    }

    public void setGostList(List<Gost> gostList) {
        this.gostList = gostList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDokumenta != null ? idDokumenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dokumenta)) {
            return false;
        }
        Dokumenta other = (Dokumenta) object;
        return (this.idDokumenta != null || other.idDokumenta == null) && (this.idDokumenta == null || this.idDokumenta.equals(other.idDokumenta));
    }

    @Override
    public String toString() {
        return "model.Dokumenta[ idDokumenta=" + idDokumenta + " ]";
    }
    
}
