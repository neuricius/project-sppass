/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "klijent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Klijent.findAll", query = "SELECT k FROM Klijent k")
    , @NamedQuery(name = "Klijent.findByIdKlijent", query = "SELECT k FROM Klijent k WHERE k.idKlijent = :idKlijent")
    , @NamedQuery(name = "Klijent.findByImeKlijenta", query = "SELECT k FROM Klijent k WHERE k.imeKlijenta = :imeKlijenta")
    , @NamedQuery(name = "Klijent.findByPrezimeKlijenta", query = "SELECT k FROM Klijent k WHERE k.prezimeKlijenta = :prezimeKlijenta")
    , @NamedQuery(name = "Klijent.findByMestoKlijenta", query = "SELECT k FROM Klijent k WHERE k.mestoKlijenta = :mestoKlijenta")
    , @NamedQuery(name = "Klijent.findByDrzavaKlijenta", query = "SELECT k FROM Klijent k WHERE k.drzavaKlijenta = :drzavaKlijenta")
    , @NamedQuery(name = "Klijent.findByTelefonKlijenta", query = "SELECT k FROM Klijent k WHERE k.telefonKlijenta = :telefonKlijenta")
    , @NamedQuery(name = "Klijent.findByEmailKlijenta", query = "SELECT k FROM Klijent k WHERE k.emailKlijenta = :emailKlijenta")
    , @NamedQuery(name = "Klijent.findByKlijentJeGost", query = "SELECT k FROM Klijent k WHERE k.klijentJeGost = :klijentJeGost")
    , @NamedQuery(name = "Klijent.findByBrDokumentaKlijenta", query = "SELECT k FROM Klijent k WHERE k.brDokumentaKlijenta = :brDokumentaKlijenta")})
public class Klijent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idKlijent")
    private Integer idKlijent;
    @Basic(optional = false)
    @Column(name = "ImeKlijenta")
    private String imeKlijenta;
    @Basic(optional = false)
    @Column(name = "PrezimeKlijenta")
    private String prezimeKlijenta;
    @Column(name = "MestoKlijenta")
    private String mestoKlijenta;
    @Column(name = "DrzavaKlijenta")
    private String drzavaKlijenta;
    @Column(name = "TelefonKlijenta")
    private String telefonKlijenta;
    @Column(name = "EmailKlijenta")
    private String emailKlijenta;
    @Basic(optional = false)
    @Column(name = "KlijentJeGost")
    private short klijentJeGost;
    @Column(name = "brDokumentaKlijenta")
    private String brDokumentaKlijenta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idKlijent", fetch = FetchType.LAZY)
    private List<Rezervacija> rezervacijaList;
    @JoinColumn(name = "SifraDokumentaKlijenta", referencedColumnName = "idDokumenta")
    @ManyToOne(fetch = FetchType.LAZY)
    private Dokumenta sifraDokumentaKlijenta;

    public Klijent() {
    }

    public Klijent(Integer idKlijent) {
        this.idKlijent = idKlijent;
    }

    public Klijent(Integer idKlijent, String imeKlijenta, String prezimeKlijenta, short klijentJeGost) {
        this.idKlijent = idKlijent;
        this.imeKlijenta = imeKlijenta;
        this.prezimeKlijenta = prezimeKlijenta;
        this.klijentJeGost = klijentJeGost;
    }

    public Integer getIdKlijent() {
        return idKlijent;
    }

    public void setIdKlijent(Integer idKlijent) {
        this.idKlijent = idKlijent;
    }

    public String getImeKlijenta() {
        return imeKlijenta;
    }

    public void setImeKlijenta(String imeKlijenta) {
        this.imeKlijenta = imeKlijenta;
    }

    public String getPrezimeKlijenta() {
        return prezimeKlijenta;
    }

    public void setPrezimeKlijenta(String prezimeKlijenta) {
        this.prezimeKlijenta = prezimeKlijenta;
    }

    public String getMestoKlijenta() {
        return mestoKlijenta;
    }

    public void setMestoKlijenta(String mestoKlijenta) {
        this.mestoKlijenta = mestoKlijenta;
    }

    public String getDrzavaKlijenta() {
        return drzavaKlijenta;
    }

    public void setDrzavaKlijenta(String drzavaKlijenta) {
        this.drzavaKlijenta = drzavaKlijenta;
    }

    public String getTelefonKlijenta() {
        return telefonKlijenta;
    }

    public void setTelefonKlijenta(String telefonKlijenta) {
        this.telefonKlijenta = telefonKlijenta;
    }

    public String getEmailKlijenta() {
        return emailKlijenta;
    }

    public void setEmailKlijenta(String emailKlijenta) {
        this.emailKlijenta = emailKlijenta;
    }

    public short getKlijentJeGost() {
        return klijentJeGost;
    }

    public void setKlijentJeGost(short klijentJeGost) {
        this.klijentJeGost = klijentJeGost;
    }

    public String getBrDokumentaKlijenta() {
        return brDokumentaKlijenta;
    }

    public void setBrDokumentaKlijenta(String brDokumentaKlijenta) {
        this.brDokumentaKlijenta = brDokumentaKlijenta;
    }

    @XmlTransient
    public List<Rezervacija> getRezervacijaList() {
        return rezervacijaList;
    }

    public void setRezervacijaList(List<Rezervacija> rezervacijaList) {
        this.rezervacijaList = rezervacijaList;
    }

    public Dokumenta getSifraDokumentaKlijenta() {
        return sifraDokumentaKlijenta;
    }

    public void setSifraDokumentaKlijenta(Dokumenta sifraDokumentaKlijenta) {
        this.sifraDokumentaKlijenta = sifraDokumentaKlijenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKlijent != null ? idKlijent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Klijent)) {
            return false;
        }
        Klijent other = (Klijent) object;
        return (this.idKlijent != null || other.idKlijent == null) && (this.idKlijent == null || this.idKlijent.equals(other.idKlijent));
    }

    @Override
    public String toString() {
        return "model.Klijent[ idKlijent=" + idKlijent + " ]";
    }
    
}
