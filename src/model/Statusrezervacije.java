/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "statusrezervacije")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statusrezervacije.findAll", query = "SELECT s FROM Statusrezervacije s")
    , @NamedQuery(name = "Statusrezervacije.findByIdStatRez", query = "SELECT s FROM Statusrezervacije s WHERE s.idStatRez = :idStatRez")
    , @NamedQuery(name = "Statusrezervacije.findByStatusRezervacije", query = "SELECT s FROM Statusrezervacije s WHERE s.statusRezervacije = :statusRezervacije")})
public class Statusrezervacije implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idStatRez")
    private Integer idStatRez;
    @Basic(optional = false)
    @Column(name = "StatusRezervacije")
    private String statusRezervacije;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idStatRez", fetch = FetchType.LAZY)
    private List<Rezervacija> rezervacijaList;

    public Statusrezervacije() {
    }

    public Statusrezervacije(Integer idStatRez) {
        this.idStatRez = idStatRez;
    }

    public Statusrezervacije(Integer idStatRez, String statusRezervacije) {
        this.idStatRez = idStatRez;
        this.statusRezervacije = statusRezervacije;
    }

    public Integer getIdStatRez() {
        return idStatRez;
    }

    public void setIdStatRez(Integer idStatRez) {
        this.idStatRez = idStatRez;
    }

    public String getStatusRezervacije() {
        return statusRezervacije;
    }

    public void setStatusRezervacije(String statusRezervacije) {
        this.statusRezervacije = statusRezervacije;
    }

    @XmlTransient
    public List<Rezervacija> getRezervacijaList() {
        return rezervacijaList;
    }

    public void setRezervacijaList(List<Rezervacija> rezervacijaList) {
        this.rezervacijaList = rezervacijaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatRez != null ? idStatRez.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusrezervacije)) {
            return false;
        }
        Statusrezervacije other = (Statusrezervacije) object;
        return (this.idStatRez != null || other.idStatRez == null) && (this.idStatRez == null || this.idStatRez.equals(other.idStatRez));
    }

    @Override
    public String toString() {
        return "model.Statusrezervacije[ idStatRez=" + idStatRez + " ]";
    }
    
}
