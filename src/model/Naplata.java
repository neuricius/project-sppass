/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "naplata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Naplata.findAll", query = "SELECT n FROM Naplata n")
    , @NamedQuery(name = "Naplata.findByIdNaplate", query = "SELECT n FROM Naplata n WHERE n.idNaplate = :idNaplate")
    , @NamedQuery(name = "Naplata.findByCenaSmestaja", query = "SELECT n FROM Naplata n WHERE n.cenaSmestaja = :cenaSmestaja")
    , @NamedQuery(name = "Naplata.findByOstaliTroskovi", query = "SELECT n FROM Naplata n WHERE n.ostaliTroskovi = :ostaliTroskovi")
    , @NamedQuery(name = "Naplata.findByMedjuZbir", query = "SELECT n FROM Naplata n WHERE n.medjuZbir = :medjuZbir")
    , @NamedQuery(name = "Naplata.findByUplaceno", query = "SELECT n FROM Naplata n WHERE n.uplaceno = :uplaceno")
    , @NamedQuery(name = "Naplata.findByPreostaliIznos", query = "SELECT n FROM Naplata n WHERE n.preostaliIznos = :preostaliIznos")
    , @NamedQuery(name = "Naplata.findByDatumIzmene", query = "SELECT n FROM Naplata n WHERE n.datumIzmene = :datumIzmene")})
public class Naplata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNaplate")
    private Integer idNaplate;
    @Basic(optional = false)
    @Column(name = "cenaSmestaja")
    private double cenaSmestaja;
    @Basic(optional = false)
    @Column(name = "ostaliTroskovi")
    private double ostaliTroskovi;
    @Basic(optional = false)
    @Column(name = "medjuZbir")
    private double medjuZbir;
    @Basic(optional = false)
    @Column(name = "Uplaceno")
    private double uplaceno;
    @Basic(optional = false)
    @Column(name = "PreostaliIznos")
    private double preostaliIznos;
    @Basic(optional = false)
    @Column(name = "datumIzmene")
    @Temporal(TemporalType.DATE)
    private Date datumIzmene;
    @JoinColumn(name = "autorIzmene", referencedColumnName = "idProfila")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profil autorIzmene;
    @JoinColumn(name = "cenaNocenja", referencedColumnName = "idCenaSmJed")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cenasmjed cenaNocenja;
    @JoinColumn(name = "idRezervacija", referencedColumnName = "idRezervacija")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Rezervacija idRezervacija;
    @JoinColumn(name = "sifraPopusta", referencedColumnName = "idPopust")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Popust sifraPopusta;
    @JoinColumn(name = "StatusNaplate", referencedColumnName = "idStatusNaplate")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Statusnaplate statusNaplate;

    public Naplata() {
    }

    public Naplata(Integer idNaplate) {
        this.idNaplate = idNaplate;
    }

    public Naplata(Integer idNaplate, double cenaSmestaja, double ostaliTroskovi, double medjuZbir, double uplaceno, double preostaliIznos, Date datumIzmene) {
        this.idNaplate = idNaplate;
        this.cenaSmestaja = cenaSmestaja;
        this.ostaliTroskovi = ostaliTroskovi;
        this.medjuZbir = medjuZbir;
        this.uplaceno = uplaceno;
        this.preostaliIznos = preostaliIznos;
        this.datumIzmene = datumIzmene;
    }

    public Integer getIdNaplate() {
        return idNaplate;
    }

    public void setIdNaplate(Integer idNaplate) {
        this.idNaplate = idNaplate;
    }

    public double getCenaSmestaja() {
        return cenaSmestaja;
    }

    public void setCenaSmestaja(double cenaSmestaja) {
        this.cenaSmestaja = cenaSmestaja;
    }

    public double getOstaliTroskovi() {
        return ostaliTroskovi;
    }

    public void setOstaliTroskovi(double ostaliTroskovi) {
        this.ostaliTroskovi = ostaliTroskovi;
    }

    public double getMedjuZbir() {
        return medjuZbir;
    }

    public void setMedjuZbir(double medjuZbir) {
        this.medjuZbir = medjuZbir;
    }

    public double getUplaceno() {
        return uplaceno;
    }

    public void setUplaceno(double uplaceno) {
        this.uplaceno = uplaceno;
    }

    public double getPreostaliIznos() {
        return preostaliIznos;
    }

    public void setPreostaliIznos(double preostaliIznos) {
        this.preostaliIznos = preostaliIznos;
    }

    public Date getDatumIzmene() {
        return datumIzmene;
    }

    public void setDatumIzmene(Date datumIzmene) {
        this.datumIzmene = datumIzmene;
    }

    public Profil getAutorIzmene() {
        return autorIzmene;
    }

    public void setAutorIzmene(Profil autorIzmene) {
        this.autorIzmene = autorIzmene;
    }

    public Cenasmjed getCenaNocenja() {
        return cenaNocenja;
    }

    public void setCenaNocenja(Cenasmjed cenaNocenja) {
        this.cenaNocenja = cenaNocenja;
    }

    public Rezervacija getIdRezervacija() {
        return idRezervacija;
    }

    public void setIdRezervacija(Rezervacija idRezervacija) {
        this.idRezervacija = idRezervacija;
    }

    public Popust getSifraPopusta() {
        return sifraPopusta;
    }

    public void setSifraPopusta(Popust sifraPopusta) {
        this.sifraPopusta = sifraPopusta;
    }

    public Statusnaplate getStatusNaplate() {
        return statusNaplate;
    }

    public void setStatusNaplate(Statusnaplate statusNaplate) {
        this.statusNaplate = statusNaplate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNaplate != null ? idNaplate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Naplata)) {
            return false;
        }
        Naplata other = (Naplata) object;
        return (this.idNaplate != null || other.idNaplate == null) && (this.idNaplate == null || this.idNaplate.equals(other.idNaplate));
    }

    @Override
    public String toString() {
        return "model.Naplata[ idNaplate=" + idNaplate + " ]";
    }
    
}
