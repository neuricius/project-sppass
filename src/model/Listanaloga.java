/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "listanaloga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Listanaloga.findAll", query = "SELECT l FROM Listanaloga l")
    , @NamedQuery(name = "Listanaloga.findByIdListaNaloga", query = "SELECT l FROM Listanaloga l WHERE l.idListaNaloga = :idListaNaloga")})
public class Listanaloga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idListaNaloga")
    private Integer idListaNaloga;
    @JoinColumn(name = "idNaloga", referencedColumnName = "idNalog")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Nalog idNaloga;
    @JoinColumn(name = "TipNaloga", referencedColumnName = "idTipNaloga")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tipnaloga tipNaloga;

    public Listanaloga() {
    }

    public Listanaloga(Integer idListaNaloga) {
        this.idListaNaloga = idListaNaloga;
    }

    public Integer getIdListaNaloga() {
        return idListaNaloga;
    }

    public void setIdListaNaloga(Integer idListaNaloga) {
        this.idListaNaloga = idListaNaloga;
    }

    public Nalog getIdNaloga() {
        return idNaloga;
    }

    public void setIdNaloga(Nalog idNaloga) {
        this.idNaloga = idNaloga;
    }

    public Tipnaloga getTipNaloga() {
        return tipNaloga;
    }

    public void setTipNaloga(Tipnaloga tipNaloga) {
        this.tipNaloga = tipNaloga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idListaNaloga != null ? idListaNaloga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Listanaloga)) {
            return false;
        }
        Listanaloga other = (Listanaloga) object;
        return (this.idListaNaloga != null || other.idListaNaloga == null) && (this.idListaNaloga == null || this.idListaNaloga.equals(other.idListaNaloga));
    }

    @Override
    public String toString() {
        return "model.Listanaloga[ idListaNaloga=" + idListaNaloga + " ]";
    }
    
}
