/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "smjedinica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Smjedinica.findAll", query = "SELECT s FROM Smjedinica s")
    , @NamedQuery(name = "Smjedinica.findByIdSmJedinice", query = "SELECT s FROM Smjedinica s WHERE s.idSmJedinice = :idSmJedinice")
    , @NamedQuery(name = "Smjedinica.findByRedniBrojSJ", query = "SELECT s FROM Smjedinica s WHERE s.redniBrojSJ = :redniBrojSJ")
    , @NamedQuery(name = "Smjedinica.findByNaziv", query = "SELECT s FROM Smjedinica s WHERE s.naziv = :naziv")
    , @NamedQuery(name = "Smjedinica.findBySprat", query = "SELECT s FROM Smjedinica s WHERE s.sprat = :sprat")
    , @NamedQuery(name = "Smjedinica.findByBrojProstorija", query = "SELECT s FROM Smjedinica s WHERE s.brojProstorija = :brojProstorija")
    , @NamedQuery(name = "Smjedinica.findByBrojSpSoba", query = "SELECT s FROM Smjedinica s WHERE s.brojSpSoba = :brojSpSoba")
    , @NamedQuery(name = "Smjedinica.findByBrKupatila", query = "SELECT s FROM Smjedinica s WHERE s.brKupatila = :brKupatila")
    , @NamedQuery(name = "Smjedinica.findByBrGlavnihLezaja", query = "SELECT s FROM Smjedinica s WHERE s.brGlavnihLezaja = :brGlavnihLezaja")
    , @NamedQuery(name = "Smjedinica.findByBrPomocnihLezaja", query = "SELECT s FROM Smjedinica s WHERE s.brPomocnihLezaja = :brPomocnihLezaja")
    , @NamedQuery(name = "Smjedinica.findByDatumIzmene", query = "SELECT s FROM Smjedinica s WHERE s.datumIzmene = :datumIzmene")
    , @NamedQuery(name = "Smjedinica.findByNapomene", query = "SELECT s FROM Smjedinica s WHERE s.napomene = :napomene")})
public class Smjedinica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSmJedinice")
    private Integer idSmJedinice;
    @Basic(optional = false)
    @Column(name = "RedniBrojSJ")
    private int redniBrojSJ;
    @Basic(optional = false)
    @Column(name = "Naziv")
    private String naziv;
    @Basic(optional = false)
    @Column(name = "Sprat")
    private int sprat;
    @Basic(optional = false)
    @Column(name = "BrojProstorija")
    private int brojProstorija;
    @Basic(optional = false)
    @Column(name = "BrojSpSoba")
    private int brojSpSoba;
    @Basic(optional = false)
    @Column(name = "BrKupatila")
    private int brKupatila;
    @Basic(optional = false)
    @Column(name = "BrGlavnihLezaja")
    private int brGlavnihLezaja;
    @Basic(optional = false)
    @Column(name = "BrPomocnihLezaja")
    private int brPomocnihLezaja;
    @Basic(optional = false)
    @Column(name = "DatumIzmene")
    @Temporal(TemporalType.DATE)
    private Date datumIzmene;
    @Basic(optional = false)
    @Column(name = "Napomene")
    private String napomene;
    @JoinColumn(name = "VrstaSmJed", referencedColumnName = "idVrstaSmJed")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Vrstasmjed vrstaSmJed;
    @JoinColumn(name = "Objekat", referencedColumnName = "idObjekta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Objekti objekat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSmJedinice", fetch = FetchType.LAZY)
    private List<Svojstvasmjed> svojstvasmjedList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSmJed", fetch = FetchType.LAZY)
    private List<Rezervacija> rezervacijaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSmJedinice", fetch = FetchType.LAZY)
    private List<Cenasmjed> cenasmjedList;

    public Smjedinica() {
    }

    public Smjedinica(Integer idSmJedinice) {
        this.idSmJedinice = idSmJedinice;
    }

    public Smjedinica(Integer idSmJedinice, int redniBrojSJ, String naziv, int sprat, int brojProstorija, int brojSpSoba, int brKupatila, int brGlavnihLezaja, int brPomocnihLezaja, Date datumIzmene, String napomene) {
        this.idSmJedinice = idSmJedinice;
        this.redniBrojSJ = redniBrojSJ;
        this.naziv = naziv;
        this.sprat = sprat;
        this.brojProstorija = brojProstorija;
        this.brojSpSoba = brojSpSoba;
        this.brKupatila = brKupatila;
        this.brGlavnihLezaja = brGlavnihLezaja;
        this.brPomocnihLezaja = brPomocnihLezaja;
        this.datumIzmene = datumIzmene;
        this.napomene = napomene;
    }

    public Integer getIdSmJedinice() {
        return idSmJedinice;
    }

    public void setIdSmJedinice(Integer idSmJedinice) {
        this.idSmJedinice = idSmJedinice;
    }

    public int getRedniBrojSJ() {
        return redniBrojSJ;
    }

    public void setRedniBrojSJ(int redniBrojSJ) {
        this.redniBrojSJ = redniBrojSJ;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getSprat() {
        return sprat;
    }

    public void setSprat(int sprat) {
        this.sprat = sprat;
    }

    public int getBrojProstorija() {
        return brojProstorija;
    }

    public void setBrojProstorija(int brojProstorija) {
        this.brojProstorija = brojProstorija;
    }

    public int getBrojSpSoba() {
        return brojSpSoba;
    }

    public void setBrojSpSoba(int brojSpSoba) {
        this.brojSpSoba = brojSpSoba;
    }

    public int getBrKupatila() {
        return brKupatila;
    }

    public void setBrKupatila(int brKupatila) {
        this.brKupatila = brKupatila;
    }

    public int getBrGlavnihLezaja() {
        return brGlavnihLezaja;
    }

    public void setBrGlavnihLezaja(int brGlavnihLezaja) {
        this.brGlavnihLezaja = brGlavnihLezaja;
    }

    public int getBrPomocnihLezaja() {
        return brPomocnihLezaja;
    }

    public void setBrPomocnihLezaja(int brPomocnihLezaja) {
        this.brPomocnihLezaja = brPomocnihLezaja;
    }

    public Date getDatumIzmene() {
        return datumIzmene;
    }

    public void setDatumIzmene(Date datumIzmene) {
        this.datumIzmene = datumIzmene;
    }

    public String getNapomene() {
        return napomene;
    }

    public void setNapomene(String napomene) {
        this.napomene = napomene;
    }

    public Vrstasmjed getVrstaSmJed() {
        return vrstaSmJed;
    }

    public void setVrstaSmJed(Vrstasmjed vrstaSmJed) {
        this.vrstaSmJed = vrstaSmJed;
    }

    public Objekti getObjekat() {
        return objekat;
    }

    public void setObjekat(Objekti objekat) {
        this.objekat = objekat;
    }

    @XmlTransient
    public List<Svojstvasmjed> getSvojstvasmjedList() {
        return svojstvasmjedList;
    }

    public void setSvojstvasmjedList(List<Svojstvasmjed> svojstvasmjedList) {
        this.svojstvasmjedList = svojstvasmjedList;
    }

    @XmlTransient
    public List<Rezervacija> getRezervacijaList() {
        return rezervacijaList;
    }

    public void setRezervacijaList(List<Rezervacija> rezervacijaList) {
        this.rezervacijaList = rezervacijaList;
    }

    @XmlTransient
    public List<Cenasmjed> getCenasmjedList() {
        return cenasmjedList;
    }

    public void setCenasmjedList(List<Cenasmjed> cenasmjedList) {
        this.cenasmjedList = cenasmjedList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSmJedinice != null ? idSmJedinice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Smjedinica)) {
            return false;
        }
        Smjedinica other = (Smjedinica) object;
        return (this.idSmJedinice != null || other.idSmJedinice == null) && (this.idSmJedinice == null || this.idSmJedinice.equals(other.idSmJedinice));
    }

    @Override
    public String toString() {
        return "model.Smjedinica[ idSmJedinice=" + idSmJedinice + " ]";
    }
    
}
