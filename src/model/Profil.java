/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "profil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profil.findAll", query = "SELECT p FROM Profil p")
    , @NamedQuery(name = "Profil.findByIdProfila", query = "SELECT p FROM Profil p WHERE p.idProfila = :idProfila")
    , @NamedQuery(name = "Profil.findByImeProfila", query = "SELECT p FROM Profil p WHERE p.imeProfila = :imeProfila")
    , @NamedQuery(name = "Profil.findByPrezimeProfila", query = "SELECT p FROM Profil p WHERE p.prezimeProfila = :prezimeProfila")
    , @NamedQuery(name = "Profil.findByBrTelefonaProfila", query = "SELECT p FROM Profil p WHERE p.brTelefonaProfila = :brTelefonaProfila")
    , @NamedQuery(name = "Profil.findByEmail", query = "SELECT p FROM Profil p WHERE p.email = :email")})
public class Profil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProfila")
    private Integer idProfila;
    @Basic(optional = false)
    @Column(name = "ImeProfila")
    private String imeProfila;
    @Basic(optional = false)
    @Column(name = "PrezimeProfila")
    private String prezimeProfila;
    @Basic(optional = false)
    @Column(name = "BrTelefonaProfila")
    private String brTelefonaProfila;
    @Basic(optional = false)
    @Column(name = "Email")
    private String email;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "odgLice1", fetch = FetchType.LAZY)
    private List<Objekti> objektiList;
    @OneToMany(mappedBy = "odgLice2", fetch = FetchType.LAZY)
    private List<Objekti> objektiList1;
    @JoinColumn(name = "Kompanija", referencedColumnName = "idKompanije")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Kompanija kompanija;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profil", fetch = FetchType.LAZY)
    private List<Nalog> nalogList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "autorIzmene", fetch = FetchType.LAZY)
    private List<Naplata> naplataList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "autorPoslednjeIzmene", fetch = FetchType.LAZY)
    private List<Rezervacija> rezervacijaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "autorUnosa", fetch = FetchType.LAZY)
    private List<Cenasmjed> cenasmjedList;

    public Profil() {
    }

    public Profil(Integer idProfila) {
        this.idProfila = idProfila;
    }

    public Profil(Integer idProfila, String imeProfila, String prezimeProfila, String brTelefonaProfila, String email) {
        this.idProfila = idProfila;
        this.imeProfila = imeProfila;
        this.prezimeProfila = prezimeProfila;
        this.brTelefonaProfila = brTelefonaProfila;
        this.email = email;
    }

    public Integer getIdProfila() {
        return idProfila;
    }

    public void setIdProfila(Integer idProfila) {
        this.idProfila = idProfila;
    }

    public String getImeProfila() {
        return imeProfila;
    }

    public void setImeProfila(String imeProfila) {
        this.imeProfila = imeProfila;
    }

    public String getPrezimeProfila() {
        return prezimeProfila;
    }

    public void setPrezimeProfila(String prezimeProfila) {
        this.prezimeProfila = prezimeProfila;
    }

    public String getBrTelefonaProfila() {
        return brTelefonaProfila;
    }

    public void setBrTelefonaProfila(String brTelefonaProfila) {
        this.brTelefonaProfila = brTelefonaProfila;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Objekti> getObjektiList() {
        return objektiList;
    }

    public void setObjektiList(List<Objekti> objektiList) {
        this.objektiList = objektiList;
    }

    @XmlTransient
    public List<Objekti> getObjektiList1() {
        return objektiList1;
    }

    public void setObjektiList1(List<Objekti> objektiList1) {
        this.objektiList1 = objektiList1;
    }

    public Kompanija getKompanija() {
        return kompanija;
    }

    public void setKompanija(Kompanija kompanija) {
        this.kompanija = kompanija;
    }

    @XmlTransient
    public List<Nalog> getNalogList() {
        return nalogList;
    }

    public void setNalogList(List<Nalog> nalogList) {
        this.nalogList = nalogList;
    }

    @XmlTransient
    public List<Naplata> getNaplataList() {
        return naplataList;
    }

    public void setNaplataList(List<Naplata> naplataList) {
        this.naplataList = naplataList;
    }

    @XmlTransient
    public List<Rezervacija> getRezervacijaList() {
        return rezervacijaList;
    }

    public void setRezervacijaList(List<Rezervacija> rezervacijaList) {
        this.rezervacijaList = rezervacijaList;
    }

    @XmlTransient
    public List<Cenasmjed> getCenasmjedList() {
        return cenasmjedList;
    }

    public void setCenasmjedList(List<Cenasmjed> cenasmjedList) {
        this.cenasmjedList = cenasmjedList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfila != null ? idProfila.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profil)) {
            return false;
        }
        Profil other = (Profil) object;
        return (this.idProfila != null || other.idProfila == null) && (this.idProfila == null || this.idProfila.equals(other.idProfila));
    }

    @Override
    public String toString() {
        return "model.Profil[ idProfila=" + idProfila + " ]";
    }
    
}
