/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "vrstasmjed")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vrstasmjed.findAll", query = "SELECT v FROM Vrstasmjed v")
    , @NamedQuery(name = "Vrstasmjed.findByIdVrstaSmJed", query = "SELECT v FROM Vrstasmjed v WHERE v.idVrstaSmJed = :idVrstaSmJed")
    , @NamedQuery(name = "Vrstasmjed.findByVrstaSmJedinice", query = "SELECT v FROM Vrstasmjed v WHERE v.vrstaSmJedinice = :vrstaSmJedinice")})
public class Vrstasmjed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idVrstaSmJed")
    private Integer idVrstaSmJed;
    @Basic(optional = false)
    @Column(name = "VrstaSmJedinice")
    private String vrstaSmJedinice;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vrstaSmJed", fetch = FetchType.LAZY)
    private List<Smjedinica> smjedinicaList;

    public Vrstasmjed() {
    }

    public Vrstasmjed(Integer idVrstaSmJed) {
        this.idVrstaSmJed = idVrstaSmJed;
    }

    public Vrstasmjed(Integer idVrstaSmJed, String vrstaSmJedinice) {
        this.idVrstaSmJed = idVrstaSmJed;
        this.vrstaSmJedinice = vrstaSmJedinice;
    }

    public Integer getIdVrstaSmJed() {
        return idVrstaSmJed;
    }

    public void setIdVrstaSmJed(Integer idVrstaSmJed) {
        this.idVrstaSmJed = idVrstaSmJed;
    }

    public String getVrstaSmJedinice() {
        return vrstaSmJedinice;
    }

    public void setVrstaSmJedinice(String vrstaSmJedinice) {
        this.vrstaSmJedinice = vrstaSmJedinice;
    }

    @XmlTransient
    public List<Smjedinica> getSmjedinicaList() {
        return smjedinicaList;
    }

    public void setSmjedinicaList(List<Smjedinica> smjedinicaList) {
        this.smjedinicaList = smjedinicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVrstaSmJed != null ? idVrstaSmJed.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vrstasmjed)) {
            return false;
        }
        Vrstasmjed other = (Vrstasmjed) object;
        return (this.idVrstaSmJed != null || other.idVrstaSmJed == null) && (this.idVrstaSmJed == null || this.idVrstaSmJed.equals(other.idVrstaSmJed));
    }

    @Override
    public String toString() {
        return "model.Vrstasmjed[ idVrstaSmJed=" + idVrstaSmJed + " ]";
    }
    
}
