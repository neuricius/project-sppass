/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "cenasmjed")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cenasmjed.findAll", query = "SELECT c FROM Cenasmjed c")
    , @NamedQuery(name = "Cenasmjed.findByIdCenaSmJed", query = "SELECT c FROM Cenasmjed c WHERE c.idCenaSmJed = :idCenaSmJed")
    , @NamedQuery(name = "Cenasmjed.findByPocetakCene", query = "SELECT c FROM Cenasmjed c WHERE c.pocetakCene = :pocetakCene")
    , @NamedQuery(name = "Cenasmjed.findByKrajCene", query = "SELECT c FROM Cenasmjed c WHERE c.krajCene = :krajCene")
    , @NamedQuery(name = "Cenasmjed.findByCena", query = "SELECT c FROM Cenasmjed c WHERE c.cena = :cena")
    , @NamedQuery(name = "Cenasmjed.findByDatumUnosa", query = "SELECT c FROM Cenasmjed c WHERE c.datumUnosa = :datumUnosa")})
public class Cenasmjed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCenaSmJed")
    private Integer idCenaSmJed;
    @Basic(optional = false)
    @Column(name = "PocetakCene")
    @Temporal(TemporalType.DATE)
    private Date pocetakCene;
    @Basic(optional = false)
    @Column(name = "KrajCene")
    @Temporal(TemporalType.DATE)
    private Date krajCene;
    @Basic(optional = false)
    @Column(name = "Cena")
    private double cena;
    @Basic(optional = false)
    @Column(name = "datumUnosa")
    @Temporal(TemporalType.DATE)
    private Date datumUnosa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cenaNocenja", fetch = FetchType.LAZY)
    private List<Naplata> naplataList;
    @JoinColumn(name = "autorUnosa", referencedColumnName = "idProfila")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profil autorUnosa;
    @JoinColumn(name = "idSmJedinice", referencedColumnName = "idSmJedinice")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Smjedinica idSmJedinice;

    public Cenasmjed() {
    }

    public Cenasmjed(Integer idCenaSmJed) {
        this.idCenaSmJed = idCenaSmJed;
    }

    public Cenasmjed(Integer idCenaSmJed, Date pocetakCene, Date krajCene, double cena, Date datumUnosa) {
        this.idCenaSmJed = idCenaSmJed;
        this.pocetakCene = pocetakCene;
        this.krajCene = krajCene;
        this.cena = cena;
        this.datumUnosa = datumUnosa;
    }

    public Integer getIdCenaSmJed() {
        return idCenaSmJed;
    }

    public void setIdCenaSmJed(Integer idCenaSmJed) {
        this.idCenaSmJed = idCenaSmJed;
    }

    public Date getPocetakCene() {
        return pocetakCene;
    }

    public void setPocetakCene(Date pocetakCene) {
        this.pocetakCene = pocetakCene;
    }

    public Date getKrajCene() {
        return krajCene;
    }

    public void setKrajCene(Date krajCene) {
        this.krajCene = krajCene;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public Date getDatumUnosa() {
        return datumUnosa;
    }

    public void setDatumUnosa(Date datumUnosa) {
        this.datumUnosa = datumUnosa;
    }

    @XmlTransient
    public List<Naplata> getNaplataList() {
        return naplataList;
    }

    public void setNaplataList(List<Naplata> naplataList) {
        this.naplataList = naplataList;
    }

    public Profil getAutorUnosa() {
        return autorUnosa;
    }

    public void setAutorUnosa(Profil autorUnosa) {
        this.autorUnosa = autorUnosa;
    }

    public Smjedinica getIdSmJedinice() {
        return idSmJedinice;
    }

    public void setIdSmJedinice(Smjedinica idSmJedinice) {
        this.idSmJedinice = idSmJedinice;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCenaSmJed != null ? idCenaSmJed.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cenasmjed)) {
            return false;
        }
        Cenasmjed other = (Cenasmjed) object;
        return (this.idCenaSmJed != null || other.idCenaSmJed == null) && (this.idCenaSmJed == null || this.idCenaSmJed.equals(other.idCenaSmJed));
    }

    @Override
    public String toString() {
        return "model.Cenasmjed[ idCenaSmJed=" + idCenaSmJed + " ]";
    }
    
}
