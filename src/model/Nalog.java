/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "nalog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nalog.findAll", query = "SELECT n FROM Nalog n")
    , @NamedQuery(name = "Nalog.findByIdNalog", query = "SELECT n FROM Nalog n WHERE n.idNalog = :idNalog")
    , @NamedQuery(name = "Nalog.findByKorisnickoIme", query = "SELECT n FROM Nalog n WHERE n.korisnickoIme = :korisnickoIme")
    , @NamedQuery(name = "Nalog.findBySifra", query = "SELECT n FROM Nalog n WHERE n.sifra = :sifra")})
public class Nalog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNalog")
    private Integer idNalog;
    @Basic(optional = false)
    @Column(name = "KorisnickoIme")
    private String korisnickoIme;
    @Basic(optional = false)
    @Column(name = "Sifra")
    private String sifra;
    @JoinColumn(name = "Kompanija", referencedColumnName = "idKompanije")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Kompanija kompanija;
    @JoinColumn(name = "Profil", referencedColumnName = "idProfila")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profil profil;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNaloga", fetch = FetchType.LAZY)
    private List<Listanaloga> listanalogaList;

    public Nalog() {
    }

    public Nalog(Integer idNalog) {
        this.idNalog = idNalog;
    }

    public Nalog(Integer idNalog, String korisnickoIme, String sifra) {
        this.idNalog = idNalog;
        this.korisnickoIme = korisnickoIme;
        this.sifra = sifra;
    }

    public Integer getIdNalog() {
        return idNalog;
    }

    public void setIdNalog(Integer idNalog) {
        this.idNalog = idNalog;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public Kompanija getKompanija() {
        return kompanija;
    }

    public void setKompanija(Kompanija kompanija) {
        this.kompanija = kompanija;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    @XmlTransient
    public List<Listanaloga> getListanalogaList() {
        return listanalogaList;
    }

    public void setListanalogaList(List<Listanaloga> listanalogaList) {
        this.listanalogaList = listanalogaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNalog != null ? idNalog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nalog)) {
            return false;
        }
        Nalog other = (Nalog) object;
        return (this.idNalog != null || other.idNalog == null) && (this.idNalog == null || this.idNalog.equals(other.idNalog));
    }

    @Override
    public String toString() {
        return "model.Nalog[ idNalog=" + idNalog + " ]";
    }
    
}
