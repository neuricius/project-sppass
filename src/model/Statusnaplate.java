/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "statusnaplate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statusnaplate.findAll", query = "SELECT s FROM Statusnaplate s")
    , @NamedQuery(name = "Statusnaplate.findByIdStatusNaplate", query = "SELECT s FROM Statusnaplate s WHERE s.idStatusNaplate = :idStatusNaplate")
    , @NamedQuery(name = "Statusnaplate.findByStatusNaplate", query = "SELECT s FROM Statusnaplate s WHERE s.statusNaplate = :statusNaplate")})
public class Statusnaplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idStatusNaplate")
    private Integer idStatusNaplate;
    @Basic(optional = false)
    @Column(name = "StatusNaplate")
    private String statusNaplate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusNaplate", fetch = FetchType.LAZY)
    private List<Naplata> naplataList;

    public Statusnaplate() {
    }

    public Statusnaplate(Integer idStatusNaplate) {
        this.idStatusNaplate = idStatusNaplate;
    }

    public Statusnaplate(Integer idStatusNaplate, String statusNaplate) {
        this.idStatusNaplate = idStatusNaplate;
        this.statusNaplate = statusNaplate;
    }

    public Integer getIdStatusNaplate() {
        return idStatusNaplate;
    }

    public void setIdStatusNaplate(Integer idStatusNaplate) {
        this.idStatusNaplate = idStatusNaplate;
    }

    public String getStatusNaplate() {
        return statusNaplate;
    }

    public void setStatusNaplate(String statusNaplate) {
        this.statusNaplate = statusNaplate;
    }

    @XmlTransient
    public List<Naplata> getNaplataList() {
        return naplataList;
    }

    public void setNaplataList(List<Naplata> naplataList) {
        this.naplataList = naplataList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusNaplate != null ? idStatusNaplate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusnaplate)) {
            return false;
        }
        Statusnaplate other = (Statusnaplate) object;
        return (this.idStatusNaplate != null || other.idStatusNaplate == null) && (this.idStatusNaplate == null || this.idStatusNaplate.equals(other.idStatusNaplate));
    }

    @Override
    public String toString() {
        return "model.Statusnaplate[ idStatusNaplate=" + idStatusNaplate + " ]";
    }
    
}
