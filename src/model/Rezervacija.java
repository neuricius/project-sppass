/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Grupa2
 */
@Entity
@Table(name = "rezervacija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rezervacija.findAll", query = "SELECT r FROM Rezervacija r")
    , @NamedQuery(name = "Rezervacija.findByIdRezervacija", query = "SELECT r FROM Rezervacija r WHERE r.idRezervacija = :idRezervacija")
    , @NamedQuery(name = "Rezervacija.findByPrijava", query = "SELECT r FROM Rezervacija r WHERE r.prijava = :prijava")
    , @NamedQuery(name = "Rezervacija.findByOdjava", query = "SELECT r FROM Rezervacija r WHERE r.odjava = :odjava")
    , @NamedQuery(name = "Rezervacija.findByBrojOdraslih", query = "SELECT r FROM Rezervacija r WHERE r.brojOdraslih = :brojOdraslih")
    , @NamedQuery(name = "Rezervacija.findByBrojDece", query = "SELECT r FROM Rezervacija r WHERE r.brojDece = :brojDece")
    , @NamedQuery(name = "Rezervacija.findByDatumPoslednjeIzmene", query = "SELECT r FROM Rezervacija r WHERE r.datumPoslednjeIzmene = :datumPoslednjeIzmene")
    , @NamedQuery(name = "Rezervacija.findByBeleskeORez", query = "SELECT r FROM Rezervacija r WHERE r.beleskeORez = :beleskeORez")})
public class Rezervacija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRezervacija")
    private Integer idRezervacija;
    @Basic(optional = false)
    @Column(name = "Prijava")
    @Temporal(TemporalType.DATE)
    private Date prijava;
    @Basic(optional = false)
    @Column(name = "Odjava")
    @Temporal(TemporalType.DATE)
    private Date odjava;
    @Column(name = "brojOdraslih")
    private Integer brojOdraslih;
    @Column(name = "brojDece")
    private Integer brojDece;
    @Column(name = "datumPoslednjeIzmene")
    @Temporal(TemporalType.DATE)
    private Date datumPoslednjeIzmene;
    @Column(name = "beleskeORez")
    private String beleskeORez;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRezervacija", fetch = FetchType.LAZY)
    private List<Naplata> naplataList;
    @JoinColumn(name = "autorPoslednjeIzmene", referencedColumnName = "idProfila")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profil autorPoslednjeIzmene;
    @JoinColumn(name = "idKlijent", referencedColumnName = "idKlijent")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Klijent idKlijent;
    @JoinColumn(name = "idSmJed", referencedColumnName = "idSmJedinice")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Smjedinica idSmJed;
    @JoinColumn(name = "idStatRez", referencedColumnName = "idStatRez")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Statusrezervacije idStatRez;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idjRezervacije", fetch = FetchType.LAZY)
    private List<Gostiposobama> gostiposobamaList;

    public Rezervacija() {
    }

    public Rezervacija(Integer idRezervacija) {
        this.idRezervacija = idRezervacija;
    }

    public Rezervacija(Integer idRezervacija, Date prijava, Date odjava) {
        this.idRezervacija = idRezervacija;
        this.prijava = prijava;
        this.odjava = odjava;
    }

    public Integer getIdRezervacija() {
        return idRezervacija;
    }

    public void setIdRezervacija(Integer idRezervacija) {
        this.idRezervacija = idRezervacija;
    }

    public Date getPrijava() {
        return prijava;
    }

    public void setPrijava(Date prijava) {
        this.prijava = prijava;
    }

    public Date getOdjava() {
        return odjava;
    }

    public void setOdjava(Date odjava) {
        this.odjava = odjava;
    }

    public Integer getBrojOdraslih() {
        return brojOdraslih;
    }

    public void setBrojOdraslih(Integer brojOdraslih) {
        this.brojOdraslih = brojOdraslih;
    }

    public Integer getBrojDece() {
        return brojDece;
    }

    public void setBrojDece(Integer brojDece) {
        this.brojDece = brojDece;
    }

    public Date getDatumPoslednjeIzmene() {
        return datumPoslednjeIzmene;
    }

    public void setDatumPoslednjeIzmene(Date datumPoslednjeIzmene) {
        this.datumPoslednjeIzmene = datumPoslednjeIzmene;
    }

    public String getBeleskeORez() {
        return beleskeORez;
    }

    public void setBeleskeORez(String beleskeORez) {
        this.beleskeORez = beleskeORez;
    }

    @XmlTransient
    public List<Naplata> getNaplataList() {
        return naplataList;
    }

    public void setNaplataList(List<Naplata> naplataList) {
        this.naplataList = naplataList;
    }

    public Profil getAutorPoslednjeIzmene() {
        return autorPoslednjeIzmene;
    }

    public void setAutorPoslednjeIzmene(Profil autorPoslednjeIzmene) {
        this.autorPoslednjeIzmene = autorPoslednjeIzmene;
    }

    public Klijent getIdKlijent() {
        return idKlijent;
    }

    public void setIdKlijent(Klijent idKlijent) {
        this.idKlijent = idKlijent;
    }

    public Smjedinica getIdSmJed() {
        return idSmJed;
    }

    public void setIdSmJed(Smjedinica idSmJed) {
        this.idSmJed = idSmJed;
    }

    public Statusrezervacije getIdStatRez() {
        return idStatRez;
    }

    public void setIdStatRez(Statusrezervacije idStatRez) {
        this.idStatRez = idStatRez;
    }

    @XmlTransient
    public List<Gostiposobama> getGostiposobamaList() {
        return gostiposobamaList;
    }

    public void setGostiposobamaList(List<Gostiposobama> gostiposobamaList) {
        this.gostiposobamaList = gostiposobamaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRezervacija != null ? idRezervacija.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rezervacija)) {
            return false;
        }
        Rezervacija other = (Rezervacija) object;
        return (this.idRezervacija != null || other.idRezervacija == null) && (this.idRezervacija == null || this.idRezervacija.equals(other.idRezervacija));
    }

    @Override
    public String toString() {
        return "model.Rezervacija[ idRezervacija=" + idRezervacija + " ]";
    }
    
}
