/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller2;

import com.gluonhq.charm.glisten.control.AutoCompleteTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

/**
 * FXML Controller class
 *
 * @author Neuricius
 */
public class LoginViewFXMLController implements Initializable {

    @FXML
    private Button okB;
    @FXML
    private Button cancelB;
    @FXML
    private Label userNameL;
    @FXML
    private Label passWordL;
    @FXML
    private PasswordField passWordPF;
    @FXML
    private AutoCompleteTextField<?> userNameTF;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loginProvera(ActionEvent event) {
    }

    @FXML
    private void izlazIzPrograma(ActionEvent event) {
    }
    
}
