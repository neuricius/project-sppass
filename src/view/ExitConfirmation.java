/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author Nebojsa Matic
 */
public class ExitConfirmation {

    public static void programExit() {
        ConfirmationDialog.confirmAction("Da li ste sigurni da zelite da izadjete?", "Izlazak iz aplikacije", "Da", "Ne");
        if (ConfirmationDialog.actionConfirmed == true) {
            System.exit(0);
        } else {
            ConfirmationDialog.conDiaStage.close();
        }
    }
}
