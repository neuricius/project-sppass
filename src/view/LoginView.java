/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.TextAlignment;
import components.DecoratedButton;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

/**
 *
 * @author Nebojsa Matic
 */
public class LoginView extends Stage {

    private static AutoCompletionBinding<String> autoCompletionBinding;
    private static String[] suggestionsLogin = {"neo", "daki"};
    private static Set<String> suggestionsLoginSet = new HashSet<>(Arrays.asList(suggestionsLogin));
    public static TextField txtUser, txtPass;
    public static Scene loginScene;
    Label userL, passL;
    public static Button btnOK, btnCancel;
    HBox userHBox, passHBox, buttonHBox;
    VBox mainVBox;

    public LoginView() {
// labela korisnicko ime
        userL = new Label("Korisnicko ime: ");
        userL.setTextAlignment(TextAlignment.RIGHT);
//        userL.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(2), new Insets(0))));
        userL.setPrefWidth(90);
        userL.setAlignment(Pos.BOTTOM_RIGHT);

// polje za unos korisnickog imena
        txtUser = new TextField();
        txtUser.setPrefWidth(200);
        txtUser.setPromptText("Unesite korisnicko ime");
        autoCompletionBinding = TextFields.bindAutoCompletion(txtUser, suggestionsLoginSet);
        txtUser.setOnKeyPressed((KeyEvent ke) -> {
            switch (ke.getCode()) {
                case TAB:
                    autoCompletionLearnWord(txtUser, txtUser.getText().trim());
                    break;
                default:
                    break;
            }
        });



// labela za sifru
        passL = new Label("Sifra: ");
        passL.setTextAlignment(TextAlignment.RIGHT);
//        passL.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(2), new Insets(0))));
        passL.setPrefWidth(40);
        passL.setAlignment(Pos.BOTTOM_RIGHT);

// polje za unos sifre
        txtPass = new PasswordField();
        txtPass.setPrefWidth(200);
        txtPass.setPromptText("Unesite Vasu sifru");
        

// dugme za prijavu/ok
        btnOK = new DecoratedButton("Prijava");
        btnOK.setMinWidth(75);
        btnOK.setGraphic(new ImageView(new Image("/resources/ikonaYes.png", 20, 20, false, false)));
        

// dugme za otkazivanje/cancel
        btnCancel = new DecoratedButton("Otkazi");
        btnCancel.setMinWidth(75);
        btnCancel.setGraphic(new ImageView(new Image("/resources/ikonaNo.png", 20, 20, false, false)));
        

// panel za korisnicko ime
        userHBox = new HBox(20, userL, txtUser);
        userHBox.setPadding(new Insets(15,5,5,0));
        userHBox.setSpacing(3);
        userHBox.setAlignment(Pos.CENTER_RIGHT);

// panel za sifru
        passHBox = new HBox(20, passL, txtPass);
        passHBox.setPadding(new Insets(5));
        passHBox.setSpacing(3);
        passHBox.setAlignment(Pos.CENTER_RIGHT);

// panel za dugmad
        buttonHBox = new HBox(20, btnOK, btnCancel);
        buttonHBox.setPadding(new Insets(10));
        buttonHBox.setAlignment(Pos.BOTTOM_RIGHT);

// panel za slaganje ostalih panela
        mainVBox = new VBox(5, userHBox, passHBox, buttonHBox);
//        mainVBox.setAlignment(Pos.BOTTOM_CENTER);

        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaLogin2.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainVBox.setBackground(background);
        //KRAJ KODA
        //

// Set the stage
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        loginScene = new Scene(mainVBox);
    }

    private void autoCompletionLearnWord(TextField tf, String newWord) {
        suggestionsLoginSet.add(newWord);

        // we dispose the old binding and recreate a new binding
        if (autoCompletionBinding != null) {
            autoCompletionBinding.dispose();
        }
        autoCompletionBinding = TextFields.bindAutoCompletion(tf, suggestionsLoginSet);
    }
}
