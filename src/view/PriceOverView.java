/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Cenasmjed;
import model.Profil;
import static util.HibernateUtil.skupiListuCena;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class PriceOverView extends Stage {

    List<Cenasmjed> priceList;

    public static Button refreshB, addPriceB, printB;

    BorderPane mainBP;

    public static TableView<Cenasmjed> priceTable;

    public static TableColumn<Cenasmjed, Integer> roomPriceIDTC;
    public static TableColumn<Cenasmjed, Float> amountTC;
    public static TableColumn<Cenasmjed, Date> priceStartDateTC, priceEndTC, entryDateTC;
    public static TableColumn<Cenasmjed, String> priceInfoTC, roomNameTC, roomNumberTC, entryAuthorTC, numberOfChildrenTC;
    public static TableColumn<Cenasmjed, Profil> entryInfoTC;

    public static Scene priceOverviewScene;

    public PriceOverView() {

        mainBP = new BorderPane();

        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Sortiraj");

        addPriceB = new Button();
        addPriceB.setMinSize(130, 25);
        addPriceB.setMaxSize(130, 25);
        addPriceB.setText("Dodaj");

        printB = new Button();
        printB.setMinSize(130, 25);
        printB.setMaxSize(130, 25);
        printB.setText("Stampaj");

        HBox boxZaDugmad = new HBox(refreshB, addPriceB, printB);
        mainBP.setBottom(boxZaDugmad);

        // CENTAR
        priceTable = new TableView<>();
        priceTable.setEditable(false);

        priceList = skupiListuCena(facilityName());
        ObservableList<Cenasmjed> data = FXCollections.observableArrayList(priceList);

        //kolone
        //osn podaci o ceni
        //glavna
        priceInfoTC = new TableColumn<>("Rezervacija");

        //redni broj (ID) cene
        roomPriceIDTC = new TableColumn<>("#");
        roomPriceIDTC.setStyle("-fx-alignment: CENTER;");
        roomPriceIDTC.setPrefWidth(25);
        roomPriceIDTC.setCellValueFactory(new PropertyValueFactory<>("idCenaSmJed"));

        //
        // naziv sobe
        roomNameTC = new TableColumn<>("Sm. Jed");
        roomNameTC.setStyle("-fx-alignment: CENTER;");
        roomNameTC.setPrefWidth(75);
        roomNameTC.setCellValueFactory(
                (CellDataFeatures<Cenasmjed, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdSmJedinice().getNaziv()));

        //
        priceInfoTC.getColumns().addAll(roomPriceIDTC, roomNameTC);
        //

        //period boravka
        //glavna kol
        TableColumn<Cenasmjed, Date> periodTrajanja = new TableColumn<>("Period Vazenja");

        //subkolone
        priceStartDateTC = new TableColumn<>("Pocetak");
        priceStartDateTC.setStyle("-fx-alignment: CENTER;");
        priceStartDateTC.setPrefWidth(80);
        priceStartDateTC.setCellValueFactory(new PropertyValueFactory<>("pocetakCene"));

        priceEndTC = new TableColumn<>("Kraj");
        priceEndTC.setStyle("-fx-alignment: CENTER;");
        priceEndTC.setPrefWidth(80);
        priceEndTC.setCellValueFactory(new PropertyValueFactory<>("krajCene"));

        periodTrajanja.getColumns().addAll(priceStartDateTC, priceEndTC);

        //kolona za iznos
        amountTC = new TableColumn<>("Iznos");
        amountTC.setStyle("-fx-alignment: CENTER;");
        amountTC.setPrefWidth(40);
        amountTC.setCellValueFactory(new PropertyValueFactory<>("cena"));

        //podaci o izmeni
        //glavna
        entryInfoTC = new TableColumn<>("Izmene");

        //subkolone
        entryAuthorTC = new TableColumn<>("Autor");
        entryAuthorTC.setPrefWidth(80);
        entryAuthorTC.setCellValueFactory(
                (CellDataFeatures<Cenasmjed, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getAutorUnosa().getImeProfila()));

        entryDateTC = new TableColumn<>("Datum");
        entryDateTC.setStyle("-fx-alignment: CENTER;");
        entryDateTC.setPrefWidth(80);
        entryDateTC.setCellValueFactory(new PropertyValueFactory<>("datumUnosa"));

        entryInfoTC.getColumns().addAll(entryAuthorTC, entryDateTC);

        priceTable.setItems(data);
        priceTable.getColumns().addAll(priceInfoTC, periodTrajanja, amountTC, entryInfoTC);

        //kod za izbor po kojoj koloni da se sortira tabela
        mainBP.setCenter(priceTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        priceOverviewScene = new Scene(mainBP);
    }

}
