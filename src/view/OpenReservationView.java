/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Nebojsa Matic
 */
public class OpenReservationView extends Stage {

// izmeniti u izmeni, stampaj i zatvori
    public static Button printResB, checkOutResB, closeViewB, gprB;

    Label clientInfoL, clientIdentificationL, roomInfoL, paymentInfoL,
            reservationHeaderL, clientNumberL, blankLabel, clientFirstNameL, 
            clientLastNameL, clientResidenceCityL, clientResidenceCountryL, 
            clientEmailL, clientDocumentL, clientDocumentNumberL, roomNumberL, 
            arrivalDateL, departureDateL, numberOfAdultsL, numberOfChildrenL, 
            numberOfNightsL, guestNotesL, pricePerPeriodL, totalPriceL, 
            otherExpensesL, subTotaL, discountL, totalBalanceL, currentBalanceL, 
            remainBalanceL, entryAuthorL, entryDateL, reservationStatusL;

    public static Label clientNumberROL, clientFirstNameROL, clientLastNameROL,
            clientResidenceCityROL, clientResidenceCountryROL, clientEmailROL,
            clientDocumentValueL, resNotesROL, pricePerPeriodROL,
            totalPriceROL, otherExpensesROL, subTotalROL,
            totalBalanceROL, currentBalanceROL, remainBalanceROL,
            numberOfAdultsROL, numberOfChildrenROL, clientDocumentTypeROL, clientRoomROL,
            numberOfNightsROL, discountROL, reservationStatusROL,
            resStartDateROL, resEndDateROL, entryAuthorNameL, entryDateValueL;

    public static CheckBox clientIsGuest;

    public static VBox mainVBox;
    
    GridPane buttonGP;

    public static Scene reservationScene;

    public OpenReservationView() {

        // glavni panel
        mainVBox = new VBox();
        mainVBox.setMinSize(800, 415);
        mainVBox.setMaxSize(800, 415);
        mainVBox.setAlignment(Pos.TOP_CENTER);

        //LEVI ELEMENT
        //komponente
        //PODACI O KLIJENTU
        //labele
        clientInfoL = new Label();
        clientInfoL.setMinSize(200, 50);
        clientInfoL.setMaxSize(200, 50);
        clientInfoL.setText("Podaci o klijentu");
        clientInfoL.setFont(Font.font("System", FontWeight.BOLD, 20));
        clientInfoL.setAlignment(Pos.CENTER);
        clientInfoL.setTextAlignment(TextAlignment.RIGHT);
        clientInfoL.setPadding(new Insets(4));

        clientNumberL = new Label();
        clientNumberL.setMinHeight(25);
        clientNumberL.setText("Broj klijenta");
        clientNumberL.setTextAlignment(TextAlignment.RIGHT);

        blankLabel = new Label();
        blankLabel.setMinHeight(25);
        blankLabel.setText("");
        blankLabel.setTextAlignment(TextAlignment.RIGHT);

        clientFirstNameL = new Label();
        clientFirstNameL.setMinHeight(25);
        clientFirstNameL.setText("Ime");
        clientFirstNameL.setTextAlignment(TextAlignment.RIGHT);

        clientLastNameL = new Label();
        clientLastNameL.setMinHeight(25);
        clientLastNameL.setText("Prezime");
        clientLastNameL.setTextAlignment(TextAlignment.RIGHT);

        clientResidenceCityL = new Label();
        clientResidenceCityL.setMinHeight(25);
        clientResidenceCityL.setText("Mesto");
        clientResidenceCityL.setTextAlignment(TextAlignment.RIGHT);

        clientResidenceCountryL = new Label();
        clientResidenceCountryL.setMinHeight(25);
        clientResidenceCountryL.setText("Drzava");
        clientResidenceCountryL.setTextAlignment(TextAlignment.RIGHT);

        clientEmailL = new Label();
        clientEmailL.setMinHeight(25);
        clientEmailL.setText("Email adresa");
        clientEmailL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        clientNumberROL = new ReadOnlyLabel();
        clientNumberROL.setMinSize(150, 25);
        clientNumberROL.setMaxSize(150, 25);

        clientIsGuest = new CheckBox("Klijent je gost");
        clientIsGuest.setMinSize(100, 25);
        clientIsGuest.setMaxSize(100, 25);
        clientIsGuest.setDisable(true);
        //prebaciti u kontrolor
//        clientIsGuest.setSelected(true);

        clientFirstNameROL = new ReadOnlyLabel();
        clientFirstNameROL.setMinSize(150, 25);
        clientFirstNameROL.setMaxSize(150, 25);

        clientLastNameROL = new ReadOnlyLabel();
        clientLastNameROL.setMinSize(150, 25);
        clientLastNameROL.setMaxSize(150, 25);

        clientResidenceCityROL = new ReadOnlyLabel();
        clientResidenceCityROL.setMinSize(150, 25);
        clientResidenceCityROL.setMaxSize(150, 25);

        clientResidenceCountryROL = new ReadOnlyLabel();
        clientResidenceCountryROL.setMinSize(150, 25);
        clientResidenceCountryROL.setMaxSize(150, 25);

        clientEmailROL = new ReadOnlyLabel();
        clientEmailROL.setMinSize(150, 25);
        clientEmailROL.setMaxSize(150, 25);

        //IDENTIFIKACIJA
        //labele
        clientIdentificationL = new Label();
        clientIdentificationL.setMinSize(230, 50);
        clientIdentificationL.setMaxSize(230, 50);
        clientIdentificationL.setText("Identifikacija klijenta");
        clientIdentificationL.setFont(Font.font("System", FontWeight.BOLD, 20));
        clientIdentificationL.setAlignment(Pos.CENTER);
        clientIdentificationL.setTextAlignment(TextAlignment.CENTER);
        clientIdentificationL.setPadding(new Insets(4));

        clientDocumentL = new Label();
        clientDocumentL.setMinHeight(25);
        clientDocumentL.setText("Vrsta dokumenta");
        clientDocumentL.setTextAlignment(TextAlignment.RIGHT);

        clientDocumentNumberL = new Label();
        clientDocumentNumberL.setMinHeight(25);
        clientDocumentNumberL.setText("  Broj dokumenta");
        clientDocumentNumberL.setTextAlignment(TextAlignment.RIGHT);

        //definisati vrste dokumenata u bazi
        clientDocumentTypeROL = new ReadOnlyLabel();
        clientDocumentTypeROL.setMinSize(150, 25);
        clientDocumentTypeROL.setMaxSize(150, 25);

        clientDocumentValueL = new ReadOnlyLabel();
        clientDocumentValueL.setMinSize(150, 25);
        clientDocumentValueL.setMaxSize(150, 25);

        //kontejneri
        VBox leviVBLabele = new VBox(clientNumberL, blankLabel, clientFirstNameL, clientLastNameL, clientResidenceCityL, clientResidenceCountryL, clientEmailL);
        leviVBLabele.setAlignment(Pos.CENTER_RIGHT);
        leviVBLabele.setSpacing(4);

        VBox leviVBPolja = new VBox(clientNumberROL, clientIsGuest, clientFirstNameROL, clientLastNameROL, clientResidenceCityROL, clientResidenceCountryROL, clientEmailROL);
        leviVBPolja.setSpacing(4);

        HBox leviSrednjiHBox = new HBox(10, leviVBLabele, leviVBPolja);
        leviSrednjiHBox.setAlignment(Pos.CENTER);

        VBox leviDonjiVBox = new VBox(4, clientDocumentL, clientDocumentNumberL);
        VBox desniDonjiVBox = new VBox(4, clientDocumentTypeROL, clientDocumentValueL);

        HBox leviDonjiHBox = new HBox(10, leviDonjiVBox, desniDonjiVBox);
        leviDonjiHBox.setAlignment(Pos.CENTER);

        VBox leviVBox = new VBox();
        leviVBox.setMinSize(280, 350);
        leviVBox.setMaxSize(280, 350);
        leviVBox.setAlignment(Pos.TOP_LEFT);
        leviVBox.getChildren().addAll(clientInfoL, leviSrednjiHBox, clientIdentificationL, leviDonjiHBox);
        leviVBox.setSpacing(4);
        leviVBox.setPadding(new Insets(0, 0, 0, 10));

        // separator
        Separator leviVertSeparator = new Separator(Orientation.VERTICAL);
        leviVertSeparator.setMinHeight(400);
        leviVertSeparator.setMaxHeight(400);
        leviVertSeparator.setMinWidth(1);
        leviVertSeparator.setMaxWidth(1);

        //CENTAR ELEMENAT
        //komponente
        //PODACI O SM JEDINICI
        //labele
        roomInfoL = new Label();
        roomInfoL.setMinSize(200, 70);
        roomInfoL.setText("Podaci o\nsmestajnoj jedinici");
        roomInfoL.setWrapText(true);
        roomInfoL.setFont(Font.font("System", FontWeight.BOLD, 18));
        roomInfoL.setAlignment(Pos.TOP_CENTER);
        roomInfoL.setTextAlignment(TextAlignment.CENTER);
        roomInfoL.setPadding(new Insets(5));

        roomNumberL = new Label();
        roomNumberL.setMinHeight(25);
        roomNumberL.setText("Sm. jedinica");
        roomNumberL.setTextAlignment(TextAlignment.RIGHT);

        arrivalDateL = new Label();
        arrivalDateL.setMinHeight(25);
        arrivalDateL.setText("Dolazak");
        arrivalDateL.setTextAlignment(TextAlignment.RIGHT);

        departureDateL = new Label();
        departureDateL.setMinHeight(25);
        departureDateL.setText("Odlazak");
        departureDateL.setTextAlignment(TextAlignment.RIGHT);

        numberOfAdultsL = new Label();
        numberOfAdultsL.setMinHeight(25);
        numberOfAdultsL.setText("Broj odraslih");
        numberOfAdultsL.setTextAlignment(TextAlignment.RIGHT);

        numberOfChildrenL = new Label();
        numberOfChildrenL.setMinHeight(25);
        numberOfChildrenL.setText("Broj dece*");
        numberOfChildrenL.setTooltip(new Tooltip("*Dete = gost mladji od 12 godina"));
        numberOfChildrenL.setTextAlignment(TextAlignment.RIGHT);

        numberOfNightsL = new Label();
        numberOfNightsL.setMinHeight(25);
        numberOfNightsL.setText("Broj nocenja");
        numberOfNightsL.setTextAlignment(TextAlignment.RIGHT);

        guestNotesL = new Label();
        guestNotesL.setMinHeight(35);
        guestNotesL.setText("Beleske");
        guestNotesL.setTextAlignment(TextAlignment.JUSTIFY);
        guestNotesL.setAlignment(Pos.BOTTOM_LEFT);

        //polja
        clientRoomROL = new ReadOnlyLabel();
        clientRoomROL.setMinSize(130, 25);
        clientRoomROL.setMaxSize(130, 25);

        resStartDateROL = new ReadOnlyLabel();
        resStartDateROL.setMinSize(130, 25);
        resStartDateROL.setMaxSize(130, 25);

        resEndDateROL = new ReadOnlyLabel();
        resEndDateROL.setMinSize(130, 25);
        resEndDateROL.setMaxSize(130, 25);

        numberOfAdultsROL = new ReadOnlyLabel();
        numberOfAdultsROL.setMinSize(50, 25);
        numberOfAdultsROL.setMaxSize(50, 25);

        numberOfChildrenROL = new ReadOnlyLabel();
        numberOfChildrenROL.setMinSize(50, 25);
        numberOfChildrenROL.setMaxSize(50, 25);

        numberOfNightsROL = new ReadOnlyLabel();
        numberOfNightsROL.setMinSize(50, 25);
        numberOfNightsROL.setMaxSize(50, 25);
        // kod za popounjavanje liste
//        clientRoomROL.setPromptText("Izaberite sobu");

        resNotesROL = new ReadOnlyLabel();
        resNotesROL.setMinSize(230, 110);
        resNotesROL.setMaxSize(230, 110);
        resNotesROL.setAlignment(Pos.TOP_LEFT);

        //kontejneri
        VBox gornjiCentarLabele = new VBox(5, roomNumberL, arrivalDateL, numberOfNightsL, departureDateL, numberOfAdultsL, numberOfChildrenL, guestNotesL);
        gornjiCentarLabele.setAlignment(Pos.TOP_RIGHT);
//        gornjiCentarLabele.setPadding(new Insets(0, 0, 0, 10));

        VBox gornjiCentarPickeri = new VBox(5, clientRoomROL, resStartDateROL, numberOfNightsROL, resEndDateROL, numberOfAdultsROL, numberOfChildrenROL);
        gornjiCentarPickeri.setAlignment(Pos.TOP_LEFT);

        HBox gornjiCentarHbox = new HBox(10, gornjiCentarLabele, gornjiCentarPickeri);
        gornjiCentarHbox.setAlignment(Pos.TOP_CENTER);
        gornjiCentarHbox.setMinWidth(250);
        gornjiCentarHbox.setMaxWidth(250);

        VBox centralniVBox = new VBox();
        centralniVBox.getChildren().addAll(roomInfoL, gornjiCentarHbox, resNotesROL);
        centralniVBox.setSpacing(0);
        centralniVBox.setAlignment(Pos.TOP_CENTER);
        centralniVBox.setMinSize(250, 350);
        centralniVBox.setMaxSize(250, 350);
        centralniVBox.setPadding(new Insets(0, 10, 0, 10));

        // separator
        Separator desniVertSeparator = new Separator(Orientation.VERTICAL);
        desniVertSeparator.setMinHeight(400);
        desniVertSeparator.setMaxHeight(400);
        desniVertSeparator.setMinWidth(1);
        desniVertSeparator.setMaxWidth(1);

        //DESNI ELEMENAT
        //komponente
        //STATUS REZERVACIJE
        //labele
        reservationStatusL = new Label();
        reservationStatusL.setPrefHeight(25);
        reservationStatusL.setText("Status rezervacije");
        reservationStatusL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        reservationStatusL.setTextFill(Color.BLUE);
        reservationStatusL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        reservationStatusROL = new ReadOnlyLabel();
        reservationStatusROL.setMinSize(100, 25);
        reservationStatusROL.setMaxSize(100, 25);

        //PODACI O PLACANJU
        //labele
        paymentInfoL = new Label();
        paymentInfoL.setPrefHeight(50);
        paymentInfoL.setText("Podaci o placanju");
        paymentInfoL.setWrapText(true);
        paymentInfoL.setFont(Font.font("System", FontWeight.BOLD, 20));
        paymentInfoL.setAlignment(Pos.CENTER);
        paymentInfoL.setTextAlignment(TextAlignment.CENTER);
        paymentInfoL.setPadding(new Insets(5));

        //
        pricePerPeriodL = new Label();
        pricePerPeriodL.setPrefHeight(25);
        pricePerPeriodL.setText("Cena nocenja");
        pricePerPeriodL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        pricePerPeriodL.setTextFill(Color.BLUE);
        pricePerPeriodL.setTextAlignment(TextAlignment.RIGHT);

        totalPriceL = new Label();
        totalPriceL.setPrefHeight(25);
        totalPriceL.setText("Ukupna cena");
        totalPriceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalPriceL.setTextAlignment(TextAlignment.RIGHT);

        otherExpensesL = new Label();
        otherExpensesL.setPrefHeight(25);
        otherExpensesL.setText("Ostali troskovi");
        otherExpensesL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        otherExpensesL.setTextAlignment(TextAlignment.RIGHT);

        subTotaL = new Label();
        subTotaL.setPrefHeight(25);
        subTotaL.setText("Medjuzbir");
        subTotaL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        subTotaL.setTextAlignment(TextAlignment.RIGHT);

        discountL = new Label();
        discountL.setPrefHeight(25);
        discountL.setText("Popust");
        discountL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        discountL.setTextAlignment(TextAlignment.RIGHT);

        totalBalanceL = new Label();
        totalBalanceL.setPrefHeight(25);
        totalBalanceL.setText("Za placanje");
        totalBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalBalanceL.setTextAlignment(TextAlignment.RIGHT);

        currentBalanceL = new Label();
        currentBalanceL.setPrefHeight(25);
        currentBalanceL.setText("Placeno");
        currentBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        currentBalanceL.setTextAlignment(TextAlignment.RIGHT);

        remainBalanceL = new Label();
        remainBalanceL.setPrefHeight(25);
        remainBalanceL.setText("Preostali iznos");
        remainBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        remainBalanceL.setTextFill(Color.BLUE);
        remainBalanceL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        pricePerPeriodROL = new ReadOnlyLabel();
        pricePerPeriodROL.setMinSize(130, 25);
        pricePerPeriodROL.setMaxSize(130, 25);
        //staviti u kontroler da se unose double vrednosti

        totalPriceROL = new ReadOnlyLabel();
        totalPriceROL.setMinSize(130, 25);
        totalPriceROL.setMaxSize(100, 25);

        otherExpensesROL = new ReadOnlyLabel();
        otherExpensesROL.setMinSize(130, 25);
        otherExpensesROL.setMaxSize(130, 25);

        subTotalROL = new ReadOnlyLabel();
        subTotalROL.setMinSize(130, 25);
        subTotalROL.setMaxSize(130, 25);

        discountROL = new ReadOnlyLabel();
        discountROL.setMinSize(130, 25);
        discountROL.setMaxSize(130, 25);

        totalBalanceROL = new ReadOnlyLabel();
        totalBalanceROL.setMinSize(130, 25);
        totalBalanceROL.setMaxSize(130, 25);

        currentBalanceROL = new ReadOnlyLabel();
        currentBalanceROL.setMinSize(130, 25);
        currentBalanceROL.setMaxSize(130, 25);

        remainBalanceROL = new ReadOnlyLabel();
        remainBalanceROL.setMinSize(130, 25);
        remainBalanceROL.setMaxSize(130, 25);

        //dugmad // posto je otvaranje rez u pitanju, preraditi
        closeViewB = new Button();
        closeViewB.setMinSize(130, 25);
        closeViewB.setMaxSize(130, 25);
        closeViewB.setText("Zatvori");

        printResB = new Button();
        printResB.setMinSize(130, 25);
        printResB.setMaxSize(130, 25);
        printResB.setText("Stampaj");

        checkOutResB = new Button();
        checkOutResB.setMinSize(130, 25);
        checkOutResB.setMaxSize(130, 25);
        checkOutResB.setText("Odjava");

        gprB = new Button();
        gprB.setMinSize(130, 25);
        gprB.setMaxSize(130, 25);
        gprB.setText("Gosti u sobi");

        //kontejneri
        HBox gornjiDesniHBox = new HBox(10, reservationStatusL, reservationStatusROL);
        gornjiDesniHBox.setPadding(new Insets(5, 0, 0, 0));
        gornjiDesniHBox.setAlignment(Pos.CENTER);

        VBox desniVBoxLabele = new VBox(5, pricePerPeriodL, totalPriceL, otherExpensesL, subTotaL, discountL,
                totalBalanceL, currentBalanceL, remainBalanceL);
        desniVBoxLabele.setAlignment(Pos.CENTER_RIGHT);

        VBox desniVBoxPolja = new VBox(5, pricePerPeriodROL,
                totalPriceROL, otherExpensesROL, subTotalROL, discountROL,
                totalBalanceROL, currentBalanceROL, remainBalanceROL);
        desniVBoxPolja.setAlignment(Pos.CENTER_LEFT);

        HBox desniHBox = new HBox(10, desniVBoxLabele, desniVBoxPolja);
        desniHBox.setAlignment(Pos.TOP_CENTER);

        buttonGP = new GridPane();
        buttonGP.setHgap(2);
        buttonGP.setVgap(5);
        buttonGP.add(printResB, 0, 0);
        buttonGP.add(checkOutResB, 1, 0);
        buttonGP.add(gprB, 0, 1);
        buttonGP.add(closeViewB, 1, 1);

        VBox desniVBox = new VBox(gornjiDesniHBox, new Separator(Orientation.HORIZONTAL), paymentInfoL, desniHBox, buttonGP);
        desniVBox.setSpacing(5);
        desniVBox.setAlignment(Pos.TOP_CENTER);
        desniVBox.setMaxWidth(250);
        desniVBox.setPrefWidth(250);
        desniVBox.setMaxWidth(250);
        desniVBox.setPadding(new Insets(0, 0, 0, 10));

        //pravljenje centralnog panela
        HBox megaGigaHBox = new HBox(leviVBox, leviVertSeparator, centralniVBox, desniVertSeparator, desniVBox);
        megaGigaHBox.setAlignment(Pos.TOP_LEFT);
        mainVBox.getChildren().add(megaGigaHBox);

        // separator
        Separator bottomSeparator = new Separator(Orientation.HORIZONTAL);
        bottomSeparator.setMinWidth(800);
        bottomSeparator.setMaxWidth(800);
        bottomSeparator.setMinHeight(1);
        bottomSeparator.setMaxHeight(1);

        mainVBox.getChildren().add(bottomSeparator);

        //DONJI ELEMENT
        //komponente
        //
        //labele
        entryAuthorL = new Label();
        entryAuthorL.setMinHeight(25);
        entryAuthorL.setText("Autor poslednje izmene: ");
        entryAuthorL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryAuthorNameL = new Label();
        entryAuthorNameL.setMinHeight(25);
        entryAuthorNameL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryDateL = new Label();
        entryDateL.setMinHeight(25);
        entryDateL.setText("Datum poslednje izmene: ");
        entryDateL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryDateValueL = new Label();
        entryDateValueL.setMinHeight(25);
        entryDateValueL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateValueL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        //kontejneri
        HBox donjiHBox = new HBox(5, entryAuthorL, entryAuthorNameL, new Separator(Orientation.VERTICAL), entryDateL, entryDateValueL);
        donjiHBox.setAlignment(Pos.CENTER_LEFT);

        donjiHBox.setPadding(new Insets(0, 0, 5, 10));
        mainVBox.getChildren().add(donjiHBox);

        //
        //TEST KOD za pozadinu prozora :)
        // new Image(url)
        Image image = new Image("/resources/pozadinaRezerv.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainVBox.setBackground(background);
        //KRAJ KODA
        //

        //set the stage
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        reservationScene = new Scene(mainVBox);
    }

}
