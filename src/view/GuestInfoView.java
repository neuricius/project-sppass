/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import components.ReadOnlyLabel;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestInfoView extends Stage {

    public static Button nextGuestB, saveGuestB, finishAddingB, cancelAddingB;

    Label guestInfoL, guestIdentificationL,
            resNoL, guestTypeL, guestFirstNameL, guestLastNameL, guestDOBL,
            guestResidenceCityL, guestResidenceCountryL, guestPhoneNoL, guestEmailL, guestDocumentL,
            guestDocumentNoL, guestNotesL, entryAuthorL, entryDateL;

    public static Label resNoROL, entryAuthorROL, entryDateROL;

    public static TextField guestFirstNameTF, guestLastNameTF,
            guestResidenceCityTF, guestResidenceCountryTF, guestPhoneNoTF,
            guestEmailTF, guestDocumentTF, guestNotesTF;

    public static ComboBox<String> guestTypeCB, docTypeCB;
    
    public static DatePicker DoBDP;

    VBox guestInfoVBox,
            leftVBoxLabels, leftVBoxFields,
            bottomLeftVBox, bottomRightVBox,
            topVBox,
            buttonLeftVBox, buttonRightVBox;

    HBox leftCenterHBox,
            bottomLeftHBox, buttonHBox, 
            bottomHBox;

    GridPane buttonGP;

    public static Scene guestInfoScene;

    public GuestInfoView() {

        // glavni panel
        guestInfoVBox = new VBox();
        guestInfoVBox.setMinSize(290, 475);
        guestInfoVBox.setMaxSize(290, 475);
        guestInfoVBox.setAlignment(Pos.TOP_CENTER);

        //LEVI ELEMENT
        //komponente
        //PODACI O KLIJENTU
        //labele
        guestInfoL = new Label();
        guestInfoL.setMinSize(200, 50);
        guestInfoL.setMaxSize(200, 50);
        guestInfoL.setText("Podaci o gostu");
        guestInfoL.setFont(Font.font("System", FontWeight.BOLD, 20));
        guestInfoL.setAlignment(Pos.CENTER);
        guestInfoL.setTextAlignment(TextAlignment.RIGHT);
        guestInfoL.setPadding(new Insets(4));
        guestInfoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(10, 20, 10, 20))));

        resNoL = new Label();
        resNoL.setMinHeight(25);
        resNoL.setText("Broj rezervacije");
        resNoL.setTextAlignment(TextAlignment.RIGHT);
        resNoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        resNoROL = new ReadOnlyLabel();
        resNoROL.setMinHeight(25);
        resNoROL.setTextAlignment(TextAlignment.RIGHT);
        resNoROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestTypeL = new Label();
        guestTypeL.setMinHeight(25);
        guestTypeL.setText("Vrsta gosta");
        guestTypeL.setTextAlignment(TextAlignment.RIGHT);
        guestTypeL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestFirstNameL = new Label();
        guestFirstNameL.setMinHeight(25);
        guestFirstNameL.setText("Ime");
        guestFirstNameL.setTextAlignment(TextAlignment.RIGHT);
        guestFirstNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestLastNameL = new Label();
        guestLastNameL.setMinHeight(25);
        guestLastNameL.setText("Prezime");
        guestLastNameL.setTextAlignment(TextAlignment.RIGHT);
        guestLastNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestDOBL  = new Label();
        guestDOBL.setMinHeight(25);
        guestDOBL.setText("Datum rodjenja");
        guestDOBL.setTextAlignment(TextAlignment.RIGHT);
        guestDOBL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
              
        guestResidenceCityL = new Label();
        guestResidenceCityL.setMinHeight(25);
        guestResidenceCityL.setText("Mesto");
        guestResidenceCityL.setTextAlignment(TextAlignment.RIGHT);
        guestResidenceCityL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestResidenceCountryL = new Label();
        guestResidenceCountryL.setMinHeight(25);
        guestResidenceCountryL.setText("Drzava");
        guestResidenceCountryL.setTextAlignment(TextAlignment.RIGHT);
        guestResidenceCountryL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestPhoneNoL = new Label();
        guestPhoneNoL.setMinHeight(25);
        guestPhoneNoL.setText("Telefon");
        guestPhoneNoL.setTextAlignment(TextAlignment.RIGHT);
        guestPhoneNoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestEmailL = new Label();
        guestEmailL.setMinHeight(25);
        guestEmailL.setText("Email adresa");
        guestEmailL.setTextAlignment(TextAlignment.RIGHT);
        guestEmailL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //polja
        guestTypeCB = new ComboBox<>();
        guestTypeCB.setMinSize(150, 25);
        guestTypeCB.setMaxSize(150, 25);
        guestTypeCB.setPromptText("Izaberite");
        // ubaciti kontroler

        guestFirstNameTF = new TextField();
        guestFirstNameTF.setMinSize(150, 25);
        guestFirstNameTF.setMaxSize(150, 25);

        guestLastNameTF = new TextField();
        guestLastNameTF.setMinSize(150, 25);
        guestLastNameTF.setMaxSize(150, 25);
        
        DoBDP = new DatePicker();
        DoBDP.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });

        guestResidenceCityTF = new TextField();
        guestResidenceCityTF.setMinSize(150, 25);
        guestResidenceCityTF.setMaxSize(150, 25);

        guestResidenceCountryTF = new TextField();
        guestResidenceCountryTF.setMinSize(150, 25);
        guestResidenceCountryTF.setMaxSize(150, 25);

        guestPhoneNoTF = new TextField();
        guestPhoneNoTF.setMinSize(150, 25);
        guestPhoneNoTF.setMaxSize(150, 25);

        guestEmailTF = new TextField();
        guestEmailTF.setMinSize(150, 25);
        guestEmailTF.setMaxSize(150, 25);

        //IDENTIFIKACIJA
        //labele
        guestIdentificationL = new Label();
        guestIdentificationL.setMinSize(230, 50);
        guestIdentificationL.setMaxSize(230, 50);
        guestIdentificationL.setText("Identifikacija gosta");
        guestIdentificationL.setFont(Font.font("System", FontWeight.BOLD, 20));
        guestIdentificationL.setAlignment(Pos.CENTER);
        guestIdentificationL.setTextAlignment(TextAlignment.CENTER);
        guestIdentificationL.setPadding(new Insets(4));
        guestIdentificationL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(10))));

        guestDocumentL = new Label();
        guestDocumentL.setMinHeight(25);
        guestDocumentL.setText("Vrsta dokumenta");
        guestDocumentL.setTextAlignment(TextAlignment.RIGHT);
        guestDocumentL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        guestDocumentNoL = new Label();
        guestDocumentNoL.setMinHeight(25);
        guestDocumentNoL.setText("Broj dokumenta");
        guestDocumentNoL.setTextAlignment(TextAlignment.RIGHT);
        guestDocumentNoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //definisati vrste dokumenata u bazi
        docTypeCB = new ComboBox<>();
        docTypeCB.setMinSize(150, 25);
        docTypeCB.setMaxSize(150, 25);
        docTypeCB.setPromptText("Izaberite dokument");

        guestDocumentTF = new TextField();
        guestDocumentTF.setMinSize(150, 25);
        guestDocumentTF.setMaxSize(150, 25);

        //kontejneri
        leftVBoxLabels = new VBox(resNoL, guestTypeL, guestFirstNameL, guestLastNameL, guestDOBL,
                guestResidenceCityL, guestResidenceCountryL, guestPhoneNoL, guestEmailL);
        leftVBoxLabels.setAlignment(Pos.CENTER_RIGHT);
        leftVBoxLabels.setSpacing(4);

        leftVBoxFields = new VBox(resNoROL, guestTypeCB, guestFirstNameTF,
                guestLastNameTF, DoBDP, guestResidenceCityTF, guestResidenceCountryTF, guestPhoneNoTF,
                guestEmailTF);
        leftVBoxFields.setSpacing(4);

        leftCenterHBox = new HBox(10, leftVBoxLabels, leftVBoxFields);
        leftCenterHBox.setAlignment(Pos.CENTER);

        bottomLeftVBox = new VBox(4, guestDocumentL, guestDocumentNoL);
        bottomLeftVBox.setAlignment(Pos.CENTER_RIGHT);

        bottomRightVBox = new VBox(4, docTypeCB, guestDocumentTF);

        bottomLeftHBox = new HBox(10, bottomLeftVBox, bottomRightVBox);
        bottomLeftHBox.setAlignment(Pos.CENTER);

        topVBox = new VBox();
        topVBox.setAlignment(Pos.TOP_LEFT);
        topVBox.getChildren().addAll(guestInfoL, leftCenterHBox,
                //                new Separator(Orientation.HORIZONTAL),
                guestIdentificationL, bottomLeftHBox);
        topVBox.setSpacing(4);
        topVBox.setPadding(new Insets(0, 0, 0, 10));

        // DUGMAD
        saveGuestB = new Button();
        saveGuestB.setMinSize(100, 25);
        saveGuestB.setMaxSize(100, 25);
        saveGuestB.setText("Sacuvaj");
        //prebaciti u kontrolor

        nextGuestB = new Button();
        nextGuestB.setMinSize(100, 25);
        nextGuestB.setMaxSize(100, 25);
        nextGuestB.setText("Sledeci");

        finishAddingB = new Button();
        finishAddingB.setMinSize(100, 25);
        finishAddingB.setMaxSize(100, 25);
        finishAddingB.setText("Zavrsi");

        cancelAddingB = new Button();
        cancelAddingB.setMinSize(100, 25);
        cancelAddingB.setMaxSize(100, 25);
        cancelAddingB.setText("Otkazi");

        // kontejner
        buttonLeftVBox = new VBox(5, nextGuestB, saveGuestB);

        buttonRightVBox = new VBox(5, cancelAddingB);

        buttonHBox = new HBox(5, buttonLeftVBox, buttonRightVBox);
        buttonHBox.setAlignment(Pos.CENTER);

        // status bar
        entryAuthorL = new Label();
        entryAuthorL.setMinHeight(25);
        entryAuthorL.setText("Autor: ");
        entryAuthorL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(1, 0, 1, 0))));

        entryAuthorROL = new Label();
        entryAuthorROL.setMinHeight(25);
        entryAuthorROL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(1, 0, 1, 0))));

        entryDateL = new Label();
        entryDateL.setMinHeight(25);
        entryDateL.setText("Datum: ");
        entryDateL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(1, 0, 1, 0))));

        entryDateROL = new Label();
        entryDateROL.setMinHeight(25);
        entryDateROL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(1, 0, 1, 0))));

// kontejneri
        bottomHBox = new HBox(5, entryAuthorL, entryAuthorROL, new Separator(Orientation.VERTICAL), entryDateL, entryDateROL);
        bottomHBox.setAlignment(Pos.CENTER_LEFT);
        bottomHBox.setPadding(new Insets(0, 0, 0, 5));

        guestInfoVBox.setPadding(new Insets(0, 5, 0, 5));
        guestInfoVBox.setSpacing(1);
        guestInfoVBox.setAlignment(Pos.TOP_CENTER);

        guestInfoVBox.getChildren().addAll(topVBox,
                new Separator(Orientation.HORIZONTAL),
                buttonHBox,
                new Separator(Orientation.HORIZONTAL),
                bottomHBox);

        //TEST KOD za pozadinu prozora :)
        // new Image(url)
        Image image = new Image("/resources/pozadinaUtil.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        guestInfoVBox.setBackground(background);
        //KRAJ KODA
        //

        //set the stage
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        guestInfoScene = new Scene(guestInfoVBox);
    }

}
