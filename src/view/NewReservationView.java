/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Nebojsa Matic
 */
public class NewReservationView extends Stage {

    public static Button printResB, saevChangesB, discardChangesB;

    Label clientInfoL, clientIdentificationL, roomInfoL, paymentInfoL,
            reservationHeaderL, clientTypeL, clientNumberL, clientNumberFieldL,
            blankLabel, clientFirstNameL, clientLastNameL, clientResidenceCityL,
            clientResidenceCountryL, clientEmailL, clientDocumentL, clientDocumentNumberL,
            roomNumberL, arrivalDateL, departureDateL, numberOfAdultsL, numberOfChildrenL,
            numberOfNightsL, guestNotesL, pricePerPeriodL, totalPriceL, otherExpensesL,
            subTotaL, discountL, totalBalanceL, currentBalanceL, remainBalanceL,
            entryAuthorL, entryDateL, reservationStatusL;

    public static Label resNoL, entryAuthorNameL, entryDateVaueL;

    public static TextField clientFirstNameTF, clientLastNameTF,
            clientResidenceCityTF, clientResidenceCountryTF, clientEmailTF,
            clientDocumentTF, guestNotesTF, pricePerPeriodTF,
            totalPriceTF, otherExpensesTF, subTotalTF,
            totalBalanceTF, currentBalanceTF, remainBalanceTF, numberOfNightsTF;

    public static CheckBox clientIsGuest;

    public static ComboBox<Integer> numberOfAdultsCB, numberOfChildrenCB;

    public static ComboBox<String> clientTypeCB, documentTypeCB, roomListCB, discountCB, reservationStatusCB;

    public static DatePicker reservStartDP, reservEndDP;

    public static VBox reservMainVBox;

    GridPane buttonGP;

    public static Scene reservationScene;

    public NewReservationView() {

        // glavni panel
        reservMainVBox = new VBox();
        reservMainVBox.setMinSize(800, 415);
        reservMainVBox.setMaxSize(800, 415);
        reservMainVBox.setAlignment(Pos.TOP_CENTER);

        //LEVI ELEMENT
        //komponente
        //PODACI O KLIJENTU
        //labele
        clientInfoL = new Label();
        clientInfoL.setMinSize(200, 50);
        clientInfoL.setMaxSize(200, 50);
        clientInfoL.setText("Podaci o klijentu");
        clientInfoL.setFont(Font.font("System", FontWeight.BOLD, 20));
        clientInfoL.setAlignment(Pos.CENTER);
        clientInfoL.setTextAlignment(TextAlignment.RIGHT);
        clientInfoL.setPadding(new Insets(4));

        clientTypeL = new Label();
        clientTypeL.setMinHeight(25);
        clientTypeL.setText("Vrsta klijenta");
        clientTypeL.setTextAlignment(TextAlignment.RIGHT);

        clientNumberL = new Label();
        clientNumberL.setMinHeight(25);
        clientNumberL.setText("Broj klijenta");
        clientNumberL.setTextAlignment(TextAlignment.RIGHT);

        blankLabel = new Label();
        blankLabel.setMinHeight(25);
        blankLabel.setText("");
        blankLabel.setTextAlignment(TextAlignment.RIGHT);

        clientFirstNameL = new Label();
        clientFirstNameL.setMinHeight(25);
        clientFirstNameL.setText("Ime");
        clientFirstNameL.setTextAlignment(TextAlignment.RIGHT);

        clientLastNameL = new Label();
        clientLastNameL.setMinHeight(25);
        clientLastNameL.setText("Prezime");
        clientLastNameL.setTextAlignment(TextAlignment.RIGHT);

        clientResidenceCityL = new Label();
        clientResidenceCityL.setMinHeight(25);
        clientResidenceCityL.setText("Mesto");
        clientResidenceCityL.setTextAlignment(TextAlignment.RIGHT);

        clientResidenceCountryL = new Label();
        clientResidenceCountryL.setMinHeight(25);
        clientResidenceCountryL.setText("Drzava");
        clientResidenceCountryL.setTextAlignment(TextAlignment.RIGHT);

        clientEmailL = new Label();
        clientEmailL.setMinHeight(25);
        clientEmailL.setText("Email adresa");
        clientEmailL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        clientTypeCB = new ComboBox<>();
        clientTypeCB.setMinSize(150, 25);
        clientTypeCB.setMaxSize(150, 25);

        // ubaciti kontrolet
        clientNumberFieldL = new ReadOnlyLabel();
        clientNumberFieldL.setMinSize(150, 25);
        clientNumberFieldL.setMaxSize(150, 25);

        clientIsGuest = new CheckBox("Klijent je gost");
        clientIsGuest.setMinSize(100, 25);
        clientIsGuest.setMaxSize(100, 25);

        clientFirstNameTF = new TextField();
        clientFirstNameTF.setMinSize(150, 25);
        clientFirstNameTF.setMaxSize(150, 25);

        clientLastNameTF = new TextField();
        clientLastNameTF.setMinSize(150, 25);
        clientLastNameTF.setMaxSize(150, 25);

        clientResidenceCityTF = new TextField();
        clientResidenceCityTF.setMinSize(150, 25);
        clientResidenceCityTF.setMaxSize(150, 25);

        clientResidenceCountryTF = new TextField();
        clientResidenceCountryTF.setMinSize(150, 25);
        clientResidenceCountryTF.setMaxSize(150, 25);

        clientEmailTF = new TextField();
        clientEmailTF.setMinSize(150, 25);
        clientEmailTF.setMaxSize(150, 25);

        //IDENTIFIKACIJA
        //labele
        clientIdentificationL = new Label();
        clientIdentificationL.setMinSize(230, 50);
        clientIdentificationL.setMaxSize(230, 50);
        clientIdentificationL.setText("Identifikacija klijenta");
        clientIdentificationL.setFont(Font.font("System", FontWeight.BOLD, 20));

        clientIdentificationL.setAlignment(Pos.CENTER);
        clientIdentificationL.setTextAlignment(TextAlignment.CENTER);
        clientIdentificationL.setPadding(new Insets(4));

        clientDocumentL = new Label();
        clientDocumentL.setMinHeight(25);
        clientDocumentL.setText("Vrsta dokumenta");
        clientDocumentL.setTextAlignment(TextAlignment.RIGHT);

        clientDocumentNumberL = new Label();
        clientDocumentNumberL.setMinHeight(25);
        clientDocumentNumberL.setText("  Broj dokumenta");
        clientDocumentNumberL.setTextAlignment(TextAlignment.RIGHT);

        //definisati vrste dokumenata u bazi
        documentTypeCB = new ComboBox<>();
        documentTypeCB.setMinSize(150, 25);
        documentTypeCB.setMaxSize(150, 25);
        documentTypeCB.setPromptText("Izaberite dokument");

        clientDocumentTF = new TextField();
        clientDocumentTF.setMinSize(150, 25);
        clientDocumentTF.setMaxSize(150, 25);

        //kontejneri
        VBox leviVBLabele = new VBox(clientTypeL, clientNumberL, blankLabel, clientFirstNameL, clientLastNameL, clientResidenceCityL, clientResidenceCountryL, clientEmailL);
        leviVBLabele.setAlignment(Pos.CENTER_RIGHT);
        leviVBLabele.setSpacing(4);

        VBox leviVBPolja = new VBox(clientTypeCB, clientNumberFieldL, clientIsGuest, clientFirstNameTF, clientLastNameTF, clientResidenceCityTF, clientResidenceCountryTF, clientEmailTF);
        leviVBPolja.setSpacing(4);

        HBox leviSrednjiHBox = new HBox(10, leviVBLabele, leviVBPolja);
        leviSrednjiHBox.setAlignment(Pos.CENTER);

        VBox leviDonjiVBox = new VBox(4, clientDocumentL, clientDocumentNumberL);
        VBox desniDonjiVBox = new VBox(4, documentTypeCB, clientDocumentTF);

        HBox leviDonjiHBox = new HBox(10, leviDonjiVBox, desniDonjiVBox);
        leviDonjiHBox.setAlignment(Pos.CENTER);

        VBox leviVBox = new VBox();
        leviVBox.setMinSize(280, 350);
        leviVBox.setMaxSize(280, 350);
        leviVBox.setAlignment(Pos.TOP_LEFT);
        leviVBox.getChildren().addAll(clientInfoL, leviSrednjiHBox, clientIdentificationL, leviDonjiHBox);
        leviVBox.setSpacing(4);
        leviVBox.setPadding(new Insets(0, 0, 0, 10));

        // separator
        Separator leviVertSeparator = new Separator(Orientation.VERTICAL);
        leviVertSeparator.setMinHeight(400);
        leviVertSeparator.setMaxHeight(400);
        leviVertSeparator.setMinWidth(1);
        leviVertSeparator.setMaxWidth(1);

        //CENTAR ELEMENAT
        //komponente
        //PODACI O SM JEDINICI
        //labele
        roomInfoL = new Label();
        roomInfoL.setMinSize(200, 70);
        roomInfoL.setText("Podaci o\nsmestajnoj jedinici");
        roomInfoL.setWrapText(true);
        roomInfoL.setFont(Font.font("System", FontWeight.BOLD, 18));
        roomInfoL.setAlignment(Pos.TOP_CENTER);
        roomInfoL.setTextAlignment(TextAlignment.CENTER);
        roomInfoL.setPadding(new Insets(5));

        roomNumberL = new Label();
        roomNumberL.setMinHeight(25);
        roomNumberL.setText("Sm. jedinica");
        roomNumberL.setTextAlignment(TextAlignment.RIGHT);

        arrivalDateL = new Label();
        arrivalDateL.setMinHeight(25);
        arrivalDateL.setText("Dolazak");
        arrivalDateL.setTextAlignment(TextAlignment.RIGHT);

        departureDateL = new Label();
        departureDateL.setMinHeight(25);
        departureDateL.setText("Odlazak");
        departureDateL.setTextAlignment(TextAlignment.RIGHT);

        numberOfAdultsL = new Label();
        numberOfAdultsL.setMinHeight(25);
        numberOfAdultsL.setText("Broj odraslih");
        numberOfAdultsL.setTextAlignment(TextAlignment.RIGHT);

        numberOfChildrenL = new Label();
        numberOfChildrenL.setMinHeight(25);
        numberOfChildrenL.setText("Broj dece*");
        numberOfChildrenL.setTooltip(new Tooltip("*Dete = gost mladji od 12 godina"));
        numberOfChildrenL.setTextAlignment(TextAlignment.RIGHT);

        numberOfNightsL = new Label();
        numberOfNightsL.setMinHeight(25);
        numberOfNightsL.setText("Broj nocenja");
        numberOfNightsL.setTextAlignment(TextAlignment.RIGHT);

        guestNotesL = new Label();
        guestNotesL.setMinHeight(35);
        guestNotesL.setText("Beleske");
        guestNotesL.setTextAlignment(TextAlignment.JUSTIFY);
        guestNotesL.setAlignment(Pos.BOTTOM_LEFT);

        //polja
        roomListCB = new ComboBox<>();
        roomListCB.setMinSize(130, 25);
        roomListCB.setMaxSize(130, 25);
        roomListCB.setPromptText("Izaberite sm.jedinicu");

        reservStartDP = new DatePicker(LocalDate.now());
        reservStartDP.setMinSize(130, 25);
        reservStartDP.setMaxSize(130, 25);

        reservEndDP = new DatePicker(reservStartDP.getValue().plusDays(7));
        reservEndDP.setMinSize(130, 25);
        reservEndDP.setMaxSize(130, 25);

        numberOfAdultsCB = new ComboBox<>();
        numberOfAdultsCB.setMinSize(50, 25);
        numberOfAdultsCB.setMaxSize(50, 25);

        numberOfChildrenCB = new ComboBox<>();
        numberOfChildrenCB.setMinSize(50, 25);
        numberOfChildrenCB.setMaxSize(50, 25);

        numberOfNightsTF = new TextField();
        numberOfNightsTF.setMinSize(50, 25);
        numberOfNightsTF.setMaxSize(50, 25);

        guestNotesTF = new TextField();
        guestNotesTF.setMinSize(230, 110);
        guestNotesTF.setMaxSize(230, 110);
        guestNotesTF.setAlignment(Pos.TOP_LEFT);

        //kontejneri
        VBox gornjiCentarLabele = new VBox(5, roomNumberL, arrivalDateL, numberOfNightsL, departureDateL, numberOfAdultsL, numberOfChildrenL, guestNotesL);
        gornjiCentarLabele.setAlignment(Pos.TOP_RIGHT);
        gornjiCentarLabele.setPadding(new Insets(0, 0, 0, 10));

        VBox gornjiCentarPickeri = new VBox(5, roomListCB, reservStartDP, numberOfNightsTF, reservEndDP, numberOfAdultsCB, numberOfChildrenCB);
        gornjiCentarPickeri.setAlignment(Pos.TOP_LEFT);

        HBox gornjiCentarHbox = new HBox(10, gornjiCentarLabele, gornjiCentarPickeri);
        gornjiCentarHbox.setAlignment(Pos.CENTER_LEFT);
        gornjiCentarHbox.setMinWidth(250);
        gornjiCentarHbox.setMaxWidth(250);

        VBox centralniVBox = new VBox();
        centralniVBox.getChildren().addAll(roomInfoL, gornjiCentarHbox, guestNotesTF);
        centralniVBox.setSpacing(0);
        centralniVBox.setAlignment(Pos.TOP_CENTER);
        centralniVBox.setMinSize(250, 350);
        centralniVBox.setMaxSize(250, 350);
        centralniVBox.setPadding(new Insets(0, 10, 0, 10));

        // separator
        Separator desniVertSeparator = new Separator(Orientation.VERTICAL);
        desniVertSeparator.setMinHeight(400);
        desniVertSeparator.setMaxHeight(400);
        desniVertSeparator.setMinWidth(1);
        desniVertSeparator.setMaxWidth(1);

        //DESNI ELEMENAT
        //komponente
        //STATUS REZERVACIJE
        //labele
        reservationStatusL = new Label();
        reservationStatusL.setPrefHeight(25);
        reservationStatusL.setText("Status rezervacije");
        reservationStatusL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        reservationStatusL.setTextFill(Color.BLUE);
        reservationStatusL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        reservationStatusCB = new ComboBox<>();
        reservationStatusCB.setMinSize(100, 25);
        reservationStatusCB.setMaxSize(100, 25);

        //PODACI O PLACANJU
        //labele
        paymentInfoL = new Label();
        paymentInfoL.setPrefHeight(50);
        paymentInfoL.setText("Podaci o placanju");
        paymentInfoL.setWrapText(true);
        paymentInfoL.setFont(Font.font("System", FontWeight.BOLD, 20));
        paymentInfoL.setAlignment(Pos.CENTER);
        paymentInfoL.setTextAlignment(TextAlignment.CENTER);
        paymentInfoL.setPadding(new Insets(5));

        //
        pricePerPeriodL = new Label();
        pricePerPeriodL.setPrefHeight(25);
        pricePerPeriodL.setText("Cena nocenja");
        pricePerPeriodL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        pricePerPeriodL.setTextFill(Color.BLUE);
        pricePerPeriodL.setTextAlignment(TextAlignment.RIGHT);

        totalPriceL = new Label();
        totalPriceL.setPrefHeight(25);
        totalPriceL.setText("Ukupna cena");
        totalPriceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalPriceL.setTextAlignment(TextAlignment.RIGHT);

        otherExpensesL = new Label();
        otherExpensesL.setPrefHeight(25);
        otherExpensesL.setText("Ostali troskovi");
        otherExpensesL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        otherExpensesL.setTextAlignment(TextAlignment.RIGHT);

        subTotaL = new Label();
        subTotaL.setPrefHeight(25);
        subTotaL.setText("Medjuzbir");
        subTotaL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        subTotaL.setTextAlignment(TextAlignment.RIGHT);

        discountL = new Label();
        discountL.setPrefHeight(25);
        discountL.setText("Popust");
        discountL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        discountL.setTextAlignment(TextAlignment.RIGHT);

        totalBalanceL = new Label();
        totalBalanceL.setPrefHeight(25);
        totalBalanceL.setText("Za placanje");
        totalBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalBalanceL.setTextAlignment(TextAlignment.RIGHT);

        currentBalanceL = new Label();
        currentBalanceL.setPrefHeight(25);
        currentBalanceL.setText("Placeno");
        currentBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        currentBalanceL.setTextAlignment(TextAlignment.RIGHT);

        remainBalanceL = new Label();
        remainBalanceL.setPrefHeight(25);
        remainBalanceL.setText("Preostali iznos");
        remainBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        remainBalanceL.setTextFill(Color.BLUE);
        remainBalanceL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        pricePerPeriodTF = new TextField();
        pricePerPeriodTF.setMinSize(130, 25);
        pricePerPeriodTF.setMaxSize(130, 25);
        pricePerPeriodTF.setEditable(false);
        //staviti u kontroler da se unose double vrednosti

        totalPriceTF = new TextField();
        totalPriceTF.setMinSize(130, 25);
        totalPriceTF.setMaxSize(100, 25);
        totalPriceTF.setEditable(false);

        otherExpensesTF = new TextField();
        otherExpensesTF.setMinSize(130, 25);
        otherExpensesTF.setMaxSize(130, 25);

        subTotalTF = new TextField();
        subTotalTF.setMinSize(130, 25);
        subTotalTF.setMaxSize(130, 25);
        subTotalTF.setEditable(false);

        discountCB = new ComboBox<>();
        discountCB.setMinSize(130, 25);
        discountCB.setMaxSize(130, 25);
        discountCB.setPromptText("Izaberite popust");

        totalBalanceTF = new TextField();
        totalBalanceTF.setMinSize(130, 25);
        totalBalanceTF.setMaxSize(130, 25);
        totalBalanceTF.setEditable(false);

        currentBalanceTF = new TextField();
        currentBalanceTF.setMinSize(130, 25);
        currentBalanceTF.setMaxSize(130, 25);

        remainBalanceTF = new TextField();
        remainBalanceTF.setMinSize(130, 25);
        remainBalanceTF.setMaxSize(130, 25);
        remainBalanceTF.setEditable(false);

        //dugmad
        saevChangesB = new Button();
        saevChangesB.setMinSize(130, 25);
        saevChangesB.setMaxSize(130, 25);
        saevChangesB.setText("Sacuvaj");
        //prebaciti u kontrolor

        printResB = new Button();
        printResB.setMinSize(130, 25);
        printResB.setMaxSize(130, 25);
        printResB.setText("Stampaj");

        discardChangesB = new Button();
        discardChangesB.setMinSize(130, 25);
        discardChangesB.setMaxSize(130, 25);
        discardChangesB.setText("Otkazi");

        //kontejneri
        HBox gornjiDesniHBox = new HBox(10, reservationStatusL, reservationStatusCB);
        gornjiDesniHBox.setPadding(new Insets(5, 0, 0, 0));
        gornjiDesniHBox.setAlignment(Pos.CENTER);

        VBox desniVBoxLabele = new VBox(5, pricePerPeriodL, totalPriceL, otherExpensesL, subTotaL, discountL,
                totalBalanceL, currentBalanceL, remainBalanceL);
        desniVBoxLabele.setAlignment(Pos.CENTER_RIGHT);

        VBox desniVBoxPolja = new VBox(5, pricePerPeriodTF,
                totalPriceTF, otherExpensesTF, subTotalTF, discountCB,
                totalBalanceTF, currentBalanceTF, remainBalanceTF);
        desniVBoxPolja.setAlignment(Pos.CENTER_LEFT);

        HBox desniHBox = new HBox(10, desniVBoxLabele, desniVBoxPolja);
        desniHBox.setAlignment(Pos.TOP_CENTER);

        buttonGP = new GridPane();
        buttonGP.setHgap(2);
        buttonGP.setVgap(2);
        buttonGP.add(printResB, 1, 0);
        buttonGP.add(saevChangesB, 0, 1);
        buttonGP.add(discardChangesB, 1, 1);

        VBox desniVBox = new VBox(gornjiDesniHBox, new Separator(Orientation.HORIZONTAL), paymentInfoL, desniHBox, buttonGP);
        desniVBox.setSpacing(5);
        desniVBox.setAlignment(Pos.TOP_CENTER);
        desniVBox.setMaxWidth(250);
        desniVBox.setPrefWidth(250);
        desniVBox.setMaxWidth(250);
        desniVBox.setPadding(new Insets(0, 10, 0, 10));

        //pravljenje centralnog panela
        HBox megaGigaHBox = new HBox(leviVBox, leviVertSeparator, centralniVBox, desniVertSeparator, desniVBox);
        megaGigaHBox.setAlignment(Pos.TOP_LEFT);
        reservMainVBox.getChildren().add(megaGigaHBox);

        // separator
        Separator bottomSeparator = new Separator(Orientation.HORIZONTAL);
        bottomSeparator.setMinWidth(800);
        bottomSeparator.setMaxWidth(800);
        bottomSeparator.setMinHeight(1);
        bottomSeparator.setMaxHeight(1);

        reservMainVBox.getChildren().add(bottomSeparator);

        //DONJI ELEMENT
        //komponente
        //
        //labele
        entryAuthorL = new Label();
        entryAuthorL.setMinHeight(25);
        entryAuthorL.setText("Autor trenutne izmene: ");
        entryAuthorL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryAuthorNameL = new Label();
        entryAuthorNameL.setMinHeight(25);
        entryAuthorNameL.setTextAlignment(TextAlignment.JUSTIFY);
        entryAuthorNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryDateL = new Label();
        entryDateL.setMinHeight(25);
        entryDateL.setText("Datum danasnje izmene: ");
        entryDateL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        entryDateVaueL = new Label();
        entryDateVaueL.setMinHeight(25);
        entryDateVaueL.setTextAlignment(TextAlignment.JUSTIFY);
        entryDateVaueL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));

        //kontejneri
        HBox donjiHBox = new HBox(5, entryAuthorL, entryAuthorNameL, new Separator(Orientation.VERTICAL), entryDateL, entryDateVaueL);
        donjiHBox.setAlignment(Pos.CENTER_LEFT);
        donjiHBox.setPadding(new Insets(0, 0, 5, 10));
        reservMainVBox.getChildren().add(donjiHBox);

        //
        //TEST KOD za pozadinu prozora :)
        // new Image(url)
        Image image = new Image("/resources/pozadinaRezerv.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        reservMainVBox.setBackground(background);
        //KRAJ KODA
        //

        //set the stage
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        reservationScene = new Scene(reservMainVBox);
    }

}
