/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import model.Klijent;
import model.Rezervacija;
import util.HibernateUtil;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class ArrivalsPerDateView extends Stage {

    public static List<Rezervacija> arrivaList;

    public static Button refreshB, addArrivalB, deleteArrivalB;

    BorderPane arrivalPane;

    public static TableView<Rezervacija> arrivalTable;
    public static TableColumn<Rezervacija, Integer> reservationID;
    public static TableColumn<Rezervacija, Date> arrivalDate;

    HBox buttonHBox, datePickerHBox;

    Label arrivalDateL;
    DatePicker arrivalDP;

    public static Scene arrivalViewScene;

    public ArrivalsPerDateView() {

        arrivalPane = new BorderPane();

        //izbor datuma
        arrivalDateL = new Label();
        arrivalDateL.setText("Izaberi datum: ");
        arrivalDateL.setTextAlignment(TextAlignment.RIGHT);

        arrivalDP = new DatePicker();
        arrivalDP.setMinSize(130, 25);
        arrivalDP.setMaxSize(130, 25);
        arrivalDP.setValue(LocalDate.now());
        arrivalDP.setOnAction(e -> {
            arrivaList = HibernateUtil.dolasciNaDan(facilityName(), arrivalDP.getValue());
            ObservableList<Rezervacija> data = FXCollections.observableArrayList(arrivaList);
            arrivalTable.setItems(data);
        });

        datePickerHBox = new HBox(5, arrivalDateL, arrivalDP);
        datePickerHBox.setAlignment(Pos.CENTER_LEFT);

        arrivalPane.setTop(datePickerHBox);

        //dugmad
        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Stampaj");

        addArrivalB = new Button();
        addArrivalB.setMinSize(130, 25);
        addArrivalB.setMaxSize(130, 25);
        addArrivalB.setText("Dodaj");

        deleteArrivalB = new Button();
        deleteArrivalB.setMinSize(130, 25);
        deleteArrivalB.setMaxSize(130, 25);
        deleteArrivalB.setText("Obrisi");

        buttonHBox = new HBox(refreshB, addArrivalB, deleteArrivalB);
        arrivalPane.setBottom(buttonHBox);

        // CENTAR
        arrivalTable = new TableView<>();
        arrivalTable.setEditable(false);

        arrivaList = HibernateUtil.dolasciNaDan(facilityName(), arrivalDP.getValue());
        ObservableList<Rezervacija> data = FXCollections.observableArrayList(arrivaList);

        //kolone
        //osn podaci o rez
        //glavna
        TableColumn<Rezervacija, String> podaciORez = new TableColumn<>("Rezervacija");

        //redni broj (ID) rezervacije
        reservationID = new TableColumn<>("#");
        reservationID.setStyle("-fx-alignment: CENTER;");
        reservationID.setPrefWidth(25);
        reservationID.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));

        //status rez
        TableColumn<Rezervacija, String> statusRezervacije = new TableColumn<>("Status");
        statusRezervacije.setStyle("-fx-alignment: CENTER;");
        statusRezervacije.setPrefWidth(75);
        statusRezervacije.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdStatRez().getStatusRezervacije()));

        //
        // naziv sobe
        TableColumn<Rezervacija, String> brojSmJed = new TableColumn<>("Sm. Jed");
        brojSmJed.setStyle("-fx-alignment: CENTER;");
        brojSmJed.setPrefWidth(75);
        brojSmJed.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdSmJed().getNaziv()));

        //
        podaciORez.getColumns().addAll(reservationID, statusRezervacije, brojSmJed);

        //broj gostiju
        //glavna kol
        TableColumn<Rezervacija, Integer> brojGostiju = new TableColumn<>("Broj Gostiju");
        brojGostiju.setPrefWidth(80);

        //subkolone
        TableColumn<Rezervacija, Integer> brojOdraslih = new TableColumn<>("Odr");
        brojOdraslih.setStyle("-fx-alignment: CENTER;");
        brojOdraslih.setPrefWidth(40);
        brojOdraslih.setCellValueFactory(new PropertyValueFactory<>("brojOdraslih"));

        TableColumn<Rezervacija, Integer> brojDece = new TableColumn<>("Deca");
        brojDece.setStyle("-fx-alignment: CENTER;");
        brojDece.setPrefWidth(40);
        brojDece.setCellValueFactory(new PropertyValueFactory<>("brojDece"));

        brojGostiju.getColumns().addAll(brojOdraslih, brojDece);

        //period boravka
        //glavna kol
        TableColumn<Rezervacija, Date> periodBoravka = new TableColumn<>("Period Boravka");

        //subkolone
        arrivalDate = new TableColumn<>("Dolazak");
        arrivalDate.setStyle("-fx-alignment: CENTER;");
        arrivalDate.setPrefWidth(80);
        arrivalDate.setCellValueFactory(new PropertyValueFactory<>("prijava"));

        TableColumn<Rezervacija, Date> datumOdlaska = new TableColumn<>("Odlazak");
        datumOdlaska.setStyle("-fx-alignment: CENTER;");
        datumOdlaska.setPrefWidth(80);
        datumOdlaska.setCellValueFactory(new PropertyValueFactory<>("odjava"));

        periodBoravka.getColumns().addAll(arrivalDate, datumOdlaska);

        //klijent
        //glavna
        TableColumn<Rezervacija, Klijent> podaciOKlijentu = new TableColumn<>("Klijent");
        podaciOKlijentu.setCellValueFactory(new PropertyValueFactory<>("idKlijent"));

        //subkolone
        TableColumn<Rezervacija, String> prezimeKlijenta = new TableColumn<>("Prezime");
        prezimeKlijenta.setPrefWidth(80);
        prezimeKlijenta.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getPrezimeKlijenta()));

        TableColumn<Rezervacija, String> imeKlijenta = new TableColumn<>("Ime");
        imeKlijenta.setPrefWidth(80);
        imeKlijenta.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getImeKlijenta()));

        TableColumn<Rezervacija, String> drzavaKlijenta = new TableColumn<>("Drzava");
        drzavaKlijenta.setMinWidth(100);
        drzavaKlijenta.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getDrzavaKlijenta()));

        TableColumn<Rezervacija, String> mestoKlijenta = new TableColumn<>("Mesto");
        mestoKlijenta.setMinWidth(100);
        mestoKlijenta.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getMestoKlijenta()));

        podaciOKlijentu.getColumns().addAll(prezimeKlijenta, imeKlijenta, drzavaKlijenta, mestoKlijenta);

        arrivalTable.setItems(data);
        arrivalTable.getColumns().addAll(podaciORez, brojGostiju, periodBoravka, podaciOKlijentu);

        //kod za izbor po kojoj koloni da se sortira tabela
        arrivalPane.setCenter(arrivalTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        arrivalViewScene = new Scene(arrivalPane);
    }

}
