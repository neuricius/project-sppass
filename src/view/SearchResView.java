/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Klijent;
import model.Rezervacija;
import util.HibernateUtil;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class SearchResView extends Stage {

    List<Rezervacija> searchResultsList;

    public static Button refreshB, addB, deleteB;

    BorderPane mainBP;

    public static TableView<Rezervacija> reservationTable;
    public static TableColumn<Rezervacija, Integer> resIDTC;
    public static TableColumn<Rezervacija, Date> arrivalDateTC;

    public static Scene searchResScene;

    public SearchResView(Integer brRez, String imeNosioca, String prezimeNosioca, String nazivSobe, LocalDate odDatum, LocalDate doDatum) {

        mainBP = new BorderPane();

        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Osvezi");

        addB = new Button();
        addB.setMinSize(130, 25);
        addB.setMaxSize(130, 25);
        addB.setText("Dodaj");

        deleteB = new Button();
        deleteB.setMinSize(130, 25);
        deleteB.setMaxSize(130, 25);
        deleteB.setText("Obrisi");

        HBox buttonHBox = new HBox(refreshB, addB, deleteB);
        mainBP.setTop(buttonHBox);

        // CENTAR
        reservationTable = new TableView<>();
        reservationTable.setEditable(false);

        //staviti drugi metod
        searchResultsList = HibernateUtil.pretragaRezervacija(facilityName(), brRez, imeNosioca, prezimeNosioca, nazivSobe, odDatum, doDatum);
        ObservableList<Rezervacija> data = FXCollections.observableArrayList(searchResultsList);

        //kolone
        //osn podaci o rez
        //glavna
        TableColumn<Rezervacija, String> resInfoTC = new TableColumn<>("Rezervacija");

        //redni broj (ID) rezervacije
        resIDTC = new TableColumn<>("#");
        resIDTC.setStyle("-fx-alignment: CENTER;");
        resIDTC.setPrefWidth(25);
        resIDTC.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));

        //status rez
        TableColumn<Rezervacija, String> resStatusTC = new TableColumn<>("Status");
        resStatusTC.setStyle("-fx-alignment: CENTER;");
        resStatusTC.setPrefWidth(75);
        resStatusTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdStatRez().getStatusRezervacije()));

        //
        // naziv sobe
        TableColumn<Rezervacija, String> roomNoTC = new TableColumn<>("Sm. Jed");
        roomNoTC.setStyle("-fx-alignment: CENTER;");
        roomNoTC.setPrefWidth(75);
        roomNoTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdSmJed().getNaziv()));

        //
        resInfoTC.getColumns().addAll(resIDTC, resStatusTC, roomNoTC);

        //broj gostiju
        //glavna kol
        TableColumn<Rezervacija, Integer> numberOfGuestsTC = new TableColumn<>("Broj Gostiju");
        numberOfGuestsTC.setPrefWidth(80);

        //subkolone
        TableColumn<Rezervacija, Integer> numberOfAdultsTC = new TableColumn<>("Odr");
        numberOfAdultsTC.setStyle("-fx-alignment: CENTER;");
        numberOfAdultsTC.setPrefWidth(40);
        numberOfAdultsTC.setCellValueFactory(new PropertyValueFactory<>("brojOdraslih"));

        TableColumn<Rezervacija, Integer> numberOfChildrenTC = new TableColumn<>("Deca");
        numberOfChildrenTC.setStyle("-fx-alignment: CENTER;");
        numberOfChildrenTC.setPrefWidth(40);
        numberOfChildrenTC.setCellValueFactory(new PropertyValueFactory<>("brojDece"));

        numberOfGuestsTC.getColumns().addAll(numberOfAdultsTC, numberOfChildrenTC);

        //period boravka
        //glavna kol
        TableColumn<Rezervacija, Date> stayDurationTC = new TableColumn<>("Period Boravka");

        //subkolone
        arrivalDateTC = new TableColumn<>("Dolazak");
        arrivalDateTC.setStyle("-fx-alignment: CENTER;");
        arrivalDateTC.setPrefWidth(80);
        arrivalDateTC.setCellValueFactory(new PropertyValueFactory<>("prijava"));

        TableColumn<Rezervacija, Date> departureDateTC = new TableColumn<>("Odlazak");
        departureDateTC.setStyle("-fx-alignment: CENTER;");
        departureDateTC.setPrefWidth(80);
        departureDateTC.setCellValueFactory(new PropertyValueFactory<>("odjava"));

        stayDurationTC.getColumns().addAll(arrivalDateTC, departureDateTC);

        //klijent
        //glavna
        TableColumn<Rezervacija, Klijent> clientInfoTC = new TableColumn<>("Klijent");
        clientInfoTC.setCellValueFactory(new PropertyValueFactory<>("idKlijent"));

        //subkolone
        TableColumn<Rezervacija, String> clientLastNameTC = new TableColumn<>("Prezime");
        clientLastNameTC.setPrefWidth(80);
        clientLastNameTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getPrezimeKlijenta()));

        TableColumn<Rezervacija, String> clientFirstNameTC = new TableColumn<>("Ime");
        clientFirstNameTC.setPrefWidth(80);
        clientFirstNameTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getImeKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCountryTC = new TableColumn<>("Drzava");
        clientResidenceCountryTC.setMinWidth(100);
        clientResidenceCountryTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getDrzavaKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCityTC = new TableColumn<>("Mesto");
        clientResidenceCityTC.setMinWidth(100);
        clientResidenceCityTC.setCellValueFactory(
                (CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getMestoKlijenta()));

        clientInfoTC.getColumns().addAll(clientLastNameTC, clientFirstNameTC, clientResidenceCountryTC, clientResidenceCityTC);

        reservationTable.setItems(data);
        reservationTable.getColumns().addAll(resInfoTC, numberOfGuestsTC, stayDurationTC, clientInfoTC);

        //kod za izbor po kojoj koloni da se sortira tabela
        mainBP.setCenter(reservationTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        searchResScene = new Scene(mainBP);
    }

}
