/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import model.Gostiposobama;
import util.HibernateUtil;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestsPerRoomSpecificView extends Stage {

    List<Gostiposobama> guestsPerRoomList;
    ObservableList<Gostiposobama> dataOL;
    public static TableView<Gostiposobama> guestsPerRoomView;
    public static TableColumn<Gostiposobama, Integer> gpsIDTC, reservationIDTC, guestIDTC;
    public static TableColumn<Gostiposobama, String> guestFirstNameTC, guestLastNameTC, guestResidenceCityTC, guestDocTypeTC, guestDocNoTC;
    public static TableColumn<Gostiposobama, Date> guestDOBTC;

    Label resNoL, roomNameL, startDateL, endDateL, numberOfAdultsL, numberOfChildrenL;

    public static Label resNoROL, roomNameROL, startDateROL,
            endDateROL, numberOfAdultsROL, numberOfChildrenROL;

    public static Button refreshGPRB, addGPRB, removeGPRB,
            printGPRB;

    BorderPane GPRMainBP;

    HBox labelHBox, buttonHBox;

    public static Scene GPRScene;

    public GuestsPerRoomSpecificView(Integer x) {

        GPRMainBP = new BorderPane();

        //panela za info o rez - top
        //labele
        resNoL = new Label();
        resNoL.setMinHeight(28);
        resNoL.setText("Rez #:");
        resNoL.setTextAlignment(TextAlignment.RIGHT);

        roomNameL = new Label();
        roomNameL.setMinHeight(28);
        roomNameL.setText("Sm.jed.:");
        roomNameL.setTextAlignment(TextAlignment.RIGHT);

        startDateL = new Label();
        startDateL.setMinHeight(28);
        startDateL.setText("Pocetak:");
        startDateL.setTextAlignment(TextAlignment.RIGHT);

        endDateL = new Label();
        endDateL.setMinHeight(28);
        endDateL.setText("Kraj:");
        endDateL.setTextAlignment(TextAlignment.RIGHT);

        numberOfAdultsL = new Label();
        numberOfAdultsL.setMinHeight(28);
        numberOfAdultsL.setText("Odrasli:");
        numberOfAdultsL.setTextAlignment(TextAlignment.RIGHT);

        numberOfChildrenL = new Label();
        numberOfChildrenL.setMinHeight(28);
        numberOfChildrenL.setText("Deca:");
        numberOfChildrenL.setTextAlignment(TextAlignment.RIGHT);

        //vrednosti
        resNoROL = new Label();
        resNoROL.setMinHeight(28);
        resNoROL.setTextAlignment(TextAlignment.LEFT);

        roomNameROL = new Label();
        roomNameROL.setMinHeight(28);
        roomNameROL.setTextAlignment(TextAlignment.LEFT);

        startDateROL = new Label();
        startDateROL.setMinHeight(28);
        startDateROL.setTextAlignment(TextAlignment.LEFT);

        endDateROL = new Label();
        endDateROL.setMinHeight(28);
        endDateROL.setTextAlignment(TextAlignment.LEFT);

        numberOfAdultsROL = new Label();
        numberOfAdultsROL.setMinHeight(28);
        numberOfAdultsROL.setTextAlignment(TextAlignment.LEFT);

        numberOfChildrenROL = new Label();
        numberOfChildrenROL.setMinHeight(28);
        numberOfChildrenROL.setTextAlignment(TextAlignment.LEFT);

        //kontejner
        labelHBox = new HBox(5,
                resNoL, resNoROL, new Separator(Orientation.VERTICAL),
                roomNameL, roomNameROL, new Separator(Orientation.VERTICAL),
                startDateL, startDateROL, new Separator(Orientation.VERTICAL),
                endDateL, endDateROL, new Separator(Orientation.VERTICAL),
                numberOfAdultsL, numberOfAdultsROL, new Separator(Orientation.VERTICAL),
                numberOfChildrenL, numberOfChildrenROL, new Separator(Orientation.VERTICAL));

        GPRMainBP.setTop(labelHBox);

        //panela za dugmad - bottom
        refreshGPRB = new Button();
        refreshGPRB.setMinSize(60, 25);
        refreshGPRB.setMaxSize(60, 25);
        refreshGPRB.setText("Osvezi");

        addGPRB = new Button();
        addGPRB.setMinSize(60, 25);
        addGPRB.setMaxSize(60, 25);
        addGPRB.setText("Dodaj");

        removeGPRB = new Button();
        removeGPRB.setMinSize(60, 25);
        removeGPRB.setMaxSize(60, 25);
        removeGPRB.setText("Obrisi");

        printGPRB = new Button();
        printGPRB.setMinSize(60, 25);
        printGPRB.setMaxSize(60, 25);
        printGPRB.setText("Stampaj");

        buttonHBox = new HBox(refreshGPRB, addGPRB, removeGPRB, printGPRB);
        GPRMainBP.setBottom(buttonHBox);

        //CENTAR
        guestsPerRoomView = new TableView<>();
        guestsPerRoomView.setEditable(false);

        guestsPerRoomList = HibernateUtil.skupiListuGostijuZaRezervaciju(x);
        dataOL = FXCollections.observableArrayList(guestsPerRoomList);

        guestIDTC = new TableColumn<>("#");
        guestIDTC.setStyle("-fx-alignment: CENTER;");
        guestIDTC.setPrefWidth(25);
        guestIDTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, Integer> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getIdGost()));

        guestFirstNameTC = new TableColumn<>("Ime");
        guestFirstNameTC.setStyle("-fx-alignment: CENTER;");
        guestFirstNameTC.setPrefWidth(100);
        guestFirstNameTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getImeGosta()));

        guestLastNameTC = new TableColumn<>("Prezime");
        guestLastNameTC.setStyle("-fx-alignment: CENTER;");
        guestLastNameTC.setPrefWidth(100);
        guestLastNameTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getPrezimeGosta()));

        guestResidenceCityTC = new TableColumn<>("Mesto");
        guestResidenceCityTC.setStyle("-fx-alignment: CENTER;");
        guestResidenceCityTC.setPrefWidth(100);
        guestResidenceCityTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getMestoGosta()));

        guestDOBTC = new TableColumn<>("Datum rodjenja");
        guestDOBTC.setStyle("-fx-alignment: CENTER;");
        guestDOBTC.setPrefWidth(100);
        guestDOBTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, Date> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getDatumRodjenjaGosta()));

        guestDocTypeTC = new TableColumn<>("Dokument");
        guestDocTypeTC.setStyle("-fx-alignment: CENTER;");
        guestDocTypeTC.setPrefWidth(100);
        guestDocTypeTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getSifraDokumentaGosta().getVrstaDokumenta()));

        guestDocNoTC = new TableColumn<>("Br. dok.");
        guestDocNoTC.setStyle("-fx-alignment: CENTER;");
        guestDocNoTC.setPrefWidth(100);
        guestDocNoTC.setCellValueFactory(
                (CellDataFeatures<Gostiposobama, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdGosta().getBrDokumentaGosta()));

        guestsPerRoomView.setItems(dataOL);
        guestsPerRoomView.getColumns().addAll(guestIDTC, guestFirstNameTC, guestLastNameTC, guestResidenceCityTC, guestDOBTC, guestDocTypeTC, guestDocNoTC);

        GPRMainBP.setCenter(guestsPerRoomView);

        //scena
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        GPRScene = new Scene(GPRMainBP);
    }

}
