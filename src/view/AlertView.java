/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import components.DecoratedButton;

/**
 *
 * @author Nebojsa Matic
 */
public class AlertView {

    public static void show(String msg, String viewTitle) {
        
        Stage alertStage = new Stage();
        alertStage.initModality(Modality.APPLICATION_MODAL);
        alertStage.getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        alertStage.setResizable(false);
        alertStage.setTitle(viewTitle);
        alertStage.setWidth(200);
        // zatvaranje stejdza
        alertStage.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                alertStage.close();
                event.consume();
            }
        });
        
        Label alertLabel = new Label();
        alertLabel.setText(msg);
//        iplabela.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(2), new Insets(0))));
        
        Button okB = new DecoratedButton();
        okB.setText("OK");
        okB.setGraphic(new ImageView(new Image("/resources/ikonaYes.png", 25, 25, false, false)));
        okB.setOnAction(e -> alertStage.close());
        
        
        VBox alertVBox = new VBox(20);
        alertVBox.getChildren().addAll(alertLabel, okB);
        alertVBox.setPadding(new Insets(10));
        alertVBox.setAlignment(Pos.CENTER);
        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaLogin.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        alertVBox.setBackground(background);
        //KRAJ KODA
        //
        
        
        Scene alertScene = new Scene(alertVBox);
        alertStage.setScene(alertScene);
        alertStage.showAndWait();
        
    }
}
