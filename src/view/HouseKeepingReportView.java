/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Rezervacija;
import util.HibernateUtil;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class HouseKeepingReportView extends Stage {

    Label arrivaListL, departuresListL;
    public static Button printB, closeB;

    List<Rezervacija> arrivaList, departuresList, otherList;

    HBox buttonHBox;
    public static VBox hkReportMainVBox, reportVBox;

    ScrollPane reportSP;

    public static Scene hkReportScene;

    public HouseKeepingReportView() {

        //dugmad
        printB = new Button();
        printB.setMinSize(130, 25);
        printB.setMaxSize(130, 25);
        printB.setText("Stampaj");

        closeB = new Button();
        closeB.setMinSize(130, 25);
        closeB.setMaxSize(130, 25);
        closeB.setText("Zatvori");

        buttonHBox = new HBox(10, printB, closeB);

        //tabela
        TreeItem<String> rootItem = new TreeItem<>();
        rootItem.setExpanded(true);

        TreeItem<String> ariivals = new TreeItem<>();
        ariivals.setValue("Dolasci");

        TreeItem<String> departures = new TreeItem<>();
        departures.setValue("Odlasci");

        TreeItem<String> other = new TreeItem<>();
        other.setValue("Ostali zauzeti kapaciteti");

        //dolasci
        arrivaList = HibernateUtil.dolasciNaDan(facilityName(), LocalDate.now());
        for (Rezervacija rez : arrivaList) {
            TreeItem<String> itemArrival = new TreeItem<>();
            itemArrival.setValue(rez.getIdSmJed().getNaziv());
            ariivals.getChildren().add(itemArrival);
        }

        //odlasci
        departuresList = HibernateUtil.odlasciNaDan(facilityName(), LocalDate.now());
        for (Rezervacija rez : departuresList) {
            TreeItem<String> itemDearture = new TreeItem<>();
            itemDearture.setValue(rez.getIdSmJed().getNaziv());
            departures.getChildren().add(itemDearture);
        }

        //ostalo
        otherList = HibernateUtil.ostaloNaDan(facilityName(), LocalDate.now());
        for (Rezervacija rez : otherList) {
            TreeItem<String> itemOther = new TreeItem<>();
            itemOther.setValue(rez.getIdSmJed().getNaziv());
            other.getChildren().add(itemOther);
        }

        rootItem.getChildren().addAll(ariivals, departures, other);

        TreeView<String> hkReportView = new TreeView<>(rootItem);
        hkReportView.setShowRoot(false);

        hkReportMainVBox = new VBox(hkReportView);
        hkReportMainVBox.setAlignment(Pos.TOP_CENTER);

        reportSP = new ScrollPane(hkReportMainVBox);
        reportSP.setMaxHeight(700);

        reportVBox = new VBox(5, reportSP, buttonHBox);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        hkReportScene = new Scene(reportVBox);

    }

}
