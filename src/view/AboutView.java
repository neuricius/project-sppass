/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Grupa2
 */
public class AboutView extends Stage {

    Label programNameL, authorNameL, companyNameL, licenceInfoL, licenceNumberL, licenceValidUntilL;

    public static Label companyNameROL, licenceNumberROL, licenceValidUntilROL;

    VBox mainVBox, licenceVBox, licenceVBoxLabels, licenceVBoxFields;

    HBox companyHBox, licenceInfoHBox;

    public static Scene scenaOProgramu;

    public AboutView() {

        programNameL = new Label();
        programNameL.setMinSize(300, 100);
        programNameL.setMaxSize(300, 100);
        programNameL.setText("SPPASS\nSistem za Pracenje Popune\nApartmanskog i Sobnog Smestaja");
        programNameL.setFont(Font.font("System", FontWeight.BOLD, 18));
        programNameL.setAlignment(Pos.CENTER);
        programNameL.setTextAlignment(TextAlignment.CENTER);
        programNameL.setPadding(new Insets(4));
        programNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(10, 20, 10, 20))));

        authorNameL = new Label();
        authorNameL.setMinHeight(25);
        authorNameL.setText("Autor: Nebojsa Neuricius Matic");
        authorNameL.setTextAlignment(TextAlignment.JUSTIFY);
        authorNameL.setFont(Font.font("System", FontWeight.BOLD, 14));
        authorNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        licenceInfoL = new Label();
        licenceInfoL.setMinSize(200, 50);
        licenceInfoL.setMaxSize(200, 50);
        licenceInfoL.setText("Podaci o licenci");
        licenceInfoL.setFont(Font.font("System", FontWeight.BOLD, 18));
        licenceInfoL.setAlignment(Pos.CENTER);
        licenceInfoL.setTextAlignment(TextAlignment.RIGHT);
        licenceInfoL.setPadding(new Insets(4));
        licenceInfoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(10, 20, 10, 20))));

        //
        companyNameL = new Label();
        companyNameL.setMinHeight(25);
        companyNameL.setText("Kompanija: ");
        companyNameL.setTextAlignment(TextAlignment.RIGHT);
        companyNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        companyNameROL = new ReadOnlyLabel();
        companyNameROL.setMinHeight(25);
        companyNameROL.setText("test");
        companyNameROL.setTextAlignment(TextAlignment.LEFT);
        companyNameROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //
        licenceNumberL = new Label();
        licenceNumberL.setMinHeight(25);
        licenceNumberL.setText("Broj licence: ");
        licenceNumberL.setTextAlignment(TextAlignment.RIGHT);
        licenceNumberL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        licenceNumberROL = new ReadOnlyLabel();
        licenceNumberROL.setMinHeight(25);
        licenceNumberROL.setText("test");
        licenceNumberROL.setTextAlignment(TextAlignment.LEFT);
        licenceNumberROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //
        licenceValidUntilL = new Label();
        licenceValidUntilL.setMinHeight(25);
        licenceValidUntilL.setText("Licenca vazi do: ");
        licenceValidUntilL.setTextAlignment(TextAlignment.JUSTIFY);
        licenceValidUntilL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        licenceValidUntilROL = new ReadOnlyLabel();
        licenceValidUntilROL.setMinHeight(25);
        licenceValidUntilROL.setText("test");
        licenceValidUntilROL.setTextAlignment(TextAlignment.LEFT);
        licenceValidUntilROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //
        licenceVBoxLabels = new VBox(5, companyNameL, licenceNumberL, licenceValidUntilL);

        licenceVBoxFields = new VBox(5, companyNameROL, licenceNumberROL, licenceValidUntilROL);
        
        licenceInfoHBox = new HBox(5, licenceVBoxLabels, licenceVBoxFields);
        licenceInfoHBox.setAlignment(Pos.CENTER);
        
        licenceVBox = new VBox(5, licenceInfoL, licenceInfoHBox);
        licenceVBox.setAlignment(Pos.CENTER);
        
        mainVBox = new VBox(5, programNameL, authorNameL, licenceVBox);
        mainVBox.setAlignment(Pos.CENTER);
        mainVBox.setPadding(new Insets(5,0,10,0));
        
        
        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaLogin2.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainVBox.setBackground(background);
        //KRAJ KODA
        //
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        scenaOProgramu = new Scene(mainVBox);
    }

}
