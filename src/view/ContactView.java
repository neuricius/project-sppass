/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Grupa2
 */
public class ContactView extends Stage {

    Label senderCompanyL, senderNameL, msgSubjectL, msgTextL;

    public static Label companyROL, senderROL;

    public static TextField msgSubjectTF, msgTextTF;

    public static Button sendMsgB, discardMsgB;

    VBox mainMailVBox, infoVBoxLabels, infoVBoxFields, msgTextVBox;

    HBox infoHBox, buttonHBox;

    public static Scene contactScene;

    public ContactView() {

        senderCompanyL = new Label();
        senderCompanyL.setMinHeight(25);
        senderCompanyL.setText("Kompanija");
        senderCompanyL.setTextAlignment(TextAlignment.RIGHT);
        senderCompanyL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        senderNameL = new Label();
        senderNameL.setMinHeight(25);
        senderNameL.setText("Posiljalac");
        senderNameL.setTextAlignment(TextAlignment.RIGHT);
        senderNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        msgSubjectL = new Label();
        msgSubjectL.setMinHeight(25);
        msgSubjectL.setText("Naslov");
        msgSubjectL.setTextAlignment(TextAlignment.RIGHT);
        msgSubjectL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        msgTextL = new Label();
        msgTextL.setMinHeight(25);
        msgTextL.setText("Tekst poruke");
        msgTextL.setAlignment(Pos.TOP_LEFT);
        msgTextL.setTextAlignment(TextAlignment.RIGHT);
        msgTextL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //
        companyROL = new ReadOnlyLabel();
        companyROL.setMinHeight(25);
        //prebaciti u kontroler
        companyROL.setTextAlignment(TextAlignment.RIGHT);
        companyROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        senderROL = new ReadOnlyLabel();
        senderROL.setMinHeight(25);
                //prebaciti u kontroler

        senderROL.setTextAlignment(TextAlignment.RIGHT);
        senderROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        msgSubjectTF = new TextField();
        msgSubjectTF.setMinSize(150, 25);
        msgSubjectTF.setMaxSize(150, 25);

        msgTextTF = new TextField();
        msgTextTF.setMinSize(280, 250);
        msgTextTF.setMaxSize(280, 250);

        //
        sendMsgB = new Button();
        sendMsgB.setMinSize(130, 25);
        sendMsgB.setMaxSize(130, 25);
        sendMsgB.setText("Posalji");

        discardMsgB = new Button();
        discardMsgB.setMinSize(130, 25);
        discardMsgB.setMaxSize(130, 25);
        discardMsgB.setText("Otkazi");

        //kont
        infoVBoxLabels = new VBox(5, senderCompanyL, senderNameL, msgSubjectL);
        infoVBoxLabels.setAlignment(Pos.CENTER_RIGHT);

        infoVBoxFields = new VBox(5, companyROL, senderROL, msgSubjectTF);
        infoVBoxFields.setAlignment(Pos.CENTER_LEFT);

        infoHBox = new HBox(10, infoVBoxLabels, infoVBoxFields);
        infoHBox.setAlignment(Pos.CENTER);

        msgTextVBox = new VBox(5, msgTextL, msgTextTF);
        msgTextVBox.setAlignment(Pos.TOP_LEFT);

        buttonHBox = new HBox(20, sendMsgB, discardMsgB);
        buttonHBox.setAlignment(Pos.CENTER);

        mainMailVBox = new VBox(5, infoHBox, msgTextVBox, buttonHBox);
        mainMailVBox.setAlignment(Pos.TOP_CENTER);
        mainMailVBox.setPadding(new Insets(5));

        //
        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaLogin2.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainMailVBox.setBackground(background);
        //KRAJ KODA
        //
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        contactScene = new Scene(mainMailVBox);
    }

}
