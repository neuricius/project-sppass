/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import components.DecoratedButton;

/**
 *
 * @author Nebojsa Matic
 */
public class ConfirmationDialog {

    public static Stage conDiaStage;
    public static boolean actionConfirmed;

    public static boolean confirmAction(String msg, String dialogTitle,
            String textConfirm, String textDeny) {

        actionConfirmed = false;

        conDiaStage = new Stage();
        conDiaStage.getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        conDiaStage.setResizable(false);
        conDiaStage.initModality(Modality.APPLICATION_MODAL);
        conDiaStage.setTitle(dialogTitle);
        conDiaStage.setMinWidth(250);
        
        
        //kod za alt-f4
        conDiaStage.addEventHandler(KeyEvent.KEY_TYPED, (KeyEvent t) -> {
            if (t.isAltDown() == true && t.getCode() == KeyCode.F4) {
            t.consume();
            }
        });
        

        Label cdLabel = new Label(msg);

        Button yesB = new DecoratedButton();
        yesB.setText(textConfirm);
        yesB.setGraphic(new ImageView(new Image("/resources/ikonaYes.png", 20, 20, false, false)));
        yesB.setOnAction(e -> btnYes_Clicked());

        Button noB = new DecoratedButton();
        noB.setText(textDeny);
        noB.setGraphic(new ImageView(new Image("/resources/ikonaNo.png", 20, 20, false, false)));
        noB.setOnAction(e -> btnNo_Clicked());

        HBox buttonHBox = new HBox(20);
        buttonHBox.getChildren().addAll(yesB, noB);
        buttonHBox.setPadding(new Insets(10));
        buttonHBox.setAlignment(Pos.CENTER);

        VBox mainVBox = new VBox(20);
        mainVBox.getChildren().addAll(cdLabel, buttonHBox);
        mainVBox.setAlignment(Pos.CENTER);
        mainVBox.setPadding(new Insets(10, 0, 0, 0));
        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaLogin.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainVBox.setBackground(background);
        //KRAJ KODA
        //
        
        

        Scene conDiaScene = new Scene(mainVBox);
        conDiaStage.setScene(conDiaScene);
        conDiaStage.showAndWait();
        return actionConfirmed;
    }

    private static void btnYes_Clicked() {
        conDiaStage.close();
        actionConfirmed = true;
    }

    private static void btnNo_Clicked() {
        conDiaStage.close();
        actionConfirmed = false;
    }

}
