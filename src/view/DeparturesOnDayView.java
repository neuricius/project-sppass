/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import model.Klijent;
import model.Rezervacija;
import util.HibernateUtil;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class DeparturesOnDayView extends Stage {

    List<Rezervacija> departuresList;

    public static Button refreshB, addB, deleteB;

    BorderPane departuresBP;

    public static TableView<Rezervacija> departuresTable;
    public static TableColumn<Rezervacija, Integer> reservationID;
    public static TableColumn<Rezervacija, Date> arrivalDate;

    HBox buttonHBox, datePickerHBox;

    Label departureDateL;
    DatePicker departureDateDP;

    public static Scene departuresScene;

    public DeparturesOnDayView() {

        departuresBP = new BorderPane();

        //izbor datuma
        departureDateL = new Label();
        departureDateL.setText("Izaberi datum: ");
        departureDateL.setTextAlignment(TextAlignment.RIGHT);

        departureDateDP = new DatePicker();
        departureDateDP.setMinSize(130, 25);
        departureDateDP.setMaxSize(130, 25);
        departureDateDP.setValue(LocalDate.now());
        departureDateDP.setOnAction(e -> {
            departuresList = HibernateUtil.odlasciNaDan(facilityName(), departureDateDP.getValue());
            ObservableList<Rezervacija> data = FXCollections.observableArrayList(departuresList);
            departuresTable.setItems(data);
        });

        datePickerHBox = new HBox(5, departureDateL, departureDateDP);
        datePickerHBox.setAlignment(Pos.CENTER);

        departuresBP.setTop(datePickerHBox);

        //dugmad
        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Stampaj");

        addB = new Button();
        addB.setMinSize(130, 25);
        addB.setMaxSize(130, 25);
        addB.setText("Dodaj");

        deleteB = new Button();
        deleteB.setMinSize(130, 25);
        deleteB.setMaxSize(130, 25);
        deleteB.setText("Obrisi");

        buttonHBox = new HBox(refreshB, addB, deleteB);
        departuresBP.setBottom(buttonHBox);

        // CENTAR
        departuresTable = new TableView<>();
        departuresTable.setEditable(false);

        departuresList = HibernateUtil.odlasciNaDan(facilityName(), departureDateDP.getValue());
        ObservableList<Rezervacija> data = FXCollections.observableArrayList(departuresList);

        //kolone
        //osn podaci o rez
        //glavna
        TableColumn<Rezervacija, String> resInfo = new TableColumn<>("Rezervacija");

        //redni broj (ID) rezervacije
        reservationID = new TableColumn<>("#");
        reservationID.setStyle("-fx-alignment: CENTER;");
        reservationID.setPrefWidth(25);
        reservationID.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));

        //status rez
        TableColumn<Rezervacija, String> resStatus = new TableColumn<>("Status");
        resStatus.setStyle("-fx-alignment: CENTER;");
        resStatus.setPrefWidth(75);
        resStatus.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdStatRez().getStatusRezervacije()));

        //
        // naziv sobe
        TableColumn<Rezervacija, String> roomNo = new TableColumn<>("Sm. Jed");
        roomNo.setStyle("-fx-alignment: CENTER;");
        roomNo.setPrefWidth(75);
        roomNo.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdSmJed().getNaziv()));

        //
        resInfo.getColumns().addAll(reservationID, resStatus, roomNo);

        //broj gostiju
        //glavna kol
        TableColumn<Rezervacija, Integer> numberOfGuests = new TableColumn<>("Broj Gostiju");
        numberOfGuests.setPrefWidth(80);

        //subkolone
        TableColumn<Rezervacija, Integer> numberOfAdults = new TableColumn<>("Odr");
        numberOfAdults.setStyle("-fx-alignment: CENTER;");
        numberOfAdults.setPrefWidth(40);
        numberOfAdults.setCellValueFactory(new PropertyValueFactory<>("brojOdraslih"));

        TableColumn<Rezervacija, Integer> numberOfChildren = new TableColumn<>("Deca");
        numberOfChildren.setStyle("-fx-alignment: CENTER;");
        numberOfChildren.setPrefWidth(40);
        numberOfChildren.setCellValueFactory(new PropertyValueFactory<>("brojDece"));

        numberOfGuests.getColumns().addAll(numberOfAdults, numberOfChildren);

        //period boravka
        //glavna kol
        TableColumn<Rezervacija, Date> stayDuration = new TableColumn<>("Period Boravka");

        //subkolone
        arrivalDate = new TableColumn<>("Dolazak");
        arrivalDate.setStyle("-fx-alignment: CENTER;");
        arrivalDate.setPrefWidth(80);
        arrivalDate.setCellValueFactory(new PropertyValueFactory<>("prijava"));

        TableColumn<Rezervacija, Date> departureDate = new TableColumn<>("Odlazak");
        departureDate.setStyle("-fx-alignment: CENTER;");
        departureDate.setPrefWidth(80);
        departureDate.setCellValueFactory(new PropertyValueFactory<>("odjava"));

        stayDuration.getColumns().addAll(arrivalDate, departureDate);

        //klijent
        //glavna
        TableColumn<Rezervacija, Klijent> clientInfo = new TableColumn<>("Klijent");
        clientInfo.setCellValueFactory(new PropertyValueFactory<>("idKlijent"));

        //subkolone
        TableColumn<Rezervacija, String> clientLastName = new TableColumn<>("Prezime");
        clientLastName.setPrefWidth(80);
        clientLastName.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getPrezimeKlijenta()));

        TableColumn<Rezervacija, String> clientFirstName = new TableColumn<>("Ime");
        clientFirstName.setPrefWidth(80);
        clientFirstName.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getImeKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCountry = new TableColumn<>("Drzava");
        clientResidenceCountry.setMinWidth(100);
        clientResidenceCountry.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getDrzavaKlijenta()));

        TableColumn<Rezervacija, String> clientResidenceCity = new TableColumn<>("Mesto");
        clientResidenceCity.setMinWidth(100);
        clientResidenceCity.setCellValueFactory(
                (TableColumn.CellDataFeatures<Rezervacija, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdKlijent().getMestoKlijenta()));

        clientInfo.getColumns().addAll(clientLastName, clientFirstName, clientResidenceCountry, clientResidenceCity);

        departuresTable.setItems(data);
        departuresTable.getColumns().addAll(resInfo, numberOfGuests, stayDuration, clientInfo);

        //kod za izbor po kojoj koloni da se sortira tabela
        departuresBP.setCenter(departuresTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        departuresScene = new Scene(departuresBP);
    }

}
