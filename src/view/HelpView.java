/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author Grupa2
 */
public class HelpView extends Stage {

    Image image1, image2, image3;

    ImageView frame1, frame2, frame3;

    TitledPane tpane1, tpane2, tpane3;

    Accordion testAccord1;

    public static Scene testScene;

    public HelpView() {

        tpane1 = new TitledPane("primer 1", new ImageView(new Image("/resources/testGif1.gif", 100, 100, true, true)));
//        tpane1.setMinSize(200, 200);
//        tpane1.setMaxSize(200, 200);

        tpane2 = new TitledPane("primer 2", new ImageView(new Image("/resources/testGif2.gif", 100, 100, true, true)));
//        tpane2.setMinSize(200, 200);
//        tpane2.setMaxSize(200, 200);

        tpane3 = new TitledPane("primer 3", new ImageView(new Image("/resources/testGif3.gif", 100, 100, true, true)));
//        tpane3.setMinSize(200, 200);
//        tpane3.setMaxSize(200, 200);

        testAccord1 = new Accordion(tpane1, tpane2, tpane3);
        testAccord1.setExpandedPane(tpane1);
        
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        testScene = new Scene(testAccord1);

    }

}
