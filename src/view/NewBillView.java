/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Nebojsa Matic
 */
public class NewBillView extends Stage {

    Label reservNumberL, pricePerPeriodL, totalPriceL, otherExpensesL, subTotaL,
            discountL, totalBalanceL, currentBalanceL, remainBalanceL, reservNumberROL;

    public static TextField pricePerPeriodTF, totalPriceTF,
            otherExpensesTF, SubTotalTF, totalBalanceTF,
            currentBalanceTF, remainBalanceTF;

    public static ComboBox<String> discountCB;

    public static Button saveChangesB, printResB, checkOutB,
            discardChangesB;

    GridPane buttonGP;

    public static Scene newBillScene;

    public NewBillView() {

        reservNumberL = new Label();
        reservNumberL.setPrefHeight(25);
        reservNumberL.setText("Sifra rezervacije");
        reservNumberL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        reservNumberL.setTextFill(Color.BLUE);
        reservNumberL.setTextAlignment(TextAlignment.RIGHT);

        reservNumberROL = new ReadOnlyLabel();
        reservNumberROL.setPrefHeight(25);
        reservNumberROL.setText("456");
        reservNumberROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        reservNumberROL.setTextFill(Color.BLUE);
        reservNumberROL.setTextAlignment(TextAlignment.RIGHT);

        pricePerPeriodL = new Label();
        pricePerPeriodL.setPrefHeight(25);
        pricePerPeriodL.setText("Cena nocenja");
        pricePerPeriodL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        pricePerPeriodL.setTextFill(Color.BLUE);
        pricePerPeriodL.setTextAlignment(TextAlignment.RIGHT);

        totalPriceL = new Label();
        totalPriceL.setPrefHeight(25);
        totalPriceL.setText("Ukupna cena");
        totalPriceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalPriceL.setTextAlignment(TextAlignment.RIGHT);

        otherExpensesL = new Label();
        otherExpensesL.setPrefHeight(25);
        otherExpensesL.setText("Ostali troskovi");
        otherExpensesL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        otherExpensesL.setTextAlignment(TextAlignment.RIGHT);

        subTotaL = new Label();
        subTotaL.setPrefHeight(25);
        subTotaL.setText("Medjuzbir");
        subTotaL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        subTotaL.setTextAlignment(TextAlignment.RIGHT);

        discountL = new Label();
        discountL.setPrefHeight(25);
        discountL.setText("Popust");
        discountL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        discountL.setTextAlignment(TextAlignment.RIGHT);

        totalBalanceL = new Label();
        totalBalanceL.setPrefHeight(25);
        totalBalanceL.setText("Za placanje");
        totalBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        totalBalanceL.setTextAlignment(TextAlignment.RIGHT);

        currentBalanceL = new Label();
        currentBalanceL.setPrefHeight(25);
        currentBalanceL.setText("Placeno");
        currentBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        currentBalanceL.setTextAlignment(TextAlignment.RIGHT);

        remainBalanceL = new Label();
        remainBalanceL.setPrefHeight(25);
        remainBalanceL.setText("Preostali iznos");
        remainBalanceL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        remainBalanceL.setTextFill(Color.BLUE);
        remainBalanceL.setTextAlignment(TextAlignment.RIGHT);

        //polja
        pricePerPeriodTF = new TextField();
        pricePerPeriodTF.setMinSize(130, 25);
        pricePerPeriodTF.setMaxSize(130, 25);
        //staviti u kontroler da se unose double vrednosti

        totalPriceTF = new TextField();
        totalPriceTF.setMinSize(130, 25);
        totalPriceTF.setMaxSize(100, 25);

        otherExpensesTF = new TextField();
        otherExpensesTF.setMinSize(130, 25);
        otherExpensesTF.setMaxSize(130, 25);

        SubTotalTF = new TextField();
        SubTotalTF.setMinSize(130, 25);
        SubTotalTF.setMaxSize(130, 25);

        discountCB = new ComboBox<>();
        discountCB.setMinSize(130, 25);
        discountCB.setMaxSize(130, 25);

        totalBalanceTF = new TextField();
        totalBalanceTF.setMinSize(130, 25);
        totalBalanceTF.setMaxSize(130, 25);

        currentBalanceTF = new TextField();
        currentBalanceTF.setMinSize(130, 25);
        currentBalanceTF.setMaxSize(130, 25);

        remainBalanceTF = new TextField();
        remainBalanceTF.setMinSize(130, 25);
        remainBalanceTF.setMaxSize(130, 25);

        //dugmad
        saveChangesB = new Button();
        saveChangesB.setMinSize(130, 25);
        saveChangesB.setMaxSize(130, 25);
        saveChangesB.setText("Sacuvaj");
        //prebaciti u kontrolor

        printResB = new Button();
        printResB.setMinSize(130, 25);
        printResB.setMaxSize(130, 25);
        printResB.setText("Stampaj");

        checkOutB = new Button();
        checkOutB.setMinSize(130, 25);
        checkOutB.setMaxSize(130, 25);
        checkOutB.setText("Odjava");

        discardChangesB = new Button();
        discardChangesB.setMinSize(130, 25);
        discardChangesB.setMaxSize(130, 25);
        discardChangesB.setText("Otkazi");

        //kontejneri
        VBox desniVBoxLabele = new VBox(5, reservNumberL, pricePerPeriodL, totalPriceL, otherExpensesL, subTotaL, discountL,
                totalBalanceL, currentBalanceL, remainBalanceL);
        desniVBoxLabele.setAlignment(Pos.CENTER_RIGHT);

        VBox desniVBoxPolja = new VBox(5, reservNumberROL, pricePerPeriodTF,
                totalPriceTF, otherExpensesTF, SubTotalTF, discountCB,
                totalBalanceTF, currentBalanceTF, remainBalanceTF);
        desniVBoxPolja.setAlignment(Pos.CENTER_LEFT);

        HBox desniHBox = new HBox(10, desniVBoxLabele, desniVBoxPolja);
        desniHBox.setAlignment(Pos.TOP_CENTER);

        buttonGP = new GridPane();
        buttonGP.setHgap(2);
        buttonGP.setVgap(2);
        buttonGP.add(printResB, 0, 0);
        buttonGP.add(checkOutB, 1, 0);
        buttonGP.add(saveChangesB, 0, 1);
        buttonGP.add(discardChangesB, 1, 1);

        VBox desniVBox = new VBox(desniHBox, buttonGP);
        desniVBox.setSpacing(5);
        desniVBox.setAlignment(Pos.TOP_CENTER);
        desniVBox.setMaxWidth(250);
        desniVBox.setPrefWidth(250);
        desniVBox.setMaxWidth(250);
        desniVBox.setPadding(new Insets(10, 10, 0, 10));

        // new Image(url)
        Image image = new Image("/resources/pozadinaUtil.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        desniVBox.setBackground(background);
        //set scena
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        newBillScene = new Scene(desniVBox);
    }

}
