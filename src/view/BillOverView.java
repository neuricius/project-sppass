/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Date;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Naplata;
import model.Profil;
import static util.HibernateUtil.skupiListuNaplata;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class BillOverView extends Stage {

    List<Naplata> billList;

    public static Button refreshB, addBillB, deleteBillB;

    BorderPane mainBP;

    public static TableView<Naplata> billTable;

    public static TableColumn<Naplata, Integer> billIDTC, resIDTC;
    public static TableColumn<Naplata, Double> priceAmountTC, stayingTotalTC, otherExpensesTC,
            subTotalTC, currentBalanceTC, remainBalanceTC;
    public static TableColumn<Naplata, Date> entryDateTC;
    public static TableColumn<Naplata, String> discountTC, billStatusTC, resCarrierTC, billInfoTC, entryAuthorTC;
    public static TableColumn<Naplata, Profil> entryDataTC;

    public static Scene billOverviewScene;

    public BillOverView() {

        mainBP = new BorderPane();

        refreshB = new Button();
        refreshB.setMinSize(130, 25);
        refreshB.setMaxSize(130, 25);
        refreshB.setText("Stampaj");

        addBillB = new Button();
        addBillB.setMinSize(130, 25);
        addBillB.setMaxSize(130, 25);
        addBillB.setText("Dodaj");

        deleteBillB = new Button();
        deleteBillB.setMinSize(130, 25);
        deleteBillB.setMaxSize(130, 25);
        deleteBillB.setText("Obrisi");

        HBox buttonHBox = new HBox(refreshB, addBillB, deleteBillB);
        mainBP.setBottom(buttonHBox);

        // CENTAR
        billTable = new TableView<>();
        billTable.setEditable(false);

        billList = skupiListuNaplata(facilityName());
        ObservableList<Naplata> data = FXCollections.observableArrayList(billList);

        //kolone
        //osn podaci o racunu
        //glavna
        billInfoTC = new TableColumn<>("Racun");

        //redni broj (ID) cene
        billIDTC = new TableColumn<>("#");
        billIDTC.setStyle("-fx-alignment: CENTER;");
        billIDTC.setPrefWidth(25);
        billIDTC.setCellValueFactory(new PropertyValueFactory<>("idNaplate"));

        //
        // sifra rezervacije
        resIDTC = new TableColumn<>("Sifra\nRezerv.");
        resIDTC.setStyle("-fx-alignment: CENTER;");
        resIDTC.setPrefWidth(50);
        resIDTC.setCellValueFactory(
                (CellDataFeatures<Naplata, Integer> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdRezervacija().getIdRezervacija()));

        //nosilac rezervacije
        resCarrierTC = new TableColumn<>("Nosilac\nRezerv.");
        resCarrierTC.setStyle("-fx-alignment: CENTER;");
        resCarrierTC.setPrefWidth(75);
        resCarrierTC.setCellValueFactory(
                (CellDataFeatures<Naplata, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getIdRezervacija().getIdKlijent().getPrezimeKlijenta()));

        //
        billInfoTC.getColumns().addAll(billIDTC, resIDTC, resCarrierTC);
        //

        //specifikacije naplate
        //glavna kol
        TableColumn<Naplata, Double> billDetails = new TableColumn<>("Detalji Racuna");

        //subkolone
        priceAmountTC = new TableColumn<>("Cena\nnocenja");
        priceAmountTC.setStyle("-fx-alignment: CENTER;");
        priceAmountTC.setPrefWidth(60);
        priceAmountTC.setCellValueFactory(
                (CellDataFeatures<Naplata, Double> param)
                -> new SimpleObjectProperty<>(param.getValue().getCenaNocenja().getCena()));

        stayingTotalTC = new TableColumn<>("Cena\nsmestaja");
        stayingTotalTC.setStyle("-fx-alignment: CENTER;");
        stayingTotalTC.setPrefWidth(60);
        stayingTotalTC.setCellValueFactory(new PropertyValueFactory<>("cenaSmestaja"));

        otherExpensesTC = new TableColumn<>("Ostali\ntroskovi");
        otherExpensesTC.setStyle("-fx-alignment: CENTER;");
        otherExpensesTC.setPrefWidth(60);
        otherExpensesTC.setCellValueFactory(new PropertyValueFactory<>("ostaliTroskovi"));

        subTotalTC = new TableColumn<>("Medjuzbir");
        subTotalTC.setStyle("-fx-alignment: CENTER;");
        subTotalTC.setPrefWidth(60);
        subTotalTC.setCellValueFactory(new PropertyValueFactory<>("medjuZbir"));

        discountTC = new TableColumn<>("Popust");
        discountTC.setStyle("-fx-alignment: CENTER;");
        discountTC.setPrefWidth(60);
        discountTC.setCellValueFactory(
                (CellDataFeatures<Naplata, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getSifraPopusta().getNazivPopusta()));

        currentBalanceTC = new TableColumn<>("Uplaceno");
        currentBalanceTC.setStyle("-fx-alignment: CENTER;");
        currentBalanceTC.setPrefWidth(60);
        currentBalanceTC.setCellValueFactory(new PropertyValueFactory<>("Uplaceno"));

        remainBalanceTC = new TableColumn<>("Preostali\niznos");
        remainBalanceTC.setStyle("-fx-alignment: CENTER;");
        remainBalanceTC.setPrefWidth(60);
        remainBalanceTC.setCellValueFactory(new PropertyValueFactory<>("PreostaliIznos"));

        billStatusTC = new TableColumn<>("Status");
        billStatusTC.setStyle("-fx-alignment: CENTER;");
        billStatusTC.setPrefWidth(100);
        billStatusTC.setCellValueFactory(
                (CellDataFeatures<Naplata, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getStatusNaplate().getStatusNaplate()));

        billDetails.getColumns().addAll(priceAmountTC, stayingTotalTC, otherExpensesTC,
                subTotalTC, discountTC, currentBalanceTC, remainBalanceTC, billStatusTC);

        //podaci o izmeni
        //glavna
        entryDataTC = new TableColumn<>("Izmene");

        //subkolone
        entryAuthorTC = new TableColumn<>("Autor");
        entryAuthorTC.setPrefWidth(80);
        entryAuthorTC.setCellValueFactory(
                (CellDataFeatures<Naplata, String> param)
                -> new SimpleObjectProperty<>(param.getValue().getAutorIzmene().getImeProfila()));

        entryDateTC = new TableColumn<>("Datum");
        entryDateTC.setStyle("-fx-alignment: CENTER;");
        entryDateTC.setPrefWidth(80);
        entryDateTC.setCellValueFactory(new PropertyValueFactory<>("datumIzmene"));

        entryDataTC.getColumns().addAll(entryAuthorTC, entryDateTC);

        billTable.setItems(data);
        billTable.getColumns().addAll(billInfoTC, billDetails, entryDataTC);

        //kod za izbor po kojoj koloni da se sortira tabela
        mainBP.setCenter(billTable);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        billOverviewScene = new Scene(mainBP);

    }

}
