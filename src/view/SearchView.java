/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author Matic Nebojsa
 */
public class SearchView extends Stage {

    public static Button searchB, cancelB;

    Label resNoL, carrierFirstNameL, carrierLastNameL, periodStartL, periodEndL, roomNameL;

    public static TextField resNoTF, carrierFirstNameTF, carrierLastNameTF;

    public static ComboBox<String> roomNameCB;

    public static DatePicker periodStartDP, periodEndDP;

    VBox mainVBox,
            resNoVBox, carrierFirstNameVBox, carrierLastNameVBox, periodStartVBox,
            periodEndVBox, roomNameLVBox;

    HBox NoAndNameHBox, datesHBox, buttonHBox;

    public static Scene SearchScene;

    public SearchView() {

        //labele
        resNoL = new Label();
        resNoL.setMinHeight(25);
        resNoL.setText("#");
        resNoL.setTextAlignment(TextAlignment.LEFT);

        carrierFirstNameL = new Label();
        carrierFirstNameL.setMinHeight(25);
        carrierFirstNameL.setText("Ime");
        carrierFirstNameL.setTextAlignment(TextAlignment.LEFT);

        carrierLastNameL = new Label();
        carrierLastNameL.setMinHeight(25);
        carrierLastNameL.setText("Prezime");
        carrierLastNameL.setTextAlignment(TextAlignment.LEFT);

        periodStartL = new Label();
        periodStartL.setMinHeight(25);
        periodStartL.setText("Od");
        periodStartL.setTextAlignment(TextAlignment.LEFT);

        periodEndL = new Label();
        periodEndL.setMinHeight(25);
        periodEndL.setText("Do");
        periodEndL.setTextAlignment(TextAlignment.LEFT);

        roomNameL = new Label();
        roomNameL.setMinHeight(25);
        roomNameL.setText("Naziv Sm. jed.");
        roomNameL.setTextAlignment(TextAlignment.LEFT);

        //polja
        searchB = new Button();
        searchB.setMinSize(120, 25);
        searchB.setMaxSize(120, 25);
        searchB.setText("Trazi");

        cancelB = new Button();
        cancelB.setMinSize(120, 25);
        cancelB.setMaxSize(120, 25);
        cancelB.setText("Otkazi");

        roomNameCB = new ComboBox<>();
        roomNameCB.setMinSize(120, 25);
        roomNameCB.setMaxSize(120, 25);

        resNoTF = new TextField();
        resNoTF.setMinSize(75, 25);
        resNoTF.setMaxSize(75, 25);

        carrierFirstNameTF = new TextField();
        carrierFirstNameTF.setMinSize(150, 25);
        carrierFirstNameTF.setMaxSize(150, 25);

        carrierLastNameTF = new TextField();
        carrierLastNameTF.setMinSize(150, 25);
        carrierLastNameTF.setMaxSize(150, 25);

        periodStartDP = new DatePicker(LocalDate.now());

        periodEndDP = new DatePicker(LocalDate.now());

        //kontejneri
        resNoVBox = new VBox(5, resNoL, resNoTF);
        resNoVBox.setAlignment(Pos.CENTER_LEFT);

        carrierFirstNameVBox = new VBox(5, carrierFirstNameL, carrierFirstNameTF);
        carrierFirstNameVBox.setAlignment(Pos.CENTER_LEFT);

        carrierLastNameVBox = new VBox(5, carrierLastNameL, carrierLastNameTF);
        carrierLastNameVBox.setAlignment(Pos.CENTER_LEFT);

        periodStartVBox = new VBox(5, periodStartL, periodStartDP);
        periodStartVBox.setAlignment(Pos.CENTER_LEFT);

        periodEndVBox = new VBox(5, periodEndL, periodEndDP);
        periodEndVBox.setAlignment(Pos.CENTER_LEFT);

        roomNameLVBox = new VBox(5, roomNameL, roomNameCB);
        roomNameLVBox.setAlignment(Pos.CENTER_LEFT);

        NoAndNameHBox = new HBox(5, resNoVBox, carrierFirstNameVBox, carrierLastNameVBox);
        NoAndNameHBox.setAlignment(Pos.CENTER_LEFT);

        datesHBox = new HBox(5, periodStartVBox, periodEndVBox);
        datesHBox.setAlignment(Pos.CENTER);

        buttonHBox = new HBox(10, searchB, cancelB);
        buttonHBox.setAlignment(Pos.CENTER);

        mainVBox = new VBox(5, NoAndNameHBox, datesHBox, roomNameLVBox, buttonHBox);
        mainVBox.setPadding(new Insets(0, 5, 10, 5));

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        SearchScene = new Scene(mainVBox);
    }

}
