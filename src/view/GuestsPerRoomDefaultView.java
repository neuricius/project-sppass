/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Gostiposobama;
import model.Rezervacija;
import util.HibernateUtil;
import static util.HibernateUtil.facilityName;

/**
 *
 * @author Nebojsa Matic
 */
public class GuestsPerRoomDefaultView extends Stage {

    public static Button printB, closeB;

    List<Rezervacija> activeReservationsList;
    
    HBox buttonHBox;
    public static VBox GPSPaneVBox, reportPaneVBox;

    ScrollPane GPSSP;

    public static Scene GPSScene;

    public GuestsPerRoomDefaultView() {

        //dugmad
        printB = new Button();
        printB.setMinSize(120, 25);
        printB.setMaxSize(120, 25);
        printB.setText("Stampaj");

        closeB = new Button();
        closeB.setMinSize(120, 25);
        closeB.setMaxSize(120, 25);
        closeB.setText("Zatvori");

        buttonHBox = new HBox(10, printB, closeB);
        
   
        //tabela
        //fiktivni root
        TreeItem<String> rootItem = new TreeItem<>();
        rootItem.setExpanded(true);

        activeReservationsList = HibernateUtil.listaAktivnihRezervacija(facilityName());
        for (Rezervacija rez : activeReservationsList) {
            TreeItem<String> nazivSobe = new TreeItem<>();
            nazivSobe.setValue(rez.getIdSmJed().getNaziv());
            nazivSobe.setExpanded(true);
            for (Gostiposobama gps : HibernateUtil.skupiListuGostijuZaRezervaciju(rez.getIdRezervacija())) {
                TreeItem<String> itemGost = new TreeItem<>();
                itemGost.setValue(gps.getIdGosta().getPrezimeGosta()+ " " + gps.getIdGosta().getImeGosta());
                nazivSobe.getChildren().add(itemGost);
            }
            rootItem.getChildren().add(nazivSobe);
        }

        TreeView<String> viewZaDomacicu = new TreeView<>(rootItem);
        viewZaDomacicu.setShowRoot(false);
//        viewZaDomacicu.setMinSize(0, 0);

        GPSPaneVBox = new VBox(viewZaDomacicu);
        GPSPaneVBox.setAlignment(Pos.TOP_CENTER);

        GPSSP = new ScrollPane(GPSPaneVBox);
        GPSSP.setMaxHeight(700);

        reportPaneVBox = new VBox(5, GPSSP, buttonHBox);

        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        GPSScene = new Scene(reportPaneVBox);

    }

}
