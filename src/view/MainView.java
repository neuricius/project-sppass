/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import components.DigitalDate;
import components.TablePane;
import components.DecoratedButton;
import components.DigitalClock;
import components.ResourcePane;
import components.DatePane;
import java.time.LocalDate;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import static util.HibernateUtil.numberOfRooms;
import static util.HibernateUtil.numberOfDates;

/**
 *
 * @author Nebojsa Matic
 */
public class MainView extends Stage {

    // deklarisanje slepera elemenata
    Label showTimeL, showDateL, numberOfGuestsL, numberOfArrivalsL,
            numberOfDeparturesL, numberOfOccupiedL, numberOfAvailableL, currentUserL, goToDayL;

    public static Label systemMsgL, currentUserNameL, numberOfGuestsCountL, numberOfArrivalsCountL,
            numberOfDeparturesCountL, numberOfOccupiedCountL, numberOfAvailableCountL;

    public static Button mainSearchB, mainNewResB, mainReportB, refreshB;

    Menu mainMenuUser, mainMenuReservations, mainMenuBilling, mainMenuReports, mainMenuHelp;

    public Menu adminMenuSettings;

    public MenuItem deleteReservationMI;

    public static MenuItem changeUserMI, logoutUserMI, exitProgramMI,
            newReservationMI, showAllReservationsMI, updateReservationsMI,
            newBillMI, billForRoomMI, billViewerMI,
            guestsPerRoomMI, arrivalsPerDayMI, departuresPerDayMI, housekeepingReportMI,
            editRoomsMI, editUsersMI, editPricingMI,
            helpAboutMI, helpHelpMI, helpContactMI;

    MenuBar mainMB, auxMB;

    public static BorderPane mainPaneBP, mainPaneCenterBP;

    HBox bottomStatusBarMainHBox, bottomLeftStatusBarHBox, bottomRightStatusBarHBox, bottomCenterStatusBarHBox,
            leftStatusBarHBox, menuBarHBox, centerPaneTop;

    VBox leftMainVBox, leftButtonBarVBox, leftStatusBarLabelsVBox, leftStatusBarValuesVBox, refreshVBox;

    AnchorPane centerAnchor;

    ScrollPane centerScroll, sp2, sp3;

    public static ScrollPane sp1;

    public static DatePicker selectDayDP;

    public static GridPane mainGridGP, tableGridGP, timeGridGP, resourcesGridGP;

    public static Scene mainScene;

    public static int roomsNo;
    private static int daysNo;

    public MainView(LocalDate someDate) {

        roomsNo = numberOfRooms();
        daysNo = numberOfDates(someDate);

        //definisanje labela
        //labela za vreme
        showTimeL = new DigitalClock();
        showTimeL.setAlignment(Pos.CENTER);
        showTimeL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));
        showTimeL.setMinSize(60, 20);
        showTimeL.setMaxSize(120, 20);
        showTimeL.setPrefSize(60, 20);

        //labela za datum
        showDateL = new DigitalDate();
        showDateL.setAlignment(Pos.CENTER);
        showDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));
        showDateL.setMinSize(80, 20);
        showDateL.setMaxSize(160, 20);
        showDateL.setPrefSize(80, 20);

        //labela gostiju
        numberOfGuestsL = new Label("Gostiju");
        numberOfGuestsL.setMinSize(40, 20);
        numberOfGuestsL.setPrefSize(50, 20);
        numberOfGuestsL.setMaxSize(80, 20);
        numberOfGuestsL.setAlignment(Pos.CENTER);

        //labela dolasci
        numberOfArrivalsL = new Label("Dolasci");
        numberOfArrivalsL.setMinSize(40, 20);
        numberOfArrivalsL.setPrefSize(50, 20);
        numberOfArrivalsL.setMaxSize(80, 20);
        numberOfArrivalsL.setAlignment(Pos.CENTER);

        //labela odlasci
        numberOfDeparturesL = new Label("Odlasci");
        numberOfDeparturesL.setMinSize(40, 20);
        numberOfDeparturesL.setPrefSize(50, 20);
        numberOfDeparturesL.setMaxSize(80, 20);
        numberOfDeparturesL.setAlignment(Pos.CENTER);

        //labela zauzeto
        numberOfOccupiedL = new Label("Zauzeto");
        numberOfOccupiedL.setMinSize(40, 20);
        numberOfOccupiedL.setPrefSize(50, 20);
        numberOfOccupiedL.setMaxSize(80, 20);
        numberOfOccupiedL.setAlignment(Pos.CENTER);

        //labela slobodno
        numberOfAvailableL = new Label("Slobodno");
        numberOfAvailableL.setMinSize(40, 20);
        numberOfAvailableL.setPrefSize(50, 20);
        numberOfAvailableL.setMaxSize(80, 20);
        numberOfAvailableL.setAlignment(Pos.CENTER);

        //labele sa iznos
        //labela br gostiju
        numberOfGuestsCountL = new Label();
        numberOfGuestsCountL.setMinSize(10, 20);
        numberOfGuestsCountL.setPrefSize(20, 20);
        numberOfGuestsCountL.setMaxSize(40, 20);
        numberOfGuestsCountL.setAlignment(Pos.CENTER);

        //labela br dolasci
        numberOfArrivalsCountL = new Label();
        numberOfArrivalsCountL.setMinSize(10, 20);
        numberOfArrivalsCountL.setPrefSize(20, 20);
        numberOfArrivalsCountL.setMaxSize(40, 20);
        numberOfArrivalsCountL.setAlignment(Pos.CENTER);

        //labela br odlasci
        numberOfDeparturesCountL = new Label();
        numberOfDeparturesCountL.setMinSize(10, 20);
        numberOfDeparturesCountL.setPrefSize(20, 20);
        numberOfDeparturesCountL.setMaxSize(40, 20);
        numberOfDeparturesCountL.setAlignment(Pos.CENTER);

        //labela br zauzeto
        numberOfOccupiedCountL = new Label();
        numberOfOccupiedCountL.setMinSize(10, 20);
        numberOfOccupiedCountL.setPrefSize(20, 20);
        numberOfOccupiedCountL.setMaxSize(40, 20);
        numberOfOccupiedCountL.setAlignment(Pos.CENTER);

        //labela br slobodno
        numberOfAvailableCountL = new Label();
        numberOfAvailableCountL.setMinSize(10, 20);
        numberOfAvailableCountL.setPrefSize(20, 20);
        numberOfAvailableCountL.setMaxSize(40, 20);
        numberOfAvailableCountL.setAlignment(Pos.CENTER);

        //labela za sistemske poruke
        systemMsgL = new Label();
        systemMsgL.setAlignment(Pos.CENTER_LEFT);
        systemMsgL.setPadding(new Insets(0, 0, 0, 5));
        systemMsgL.setFocusTraversable(false);
        systemMsgL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(1), new Insets(0))));
        systemMsgL.setMinHeight(20);
        systemMsgL.setPrefHeight(20);
        systemMsgL.setMaxHeight(20);

        //labele za ispis korisnika
        currentUserL = new Label();
        currentUserL.setAlignment(Pos.CENTER_LEFT);
        currentUserL.setPadding(new Insets(0, 0, 0, 5));
        currentUserL.setFocusTraversable(false);
        currentUserL.setText("Trenutni korisnik: ");
        currentUserL.setMinSize(100, 20);
        currentUserL.setPrefSize(100, 20);
        currentUserL.setMaxSize(100, 20);

        currentUserNameL = new Label();
        currentUserNameL.setAlignment(Pos.CENTER_LEFT);
        currentUserNameL.setFocusTraversable(false);
        currentUserNameL.setMinSize(60, 20);
        currentUserNameL.setPrefSize(60, 20);
        currentUserNameL.setMaxSize(60, 20);

        // definisanje dugmadi
        //dugme za pretragu
        mainSearchB = new DecoratedButton();
        Image lupica = new Image("/resources/ikonaSearch.png", 40, 40, false, false);
        ImageView lupicaView = new ImageView(lupica);
        mainSearchB.setGraphic(lupicaView);
        mainSearchB.setMinSize(40, 40);
        mainSearchB.setPrefSize(60, 60);
        mainSearchB.setMaxSize(60, 60);

        //dugme za dodavanje rezervacije
        mainNewResB = new DecoratedButton();
        Image novaRez = new Image("/resources/ikonaZvonce.png", 40, 40, false, false);
        ImageView novaRezView = new ImageView(novaRez);
        mainNewResB.setGraphic(novaRezView);
        mainNewResB.setMinSize(40, 40);
        mainNewResB.setPrefSize(60, 60);
        mainNewResB.setMaxSize(60, 60);

        //dugme za izradu izvestaja
        mainReportB = new DecoratedButton();
        Image izvestaj = new Image("/resources/ikonaReport.png", 40, 40, false, false);
        ImageView izvestajView = new ImageView(izvestaj);
        mainReportB.setGraphic(izvestajView);
        mainReportB.setMinSize(40, 40);
        mainReportB.setPrefSize(60, 60);
        mainReportB.setMaxSize(60, 60);

        // definisanje menija
        mainMB = new MenuBar();
        mainMB.setMaxHeight(25);
        mainMB.setMinWidth(300);
        mainMB.setPrefWidth(300);
        mainMB.setMaxWidth(1920);

        auxMB = new MenuBar();
        auxMB.setMaxHeight(25);
        auxMB.setMinWidth(170);
        auxMB.setPrefWidth(290);
        auxMB.setMaxWidth(500);

        //glavni meni
        mainMenuUser = new Menu("Korisnik");
        mainMenuUser.setGraphic(new ImageView(new Image("/resources/ikonaKorisnik.png", 25, 25, false, false)));
        mainMenuReservations = new Menu("Rezervacija");
        mainMenuReservations.setGraphic(new ImageView(new Image("/resources/ikonaZvonceMeni.png", 25, 25, false, false)));
        mainMenuBilling = new Menu("Racuni");
        mainMenuBilling.setGraphic(new ImageView(new Image("/resources/ikonaEuroMeni.png", 25, 25, false, false)));
        mainMenuReports = new Menu("Izvestaji");
        mainMenuReports.setGraphic(new ImageView(new Image("/resources/ikonaIzvestajMeni.png", 25, 25, false, false)));
        mainMB.getMenus().addAll(mainMenuUser, mainMenuReservations, mainMenuBilling, mainMenuReports);
        mainMB.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

        //meni korisnik
        changeUserMI = new MenuItem("Promena Korisnika", new ImageView(new Image("/resources/ikonaSwitch.png", 25, 25, false, false)));
        logoutUserMI = new MenuItem("Odjava Korisnika", new ImageView(new Image("/resources/ikonaLogOut.png", 25, 25, false, false)));
        exitProgramMI = new MenuItem("Izlaz iz programa", new ImageView(new Image("/resources/ikonaExit.png", 25, 25, false, false)));
        exitProgramMI.setAccelerator(KeyCombination.keyCombination("Alt+F4"));
        mainMenuUser.getItems().addAll(changeUserMI, logoutUserMI, new SeparatorMenuItem(), exitProgramMI);

        //meni za rezervacije
        newReservationMI = new MenuItem("Dodaj novu rezervaciju", new ImageView(new Image("/resources/ikonaDodaj.png", 25, 25, false, false)));
        showAllReservationsMI = new MenuItem("Pregled svih rezervacija", new ImageView(new Image("/resources/ikonaDvogled.png", 25, 25, false, false)));
        updateReservationsMI = new MenuItem("Azuriranje rezervacija", new ImageView(new Image("/resources/ikonaSwitch.png", 25, 25, false, false)));
        deleteReservationMI = new MenuItem("Brisanje rezervacije", new ImageView(new Image("/resources/ikonaDelete.png", 25, 25, false, false)));
        mainMenuReservations.getItems().addAll(newReservationMI, showAllReservationsMI, updateReservationsMI, deleteReservationMI);

        //meni za racune
        newBillMI = new MenuItem("Napravi racun", new ImageView(new Image("/resources/ikonaRacun.png", 25, 25, false, false)));
        billForRoomMI = new MenuItem("Racun za sobu"); // zasad neaktivno 
        billViewerMI = new MenuItem("Pregled racuna", new ImageView(new Image("/resources/ikonaTabela.png", 25, 25, false, false)));
        mainMenuBilling.getItems().addAll(newBillMI, billViewerMI);

        //meni za izvestaje 
        guestsPerRoomMI = new MenuItem("Gosti po sobama", new ImageView(new Image("/resources/ikonaGosti.png", 25, 25, false, false)));
        arrivalsPerDayMI = new MenuItem("Dolasci za dan", new ImageView(new Image("/resources/ikonaDolasci.png", 25, 25, false, false)));
        departuresPerDayMI = new MenuItem("Odlasci za dan", new ImageView(new Image("/resources/ikonaOdlasci.png", 25, 25, false, false)));
        housekeepingReportMI = new MenuItem("Izvestaj za domacinstvo", new ImageView(new Image("/resources/ikonaDomacinstvo.png", 20, 20, false, false)));
        mainMenuReports.getItems().addAll(guestsPerRoomMI, arrivalsPerDayMI, departuresPerDayMI, housekeepingReportMI);

        //pomocni meni 
        adminMenuSettings = new Menu("Podesavanja");
        adminMenuSettings.setGraphic(new ImageView(new Image("/resources/ikonaSettingsMeni.png", 25, 25, false, false)));

        mainMenuHelp = new Menu("Pomoc");
        mainMenuHelp.setGraphic(new ImageView(new Image("/resources/ikonaPomocMeni.png", 25, 25, false, false)));
        auxMB.getMenus().addAll(mainMenuHelp, adminMenuSettings);
        auxMB.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

        //meni za podesavanja (samo za admin pristup)
        editRoomsMI = new MenuItem("Uredjivanje soba", new ImageView(new Image("/resources/ikonaEditSoba.png", 25, 25, false, false)));
        editUsersMI = new MenuItem("Uredjivanje korisnika", new ImageView(new Image("/resources/ikonaEditUser.png", 25, 25, false, false)));
        editPricingMI = new MenuItem("Uredjivanje cena", new ImageView(new Image("/resources/ikonaEditPrice.png", 25, 25, false, false)));
        adminMenuSettings.getItems().addAll(editRoomsMI, editUsersMI, editPricingMI);

        //meni za pomoc
        helpHelpMI = new MenuItem("Pomoc", new ImageView(new Image("/resources/ikonaPomoc.png", 25, 25, false, false)));
        helpAboutMI = new MenuItem("O programu", new ImageView(new Image("/resources/ikonaAbout.png", 25, 25, false, false)));
        helpContactMI = new MenuItem("Kontakt", new ImageView(new Image("/resources/ikonaKontakt.png", 25, 25, false, false)));
        mainMenuHelp.getItems().addAll(helpHelpMI, helpAboutMI, helpContactMI);

        //pakovanje u boxove i panele
        //definisanje glavnog panela
        mainPaneBP = new BorderPane();
        mainPaneBP.setMinSize(640, 480);
        mainPaneBP.setPrefSize(800, 600);
        mainPaneBP.setMaxSize(1920, 1080);
//        mainPaneBP.setBackground(Background.EMPTY);

        //
        //TEST KOD
        // new Image(url)
        Image image = new Image("/resources/pozadinaMain.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainPaneBP.setBackground(background);
        //KRAJ KODA
        //
        mainPaneBP.isScaleShape();
        mainPaneBP.isResizable();

        // definisanje TOP elementa
        menuBarHBox = new HBox();
        menuBarHBox.setMinWidth(Control.USE_PREF_SIZE);
        menuBarHBox.setMaxWidth(Double.MAX_VALUE);
        menuBarHBox.setMaxHeight(25);
        menuBarHBox.getChildren().addAll(mainMB, auxMB);
        HBox.setHgrow(mainMB, Priority.ALWAYS);
        mainPaneBP.setTop(menuBarHBox);

        // definisanje BOTTOM elementa
        bottomLeftStatusBarHBox = new HBox();
        bottomLeftStatusBarHBox.setMinHeight(25);
        bottomLeftStatusBarHBox.setAlignment(Pos.CENTER_LEFT);
        bottomLeftStatusBarHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        bottomLeftStatusBarHBox.getChildren().addAll(currentUserL, currentUserNameL, new Separator(Orientation.VERTICAL), systemMsgL);

        bottomCenterStatusBarHBox = new HBox();
        bottomCenterStatusBarHBox.setMinHeight(25);
        bottomCenterStatusBarHBox.setNodeOrientation(NodeOrientation.INHERIT);
        HBox.setHgrow(bottomCenterStatusBarHBox, Priority.ALWAYS);

        bottomRightStatusBarHBox = new HBox();
        bottomRightStatusBarHBox.setMinHeight(25);
        bottomRightStatusBarHBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        bottomRightStatusBarHBox.getChildren().addAll(showTimeL,
                new Separator(Orientation.VERTICAL),
                showDateL,
                new Separator(Orientation.VERTICAL));

        bottomStatusBarMainHBox = new HBox();
        bottomStatusBarMainHBox.setMinSize(640, 25);
        bottomStatusBarMainHBox.setPrefSize(800, 25);
        bottomStatusBarMainHBox.setMaxSize(1920, 25);
        bottomStatusBarMainHBox.getChildren().addAll(bottomLeftStatusBarHBox, bottomCenterStatusBarHBox, bottomRightStatusBarHBox);
        mainPaneBP.setBottom(bottomStatusBarMainHBox);

        //definisanje LEFT elementa
        leftMainVBox = new VBox();
        leftMainVBox.setMinSize(110, 550);
        leftMainVBox.setPrefSize(110, 910);
        leftMainVBox.setMaxWidth(110);
        leftMainVBox.setMinWidth(110);

        leftButtonBarVBox = new VBox();
        leftButtonBarVBox.setMinSize(110, 250);
        leftButtonBarVBox.setPrefSize(110, 250);
        leftButtonBarVBox.setMaxWidth(110);
        leftButtonBarVBox.setMinWidth(110);
        leftButtonBarVBox.setSpacing(15);
        leftButtonBarVBox.setAlignment(Pos.CENTER);
        leftButtonBarVBox.getChildren().addAll(mainSearchB, mainNewResB, mainReportB);

        leftStatusBarLabelsVBox = new VBox();
        leftStatusBarLabelsVBox.setMinSize(75, 100);
        leftStatusBarLabelsVBox.setMinSize(75, 100);
        leftStatusBarLabelsVBox.setMaxWidth(75);
        leftStatusBarLabelsVBox.getChildren().addAll(numberOfGuestsL,
                new Separator(Orientation.HORIZONTAL), numberOfArrivalsL,
                new Separator(Orientation.HORIZONTAL), numberOfDeparturesL,
                new Separator(Orientation.HORIZONTAL), numberOfOccupiedL,
                new Separator(Orientation.HORIZONTAL), numberOfAvailableL,
                new Separator(Orientation.HORIZONTAL));

        leftStatusBarValuesVBox = new VBox();
        leftStatusBarValuesVBox.setMinSize(30, 100);
        leftStatusBarValuesVBox.setMinSize(30, 100);
        leftStatusBarValuesVBox.setMaxWidth(30);
        leftStatusBarValuesVBox.getChildren().addAll(numberOfGuestsCountL,
                new Separator(Orientation.HORIZONTAL), numberOfArrivalsCountL,
                new Separator(Orientation.HORIZONTAL), numberOfDeparturesCountL,
                new Separator(Orientation.HORIZONTAL), numberOfOccupiedCountL,
                new Separator(Orientation.HORIZONTAL), numberOfAvailableCountL,
                new Separator(Orientation.HORIZONTAL));

        leftStatusBarHBox = new HBox();
        leftStatusBarHBox.setPrefSize(110, 100);
        leftStatusBarHBox.getChildren().addAll(leftStatusBarLabelsVBox,
                new Separator(Orientation.VERTICAL), leftStatusBarValuesVBox);

        leftMainVBox.getChildren().addAll(leftButtonBarVBox,
                new Separator(Orientation.HORIZONTAL), leftStatusBarHBox);
        mainPaneBP.setLeft(leftMainVBox);

        // definisanje CENTER elementa
        // kolona sa sobama
        resourcesGridGP = new GridPane();
        resourcesGridGP.setMinWidth(100);
        resourcesGridGP.setMaxWidth(100);
        for (int row = 0; row < roomsNo; row++) {
            resourcesGridGP.add(new ResourcePane(row), 0, row);
        }
        resourcesGridGP.setVgap(0);
        resourcesGridGP.setHgap(0);
        resourcesGridGP.setFocusTraversable(false);
        resourcesGridGP.setGridLinesVisible(true);

        // traka sa datumima
        timeGridGP = new GridPane();
        for (int column = 0; column < daysNo; column++) {
            DatePane datumPolje = new DatePane(column);
            GridPane.setColumnIndex(datumPolje, column);
            GridPane.setRowIndex(datumPolje, 0);
            timeGridGP.getChildren().add(datumPolje);
        }
        timeGridGP.setMinHeight(29);
        timeGridGP.setMaxHeight(47);
        timeGridGP.setVgap(0);
        timeGridGP.setHgap(0);
        timeGridGP.setGridLinesVisible(true);
        timeGridGP.setFocusTraversable(false);
        timeGridGP.setStyle("-fx-focus-color: transparent;\n"
                + "-fx-faint-focus-color: transparent;");

        // grid
        tableGridGP = new GridPane();

        for (int row = 0; row < roomsNo; row++) {
            for (int column = 0; column < daysNo; column++) {
                TablePane gridPolje = new TablePane();
                GridPane.setColumnIndex(gridPolje, column);
                GridPane.setRowIndex(gridPolje, row);
                tableGridGP.getChildren().add(gridPolje);
            }
        }

        tableGridGP.setGridLinesVisible(true);
        tableGridGP.setVgap(0);
        tableGridGP.setHgap(0);
        tableGridGP.setFocusTraversable(false);

        //glavna
        mainGridGP = new GridPane();
        RowConstraints row0 = new RowConstraints();
        ColumnConstraints col0 = new ColumnConstraints();

        row0.setMinHeight(30);
        row0.setMaxHeight(30);
        row0.setVgrow(Priority.NEVER);
        mainGridGP.getRowConstraints().add(row0);

        col0.setMinWidth(100);
        col0.setMaxWidth(100);
        col0.setHgrow(Priority.NEVER);
        mainGridGP.getColumnConstraints().add(col0);
        mainGridGP.setHgap(0);
        mainGridGP.setVgap(0);
        mainGridGP.setPadding(new Insets(5, 0, 0, 0));
//        mainGridGP.setGridLinesVisible(true);
        mainGridGP.setFocusTraversable(false);
        mainGridGP.setStyle("-fx-focus-color: transparent;\n"
                + "-fx-faint-focus-color: transparent;");

        // vreme 1,0
        sp1 = new ScrollPane(timeGridGP);
        sp1.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp1.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp1.setMinHeight(31);
        sp1.setMaxHeight(31);
        sp1.setMinViewportHeight(28);
        sp1.prefViewportHeightProperty().set(28);
        sp1.setVmax(0.74);
        sp1.setVmin(-0.1);
        sp1.setPannable(true);
        mainGridGP.add(sp1, 1, 0);

        // resursi 0,1
        sp2 = new ScrollPane(resourcesGridGP);
        sp2.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp2.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp2.setPannable(true);
        mainGridGP.add(sp2, 0, 1);

        //tabela 1,1
        sp3 = new ScrollPane(tableGridGP);
        sp3.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp3.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp3.setPannable(true);
        mainGridGP.add(sp3, 1, 1);

        // linkovanje scrollbarova
        sp3.vvalueProperty().bindBidirectional(sp2.vvalueProperty());
        sp3.hvalueProperty().bindBidirectional(sp1.hvalueProperty());

        //definisanje border layouta za centralni deo
        centerPaneTop = new HBox();
        centerPaneTop.setSpacing(10);
        centerPaneTop.setAlignment(Pos.CENTER);

        goToDayL = new Label();
        goToDayL.setText("Idi na dan");
        goToDayL.setTextAlignment(TextAlignment.RIGHT);
        goToDayL.setAlignment(Pos.CENTER_RIGHT);

        selectDayDP = new DatePicker(LocalDate.now());

        //TEST KOD
        refreshB = new Button("Azuriranje", new ImageView(new Image("/resources/ikonaSwitch.png", 25, 25, false, false)));
        refreshB.setMinWidth(95);
        refreshB.setMaxHeight(25);
        refreshVBox = new VBox(refreshB);
        refreshVBox.setAlignment(Pos.CENTER);
        mainGridGP.add(refreshVBox, 0, 0);

        centerPaneTop.setAlignment(Pos.CENTER);
        centerPaneTop.getChildren().addAll(goToDayL, selectDayDP);

        mainPaneCenterBP = new BorderPane();
        mainPaneCenterBP.setPadding(new Insets(5, 0, 0, 0));
        mainPaneCenterBP.setTop(centerPaneTop);
        mainPaneCenterBP.setCenter(mainGridGP);

        mainPaneBP.setCenter(mainPaneCenterBP);

        // podesavanje scene
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        mainScene = new Scene(mainPaneBP);

    }
}
