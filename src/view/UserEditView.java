/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author Nebojsa Matic
 */
public class UserEditView extends Stage {

    Label userIDL, userNameL, userPassL, userFirstNameL, userLastNameL, userPhoneL, userEmailL,
            userCompanyL, userObjectL;

    public static TextField userNameTF, userPassTF, userFirstNameTF, userLastNameTF, userPhoneTF, userEmailTF;

    public static ComboBox<Integer> userIDCB;
    
    public static ComboBox<String> userCompanyCB, userObjectCB;

    public static Button addUserB, deleteUserB, printUserB, saveChangesB, closeViewB;

    HBox userPassHBox, centerHBox, buttonHBox;

    VBox mainVBox,
            userPassVBox,
            centerVBoxLabels, centerVBoxValues;

    public static Scene userEditorScene;

    public UserEditView() {

        // LABELE
        //gornji deo
        userIDL = new Label();
        userIDL.setMinHeight(25);
        userIDL.setText("Redni broj korisnika");
        userIDL.setTextAlignment(TextAlignment.RIGHT);
        userIDL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userFirstNameL = new Label();
        userFirstNameL.setMinHeight(25);
        userFirstNameL.setText("Ime");
        userFirstNameL.setTextAlignment(TextAlignment.RIGHT);
        userFirstNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userNameL = new Label();
        userNameL.setMinHeight(25);
        userNameL.setText("Korisnicko ime");
        userNameL.setTextAlignment(TextAlignment.RIGHT);
        userNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userPassL = new Label();
        userPassL.setMinHeight(25);
        userPassL.setText("Sifra korisnika");
        userPassL.setTextAlignment(TextAlignment.RIGHT);
        userPassL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userLastNameL = new Label();
        userLastNameL.setMinHeight(25);
        userLastNameL.setText("Prezime");
        userLastNameL.setTextAlignment(TextAlignment.RIGHT);
        userLastNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userPhoneL = new Label();
        userPhoneL.setMinHeight(25);
        userPhoneL.setText("Telefon");
        userPhoneL.setTextAlignment(TextAlignment.RIGHT);
        userPhoneL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userEmailL = new Label();
        userEmailL.setMinHeight(25);
        userEmailL.setText("eMail");
        userEmailL.setTextAlignment(TextAlignment.RIGHT);
        userEmailL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userCompanyL = new Label();
        userCompanyL.setMinHeight(25);
        userCompanyL.setText("Kompanija");
        userCompanyL.setTextAlignment(TextAlignment.RIGHT);
        userCompanyL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        userObjectL = new Label();
        userObjectL.setMinHeight(25);
        userObjectL.setText("Objekat");
        userObjectL.setTextAlignment(TextAlignment.RIGHT);
        userObjectL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //POLJA
        userIDCB = new ComboBox<>();
        userIDCB.setMinSize(100, 25);
        userIDCB.setMaxSize(100, 25);

        userFirstNameTF = new TextField();
        userFirstNameTF.setMinSize(150, 25);
        userFirstNameTF.setMaxSize(150, 25);

        userLastNameTF = new TextField();
        userLastNameTF.setMinSize(150, 25);
        userLastNameTF.setMaxSize(150, 25);

        userNameTF = new TextField();
        userNameTF.setMinSize(150, 25);
        userNameTF.setMaxSize(150, 25);

        userPassTF = new TextField();
        userPassTF.setMinSize(150, 25);
        userPassTF.setMaxSize(150, 25);

        userLastNameTF = new TextField();
        userLastNameTF.setMinSize(150, 25);
        userLastNameTF.setMaxSize(150, 25);

        userPhoneTF = new TextField();
        userPhoneTF.setMinSize(150, 25);
        userPhoneTF.setMaxSize(150, 25);

        userEmailTF = new TextField();
        userEmailTF.setMinSize(150, 25);
        userEmailTF.setMaxSize(150, 25);

        userCompanyCB = new ComboBox<>();
        userCompanyCB.setMinSize(150, 25);
        userCompanyCB.setMaxSize(150, 25);

        userObjectCB = new ComboBox<>();
        userObjectCB.setMinSize(150, 25);
        userObjectCB.setMaxSize(150, 25);

        //DUGMAD
        addUserB = new Button();
        addUserB.setMinSize(25, 25);
        addUserB.setMaxSize(25, 25);
        addUserB.setGraphic(new ImageView(new Image("/resources/ikonaDodaj.png", 20, 20, false, false)));

        deleteUserB = new Button();
        deleteUserB.setMinSize(25, 25);
        deleteUserB.setMaxSize(25, 25);
        deleteUserB.setGraphic(new ImageView(new Image("/resources/ikonaOduzmi.png", 20, 20, false, false)));

        printUserB = new Button();
        printUserB.setMinSize(70, 25);
        printUserB.setMaxSize(70, 25);
        printUserB.setText("Stampaj");

        saveChangesB = new Button();
        saveChangesB.setMinSize(70, 25);
        saveChangesB.setMaxSize(70, 25);
        saveChangesB.setText("Sacuvaj");

        closeViewB = new Button();
        closeViewB.setMinSize(70, 25);
        closeViewB.setMaxSize(70, 25);
        closeViewB.setText("Zatvori");

        //KONTEJNERI
        //redni broj korisnika
        userPassHBox = new HBox(5, userIDCB, addUserB, deleteUserB);
        userPassHBox.setAlignment(Pos.CENTER);

        userPassVBox = new VBox(5, userIDL, userPassHBox);
        userPassVBox.setAlignment(Pos.CENTER);

        ///TEST KOD
        centerVBoxLabels = new VBox(5, userNameL, userPassL, userFirstNameL, userLastNameL, userPhoneL, userEmailL,
                userCompanyL, userObjectL);
        centerVBoxLabels.setAlignment(Pos.CENTER_RIGHT);

        centerVBoxValues = new VBox(5, userNameTF, userPassTF, userFirstNameTF, userLastNameTF, userPhoneTF, userEmailTF,
                userCompanyCB, userObjectCB);
        centerVBoxValues.setAlignment(Pos.CENTER_LEFT);

        centerHBox = new HBox(10, centerVBoxLabels, centerVBoxValues);
        centerHBox.setAlignment(Pos.CENTER);
        centerHBox.setPadding(new Insets(5));

        // panela za dugmad
        buttonHBox = new HBox(5, saveChangesB, closeViewB);
        buttonHBox.setAlignment(Pos.CENTER);
        buttonHBox.setPadding(new Insets(5));

        //glavni panel
        mainVBox = new VBox(5, userPassVBox, centerHBox, buttonHBox);
        mainVBox.setMinSize(270.0, 350);
        mainVBox.setMaxSize(270.0, 350);
        mainVBox.setAlignment(Pos.CENTER_RIGHT);
        mainVBox.setPadding(new Insets(10));
        // new Image(url)
        Image image = new Image("/resources/pozadinaUtil.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainVBox.setBackground(background);
        //KRAJ KODA
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        // Set the stage
        userEditorScene = new Scene(mainVBox);

    }

}
