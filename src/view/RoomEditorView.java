/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author Nebojsa Matic
 */
public class RoomEditorView extends Stage {

    public static Label roomNumberL, roomHHNameL, roomNameL, roomTypeL,
            floorNumberL, bedNumberMainL, bedNumberAuxL,
            numberOfRoomsL, numberOfBedroomsL, numberOfBathroomsL,
            roomNotesL, roomPropertiesL,
            roomAuthorNameL, roomAuthorValueL,
            entryDateNameL, entryDateValueL;

    public static TextField roomNameTF,
            floorNumberTF, bedNumberMainTF, bedNumberAuxTF,
            numberOfRoomsTF, numberOfBedroomsTF, numberOfBathroomsTF,
            roomNotesTF;

    public static ComboBox<String> roomNumberCB, roomTypeCB, roomHHNameCB;

    public static CheckBox hasWiFi, hasACU, hasTV, hasTerrace;

    public static Button addRoomB, deleteRoomB, discardChangesB, saveChangesB;

    HBox upperMainHBox, roomNumberHBox,
            centerMainHBox, centerLeftHBoxMain, centerRightHBoxMain,
            bottomMainHBox, donjiGlavniHBoxDesno,
            buttonHBox,
            statusBarHbox;

    VBox roomEditMainPane,
            upperLeftVBox, upperRightVBox,
            roomNumberVBox, roomHHNameVBox, roomNameVBox, roomTypeVBox,
            centerLeftVBoxLabels, centerLeftVBoxFields,
            centerRightVBoxLabels, centerRightVBoxFields,
            bottomLeftVBoxCB,
            brSprataVBox, brGlavnihLezajevaVBox, brPomocnihLezajevaVBox,
            brProstorijaVBox, brSpavSobaVBox, brKupatilaVBox,
            roomNotesVBox, svojstvaSmJedVBoxLevo, svojstvaSmJedVBoxDesno;

    public static Scene roomEditScene;

    public RoomEditorView() {

        //Dugmad
        discardChangesB = new Button();
        discardChangesB.setMinSize(130, 25);
        discardChangesB.setMaxSize(130, 25);
        discardChangesB.setText("Otkazi");

        saveChangesB = new Button();
        saveChangesB.setMinSize(130, 25);
        saveChangesB.setMaxSize(130, 25);
        saveChangesB.setText("Sacuvaj");

        // LABELE
        //gornji deo
        roomNumberL = new Label();
        roomNumberL.setMinHeight(25);
        roomNumberL.setText("Naziv");
        roomNumberL.setTextAlignment(TextAlignment.RIGHT);
        roomNumberL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        roomHHNameL = new Label();
        roomHHNameL.setMinHeight(25);
        roomHHNameL.setText("Naziv objekta");
        roomHHNameL.setTextAlignment(TextAlignment.RIGHT);
        roomHHNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        roomNameL = new Label();
        roomNameL.setMinHeight(25);
        roomNameL.setText("Naziv");
        roomNameL.setTextAlignment(TextAlignment.RIGHT);
        roomNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        roomTypeL = new Label();
        roomTypeL.setMinHeight(25);
        roomTypeL.setText("Vrsta");
        roomTypeL.setTextAlignment(TextAlignment.RIGHT);
        roomTypeL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //sredni deo
        floorNumberL = new Label();
        floorNumberL.setMinHeight(25);
        floorNumberL.setText("Sprat");
        floorNumberL.setAlignment(Pos.CENTER_RIGHT);
        floorNumberL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        bedNumberMainL = new Label();
        bedNumberMainL.setMinHeight(25);
        bedNumberMainL.setText("Br glavnih lezaja");
        bedNumberMainL.setTextAlignment(TextAlignment.RIGHT);
        bedNumberMainL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        bedNumberAuxL = new Label();
        bedNumberAuxL.setMinHeight(25);
        bedNumberAuxL.setText("Br pomocnih lezaja");
        bedNumberAuxL.setTextAlignment(TextAlignment.RIGHT);
        bedNumberAuxL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        numberOfRoomsL = new Label();
        numberOfRoomsL.setMinHeight(25);
        numberOfRoomsL.setText("Br prostorija");
        numberOfRoomsL.setTextAlignment(TextAlignment.RIGHT);
        numberOfRoomsL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        numberOfBedroomsL = new Label();
        numberOfBedroomsL.setMinHeight(25);
        numberOfBedroomsL.setText("Br spavacih soba");
        numberOfBedroomsL.setTextAlignment(TextAlignment.RIGHT);
        numberOfBedroomsL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        numberOfBathroomsL = new Label();
        numberOfBathroomsL.setMinHeight(25);
        numberOfBathroomsL.setText("Br kupatila");
        numberOfBathroomsL.setTextAlignment(TextAlignment.RIGHT);
        numberOfBathroomsL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //donji deo
        roomNotesL = new Label();
        roomNotesL.setMinHeight(25);
        roomNotesL.setText("Napomene");
        roomNotesL.setAlignment(Pos.CENTER_RIGHT);
        roomNotesL.setTextAlignment(TextAlignment.RIGHT);
        roomNotesL.setBackground(new Background(new BackgroundFill(Color.gray(0.88), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        roomPropertiesL = new Label();
        roomPropertiesL.setMinHeight(25);
        roomPropertiesL.setText("Svojstva");
        roomPropertiesL.setTextAlignment(TextAlignment.RIGHT);
        roomPropertiesL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //status bar
        roomAuthorNameL = new Label();
        roomAuthorNameL.setMinHeight(25);
        roomAuthorNameL.setText("Autor:");
        roomAuthorNameL.setTextAlignment(TextAlignment.RIGHT);
        roomAuthorNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.75), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        roomAuthorValueL = new Label();
        roomAuthorValueL.setMinHeight(25);
//        prebaciti u kontroler
        roomAuthorValueL.setText("Nebojsa");
        roomAuthorValueL.setTextAlignment(TextAlignment.RIGHT);
        roomAuthorValueL.setBackground(new Background(new BackgroundFill(Color.gray(0.75), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        entryDateNameL = new Label();
        entryDateNameL.setMinHeight(25);
        entryDateNameL.setText("Datum poslednje izmene:");
        entryDateNameL.setTextAlignment(TextAlignment.RIGHT);
        entryDateNameL.setBackground(new Background(new BackgroundFill(Color.gray(0.75), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        entryDateValueL = new Label();
        entryDateValueL.setMinHeight(25);
////        prebaciti u kontroler
        entryDateValueL.setTextAlignment(TextAlignment.RIGHT);
        entryDateValueL.setBackground(new Background(new BackgroundFill(Color.gray(0.75), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //POLJA
        //gornji deo
        roomNumberCB = new ComboBox<>();
        roomNumberCB.setMinSize(120, 25);
        roomNumberCB.setMaxSize(120, 25);

        addRoomB = new Button();
        addRoomB.setMinSize(25, 25);
        addRoomB.setMaxSize(25, 25);
        addRoomB.setGraphic(new ImageView(new Image("/resources/ikonaDodaj.png", 20, 20, false, false)));

        deleteRoomB = new Button();
        deleteRoomB.setMinSize(25, 25);
        deleteRoomB.setMaxSize(25, 25);
        deleteRoomB.setGraphic(new ImageView(new Image("/resources/ikonaOduzmi.png", 20, 20, false, false)));

        roomHHNameCB = new ComboBox<>();
        roomHHNameCB.setMinSize(150, 25);
        roomHHNameCB.setMaxSize(150, 25);

        roomNameTF = new TextField();
        roomNameTF.setMinSize(150, 25);
        roomNameTF.setMaxSize(150, 25);

        roomTypeCB = new ComboBox<>();
        roomTypeCB.setMinSize(150, 25);
        roomTypeCB.setMaxSize(150, 25);

        //srednji deo
        floorNumberTF = new TextField();
        floorNumberTF.setMinSize(25, 25);
        floorNumberTF.setMaxSize(25, 25);

        bedNumberMainTF = new TextField();
        bedNumberMainTF.setMinSize(25, 25);
        bedNumberMainTF.setMaxSize(25, 25);

        bedNumberAuxTF = new TextField();
        bedNumberAuxTF.setMinSize(25, 25);
        bedNumberAuxTF.setMaxSize(25, 25);

        numberOfRoomsTF = new TextField();
        numberOfRoomsTF.setMinSize(25, 25);
        numberOfRoomsTF.setMaxSize(25, 25);

        numberOfBedroomsTF = new TextField();
        numberOfBedroomsTF.setMinSize(25, 25);
        numberOfBedroomsTF.setMaxSize(25, 25);

        numberOfBathroomsTF = new TextField();
        numberOfBathroomsTF.setMinSize(25, 25);
        numberOfBathroomsTF.setMaxSize(25, 25);

        //donji deo
        roomNotesTF = new TextField();
        roomNotesTF.setMinSize(150, 150);
        roomNotesTF.setMaxSize(150, 150);
        roomNotesTF.setAlignment(Pos.TOP_LEFT);

        hasWiFi = new CheckBox("WiFi");
        hasWiFi.setMinSize(55, 25);
        hasWiFi.setMaxSize(55, 25);
        hasWiFi.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        hasWiFi.setBackground(new Background(new BackgroundFill(Color.gray(0.84), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        hasACU = new CheckBox("Klima");
        hasACU.setMinSize(55, 25);
        hasACU.setMaxSize(55, 25);
        hasACU.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        hasACU.setBackground(new Background(new BackgroundFill(Color.gray(0.84), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        hasTV = new CheckBox("TV");
        hasTV.setMinSize(40, 25);
        hasTV.setMaxSize(40, 25);
        hasTV.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        hasTV.setBackground(new Background(new BackgroundFill(Color.gray(0.84), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        hasTerrace = new CheckBox("Terasa");
        hasTerrace.setMinSize(60, 25);
        hasTerrace.setMaxSize(60, 25);
        hasTerrace.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        hasTerrace.setBackground(new Background(new BackgroundFill(Color.gray(0.84), new CornerRadii(3), new Insets(3, 0, 3, 0))));

        //kontejneri
        //gornji deo
        //leva polovina
        roomNumberHBox = new HBox(5, roomNumberCB, addRoomB, deleteRoomB);
        roomNumberHBox.setAlignment(Pos.CENTER);

        roomNumberVBox = new VBox(roomNumberL, roomNumberHBox);
        roomNumberVBox.setAlignment(Pos.CENTER_LEFT);

        roomHHNameVBox = new VBox(roomHHNameL, roomHHNameCB);
        roomHHNameVBox.setAlignment(Pos.CENTER_LEFT);

        upperLeftVBox = new VBox(roomNumberVBox, roomHHNameVBox);
        upperLeftVBox.setAlignment(Pos.CENTER_RIGHT);
        upperLeftVBox.setPadding(new Insets(5));

        //desna polovina
        roomNameVBox = new VBox(roomNameL, roomNameTF);
        roomNameVBox.setAlignment(Pos.CENTER_LEFT);

        roomTypeVBox = new VBox(roomTypeL, roomTypeCB);
        roomTypeVBox.setAlignment(Pos.CENTER_LEFT);

        upperRightVBox = new VBox(roomNameVBox, roomTypeVBox);
        upperRightVBox.setAlignment(Pos.CENTER_LEFT);
        upperRightVBox.setPadding(new Insets(5));

        // gornji glavni
        upperMainHBox = new HBox(15, upperLeftVBox, upperRightVBox);
        upperMainHBox.setPadding(new Insets(5));
        upperMainHBox.setAlignment(Pos.TOP_CENTER);
        upperMainHBox.setStyle("-fx-border-color: lightgrey;");

        //srednji
        //leva polovina
        centerLeftVBoxLabels = new VBox(5, floorNumberL, bedNumberMainL, bedNumberAuxL);
        centerLeftVBoxLabels.setAlignment(Pos.CENTER_RIGHT);

        centerLeftVBoxFields = new VBox(5, floorNumberTF, bedNumberMainTF, bedNumberAuxTF);
        centerLeftVBoxFields.setAlignment(Pos.CENTER_LEFT);

        centerLeftHBoxMain = new HBox(5, centerLeftVBoxLabels, centerLeftVBoxFields);

        //desna polovina
        centerRightVBoxLabels = new VBox(5, numberOfRoomsL, numberOfBedroomsL, numberOfBathroomsL);
        centerRightVBoxLabels.setAlignment(Pos.CENTER_RIGHT);

        centerRightVBoxFields = new VBox(5, numberOfRoomsTF, numberOfBedroomsTF, numberOfBathroomsTF);
        centerRightVBoxFields.setAlignment(Pos.CENTER_LEFT);

        centerRightHBoxMain = new HBox(5, centerRightVBoxLabels, centerRightVBoxFields);

        //gornji glavni
        centerMainHBox = new HBox(30, centerLeftHBoxMain, centerRightHBoxMain);
        centerMainHBox.setPadding(new Insets(10));
        centerMainHBox.setAlignment(Pos.CENTER);
        centerMainHBox.setStyle("-fx-border-color: lightgrey;");

        //donji
        //leva polovina
        roomNotesVBox = new VBox(roomNotesL, roomNotesTF);
        roomNotesVBox.setAlignment(Pos.TOP_LEFT);
//        roomNotesVBox.setPadding(new Insets(10));

        //desna polovina
        bottomLeftVBoxCB = new VBox(5, hasWiFi, hasACU, hasTV, hasTerrace);
        bottomLeftVBoxCB.setAlignment(Pos.CENTER_RIGHT);

        //donji glavni
        bottomMainHBox = new HBox(50, bottomLeftVBoxCB, roomNotesVBox);
        bottomMainHBox.setAlignment(Pos.TOP_RIGHT);
        bottomMainHBox.setPadding(new Insets(10));
        bottomMainHBox.setStyle("-fx-border-color: lightgrey;");

        //dugmad
        buttonHBox = new HBox(30, saveChangesB, discardChangesB);
        buttonHBox.setAlignment(Pos.CENTER);
        buttonHBox.setPadding(new Insets(10));
        buttonHBox.setStyle("-fx-border-color: lightgrey;");

        //ststusbar
        statusBarHbox = new HBox(5, roomAuthorNameL, roomAuthorValueL, new Separator(Orientation.VERTICAL), entryDateNameL, entryDateValueL, new Separator(Orientation.VERTICAL));
        statusBarHbox.setAlignment(Pos.BOTTOM_LEFT);
        statusBarHbox.setPadding(new Insets(0, 0, 0, 5));

        //glavni panel
        roomEditMainPane = new VBox(upperMainHBox, centerMainHBox, bottomMainHBox, buttonHBox, statusBarHbox);

        // Set the stage
        // new Image(url)
        Image image = new Image("/resources/pozadinaUtil.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        roomEditMainPane.setBackground(background);
        //KRAJ KODA
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        roomEditScene = new Scene(roomEditMainPane);
    }

}
