/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import components.ReadOnlyLabel;

/**
 *
 * @author Nebojsa Matic
 */
public class AddRoomPriceView extends Stage {

    public static Label rpNoL, roomIdL, priceStartDateL, priceEndDateL,
            priceAmountL, entryDateL, entryAuthorL,
            rpNoROL, entryAuthorROL, entryDateROL;

    public static TextField priceAmountTF;

    public static ComboBox<String> selectRoomCB;

    public static DatePicker startDateDP, endDateDP;

    public static Button acceptB, cancelB;

    HBox columnHBox, buttonHBox;

    VBox mainEditorVBox, rpNoVBox, roomVBox, priceStartDateVBox,
            priceEndDateVBox, priceAmountVBox, entryAuthorVBox, entryDateVBox;

    public static Scene priceAddScene;

    public AddRoomPriceView() {

        //LABELE
        rpNoL = new Label();
        rpNoL.setMinSize(25, 25);
        rpNoL.setText("#");
        rpNoL.setTextAlignment(TextAlignment.RIGHT);
        rpNoL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        rpNoL.setStyle("-fx-border-color: grey;");
        rpNoL.setAlignment(Pos.CENTER);

        roomIdL = new Label();
        roomIdL.setMinSize(100, 25);
        roomIdL.setText("Naziv s.j");
        roomIdL.setTextAlignment(TextAlignment.RIGHT);
        roomIdL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        roomIdL.setStyle("-fx-border-color: grey;");
        roomIdL.setAlignment(Pos.CENTER);

        priceStartDateL = new Label();
        priceStartDateL.setMinSize(100, 25);
        priceStartDateL.setText("Pocetak");
        priceStartDateL.setTextAlignment(TextAlignment.RIGHT);
        priceStartDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        priceStartDateL.setStyle("-fx-border-color: grey;");
        priceStartDateL.setAlignment(Pos.CENTER);

        priceEndDateL = new Label();
        priceEndDateL.setMinSize(100, 25);
        priceEndDateL.setText("Kraj");
        priceEndDateL.setAlignment(Pos.CENTER);
        priceEndDateL.setTextAlignment(TextAlignment.RIGHT);
        priceEndDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        priceEndDateL.setStyle("-fx-border-color: grey;");
        priceEndDateL.setAlignment(Pos.CENTER);

        priceAmountL = new Label();
        priceAmountL.setMinSize(50, 25);
        priceAmountL.setText("Iznos");
        priceAmountL.setTextAlignment(TextAlignment.RIGHT);
        priceAmountL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        priceAmountL.setStyle("-fx-border-color: grey;");
        priceAmountL.setAlignment(Pos.CENTER);

        entryDateL = new Label();
        entryDateL.setMinSize(100, 25);
        entryDateL.setText("Datum Unosa");
        entryDateL.setTextAlignment(TextAlignment.RIGHT);
        entryDateL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        entryDateL.setStyle("-fx-border-color: grey;");
        entryDateL.setAlignment(Pos.CENTER);

        entryAuthorL = new Label();
        entryAuthorL.setMinSize(75, 25);
        entryAuthorL.setText("Autor");
        entryAuthorL.setTextAlignment(TextAlignment.RIGHT);
        entryAuthorL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        entryAuthorL.setStyle("-fx-border-color: grey;");
        entryAuthorL.setAlignment(Pos.CENTER);

        // POLJA
        rpNoROL = new ReadOnlyLabel();
        rpNoROL.setMinSize(25, 25);
        //placeholder, prebaciti u kontrolor
        rpNoROL.setText("1");
        rpNoROL.setTextAlignment(TextAlignment.RIGHT);
        rpNoROL.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(3), new Insets(3, 0, 3, 0))));
        rpNoROL.setStyle("-fx-border-color: grey;");
        rpNoROL.setAlignment(Pos.CENTER);

        selectRoomCB = new ComboBox<>();
        selectRoomCB.setMinSize(100, 25);

        startDateDP = new DatePicker();
        startDateDP.setMinSize(100, 25);
        startDateDP.setMaxSize(100, 25);

        endDateDP = new DatePicker();
        endDateDP.setMinSize(100, 25);
        endDateDP.setMaxSize(100, 25);

        priceAmountTF = new TextField();
        priceAmountTF.setMinSize(50, 25);
        priceAmountTF.setMaxSize(50, 25);

        entryAuthorROL = new ReadOnlyLabel();
        entryAuthorROL.setMinSize(75, 25);
        entryAuthorROL.setMaxSize(75, 25);
        //placeholder, prebaciti u kontroler
        entryAuthorROL.setText("Nebojsa");

        entryDateROL = new ReadOnlyLabel();
        entryDateROL.setMinSize(100, 25);
        entryDateROL.setMaxSize(100, 25);
        entryDateROL.setText(LocalDate.now().toString());

        //DUGMAD
        acceptB = new Button();
        acceptB.setMinSize(130, 30);
        acceptB.setMaxSize(130, 30);
        acceptB.setText("Potvrdi");
        acceptB.setGraphic(new ImageView(new Image("/resources/ikonaYes.png", 20, 20, false, false)));

        cancelB = new Button();
        cancelB.setMinSize(130, 30);
        cancelB.setMaxSize(130, 30);
        cancelB.setText("Ponisti");
        cancelB.setGraphic(new ImageView(new Image("/resources/ikonaNo.png", 20, 20, false, false)));

        // KONTEJNERI
        // panela za kolone + polja
        rpNoVBox = new VBox(rpNoL, rpNoROL);
        rpNoVBox.setAlignment(Pos.CENTER);

        roomVBox = new VBox(roomIdL, selectRoomCB);
        roomVBox.setAlignment(Pos.CENTER);

        priceStartDateVBox = new VBox(priceStartDateL, startDateDP);
        priceStartDateVBox.setAlignment(Pos.CENTER);

        priceEndDateVBox = new VBox(priceEndDateL, endDateDP);
        priceEndDateVBox.setAlignment(Pos.CENTER);

        priceAmountVBox = new VBox(priceAmountL, priceAmountTF);
        priceAmountVBox.setAlignment(Pos.CENTER);

        entryDateVBox = new VBox(entryDateL, entryDateROL);
        entryDateVBox.setAlignment(Pos.CENTER);

        entryAuthorVBox = new VBox(entryAuthorL, entryAuthorROL);
        entryAuthorVBox.setAlignment(Pos.CENTER);

        columnHBox = new HBox(rpNoVBox, roomVBox, priceStartDateVBox,
                priceEndDateVBox, priceAmountVBox, entryAuthorVBox, entryDateVBox);
        columnHBox.setAlignment(Pos.CENTER);

        //panela za dugmad
        buttonHBox = new HBox(10, cancelB, acceptB);
        buttonHBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        buttonHBox.setMinHeight(40);
        buttonHBox.setPadding(new Insets(10, 0, 10, 10));

        //glavni
        mainEditorVBox = new VBox(columnHBox, buttonHBox);
        mainEditorVBox.setAlignment(Pos.TOP_CENTER);

        // new Image(url)
        Image image = new Image("/resources/pozadinaUtil.png");
        // new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
        // new BackgroundImage(image, repeatX, repeatY, position, size)
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        // new Background(images...)
        Background background = new Background(backgroundImage);
        mainEditorVBox.setBackground(background);
        //KRAJ KODA

        // SCENA
        getIcons().add(new Image("/resources/ikonaLogo.png", 50, 50, false, false));
        priceAddScene = new Scene(mainEditorVBox);
    }

}
