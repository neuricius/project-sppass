/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.util.Duration;

/**
 * Creates a digital clock display as a simple label. Format of the clock
 * display is hh:mm:ss aa, where: hh Hour in am/pm (1-12) mm Minute in hour ss
 * Second in minute aa Am/pm marker Time is the system time for the local
 * timezone. preuzeto sa
 * https://stackoverflow.com/questions/13227809/displaying-changing-values-in-javafx-label
 */
public class DigitalClock extends Label {

    public DigitalClock() {
        findTime();
    }

    // the digital clock updates once a second.
    private void findTime() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    LocalTime currentDateTime = LocalTime.now();
                    DateTimeFormatter formatTime = DateTimeFormatter.ofPattern("hh:mm a");
                    String testTime = currentDateTime.format(formatTime);
                    setText(testTime);
        }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
