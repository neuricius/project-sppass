/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import view.ConfirmationDialog;
import view.ExitConfirmation;

/**
 *
 * @author Nebojsa Matic
 */
public interface WindowDecorator {

    static void noConfirmationDecorator(Stage stage) {
        stage.addEventHandler(KeyEvent.KEY_TYPED, (KeyEvent t) -> {
            if (t.isAltDown() == true && t.getCode() == KeyCode.F4) {
                t.consume();
                stage.close();
            }
        });

        stage.setOnCloseRequest((WindowEvent ev) -> {
            ev.consume();
            stage.close();
        });
    }

    static void confirmationDecorator(Stage stage) {
        stage.addEventHandler(KeyEvent.KEY_TYPED, (KeyEvent t) -> {
            if (ConfirmationDialog.confirmAction("Jeste li sigurni?\nPromene se nece sacuvati!", "Izlazak iz prozora", "Da", "Ne")) {
                if (t.isAltDown() == true && t.getCode() == KeyCode.F4) {
                    t.consume();
                    stage.close();
                }
            }
        });

        stage.setOnCloseRequest((WindowEvent ev) -> {
            if (ConfirmationDialog.confirmAction("Jeste li sigurni?/nPromene se nece sacuvati!", "Izlazak iz prozora", "Da", "Ne")) {
                ev.consume();
                stage.close();
            } else {
                ev.consume();
            }
        });
    }

    static void mainDecorator(Stage stage) {
        stage.addEventHandler(KeyEvent.KEY_TYPED, (KeyEvent t) -> {
            if (t.isAltDown() == true && t.getCode() == KeyCode.F4) {
                ExitConfirmation.programExit();
            }
            t.consume();
        });

        stage.setOnCloseRequest((WindowEvent ev) -> {
            ev.consume();
            ExitConfirmation.programExit();
        });
    }

}
