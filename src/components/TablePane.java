/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import static controller.MainCTRL.findNodeCoordinates;

/**
 *
 * @author Grupa2
 */
public class TablePane extends MapPane {

    public TablePane() {

        this.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(1), new Insets(1))));

        setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (event.getClickCount() == 2) {
                    try {
                        event.consume();
                        findNodeCoordinates(this);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(TablePane.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    if (event.getButton().equals(MouseButton.SECONDARY)) {
//                        System.out.println("uja");
                        new ReservationsContext(null);
                    } else {
                        event.consume();
                    }
                }
            }
        });

        setOnContextMenuRequested(e -> {
            new ReservationsContext(null).show(this, e.getScreenX(), e.getScreenY());
            System.out.println("tralaalala");
        });
    }

}
