/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import static util.HibernateUtil.returnDate;

/**
 *
 * @author Nebojsa Matic
 */
public class DatePane extends MapPane {

    double paneHeight = 47;
    double paneWidth = 30;

    public DatePane(int danPoDan) {
        this.setMinSize(paneWidth, paneHeight);
        this.setMaxSize(paneWidth, paneHeight);
        this.setPadding(new Insets(-5, 0, 0, 0));

        String DateTimeFormatPattern ="dd\n ";
        this.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(1), new Insets(1))));
        Label dateLabel = new Label();
        dateLabel.setText(convertMonthSrb(returnDate(danPoDan)) + "\n"+ returnDate(danPoDan).format(DateTimeFormatter.ofPattern(DateTimeFormatPattern))
                + dayOfWeekFromDate(returnDate(danPoDan)));
        

        dateLabel.setMinSize(28, paneHeight);
        dateLabel.setPrefSize(0, 0);
        dateLabel.setMaxSize(28, paneHeight);
        dateLabel.setPadding(new Insets(-3, 0, 0, 0));
        dateLabel.setFont(Font.font(11));
        dateLabel.setAlignment(Pos.TOP_CENTER);
        dateLabel.setTextAlignment(TextAlignment.CENTER);
        this.getChildren().add(dateLabel);
    }

    public static String convertMonthSrb(LocalDate x) {
        String s = "";
        switch (x.getMonth()) {
            case JANUARY:
                s = "Jan";
                break;
            case FEBRUARY:
                s = "Feb";
                break;
            case MARCH:
                s = "Mar";
                break;
            case APRIL:
                s = "Apr";
                break;
            case MAY:
                s = "Maj";
                break;
            case JUNE:
                s = "Jun";
                break;
            case JULY:
                s = "Jul";
                break;
            case AUGUST:
                s = "Avg";
                break;
            case SEPTEMBER:
                s = "Sep";
                break;
            case OCTOBER:
                s = "Okt";
                break;
            case NOVEMBER:
                s = "Nov";
                break;
            case DECEMBER:
                s = "Dec";
                break;
                
        }
        
        return s;
    }
    
    public static String dayOfWeekFromDate(LocalDate x) {
        String s = "";
        switch (x.getDayOfWeek()) {
            case MONDAY:
                s = "Pon";
                break;
            case TUESDAY:
                s = "Uto";
                break;
            case WEDNESDAY:
                s = "Sre";
                break;
            case THURSDAY:
                s = "Cet";
                break;
            case FRIDAY:
                s = "Pet";
                break;
            case SATURDAY:
                s = "Sun";
                break;
            case SUNDAY:
                s = "Ned";
                break;
        }
        return s;
    }
}
