/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import static util.HibernateUtil.*;

/**
 *
 * @author Matic Nebojsa
 */
public class ResourcePane extends MapPane {

    double resourcesPaneWidth = 98;
    double resourcesPaneHeight = 30;

    public ResourcePane(int x) {
        this.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, new CornerRadii(1), new Insets(1))));
        this.setMinWidth(resourcesPaneWidth);
        this.setMaxWidth(resourcesPaneWidth);
        this.setMinHeight(resourcesPaneHeight);
        this.setMaxHeight(resourcesPaneHeight);
        Label resourceLabel = new Label();
        resourceLabel.setText(vratiNazivSobe(x));
        resourceLabel.setMinWidth(resourcesPaneWidth);
        resourceLabel.setMaxWidth(resourcesPaneWidth);
        resourceLabel.setMinHeight(resourcesPaneHeight);
        resourceLabel.setMaxHeight(resourcesPaneHeight);
        resourceLabel.setAlignment(Pos.CENTER);
        resourceLabel.setTextAlignment(TextAlignment.JUSTIFY);
        this.getChildren().add(resourceLabel);

    }

}
