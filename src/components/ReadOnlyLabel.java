/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

/**
 *
 * @author Nebojsa Matic
 */
public class ReadOnlyLabel extends Label{

    public ReadOnlyLabel() {
        setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(0), new Insets(0, 0, 0, 0))));
        setStyle("-fx-border-color: grey;");
        setPadding(new Insets(0,0,0,5));
    }
    
    
}
