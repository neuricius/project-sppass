/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import javafx.scene.layout.Region;

/**
 *
 * @author Nebojsa Matic
 */
public class MapPane extends Region {

    double paneHeight = 30;
    double paneWidth = 30;

    public MapPane() {

        setMinSize(paneWidth, paneHeight);
        setMaxSize(paneWidth, paneHeight);

    }

}
