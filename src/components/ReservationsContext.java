/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import controller.MainCTRL;
import controller.NewReservationCTRL;
import controller.OpenResCTRL;
import view.MainView;
import view.ConfirmationDialog;
import static util.AutoAzuriranjeBaze2.odradiAAB;
import util.HibernateUtil;
import static util.HibernateUtil.odjaviRezervaciju;
import static util.HibernateUtil.vratiRezervacijuPoId;
import util.PrintActionListener;
import static util.Stampac.generatePngFromContainer;
import static view.MainView.mainGridGP;
import static util.HibernateUtil.userLevelCheck;

/**
 *
 * @author Nebojsa Matic
 */
public class ReservationsContext extends ContextMenu {

    public ContextMenu resContMenu;

    public ReservationsContext(Integer idRez) {

        resContMenu = new ContextMenu();

        MenuItem openRes = new MenuItem("Otvori");
        if (idRez != null) {
            openRes.setOnAction(e -> {
                try {
                    new OpenResCTRL(vratiRezervacijuPoId(idRez));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ReservationsContext.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        } else {
            openRes.setDisable(true);
        }

        MenuItem addRes = new MenuItem("Dodaj novu");
        addRes.setOnAction(e -> {
            try {
                new NewReservationCTRL();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReservationsContext.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MenuItem deleteRes = new MenuItem("Obrisi");
        if (idRez != null) {
            deleteRes.setDisable(userLevelCheck());

            //primer da lambda ne radi uvek
            deleteRes.setOnAction(new EventHandler() {

                @Override
                public void handle(Event t) {
                    if (ConfirmationDialog.confirmAction("Da li ste sigurni da zelite da obrisete rezervaciju?", "Brisanje rezervacije", "Da", "Ne")) {
                        HibernateUtil.obrisiRez(idRez);
                        MainCTRL.updateResScreen();
//                    tableGridGP.getChildren().remove(pocetak);
//                    tableGridGP.getChildren().remove(trajanje);
//                    tableGridGP.getChildren().remove(kraj);
                    }
                    t.consume();
                }
            });
        } else {
            deleteRes.setDisable(true);
        }

        MenuItem printMap = new MenuItem("Odstampaj");
        printMap.setOnAction(e -> {
            new Thread(new PrintActionListener(generatePngFromContainer(mainGridGP))).start();
            MainView.systemMsgL.setText("Mapa poslata na stampu");
        });

        MenuItem checkOutRes = new MenuItem("Odjavi");
        checkOutRes.setOnAction(e -> {
            if (ConfirmationDialog.confirmAction("Da li ste sigurni?", "Odjava rezervacije", "Da", "Ne")) {
                odjaviRezervaciju(idRez);
                odradiAAB();
            } else {
                e.consume();
            }
        });

        getItems().addAll(addRes, new SeparatorMenuItem(), openRes, checkOutRes, deleteRes, new SeparatorMenuItem(), printMap);
    }

}
