/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Nebojsa Matic
 */
public class DecoratedButton extends Button {

    public DecoratedButton() {
        super();
        addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if (t.getCode() == KeyCode.ENTER) {
                super.fire();
            }
        });
    }

    public DecoratedButton(String string) {
        super(string);
        addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if (t.getCode() == KeyCode.ENTER) {
                super.fire();
            }
        });
    }

    public DecoratedButton(String string, Node node) {
        super(string, node);
        addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if (t.getCode() == KeyCode.ENTER) {
                super.fire();
            }
        });
    }

}
