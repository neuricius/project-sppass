/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import controller.NewReservationCTRL;
import controller.OpenResCTRL;
import model.Rezervacija;
import static util.HibernateUtil.vratiRezervacijuPoId;
import static controller.MainCTRL.findIndexForDate;
import static controller.MainCTRL.findIndexForRoom;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import javafx.scene.control.OverrunStyle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import util.HibernateUtil;
import static view.MainView.tableGridGP;

/**
 *
 * @author Nebojsa Matic
 */
public class ReservationGroup extends Group {

    // iz prozora
    public ReservationGroup(Rezervacija rez) {

        String firstName = rez.getIdKlijent().getImeKlijenta();
        String lastName = rez.getIdKlijent().getPrezimeKlijenta();
        String roomName = rez.getIdSmJed().getNaziv();
        LocalDate reservationStart = Instant.ofEpochMilli(rez.getPrijava().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate reservationEnd = Instant.ofEpochMilli(rez.getOdjava().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        int reservationStatus = rez.getIdStatRez().getIdStatRez();

        // GRAFIKA
        // pocetak rez
        //staviti petlju da ne crta ako je pocetak van mape
        if ((reservationStart.isAfter(HibernateUtil.dateGridStart) || reservationStart.isEqual(HibernateUtil.dateGridStart)) && reservationStart.isBefore(HibernateUtil.dateGridEnd.plusDays(1))) {
            Polygon startResPoly = new Polygon();
            startResPoly.getPoints().addAll(0.0, 28.0,
                    30.0, 2.0,
                    30.0, 28.0);
            GridPane.setMargin(startResPoly, new Insets(1, 0, 1, 0));
            GridPane.setRowIndex(startResPoly, findIndexForRoom(roomName));
            GridPane.setColumnIndex(startResPoly, findIndexForDate(reservationStart));

            //petlja za setfill
            switch (reservationStatus) {
                case 1:
                    startResPoly.setFill(Color.BEIGE);
                    break;
                case 2:
                    startResPoly.setFill(Color.LIGHTGREEN);
                    break;
                case 3:
                    startResPoly.setFill(Color.LIGHTSALMON);
                    break;
                case 4:
                    startResPoly.setFill(Color.GRAY);
                    break;
            }

            startResPoly.setOnMouseClicked((MouseEvent event) -> {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 2) {
                        try {
                            new OpenResCTRL(vratiRezervacijuPoId(rez.getIdRezervacija()));
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(ReservationGroup.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        event.consume();// make the drag smooth
                    }
                }
            });

            tableGridGP.add(startResPoly, findIndexForDate(reservationStart), findIndexForRoom(roomName), 1, 1);
        }

        //
        //sredina
        //staviti petlju da span obuhvata samo delove u mapi
        if (isLabelNeeded(HibernateUtil.dateGridStart, HibernateUtil.dateGridEnd, reservationStart, reservationEnd)) {
            Label reservationLabel = new Label();
            reservationLabel.setTooltip(new Tooltip(firstName + " " + lastName));
            // ovo pravi problem sa pomeranjem grida
//            reservationLabel.setText(firstName + " " + lastName);
//            reservationLabel.setTextOverrun(OverrunStyle.CLIP);
//            reservationLabel.setTextAlignment(TextAlignment.JUSTIFY);
//            reservationLabel.setWrapText(true);
            reservationLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            GridPane.setMargin(reservationLabel, new Insets(1, 0, 1, 0));
            GridPane.setRowIndex(reservationLabel, findIndexForRoom(roomName));
            GridPane.setColumnIndex(reservationLabel, findIndexForDate(reservationStart));
            switch (reservationStatus) {
                case 1:
                    reservationLabel.setBackground(new Background(new BackgroundFill(Color.BEIGE, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                    break;
                case 2:
                    reservationLabel.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                    break;
                case 3:
                    reservationLabel.setBackground(new Background(new BackgroundFill(Color.LIGHTSALMON, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                    break;
                case 4:
                    reservationLabel.setBackground(new Background(new BackgroundFill(Color.GRAY, new CornerRadii(1), new Insets(1, 0, 1, 0))));
                    break;
            }

            if (reservationStart.isBefore(HibernateUtil.dateGridStart)) {
                tableGridGP.add(reservationLabel, findIndexForDate(HibernateUtil.dateGridStart), findIndexForRoom(roomName), countLabelSizeOnGrid(HibernateUtil.dateGridStart, HibernateUtil.dateGridEnd, reservationStart, reservationEnd), 1);
            } else {
                tableGridGP.add(reservationLabel, findIndexForDate(reservationStart) + 1, findIndexForRoom(roomName), countLabelSizeOnGrid(HibernateUtil.dateGridStart, HibernateUtil.dateGridEnd, reservationStart, reservationEnd), 1);
            }

            reservationLabel.setContextMenu(new ReservationsContext(rez.getIdRezervacija()));

            reservationLabel.setOnMouseClicked((MouseEvent event) -> {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 2) {
                        try {
                            new OpenResCTRL(vratiRezervacijuPoId(rez.getIdRezervacija()));
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(ReservationGroup.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        event.consume();// make the drag smooth
                    }
                }
            });
//            reservationLabel.setText(firstName + " " + lastName);
            reservationLabel.setText(calculateClipString(firstName + " " + lastName, countLabelSizeOnGrid(HibernateUtil.dateGridStart, HibernateUtil.dateGridEnd, reservationStart, reservationEnd)));
        }

        //
        // kraj
        //staviti petlju da ne crta ako je kraj van mape
        if ((reservationEnd.isBefore(HibernateUtil.dateGridEnd.plusDays(1)) || reservationEnd.isEqual(HibernateUtil.dateGridEnd)) && (reservationEnd.isEqual(HibernateUtil.dateGridStart) || reservationEnd.isAfter(HibernateUtil.dateGridStart))) {
            Polygon endResPoly = new Polygon();
            endResPoly.getPoints().addAll(0.0, 2.0,
                    0.0, 28.0,
                    28.0, 2.0);
            GridPane.setMargin(endResPoly, new Insets(1, 0, 1, 0));
            GridPane.setRowIndex(endResPoly, findIndexForRoom(roomName));
            GridPane.setColumnIndex(endResPoly, findIndexForDate(reservationEnd));

            switch (reservationStatus) {
                case 1:
                    endResPoly.setFill(Color.BEIGE);
                    break;
                case 2:
                    endResPoly.setFill(Color.LIGHTGREEN);
                    break;
                case 3:
                    endResPoly.setFill(Color.LIGHTSALMON);
                    break;
                case 4:
                    endResPoly.setFill(Color.GRAY);
                    break;
            }

            endResPoly.setOnMouseClicked((MouseEvent event) -> {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 2) {
                        try {
                            new OpenResCTRL(vratiRezervacijuPoId(rez.getIdRezervacija()));
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(ReservationGroup.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        event.consume();// make the drag smooth
                    }
                }
            });

            tableGridGP.add(endResPoly, findIndexForDate(reservationEnd), findIndexForRoom(roomName), 1, 1);
        }

        NewReservationCTRL.isEverythingFinished = true;

    }

    //proveriti logiku
    private int countLabelSizeOnGrid(LocalDate gridStart, LocalDate gridEnd, LocalDate resStart, LocalDate resEnd) {
        //default label size
        int totalDays = (int) resStart.until(resEnd, ChronoUnit.DAYS) - 1;

        if (resStart.isBefore(gridStart)) {
            totalDays -= (int) resStart.until(gridStart, ChronoUnit.DAYS) - 1;
        }

        if (resEnd.isAfter(gridEnd)) {
            totalDays -= (int) gridEnd.until(resEnd, ChronoUnit.DAYS) - 1;
        }

        return totalDays;
    }

    private boolean isLabelNeeded(LocalDate gridStart, LocalDate gridEnd, LocalDate resStart, LocalDate resEnd) {
        return (countLabelSizeOnGrid(gridStart, gridEnd, resStart, resEnd) >= 1);
    }

    //secenje teksta na zeljenu velicinu
    private String calculateClipString(String text, int calcWidth) {
        double labelWidth = calcWidth * 20;

        Text layoutText = new Text(text);

        if (layoutText.getLayoutBounds().getWidth() < labelWidth) {
            return text;
        } else {
            layoutText.setText(text + "...");
            while (layoutText.getLayoutBounds().getWidth() > labelWidth) {
                text = text.substring(0, text.length() - 1);
                layoutText.setText(text + "...");
            }
            return text + "...";
        }
    }
}
