/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.util.Duration;

/**
 * Creates a digital clock display as a simple label. Format of the clock
 * display is hh:mm:ss aa, where: hh Hour in am/pm (1-12) mm Minute in hour ss
 * Second in minute aa Am/pm marker Time is the system time for the local
 * timezone. preuzeto sa
 * https://stackoverflow.com/questions/13227809/displaying-changing-values-in-javafx-label
 * dopunjeno sa https://www.tutorialspoint.com/java8/java8_datetime_api.htm
 */
public class DigitalDate extends Label {

    /**
     * Default konstruktor
     */
    public DigitalDate() {
        findDate();
    }

    // the digital clock updates once a minute.
    private void findDate() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.minutes(0), (ActionEvent actionEvent) -> {
                    LocalDateTime currentDateTime = LocalDateTime.now();
                    DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd-MM-uu");
                    DateTimeFormatter formatDay = DateTimeFormatter.ofPattern("EEE");
                    
                    String accurateDate = currentDateTime.format(formatDate);
                    String dayOfWeek = dayOfWeekFromString(currentDateTime.format(formatDay));
                    
                    setText(dayOfWeek + ", " + accurateDate);
        }),
                new KeyFrame(Duration.minutes(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private String dayOfWeekFromString(String x) {
        String s = null;
        switch (x) {
            case "Mon":
                s = "Pon";
                break;
            case "Tue":
                s = "Uto";
                break;
            case "Wed":
                s = "Sre";
                break;
            case "Thu":
                s = "Cet";
                break;
            case "Fri":
                s = "Pet";
                break;
            case "Sat":
                s = "Sub";
                break;
            case "Sun":
                s = "Ned";
                break;
        }
        return s;
    }
}
